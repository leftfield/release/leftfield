# NOTE: before you run this, copy Makefile.local.template to Makefile.local and adjust it to fit your setup!

include Makefile.local

# if root is not excluded, and the "root-config" executable is in the path, assume that ROOT is usable
HAVE_ROOT:=0
ifneq ($(EXCLUDE_ROOT),TRUE)
ifeq ($(shell root-config --version >/dev/null 2>&1; echo $$?),0)
	HAVE_ROOT:=1
endif
endif

ifeq ($(HAVE_ROOT),1)
	CXXFLAGS+=$(shell root-config --cflags)
	CXXFLAGS+=-DROOT_AVAILABLE
	CXX_EXTRA_LIBS+=$(shell root-config --libs)
endif

LIBHDR:=$(shell find src/ -name "*.h")

LIBSRC:=$(shell find src -path src/stuff -prune -false -o -name "*.cc")
LIBOBJ:= $(LIBSRC:%.cc=%.o)

LIBRARY:= lib/libcubedfields.a

# non-ROOT codes
CODES:= $(shell find codes -path codes/ROOT -prune -false -o -name "*.cc")
CODES:= $(CODES:%.cc=%.exe)

# codes to build with ROOT:
ROOTCODES:= $(shell find codes/ROOT -name "*.cc")
ROOTCODES:= $(ROOTCODES:%.cc=%.exe)

# Print codes for info:
$(info    Building the following codes: $(CODES))

ifeq ($(HAVE_ROOT),1)
 $(info    Building with ROOT support.)
 $(info    Building the following ROOT codes: $(ROOTCODES))
 CODES+=$(ROOTCODES)
endif


default: $(CODES)

$(LIBOBJ): $(LIBHDR)

$(LIBRARY): $(LIBOBJ)
	ar rcs $@ $^

%.o : %.cc
	$(CXX) $(CXXFLAGS) -Isrc -Isrc/base -c $< -o $@

$(CODES): $(LIBRARY)

$(ROOTCODES): $(LIBRARY)

%.exe : %.cc
	$(CXX) $(CXXFLAGS) -Isrc -Isrc/base $< $(LIBRARY) $(CXX_EXTRA_LIBS) -o $@

clean:
	rm -f $(LIBOBJ) $(LIBRARY) $(CODES)
