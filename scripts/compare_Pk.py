# LEFTfield - Lagrangian, effective-field-theory-based forward model
# of cosmological density fields.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2017-2021 Max-Planck-Society
# Author: Fabian Schmidt

import numpy as np
import math
import sys
import re

def compare_cols(file1, file2, column, column2=-1, factor=1.,
                 label=""):
    if column2 < 0:
        column2 = column
        
    try:
        d1 = np.loadtxt(file1, usecols=(column), unpack=True)
        d2 = np.loadtxt(file2, usecols=(column2), unpack=True)
        d2 *= factor
        sel = ((d1 != 0.) & (d2 != 0.))
        if np.sum(sel) == 0:
            print("No nonzero values for column %d." % column)
            return None
    
        diff = d1[sel] - d2[sel]
        # put a floor on reference value being divided by, to avoid almost-zero entries from spoiling result
        reldiff = 2.*diff / np.maximum(1.e-10 + 0.*(d1[sel]+d2[sel]), d1[sel]+d2[sel])
        print("Column {0} / {1}: RMS absolute difference = {2:.6e}; RMS relative difference = {3:.6e} ({4})".format(column, column2, np.sqrt(np.mean(diff**2.)), np.sqrt(np.mean(reldiff**2.)), label))
    except IOError:
        print("IOError in compare_cols.")
        return None
    except ValueError:
        print("ValueError in compare_cols. Presumably mismatch in k values:")
        print(d1)
        print(d2)
        return None


if len(sys.argv) < 2:
    print("\n Usage: %s < file1 > < file2 > [<max column>] [<factor>]\n" % sys.argv[0])
    sys.exit()

# default settings for Pk files:
mincol=3
maxcol=103

# read in arguments
file1 = sys.argv[1]
file2 = sys.argv[2]
f=1.
if len(sys.argv) > 3:
    maxcol=int(sys.argv[3])
if len(sys.argv) > 4:
    f=float(sys.argv[4])

try:
    # read in legend for each file in order to match columns
    f1 = open(file1)
    f1.readline() # skip first line; could be improved to search for line whose first entry is '# k_low'
    keys1 = f1.readline().split(" / ")
    f2 = open(file2)
    f2.readline() # skip first line; could be improved to search for line whose first entry is '# k_low'
    keys2 = f2.readline().split(" / ")
    
    # debug
    #print(keys1)
    #print(keys2)
    
    # compute for matching columns
    for c in np.arange(mincol, maxcol, 2):
        key = keys1[c]
        try:
            # try finding same key
            c2 = keys2.index(key)
            # debug
            print("{0}: {1} / {2}".format(key, c, c2))
            compare_cols(file1, file2, c, c2, f, key)
        except ValueError:
            # try switching order of operators
            p = re.compile(r"P_<([^|]+)\|([^>]+)>")
            m = p.match(key)
            O1 = m.group(1).strip()
            O2 = m.group(2).strip()
            keyprime = r"P_<" + O2 + r" | " + O1 + r">(k) [(h^-1 Mpc)^3]"
            # debug
            print("Trying with swapped operators '{0}' -> '{1}'".format(key, keyprime))
            try:
                c2 = keys2.index(keyprime)
                compare_cols(file1, file2, c, c2, f, key)
            except ValueError:
                print("No column labeled {0} (or swapped version) in second file.".format(key))

except:
    exit
