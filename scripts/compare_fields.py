# LEFTfield - Lagrangian, effective-field-theory-based forward model
# of cosmological density fields.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (C) 2017-2021 Max-Planck-Society
# Author: Fabian Schmidt

import numpy as np
import math
import sys
import h5py as h5


if len(sys.argv) < 4:
    print("\n Usage: %s < file1 > < file2 > <field name> [<factor>]\n" % sys.argv[0])
    sys.exit()

factor=1.
if len(sys.argv) > 4:
    factor=float(sys.argv[4])
        
try:
    f1 = h5.File(sys.argv[1], mode="r")
    f2 = h5.File(sys.argv[2], mode="r")
    field = sys.argv[3]

    a1 = np.array(f1[field])
    a2 = np.array(f2[field])
    print("Found array of shape ", a1.shape)
    if a1.shape != a2.shape:
        print("Shape mismatch between arrays; second array: ", a2.shape)
        exit

    a2 *= factor
    sel = ((a1 != 0.) & (a2 != 0.))
    if np.sum(sel) == 0:
        print("No nonzero values for field %s." % field)
        exit
    
    diff = a1[sel] - a2[sel]
    reldiff = 0.5*diff / (a1[sel]+a2[sel])
    print("Field {0}: RMS absolute difference = {1:.6e}; RMS relative difference = {2:.6e}".format(field, np.sqrt(np.mean(diff**2.)), np.sqrt(np.mean(reldiff**2.))))

except:
    print("Couldn't open files or unable to find field '%s' in both files." % field)

