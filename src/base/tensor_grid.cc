/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <string.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <cassert>
#include "tensor_grid.h"
#include "scalar_grid.h"
#include "vector_grid.h"


namespace LEFTfield {

// default constructor
// - allocate grid if N_grid > 0
tensor_grid::tensor_grid(const st N_grid, const bool isreal)
  : grid(N_grid, isreal, TENSOR)
{
  if (NG > 0) { // allocate
    resize(NG);
    fill(0.);
    set_real(isreal);
  }
  // otherwise empty grid
}

// // copy constructor
// tensor_grid::tensor_grid(const tensor_grid& g)
//   : grid(g.NG, g.real, TENSOR)
// {
//   for (st I=0; I < 6; I++)
//     kg[I] = g.kg[I];
// }

// copy constructor from scalar grid
tensor_grid::tensor_grid(const scalar_grid& g, const bool subtract_trace)
  : grid(g.getNG(), g.is_real(), TENSOR)
{
  if (NG)  {
    resize(NG);
    generate_tidal(g, subtract_trace);
  }
}

// copy constructor assigning the symmetrized matrix product of the two given
// tensor grids given, specifically (*this)_{ab} = g1_{(ac} g2_{cb)}
// - arguments can be the same grid
// - implemented as constructor rather than operator to save memory 
tensor_grid::tensor_grid(const tensor_grid& g1, const tensor_grid& g2)
  : grid(g1.getNG(), true, TENSOR)
{
  NT_assert(g1.NG == g2.NG, "ERROR in tensor_grid::tensor_grid(g1,g2): grid sizes differ!");
  NT_assert(g1.real && g2.real, "ERROR in tensor_grid::tensor_grid(g1,g2): attempting to multiply at least one Fourier space grid.");

  resize(NG);

  // debug, for timing
  logger.printf("tensor_grid::tensor_grid(g1,g2)", DEBUG, "begun multiplication.");
#pragma omp parallel default(shared)
  {
#pragma omp for
  for (st i=0; i < NG2*NG; i++)  {
    for (st J=0; J < 3; J++)  {
      for (st I=0; I <= J; I++)  {
	// now multiply g1 and g2 and symmetrize
	double a=0.;
	for (st K=0; K < 3; K++)
	  a += (g1(I,K))[i] * (g2(K,J))[i]
	    + (g1(J,K))[i] * (g2(K,I))[i];

	((*this)(I,J))[i] = 0.5*a;
      }
    }
  }
  }
  logger.printf("tensor_grid::tensor_grid(g1,g2)", DEBUG, "finished.");
}


// applies tensor to given vector element: (Re M)_mn(i,j,k) v^n
real_vec3 tensor_grid::Re(const st i, const st j, const st k,
		          const real_vec3& v) const
{
  st ind = i*NG2+j*NG+k;
  return {    
    // 11, 12, 13 -> 0, 3, 4
    kg[0][ind]*v[0] + kg[3][ind]*v[1] + kg[4][ind]*v[2],
    // 21, 22, 23 -> 3, 1, 5
    kg[3][ind]*v[0] + kg[1][ind]*v[1] + kg[5][ind]*v[2],
    // 31, 32, 33 -> 4, 5, 2    
    kg[4][ind]*v[0] + kg[5][ind]*v[1] + kg[2][ind]*v[2] };
}


// operations

#if 0
tensor_grid& tensor_grid::operator=(const tensor_grid& grid)
{
  if (this == &grid) return *this; // handling of self assignment
  NT_assert(grid.NG == NG, "ERROR in tensor_grid::=: grid sizes differ!");
  set_real(grid.real);

  for (st I=0; I < 6; I++)
    kg[I] = grid.kg[I];

  return *this;
}
#endif


tensor_grid& tensor_grid::operator+=(const tensor_grid& grid)
{
  NT_assert(grid.NG == NG, "ERROR in tensor_grid::+=: grid sizes differ!");
  NT_assert(grid.real == real, "ERROR in tensor_grid::+=: attempting to add real and Fourier space grids.");
  
  for (st I=0; I < 6; I++)  {
    kg[I] += grid.kg[I];
  }

  return *this;
}


tensor_grid& tensor_grid::operator*=(const double val)
{
  for (st I=0; I < 6; I++)  {
    kg[I] *= val;
  }

  return *this;
}

tensor_grid& tensor_grid::operator*=(const scalar_grid& g)
{
  for (st I=0; I < 6; I++)  {
    kg[I] *= g;
  }

  return *this;
}

// tensor_grid& tensor_grid::operator+=(const complex val)
// {
//   for (st I=0; I < 6; I++)  {
//     kg[I] += val;
//   }

//   return *this;
// }

// tensor_grid& tensor_grid::operator-=(const complex val)
// {
//   for (st I=0; I < 6; I++)  {
//     kg[I] -= val;
//   }

//   return *this;
// }


// - contraction to scalar grid
scalar_grid tensor_grid::operator*(const tensor_grid& g2) const
{
  NT_assert(g2.NG == NG, "ERROR in tensor_grid::*: grid sizes differ!");
  NT_assert(real && g2.real, "ERROR in tensor_grid::*: attempting to contract Fourier space grids.");

  const auto &g1(*this);
  scalar_grid prod(g1.kg[0] * g2.kg[0]);
  for (st I=1; I < 6; I++)  {
    // factor 2 for off-diagonal elements
    const double f = I < 3 ? 1. : 2.;
    prod.add(f, g1.kg[I] * g2.kg[I]);
  }

  prod.set_real(true);
  return prod;
}


// - contraction with vector grid
vector_grid tensor_grid::operator*(const vector_grid& v) const
{
  NT_assert(v.NG == NG, "ERROR in tensor_grid::*: grid sizes differ!");
  NT_assert(real && v.real, "ERROR in tensor_grid::*: attempting to contract Fourier space grids.");

  vector_grid prod(NG, true, 0.);

  prod.vg[0].add(1., kg[0] * v.vg[0]);
  prod.vg[0].add(1., kg[3] * v.vg[1]);
  prod.vg[0].add(1., kg[4] * v.vg[2]);
  prod.vg[1].add(1., kg[3] * v.vg[0]);
  prod.vg[1].add(1., kg[1] * v.vg[1]);
  prod.vg[1].add(1., kg[5] * v.vg[2]);
  prod.vg[2].add(1., kg[4] * v.vg[0]);
  prod.vg[2].add(1., kg[5] * v.vg[1]);
  prod.vg[2].add(1., kg[2] * v.vg[2]);
  
  return prod;
}


// - trace
scalar_grid tensor_grid::trace() const
{
  scalar_grid tr(kg[0]);
  tr += kg[1];
  tr += kg[2];
  return tr;
}


// - contraction to scalar grid = tr [ (*this) B F ]
scalar_grid tensor_grid::doubleContraction(const tensor_grid& M2,
					   const tensor_grid& M3) const
{
  NT_assert(real, "ERROR in tensor_grid::doubleContraction: must be in real space for this operation.");
  
  scalar_grid g(NG, true);
#pragma omp parallel default(shared)
    {
#pragma omp for
      for (st i=0; i < NG2*NG; i++)  {
	double A[6] = {
	  kg[0][i],
	  kg[1][i],
	  kg[2][i],
	  kg[3][i],
	  kg[4][i],
	  kg[5][i] };
	double B[6] = {
	  M2.kg[0][i],
	  M2.kg[1][i],
	  M2.kg[2][i],
	  M2.kg[3][i],
	  M2.kg[4][i],
	  M2.kg[5][i] };
	double F[6] = {
	  M3.kg[0][i],
	  M3.kg[1][i],
	  M3.kg[2][i],
	  M3.kg[3][i],
	  M3.kg[4][i],
	  M3.kg[5][i] };

	double C =
	  (A[0]*B[0]+A[3]*B[3]+A[4]*B[4])*F[0]
	  +(A[1]*B[1]+A[3]*B[3]+A[5]*B[5])*F[1]
	  +(A[2]*B[2]+A[4]*B[4]+A[5]*B[5])*F[2]
	  +(A[3]*B[0]+A[1]*B[3]+A[5]*B[4])*F[3]
	  +(A[3]*B[1]+A[0]*B[3]+A[4]*B[5])*F[3]
	  +(A[4]*B[0]+A[5]*B[3]+A[2]*B[4])*F[4]
	  +(A[4]*B[2]+A[0]*B[4]+A[3]*B[5])*F[4]
	  +(A[5]*B[2]+A[3]*B[4]+A[1]*B[5])*F[5]
	  +(A[5]*B[1]+A[4]*B[3]+A[2]*B[5])*F[5];

	// not ideal, but this is a negligible fraction of total cost
	g[i] = C;
	//= *reinterpret_cast<fftw_complex*> (&C); 
      }
    }

    g.set_real(true);
    return g;
}



scalar_grid tensor_grid::determinant() const
{
  return 1./6. * doubleEpsContraction(*this, *this);
}

// returns
// \epsilon^ijk \epsilon^lmn (*this)_il B_jm F_kn
// - B and F can be the same and equal to *this
scalar_grid tensor_grid::doubleEpsContraction(const tensor_grid& M2,
					      const tensor_grid& M3) const
{
  NT_assert(real && M2.is_real() && M3.is_real(),
	    "ERROR in tensor_grid::doubleEpsContraction: must be in real space for this operation.");
  
  scalar_grid g(NG, true);
#pragma omp parallel default(shared)
    {
#pragma omp for
      for (st i=0; i < NG2*NG; i++)  {
	double A[6] = {
	  kg[0][i],
	  kg[1][i],
	  kg[2][i],
	  kg[3][i],
	  kg[4][i],
	  kg[5][i] };
	double B[6] = {
	  M2.kg[0][i],
	  M2.kg[1][i],
	  M2.kg[2][i],
	  M2.kg[3][i],
	  M2.kg[4][i],
	  M2.kg[5][i] };
	double F[6] = {
	  M3.kg[0][i],
	  M3.kg[1][i],
	  M3.kg[2][i],
	  M3.kg[3][i],
	  M3.kg[4][i],
	  M3.kg[5][i] };

	double C =
	  A[2]*B[1]*F[0]+A[1]*B[2]*F[0]-2.*A[5]*B[5]*F[0]+A[2]*B[0]*F[1]
	  +A[0]*B[2]*F[1]-2.*A[4]*B[4]*F[1]+A[1]*B[0]*F[2]+A[0]*B[1]*F[2]
	  -2.*A[3]*B[3]*F[2]-2.*A[3]*B[2]*F[3]-2.*A[2]*B[3]*F[3]
	  +2.*A[5]*B[4]*F[3]+2.*A[4]*B[5]*F[3]-2.*A[4]*B[1]*F[4]
	  +2.*A[5]*B[3]*F[4]-2.*A[1]*B[4]*F[4]+2.*A[3]*B[5]*F[4]
	  -2.*A[5]*B[0]*F[5]+2.*A[4]*B[3]*F[5]+2.*A[3]*B[4]*F[5]
	  -2.*A[0]*B[5]*F[5];

	g[i] = C;
      }
    }

    g.set_real(true);
    return g;
}

// - vector product
vector_grid tensor_grid::operator%(const tensor_grid& M2) const
{
  NT_assert(real && M2.is_real(),
	    "ERROR in operator%%(tensor_grid): must be in real space for this operation.");
  NT_assert(NG == M2.NG, "ERROR in operator%%(tensor_grid): grid sizes differ!");
  
  const auto &M1(*this);
  vector_grid v(M1.NG, true, 0.);
#pragma omp parallel default(shared)
    {
#pragma omp for
      for (st i=0; i < M1.NG2*M1.NG; i++)  {
	double A[6] = {
	  M1.kg[0][i],
	  M1.kg[1][i],
	  M1.kg[2][i],
	  M1.kg[3][i],
	  M1.kg[4][i],
	  M1.kg[5][i] };
	double B[6] = {
	  M2.kg[0][i],
	  M2.kg[1][i],
	  M2.kg[2][i],
	  M2.kg[3][i],
	  M2.kg[4][i],
	  M2.kg[5][i] };

	double C[3] = {
	  -A[5]*B[1] + A[5]*B[2] - A[4]*B[3] + A[3]*B[4] + A[1]*B[5] - A[2]*B[5],
	  A[4]*B[0] - A[4]*B[2] + A[5]*B[3] - A[0]*B[4] + A[2]*B[4] - A[3]*B[5],
	  -A[3]*B[0] + A[3]*B[1] + A[0]*B[3] - A[1]*B[3] - A[5]*B[4] + A[4]*B[5]
	};

	for (st I=0; I < 3; I++) {
	  v[I][i] = C[I];
	}
      }
    }

    v.set_real(true);
    return v;
}


// generate from scalar field via
// (\partial_i\partial_j/\laplace - 1/3 \delta_ij) grid
void tensor_grid::generate_tidal(const scalar_grid& grid,
				 const bool subtract_trace)
{
  NT_assert(grid.getNG() == NG, "ERROR in tensor_grid::generate_tidal grid sizes differ!");
  NT_assert(!grid.is_real(), "ERROR in tensor_grid::generate_tidal: grid needs to be in Fourier space!");
 
  // reset grid
  fill(0.);
  set_real(false);

  // run over Fourier-space grid
  iterateFourierHalfParallel([this,&grid,subtract_trace](st idx, st idxc, double kxq, double kyq, double kzq, double K2)
    {
    double ilapl = 1./K2;

    // !!! Following could be made more elegant using "complex" !!!
    double ak, bk;
    hartley_load(grid[idx], grid[idxc], ak, bk);
    const double rd = subtract_trace ? ak/3. : 0.,
                 id = subtract_trace ? bk/3. : 0.;

    // diagonals:
    hartley_store(kxq*kxq * ilapl * ak - rd, kxq*kxq * ilapl * bk - id, kg[0][idx],kg[0][idxc]);
    hartley_store(kyq*kyq * ilapl * ak - rd, kyq*kyq * ilapl * bk - id, kg[1][idx],kg[1][idxc]);
    hartley_store(kzq*kzq * ilapl * ak - rd, kzq*kzq * ilapl * bk - id, kg[2][idx],kg[2][idxc]);

    // off diagonals:
    //     3:  12
    //     4:  13
    //     5:  23
    hartley_store(kxq*kyq * ilapl * ak, kxq*kyq * ilapl * bk, kg[3][idx],kg[3][idxc]);
    hartley_store(kxq*kzq * ilapl * ak, kxq*kzq * ilapl * bk, kg[4][idx],kg[4][idxc]);
    hartley_store(kyq*kzq * ilapl * ak, kyq*kzq * ilapl * bk, kg[5][idx],kg[5][idxc]);
    });
  // null out unphysical constant mode
  for (st I=0; I < 6; I++)  {
    kg[I][0] = 0.;
  }
}



// returns the result of, in Fourier space
// - k_i k_j T_ij(\vec k)
// where k refers to grid::Lbox
scalar_grid tensor_grid::divergence() const
{
  NT_assert(!real, "ERROR in tensor_grid::divergence: need to be in Fourier space!");

  scalar_grid div(NG);
  iterateFourierHalfParallel([this,&div](st idx, st idxc, double kxq, double kyq, double kzq, double)
    {
    double Rd = 0., Id = 0.;
    double Kq[3] = {kxq, kyq, kzq};
    // - diagonal parts
    for (st I=0; I < 3; I++)  {
      double K2 = Kq[I]*Kq[I];
      auto tmp = hartley_load(kg[I][idx],kg[I][idxc]);
      Rd -= K2 * tmp.real();
      Id -= K2 * tmp.imag();
    }
    // - offdiagonal parts (x2)
    double K2 = 2.*Kq[0]*Kq[1];
    auto tmp = hartley_load(kg[3][idx],kg[3][idxc]);
    Rd -= K2 * tmp.real();
    Id -= K2 * tmp.imag();
    K2 = 2.*Kq[0]*Kq[2];
    tmp = hartley_load(kg[4][idx],kg[4][idxc]);
    Rd -= K2 * tmp.real();
    Id -= K2 * tmp.imag();
    K2 = 2.*Kq[1]*Kq[2];
    tmp = hartley_load(kg[5][idx],kg[5][idxc]);
    Rd -= K2 * tmp.real();
    Id -= K2 * tmp.imag();
    hartley_store(Rd, Id, div[idx], div[idxc]);
    });

  // ensure k=0 mode is zero
  div[0] = 0.;
 
  div.set_real(false);

  return div;
}


void tensor_grid::print_stats(FILE *fp) 
{
  bool swtch = false;
  if (! is_real())  { // go to real space if necessary
    go_real();
    swtch = true;
  }
  double R[6], VR[6];
  go_real();
  fprintf(fp, "tensor_grid:\n");
  for (st J=0; J<6; J++)  {
    R[J] = 0.;
    VR[J] = 0.;
    
#pragma omp parallel default(shared)
    {
#pragma omp for
    for (st i=0; i < NG2*NG; i++)  {
      R[J] += kg[J][i];
      VR[J] += kg[J][i] * kg[J][i];
    }
    }
    R[J] /= double(NG2*NG);
    VR[J] /= double(NG2*NG);
    VR[J] -= pow(R[J], 2.);

    fprintf(fp, "             mean^%zu = %.6e\n", J, R[J]);
    fprintf(fp, "          sqrt(V^%zu) = %.6e\n", J, sqrt(VR[J]));
  }

  if (swtch) go_fourier(); // go back to Fourier space if we were there before
}


// file I/O

void tensor_grid::write_nbtl_s(std::ostream &file) const
{
  for (st I=0; I < 6; I++) kg[I].write_nbtl_s(file);
}

void tensor_grid::read_nbtl_s(std::istream &file)
{
  for (st I=0; I < 6; I++) kg[I].read_nbtl_s(file);
}

} // end of namespace LEFTfield
