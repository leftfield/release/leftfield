/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef ASSIGNERS_H
#define ASSIGNERS_H

#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include <functional>

namespace LEFTfield {

// weight function type
using weightFunction = std::function<double(const double M)>;

// default: trivial weight function
extern weightFunction defWeight;

/* Properties of the assigner classes:
   - the constructor takes a NG value; it does not allocate any memory
   - assign() can be called multiple times
     when called with unallocated internal memory, all necessary arrays are allocated
   - getGrid() can only be called after assign() has been called at least once
     it returns a scalar_grid with zero mean
     at the end of getGrid() all internal arrays are deallocated again
*/
class ScalarGridAssigner
  {
  protected:
    st ng;
    sptr<scalar_grid> grid;

  public:
    ScalarGridAssigner(st NG) : ng(NG) {}
    virtual ~ScalarGridAssigner() {}
    
    virtual void assign(const std::vector<double> &pos, const std::vector<double> &mass) = 0;    
    virtual sptr<scalar_grid> getGrid() = 0;
    
    st getNG() const { return ng; }
    void reset() {
      if (grid) grid->fill(0.);
    }
  };

class NGPAssigner: public ScalarGridAssigner
  {
  public:
    NGPAssigner(st NG): ScalarGridAssigner(NG) {}
    virtual void assign(const std::vector<double> &pos, const std::vector<double> &mass);
    virtual sptr<scalar_grid> getGrid()
      { grid->subtract_mean(); auto res=grid; grid.reset(); return res; }
  };

class CICAssigner: public ScalarGridAssigner
  {
  public:
    CICAssigner(st NG): ScalarGridAssigner(NG) {}
    virtual void assign(const std::vector<double> &pos, const std::vector<double> &mass);
    virtual sptr<scalar_grid> getGrid()
      { grid->subtract_mean(); auto res=grid; grid.reset(); return res; }
  };

class FTAssigner: public ScalarGridAssigner
  {
  private:
    sptr<vector_grid> sdisp;

  public:
    FTAssigner(st NG): ScalarGridAssigner(NG) {}
    virtual void assign(const std::vector<double> &pos, const std::vector<double> &mass);
    virtual sptr<scalar_grid> getGrid();
  };

class FTNLOAssigner: public ScalarGridAssigner
  {
  private:
    sptr<vector_grid> sdisp;
    sptr<tensor_grid> s2disp;

  public:
    FTNLOAssigner(st NG): ScalarGridAssigner(NG) {}
    virtual void assign(const std::vector<double> &pos, const std::vector<double> &mass);
    virtual sptr<scalar_grid> getGrid();
  };


// read particles in Gadget2 snapshot format
// - filenames are <base_filename>.<i>, where i = 0, 1, ... Nfiles-1
// - move each particle by shiftx/y/z grid cells
// - if nbar > 0, subsample particles to comoving number density nbar
// - returns box size specified in Gadget header
double assign_density_Gadget(ScalarGridAssigner &assigner,
                             const std::string &base_filename,
                             const int Nfiles,
                             const double shiftx=0,
                             const double shifty=0,
                             const double shiftz=0,
                             const double nbar=0);


// read particle file from modified gravity Nbody code (Oyaizu, Hu, Lima, Schmidt)
void assign_density_HPM(ScalarGridAssigner &assigner,
                        const std::string &file, const st NG_HPM,
                        const st NP,
                        const double shiftx=0,
                        const double shifty=0,
                        const double shiftz=0);

// read particle positions and optionally mass from HDF5 catalog file
// - position data set is assumed to be N x 3
// - mass data is assumed to be N (ignored if data set name is empty)
// - mass fraction data (for fraction of gas mass in diferent elements) is assumed to be N x 10 (this was written specifically for use with Arepo/TNG HDF5 format, but easily adaptable to other cases)
// - elemind is used to select which of the above 10 gas elements to consider
// - H mass fraction data (for fraction of H mass in neutral from) is assumed to be N (this was written specifically for used with Arepo/TNG HDF5 format, but easily adaptable to other cases)
// - Lbox is used to convert positions to grid units
// - optionally assign only within given mass range, and using weight function
// - returns sum of weights
double assign_density_HDF5catalog(ScalarGridAssigner &assigner,
				  const std::string &file,
				  const double Lbox,
				  const std::string &position_dset,
				  const std::string &mass_dset = "",
				  const std::string &element_dset = "",
				  const int elemind = 0,
				  const std::string &hfrac_dset = "",
				  const double Mmin = 0.,
				  const double Mmax = 1.e30,
				  weightFunction func = defWeight);

// read in halos in ASCII format
// - given box size Lbox which has to be in same units as halo coordinates in file
// - xcol, ycol, zcol: indicate column number for coordinates, counting from 1
// - if Mcol > 0, select halos by mass according to given arguments
//   (obviously should also match units in halo catalog)
// - returns number of halos assigned to grid
st assign_density_halos(ScalarGridAssigner &assigner, const std::string &file, const double Lbox,
                        const int xcol, const int ycol, const int zcol,
                        const int Mcol,
                        const double Mmin, const double Mmax,
			weightFunction fweight = defWeight);

// for convenience: AHF output format
// - note unit conversion, as AHF uses h^-1 kpc for halo positions
inline
  st assign_density_AHF_halos(ScalarGridAssigner &assigner,const std::string &file, const double Lbox_in_Mpch,
			      const double Mmin = 0., const double Mmax = 1.e16,
			      weightFunction fweight = defWeight)
  {
    return assign_density_halos(assigner, file, 1000.*Lbox_in_Mpch,
				6, 7, 8, Mmin > 0. ? 4 : 0,
     				Mmin, Mmax, fweight);
  }

// for convenience: AHF format as reprocessed by Titouan
// - note unit conversion, as AHF uses h^-1 kpc for halo positions
inline
st assign_density_Titouan_halos(ScalarGridAssigner &assigner,
                                const std::string &file,
				const double Lbox_in_Mpch,
                                const double Mmin = 0.,
				const double Mmax = 1.e16,
				weightFunction fweight = defWeight)
  {
    return assign_density_halos(assigner, file, 1000.*Lbox_in_Mpch,
				2, 3, 4, Mmin > 0. ? 1 : 0,
     				Mmin, Mmax, fweight);
  }

} // end of namespace LEFTfield

#endif
