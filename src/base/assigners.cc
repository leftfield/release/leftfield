/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include "assigners.h"
#include <numeric>
#include <algorithm>
#include "HDF5_IO.h"

namespace LEFTfield {

// default: trivial weight function
weightFunction defWeight = [](const double) { return 1.; };

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Assignment routines

// - cloud in cell
void CICAssigner::assign(const std::vector<double> &pos,
                         const std::vector<double> &mass)
{
  st NG=ng;
  if (!grid)
    grid = make_sptr<scalar_grid>(ng);
  auto &grd(*grid);
  auto NP = mass.size();
  NT_assert(NP*3 == pos.size(), "vector length mismatch");
  st c=0;
  // debug
  // double M=0.;
  for (st I=0; I < NP; I++)  {
    double m = mass[I];
    // M += m;

    // wrap around
    double x = pos[c++];
    if ((x<0) || (x>=NG))
      {
      x = fmod(x, NG);
      if (x < 0.) x += double(NG);
      }
    double y = pos[c++];
    if ((y<0) || (y>=NG))
      {
      y = fmod(y, NG);
      if (y < 0.) y += double(NG);
      }
    double z = pos[c++];
    if ((z<0) || (z>=NG))
      {
      z = fmod(z, NG);
      if (z < 0.) z += double(NG);
      }

    // do CIC
    st i = st(x), j = st(y), k = st(z);
    double dx = x-double(i), dy = y-double(j), dz = z-double(k);
    double tx = 1.-dx, ty = 1.-dy, tz = 1.-dz;
    st ip = i+1, jp = j+1, kp = k+1;
    if (ip>=NG) ip-=NG;
    if (jp>=NG) jp-=NG;
    if (kp>=NG) kp-=NG;

    grd(i,j,k) += m * tx * ty * tz;
    grd(i,j,kp) += m * tx * ty * dz;
    grd(i,jp,k) += m * tx * dy * tz;
    grd(i,jp,kp) += m * tx * dy * dz;
    grd(ip,j,k) += m * dx * ty * tz;
    grd(ip,j,kp) += m * dx * ty * dz;
    grd(ip,jp,k) += m * dx * dy * tz;
    grd(ip,jp,kp) += m * dx * dy * dz;
  }
}


// - nearest grid point
void NGPAssigner::assign(const std::vector<double> &pos,
                         const std::vector<double> &mass)
{
  st NG=ng;
  if (!grid)
    grid = make_sptr<scalar_grid>(ng);
  auto &grd(*grid);
  auto NP = mass.size();
  if (NP*3 != pos.size()) throw "vector length mismatch";
  st c=0;
  for (st I=0; I < NP; I++)  {
    double m = mass[I];

    // do NGP assignment of density
    // - NGP position
    double xn = round(pos[c++]);
    double yn = round(pos[c++]);
    double zn = round(pos[c++]);

    // - get indices and wraparound
    int i = int(xn) % NG, j = int(yn) % NG, k = int(zn) % NG;
    if (i < 0) i += NG;
    if (j < 0) j += NG;
    if (k < 0) k += NG;

    // - assign density
    grd(i,j,k) += m;
  }

}


// - Fourier-Taylor
// -- note that 'complete_FT_assignment' needs to be called after
//    assign_density_* routine is done
void FTAssigner::assign(const std::vector<double> &pos,
                        const std::vector<double> &mass)
{
  st NG=ng;
  if (!grid)
    {
    grid = make_sptr<scalar_grid>(ng);
    sdisp = make_sptr<vector_grid>(ng);
    }
  auto NP = mass.size();
  if (NP*3 != pos.size()) throw "vector length mismatch";
  st c=0;
  for (st I=0; I < NP; I++)  {
    double m = mass[I];

    // do NGP assignments of density and displacement

    // do NGP assignment of density
    // - NGP position and displacement
    double dx = pos[c++];
    double dy = pos[c++];
    double dz = pos[c++];
    double xn = round(dx);
    double yn = round(dy);
    double zn = round(dz);
    dx -= xn;
    dy -= yn;
    dz -= zn;

    // - get indices and wraparound
    int i = int(xn) % NG, j = int(yn) % NG, k = int(zn) % NG;
    if (i < 0) i += NG;
    if (j < 0) j += NG;
    if (k < 0) k += NG;
    st ind = st(i)*NG*NG + st(j)*NG + st(k);

    // - assign density and subgrid displacement
    (*grid)[ind] += m;
    // !!! TO BE IMPROVED
    (*sdisp)[0][ind] += m * dx;
    (*sdisp)[1][ind] += m * dy;
    (*sdisp)[2][ind] += m * dz;
  }
}


// - call this routine to complete Fourier-Taylor assignment
sptr<scalar_grid> FTAssigner::getGrid()
{
  // - go to Fourier space
  grid->go_fourier();
  sdisp->go_fourier();

  // - add minus the divergence of subgrid displacement field
  grid->add(-1., sdisp->divergence());

  grid->subtract_mean();

  // - finally, clean up vector_grid
  logger.printf("FTAssigner::getGrid", DEBUG, "deallocating displacement vector grid.");
  auto res = grid;
  grid.reset();
  sdisp.reset();
  return res;
}


// - next-to-leading order Fourier-Taylor
// -- note that 'complete_FTNLO_assignment' needs to be called after
//    assign_density_* routine is done
void FTNLOAssigner::assign(const std::vector<double> &pos,
                           const std::vector<double> &mass)
{
  st NG=ng;
  if (!grid)
    {
    grid = make_sptr<scalar_grid>(ng);
    sdisp = make_sptr<vector_grid>(ng);
    s2disp = make_sptr<tensor_grid>(ng);
    }
  auto NP = mass.size();
  if (NP*3 != pos.size()) throw "vector length mismatch";

  st c=0;
  for (st I=0; I < NP; I++)  {
    double m = mass[I];

    // do NGP assignment of density
    // - NGP position and displacement
    double dx = pos[c++];
    double dy = pos[c++];
    double dz = pos[c++];
    double xn = round(dx);
    double yn = round(dy);
    double zn = round(dz);
    dx -= xn;
    dy -= yn;
    dz -= zn;

    // - get indices and wraparound
    int i = int(xn) % NG, j = int(yn) % NG, k = int(zn) % NG;
    if (i < 0) i += NG;
    if (j < 0) j += NG;
    if (k < 0) k += NG;
    st ind = st(i)*NG*NG + st(j)*NG + st(k);

    // - assign density and subgrid displacement
    (*grid)[ind] += m;
    (*sdisp)[0][ind] += m * dx;
    (*sdisp)[1][ind] += m * dy;
    (*sdisp)[2][ind] += m * dz;

    (*s2disp)[0][ind] += m * dx*dx;
    (*s2disp)[1][ind] += m * dy*dy;
    (*s2disp)[2][ind] += m * dz*dz;
    (*s2disp)[3][ind] += m * dx*dy;
    (*s2disp)[4][ind] += m * dx*dz;
    (*s2disp)[5][ind] += m * dy*dz;
  }

}


// - call this routine to complete Fourier-Taylor assignment
sptr<scalar_grid> FTNLOAssigner::getGrid()
{
  // - go to Fourier space
  grid->go_fourier();
  sdisp->go_fourier();
  s2disp->go_fourier();

  // - add minus the divergence of subgrid displacement field
  grid->add(-1., sdisp->divergence());

  // - same for (displacement field)^2 tensor field
  grid->add(0.5, s2disp->divergence());

  grid->subtract_mean();

  // - finally, clean up vector_grid, tensor_grid
  logger.printf("FTNLOAssigner::getGrid", DEBUG, "deallocating displacement vector and tensor grids.");
  auto res = grid;
  grid.reset();
  sdisp.reset();
  s2disp.reset();
  return res;
}


// -------------------------------------------------------------------------


// -------------------------------------------------------------------------
// read in particles in HPM format
// - NG_HPM: number of grid cells in HPM (for code unit conversion)
// - NP: number of particles
// - shiftx/y/z: shift particles by this amount (in code units) before
//   density assignment
void assign_density_HPM(ScalarGridAssigner &assigner,
                        const std::string &file, const st NG_HPM,
                        const st NP,
                        const double shiftx,
                        const double shifty,
                        const double shiftz)
{
  std::ifstream partFile(file);
  NT_assert(partFile.is_open(), "scalar_grid::assign_density_HPM: couldn't open particle file '%s'.", file.c_str());

  st NG = assigner.getNG();
  const double m = double(NG*NG*NG) / double(NP);

  // allocate vector
  static const st chunksize = 1000000;
  std::vector<double> readvec, posvec, mvec;

  // assign densities (CIC)
  st pCount = 0;
  const double uc = double(NG) / double(NG_HPM); // unit conversion
  while (pCount < NP) {
    const st Nread = std::max(chunksize, NP-pCount);
    readvec.resize(6*Nread);
    posvec.resize(3*Nread);
    mvec.resize(Nread);
    partFile.read((char*) readvec.data(), sizeof(double)*6*Nread);
    if (partFile.eof())  {
      // could be that we should abort here, but let's keep it at warning for now
      logger.printf("assign_density_HPM", ESSENTIAL,
		    "Premature EOF: pcount = %zu, NP = %zu", pCount, NP);
      break;
    }
    pCount += Nread;

    // copy to position and mass vectors
    // - apply unit conversion and shift
    for (st I=0; I < Nread; I++)  {
      posvec[3*I]   = uc * readvec[6*I]   + shiftx;
      posvec[3*I+1] = uc * readvec[6*I+1] + shifty;
      posvec[3*I+2] = uc * readvec[6*I+2] + shiftz;
      mvec[I] = m;
    }

    // now assign
    assigner.assign(posvec, mvec);
    logger.printf("assign_density_HPM", INFO,
		  "... read %zu particles...", pCount);
  }

  logger.printf("assign_density_HPM", INFO,
		"Read in %zu (!= %zu) particles.", pCount, NP);
}

// -------------------------------------------------------------------------

// read particle positions and optionally mass from HDF5 catalog file
// - position data set is assumed to be N x 3
// - mass data is assumed to be N (ignored if data set name is empty)
// - mass fraction data (for fraction of gas mass in different elements) is assumed to be N x 10; ignored if empty (this was written specifically for use with Arepo/TNG HDF5 format)
// - elemind is used to select which of the above 10 gas elements to consider
// - H mass fraction data (for fraction of H mass in neutral from) is assumed to be N; ignored if empty (this was written specifically for use with Arepo/TNG HDF5 format)
// - optionally assign only within given mass range, and using weight function
// - default weight 1
// - returns sum of weights
double assign_density_HDF5catalog(ScalarGridAssigner &assigner,
				  const std::string &file,
				  const double Lbox,
				  const std::string &position_dset,
				  const std::string &mass_dset,
				  const std::string &element_dset,
				  const int elemind,
				  const std::string &hfrac_dset,
				  const double Mmin, const double Mmax,
				  weightFunction fweight)

{
  LogContext lc("assign_density_HDF5catalog");
  std::vector<double> pos, mvec, elem, hfrac;
  std::vector<st> dim;
  read_HDF5_dataset(file, position_dset, pos, dim);
  NT_assert(dim.size() == 2 && dim[1] == 3,
	    "assign_density_HDF5catalog: wrong array dimensions for position array (ndims = %zu, dim[1] = %zu)", dim.size(), dim[1]);
  st Npart = dim[0];
  logger.printf(INFO, "assigning %zu particles.", Npart);

  // 1. convert positions to code units
  const st NG = assigner.getNG();
  st idx = 0;
  for (auto &x: pos) {
    x *= double(NG) / Lbox;
    if (x > double(NG)) x -= double(NG);
    if (x < 0.) x += double(NG);
    pos[idx++] = x;
  }

  // 2. read in masses, apply cuts and weight
  if (mass_dset.length() > 0) {
    read_HDF5_dataset(file, mass_dset, mvec, dim);
    NT_assert(dim.size() == 1 && dim[0] == Npart,
	      "assign_density_HDF5catalog: wrong array dimensions for mass array (ndims = %zu, dim[0] = %zu)", dim.size(), dim[0]);
    // have mass field, with max/min cuts
    for (st i=0; i < Npart; i++){
      double m = mvec[i];
      double M = (m >= Mmin && m < Mmax) ? m : 0.;
      mvec[i] = M;
    }

    // 3. read in element mass fractions, and apply them
    if(element_dset.length() > 0) {
      read_HDF5_dataset(file, element_dset, elem, dim);
      NT_assert(dim.size() == 2 && dim[0] == Npart && dim[1] == 10,
	      "assign_density_HDF5catalog: wrong array dimensions for element array (ndims = %zu, dim[0] = %zu, dim[1] = %zu)", dim.size(), dim[0], dim[1]);
      // have element fractions, now apply
      for (st i=0; i < Npart; i++){
        double m = mvec[i];
	double e = elem[i*10 + elemind];
        mvec[i] = m*e;
      }
      // 4. read in H mass fractions, and apply them
      if (hfrac_dset.length() > 0 && elemind == 0){
        read_HDF5_dataset(file, hfrac_dset, hfrac, dim);
        NT_assert(dim.size() == 1 && dim[0] == Npart,
              "assign_density_HDF5catalog: wrong array dimensions for H mass fraction array (ndims = %zu, dim[0] = %zu)", dim.size(), dim[0]);
        // have H fractions, now apply
        for (st i=0; i < Npart; i++){
        double m = mvec[i];
        double hf = hfrac[i];
        mvec[i] = m*hf;
        }
      }
    }

    // all element mass fractions applied, now apply desired mass weighting
    for (st i=0; i < Npart; i++){
      double m = mvec[i];
      mvec[i] = fweight(m);
    }


  } else {
    logger.printf(INFO, "no mass field given, setting all masses to 1.");
    mvec.resize(Npart);
    std::fill(mvec.begin(), mvec.end(), 1.);
  }

  double sow = std::accumulate(mvec.begin(), mvec.end(), 0.);
  logger.printf(DEBUG, "sum of weights: %.5e.", sow);

  // now assign
  assigner.assign(pos, mvec);

  return sow;
}


// -------------------------------------------------------------------------

// read in halos in ASCII format
// - given box size Lbox which has to be in same units as halo coordinates in file
// - xcol, ycol, zcol: indicate column number for coordinates, counting from 1
// - if Mcol > 0, select halos by mass according to given arguments
//   (obviously should also match units in halo catalog)
// - returns number of halos assigned to grid
st assign_density_halos(ScalarGridAssigner &assigner, const std::string &file, const double Lbox,
			const int xcol, const int ycol, const int zcol,
			const int Mcol,
			const double Mmin, const double Mmax,
			weightFunction fweight)
{
  std::ifstream haloFile(file);
  NT_assert(haloFile.is_open(), "scalar_grid::assign_density_halos: couldn't open halo file '%s'.", file.c_str());

  st NG = assigner.getNG();
  // read in halos
  // std::vector<double> xvec, yvec, zvec, wvec;
  std::vector<double> posvec, wvec;
  const int Ncol = std::max(xcol, std::max(ycol, std::max(zcol, Mcol)));
  std::vector<double> val(Ncol);
  char str[2048];
  st ih=0;
  while (1)  {
    haloFile.getline(str, 1024);
    if (haloFile.eof())  break;
    ih++;
    std::basic_istringstream<char> strstr(str);
    for (int i=0; i < Ncol; i++) strstr >> val[i];
    double w = 1.;
    if (Mcol > 0)  {
      double M = val[Mcol-1];
      if (! (M >= Mmin && M < Mmax) ) continue;

      // determine weight
      w = fweight(M);
    }
    wvec.push_back(w);

    if (!(ih % 10000))  {
      logger.printf("scalar_grid::assign_density_halos", DEBUG,
		    "halo %zu: %.3e / %.3e / %.3e, M = %.3e, Ncol = %d", ih,
		    val[xcol-1], val[ycol-1], val[zcol-1], Mcol > 0 ? val[Mcol-1] : 0., Ncol);
    }

    // note: column numbers are counting from one
    double x = val[xcol-1]/Lbox * double(NG);
    if (x > double(NG)) x -= double(NG);
    if (x < 0.) x += double(NG);
    posvec.push_back(x);
    double y = val[ycol-1]/Lbox * double(NG);
    if (y > double(NG)) y -= double(NG);
    if (y < 0.) y += double(NG);
    posvec.push_back(y);
    double z = val[zcol-1]/Lbox * double(NG);
    if (z > double(NG)) z -= double(NG);
    if (z < 0.) z += double(NG);
    posvec.push_back(z);

    // bounds check (if Lbox is wrongly specified)
    NT_assert(x >= 0. && x < double(NG),
	      "scalar_grid::assign_density_halos: x = %.5e out of bounds, NG = %zu.", x, NG);
    NT_assert(y >= 0. && y < double(NG),
	      "scalar_grid::assign_density_halos: y = %.5e out of bounds, NG = %zu.", y, NG);
    NT_assert(z >= 0. && z < double(NG),
	      "scalar_grid::assign_density_halos: z = %.5e out of bounds, NG = %zu.", z, NG);
  }
  haloFile.close();
  const st Nh = wvec.size();

  // determine proper weight
  // get sum of weights and normalizing factor (to ensure mean 0)
  double sow = 0.;
  for (st i = 0; i < wvec.size(); i++)  sow += wvec[i];
  const double fn = double(NG*NG*NG) / sow;
  for (st i = 0; i < wvec.size(); i++)  wvec[i] *= fn;
  logger.printf("scalar_grid::assign_density_halos", INFO, "read in %zu (out of %zu) halos. Halo grid mass: %.5e", Nh, ih, fn);

  // now assign
  assigner.assign(posvec, wvec);

  if (logger.getLevel() >= DEBUG)  {
    logger.printf("assign_density_halos", DEBUG, "Halo field:");
    assigner.getGrid()->print_stats(stderr);
  }

  return Nh;
}

// read in particles in Gadget format
#include "assign_density_Gadget.inc"

} // end of namespace LEFTfield
