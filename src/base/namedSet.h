/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef NAMEDSET_H
#define NAMEDSET_H

#include <stdio.h>
#include <string>
#include <vector>
#include <cassert>
#include <algorithm>
#include <map>

// *********************************************************************
// eftlike
//
// namedSet: encapsulates std::vector<T> with named lookup and some
//           additional utility functions
//
// (C) Fabian Schmidt, 2021
// *********************************************************************

#include "types.h"
#include "error_handling.h"
#include "log.h"

namespace LEFTfield {

// *********************************************************************

template<typename T>
class namedSet {
 protected:
  std::map<std::string, T> objs;

 public:
  using iterator = typename std::map<std::string, T>::iterator;
  using const_iterator = typename std::map<std::string, T>::const_iterator;

  namedSet() { }

  // can be used with brace initializer: { { "p1", 1 }, { "p2", 0.5 } } e.g.
  namedSet(const std::map<std::string, T>& nameval)
    : objs(nameval)
  {}

  virtual ~namedSet()
  {}

  iterator begin()
    { return objs.begin(); }
  iterator end()
    { return objs.end(); }
  const_iterator begin() const
    { return objs.cbegin(); }
  const_iterator end() const
    { return objs.cend(); }

  st getN() const {
    return objs.size();
  }

  bool have(const std::string& name) const {
    return objs.find(name) != objs.end();
  }
  
  // *** get/set individual object ***
  // - by name
  T &operator[](const std::string& name) {
    NT_assert(have(name), "Key '", name, "' not found in namedSet");
    return objs.at(name);
  }
  const T &operator[](const std::string& name) const {
    NT_assert(have(name), "Key '", name, "' not found in namedSet");
    return objs.at(name);
  }
  // - by name, with fallback argument if not found
  // - overload () here, as we need two arguments (and overloading []
  //    with std::tuple leads to ambiguous overload for brace initialization)
  // - example usage: objset("b_delta^2", 0.)
  // - note: NO NON-CONST version.
  const T& operator()(const std::string& name, const T &pdef) const {
    return have(name) ? (*this)[name] : pdef;
  }


  // add an object
  void add(const std::string& name, const T& p) {
    if (have(name)) {
	logger.printf("namedSet::add", DEBUG,
		      "Note: already have object with name '%s', just updating it.", name.c_str());
    }
	  objs[name] = p;
  }

  // rename an object
  void rename(const std::string& name, const std::string &newname) {
    auto nodeHandler = objs.extract(name);
    nodeHandler.key() = newname;
    objs.insert(std::move(nodeHandler));
  }

  // append another namedSet
  void append(const namedSet& par) {
    for (const auto& p: par)
      add(p.first, p.second);
  }

  // append a map
  void append(const std::map<std::string, T>& nameval) {
    for (const auto& [name, p]: nameval)
      add(name, p);
  }

  // erase field, if exists
  void erase(const std::string &name) {
    if (have(name))
      objs.erase(name);
  }
  // erase from iterator
  iterator erase(iterator it) {
    return objs.erase(it);
  }

  void clear()
  {
    objs.clear();
  }
  
  // *** apply function ***
  template<typename Func>
  void apply(Func func)
  {
    for (auto& t: objs)
      func(t.second, t.first);
  }
};

// *********************************************************************

} // end of namespace LEFTfield

#endif
