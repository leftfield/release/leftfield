/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef LOG_H
#define LOG_H

#include <cstdio>
#include <string>
#include <cstdarg>
#include <cstdlib>

namespace LEFTfield {

typedef enum {ESSENTIAL=0, INFO=1, DEBUG=2, WHATAMIDOING=3 } errlevels;

class LogContext;

class Logger
  {
  private:
    errlevels lvl;
    std::string context;

    // the following are called by LogContext
    void setContext(const std::string &context_)
    { context = context_; }
    
    const std::string &getContext() const
      { return context; }

    friend class LogContext;

  public:
    Logger(errlevels lvl_, const std::string &context_)
      : lvl(lvl_), context(context_)
    {
      // check if environment variable set
      char *penv = getenv("NT_VERBOSITY_LEVEL");
      if (penv != NULL)  {
	int ilv = atoi(penv);
	if (ilv >=0 && ilv <= 3)  {
	  lvl = errlevels(ilv);
	  this->printf("Logger", ESSENTIAL,
		       "Found error level %d from environment.",
		       ilv);
	}
      }
    }

    void printf(errlevels level, const char *format ...) const
      {
      if (level<=lvl)
        {
        if (context!="")
          fprintf(stderr, "[%s] ", context.c_str());
        va_list args;
        va_start(args, format);
        vfprintf(stderr, format, args);
        va_end(args);
	fprintf(stderr, "\n");
        }
      }
    void printf(const std::string &cntxt, errlevels level,
      const char *format ...) const
      {
      if (level<=lvl)
        {
        if (cntxt!="")
          fprintf(stderr, "[%s] ", cntxt.c_str());
        va_list args;
        va_start(args, format);
        vfprintf(stderr, format, args);
        va_end(args);
	fprintf(stderr, "\n");
        }
      }

    void setLevel(errlevels lvl_)
      { lvl = lvl_; }

    errlevels getLevel() const
      { return lvl; }
  };

extern Logger logger;

class LogContext
  {
  private:
    std::string oldcontext;
  public:
    LogContext(const std::string &newcontext)
      : oldcontext(logger.getContext())
      { logger. setContext(newcontext); }
    ~LogContext()
      { logger. setContext(oldcontext); }
  };

} // end of namespace LEFTfield

#endif
