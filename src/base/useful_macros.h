/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef DUCC0_USEFUL_MACROS_H
#define DUCC0_USEFUL_MACROS_H

#if defined(__GNUC__)
#define DUCC0_NOINLINE __attribute__((noinline))
#define DUCC0_RESTRICT __restrict__
#define DUCC0_ALIGNED(align) __attribute__ ((aligned(align)))
#define DUCC0_PREFETCH_R(addr) __builtin_prefetch(addr);
#define DUCC0_PREFETCH_W(addr) __builtin_prefetch(addr,1);
#elif defined(_MSC_VER)
#define DUCC0_NOINLINE __declspec(noinline)
#define DUCC0_RESTRICT __restrict
#define DUCC0_ALIGNED(align)
#define DUCC0_PREFETCH_R(addr)
#define DUCC0_PREFETCH_W(addr)
#else
#define DUCC0_NOINLINE
#define DUCC0_RESTRICT
#define DUCC0_ALIGNED(align)
#define DUCC0_PREFETCH_R(addr)
#define DUCC0_PREFETCH_W(addr)
#endif

#endif
