/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <fstream>
#include <cstdio>
#include <numeric>
#include <unistd.h>
#include "hdf5.h"
#include "HDF5_IO.h"
#include "error_handling.h"


namespace LEFTfield {

class HDF5_silencer {
  private:
    herr_t (*old_func)(hid_t, void*);
    void *old_client_data;

  public:
    HDF5_silencer() {
      H5Eget_auto(H5E_DEFAULT, &old_func, &old_client_data);
      /* Turn off error printing */
      H5Eset_auto(H5E_DEFAULT, NULL, NULL);
    }

    ~HDF5_silencer() {
      H5Eset_auto(H5E_DEFAULT, old_func, old_client_data);
    }
};

template<typename T> T MYH5CHECK(T res, const std::string &details="") {
  if (res>=0) return res;
  std::string msg = "HDF5 error";
  if (details!="")
    msg += std::string(": ") + details;
  
  NT_fail(msg);
}

template<typename T> hid_t H5type ();
template<> hid_t H5type<double>() { return H5T_NATIVE_DOUBLE; }

template<typename T> void read_HDF5_dataset(const std::string& filename,
  const std::string& data_set_name, std::vector<T> &data,
  std::vector<size_t> &dimensions)
{
  HDF5_silencer silencer;
  auto file = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
  if (file<0) throw FileNotFound();
  // read the data
  auto dset = H5Dopen(file, data_set_name.c_str(), H5P_DEFAULT);
  if (dset<0) throw DatasetNotFound();
  auto dspace = MYH5CHECK(H5Dget_space(dset), "getting data space");
  auto ndim = H5Sget_simple_extent_ndims(dspace);
  std::vector<hsize_t> dim(ndim);
  MYH5CHECK(H5Sget_simple_extent_dims(dspace, dim.data(), nullptr), "getting array dimensions");
  dimensions.resize(ndim);
  for (int i=0; i<ndim; ++i)
    dimensions[i] = dim[i];
  auto ndata = std::accumulate(dimensions.begin(), dimensions.end(), 1, std::multiplies<size_t>());
  data.resize(ndata);
  MYH5CHECK(H5Dread(dset, H5type<T>(), H5S_ALL, H5S_ALL, H5P_DEFAULT, data.data()), "reading array data");
  MYH5CHECK(H5Sclose(dspace), "closing data space");
  MYH5CHECK(H5Dclose(dset), "closing data set");
  MYH5CHECK(H5Fclose(file), "closing file");
}

template void read_HDF5_dataset(const std::string& filename,
  const std::string& data_set_name, std::vector<double> &data,
  std::vector<size_t> &dimensions);

bool HDF5_attribute_present(const std::string& filename,
  const std::string& data_set_name, const std::string& attribute_name)
{
  HDF5_silencer silencer;
  auto file = MYH5CHECK(H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT), "opening file for reading");
  auto dset = MYH5CHECK(H5Dopen(file, data_set_name.c_str(), H5P_DEFAULT), "opening data set for reading");

  bool have_attr = H5Aexists(dset, attribute_name.c_str())>0;

  MYH5CHECK(H5Dclose(dset), "closing data set");
  MYH5CHECK(H5Fclose(file), "closing file");
  return have_attr;
}

template<typename T> T read_HDF5_attribute(const std::string& filename,
  const std::string& data_set_name, const std::string& attribute_name)
{
  HDF5_silencer silencer;
  auto file = MYH5CHECK(H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT), "opening file for reading");
  auto dset = MYH5CHECK(H5Dopen(file, data_set_name.c_str(), H5P_DEFAULT), "opening data set for reading");

  bool have_attr = H5Aexists(dset, attribute_name.c_str())>0;
  if (!have_attr)
    NT_fail("HDF5 attribute not found in file");

  auto attr = H5Aopen(dset, attribute_name.c_str(), H5P_DEFAULT);
  auto attrtype = MYH5CHECK(H5Aget_type(attr), "getting attribute type");
  auto adim = MYH5CHECK(H5Tget_size(attrtype));
//  NT_assert(adim==1, "attribute is not a scalar value");
  T res;
  auto space = MYH5CHECK(H5Aget_space(attr), "getting attribute space");
  auto ndims = MYH5CHECK(H5Sget_simple_extent_ndims(space), "getting attribute size");
  NT_assert(ndims==0, "need a scalar data space for attribute");
  auto memtype = MYH5CHECK(H5Tcopy(H5type<T>()), "copying type");
  MYH5CHECK(H5Tset_size(memtype, adim), "setting type size");
  MYH5CHECK(H5Aread(attr, memtype, &res), "reading attribute");
  MYH5CHECK(H5Tclose(memtype), "closing type");
  MYH5CHECK(H5Sclose(space), "closing attribute space");
  MYH5CHECK(H5Tclose(attrtype), "closing attribute type");
  MYH5CHECK(H5Aclose(attr), "closing attribute");
  MYH5CHECK(H5Dclose(dset), "closing data set");
  MYH5CHECK(H5Fclose(file), "closing file");
  return res;
}

template double read_HDF5_attribute(const std::string& filename,
  const std::string& data_set_name, const std::string& attribute_name);

template<> std::string read_HDF5_attribute(const std::string& filename,
  const std::string& data_set_name, const std::string& attribute_name)
{
  HDF5_silencer silencer;
  auto file = MYH5CHECK(H5Fopen(filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT), "opening file for reading");
  auto dset = MYH5CHECK(H5Dopen(file, data_set_name.c_str(), H5P_DEFAULT), "opening data set for reading");

  bool have_attr = H5Aexists(dset, attribute_name.c_str())>0;
  if (!have_attr)
    NT_fail("HDF5 attribute not found in file");

  auto attr = H5Aopen(dset, attribute_name.c_str(), H5P_DEFAULT);
  auto attrtype = MYH5CHECK(H5Aget_type(attr), "getting attribute type");
  auto adim = MYH5CHECK(H5Tget_size(attrtype));
  std::vector<char> buf(adim+1, 0);
  auto space = MYH5CHECK(H5Aget_space(attr), "getting attribute space");
  auto ndims = MYH5CHECK(H5Sget_simple_extent_ndims(space), "getting attribute size");
  NT_assert(ndims==0, "need a scalar data space for attribute");
  auto memtype = MYH5CHECK(H5Tcopy(H5T_C_S1), "copying type");
  MYH5CHECK(H5Tset_size(memtype, adim), "setting type size");
  MYH5CHECK(H5Aread(attr, memtype, buf.data()), "reading attribute");
  std::string res(buf.data());
  MYH5CHECK(H5Tclose(memtype), "closing type");
  MYH5CHECK(H5Sclose(space), "closing attribute space");
  MYH5CHECK(H5Tclose(attrtype), "closing attribute type");
  MYH5CHECK(H5Aclose(attr), "closing attribute");
  MYH5CHECK(H5Dclose(dset), "closing data set");
  MYH5CHECK(H5Fclose(file), "closing file");
  return res;
}

template<typename T> void write_HDF5_dataset(const std::string& filename,
  const std::string& data_set_name, const std::vector<T> &data,
  const std::vector<st> &dimensions)
{
  HDF5_silencer silencer;
  hid_t file;
  if (access(filename.c_str(), F_OK)==0)
    file = MYH5CHECK(H5Fopen(filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT), "opening file for modification");
  else
    file = MYH5CHECK(H5Fcreate(filename.c_str(), H5F_ACC_EXCL, H5P_DEFAULT, H5P_DEFAULT), "creating file");
  auto ndim = dimensions.size();
  std::vector<hsize_t> dim(ndim);
  for (st i=0; i<ndim; ++i)
    dim[i] = hsize_t(dimensions[i]);
  auto ndata = std::accumulate(dimensions.begin(), dimensions.end(), 1, std::multiplies<size_t>());
  NT_assert(data.size()==st(ndata), "array size mismatch");
  auto space = MYH5CHECK(H5Screate_simple(ndim, dim.data(), nullptr), "creating data space");
  auto lcpl = MYH5CHECK(H5Pcreate(H5P_LINK_CREATE), "creating link");
  MYH5CHECK(H5Pset_create_intermediate_group(lcpl,1), "creating intermediate group");
  auto dset = MYH5CHECK(H5Dcreate(file, data_set_name.c_str(), H5type<T>(), space, lcpl,
                H5P_DEFAULT, H5P_DEFAULT), "creating data set");
  MYH5CHECK(H5Pclose(lcpl), "closing link"); 
  MYH5CHECK(H5Dwrite(dset, H5type<T>(), H5S_ALL, H5S_ALL, H5P_DEFAULT,
                data.data()), "writing data");
  MYH5CHECK(H5Dclose(dset), "closing data set");
  MYH5CHECK(H5Sclose(space), "closing data space");
  MYH5CHECK(H5Fclose(file), "closing file");
}

template void write_HDF5_dataset(const std::string& filename,
  const std::string& data_set_name, const std::vector<double> &data,
  const std::vector<st> &dimensions);

template<typename T> void write_HDF5_attribute(const std::string& filename,
  const std::string& data_set_name, const std::string& attribute_name,
  const T &value)
{
  hid_t file;
  if (access(filename.c_str(), F_OK)==0)
    file = MYH5CHECK(H5Fopen(filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT), "opening file for modification");
  else
    file = MYH5CHECK(H5Fcreate(filename.c_str(), H5F_ACC_EXCL, H5P_DEFAULT, H5P_DEFAULT), "creating file");
  auto dset = MYH5CHECK(H5Dopen(file, data_set_name.c_str(), H5P_DEFAULT));

  auto attrtype = MYH5CHECK(H5Tcopy(H5type<T>()), "copying type");
  auto memtype = MYH5CHECK(H5Tcopy (H5type<T>()), "copying type");
  auto aspace = MYH5CHECK(H5Screate(H5S_SCALAR), "creating attribute space");
  auto attr = MYH5CHECK(H5Acreate (dset, attribute_name.c_str(), attrtype, aspace, H5P_DEFAULT,
                H5P_DEFAULT), "creating attribute");
  MYH5CHECK(H5Awrite(attr, memtype, &value));
  MYH5CHECK(H5Aclose(attr), "closing attribute");
  MYH5CHECK(H5Sclose(aspace), "closing attribute space");
  MYH5CHECK(H5Tclose(memtype), "closing type");
  MYH5CHECK(H5Tclose(attrtype), "closing attribute type");
  MYH5CHECK(H5Dclose(dset), "closing data set");
  MYH5CHECK(H5Fclose(file), "closing file");
}

template void write_HDF5_attribute(const std::string& filename,
  const std::string& data_set_name, const std::string& attribute_name,
  const double &value);

template<> void write_HDF5_attribute(const std::string& filename,
  const std::string& data_set_name, const std::string& attribute_name,
  const std::string &value)
{
  HDF5_silencer silencer;
  hid_t file;
  if (access(filename.c_str(), F_OK)==0)
    file = MYH5CHECK(H5Fopen(filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT), "opening file for modification");
  else
    file = MYH5CHECK(H5Fcreate(filename.c_str(), H5F_ACC_EXCL, H5P_DEFAULT, H5P_DEFAULT), "creating file");
  auto dset = MYH5CHECK(H5Dopen(file, data_set_name.c_str(), H5P_DEFAULT));

  auto attrtype = MYH5CHECK(H5Tcopy(H5T_C_S1), "copying type");
  MYH5CHECK(H5Tset_size(attrtype, value.size()+1), "setting type size");
  auto memtype = MYH5CHECK(H5Tcopy (H5T_C_S1), "copying type");
  MYH5CHECK(H5Tset_size(memtype, value.size()+1), "setting type size");
  auto aspace = MYH5CHECK(H5Screate(H5S_SCALAR), "creating attribute space");
  auto attr = MYH5CHECK(H5Acreate (dset, attribute_name.c_str(), attrtype, aspace, H5P_DEFAULT,
                H5P_DEFAULT), "creating attribute");
  MYH5CHECK(H5Awrite(attr, memtype, value.c_str()));
  MYH5CHECK(H5Aclose(attr), "closing attribute");
  MYH5CHECK(H5Sclose(aspace), "closing attribute space");
  MYH5CHECK(H5Tclose(memtype), "closing type");
  MYH5CHECK(H5Tclose(attrtype), "closing attribute type");
  MYH5CHECK(H5Dclose(dset), "closing data set");
  MYH5CHECK(H5Fclose(file), "closing file");
}

bool fileExists(const std::string& filename) {
  std::ifstream inp(filename.c_str());
  return inp.good();
}

void removeIfExists(const std::string& filename) {
  if (fileExists(filename)) {
    logger.printf("removeIfExists", INFO, "Removing file '%s'.", filename.c_str());
    NT_assert(std::remove(filename.c_str( ))==0, "error deleting file");
  }
}

} // end of namespace LEFTfield
