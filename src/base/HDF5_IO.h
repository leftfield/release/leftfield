/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef NBODYTOOLS_HDF5_IO_H
#define NBODYTOOLS_HDF5_IO_H

#include <vector>
#include <exception>
#include <string>
#include "types.h"

namespace LEFTfield {

class HDF5Exception: public std::exception {};
class FileNotFound: public HDF5Exception
{
public:
  virtual const char *what() const noexcept { return "HDF5 file not found"; }
};
class DatasetNotFound: public HDF5Exception
{
public:
  virtual const char *what() const noexcept { return "Data set not found in HDF5 file"; }
};

// read a named dataset from an HDF5 file into a linear vector ('data')
// - the actual dimensions of the dataset are returned in 'dimensions'
template<typename T> void read_HDF5_dataset(const std::string& filename,
  const std::string& data_set_name, std::vector<T> &data,
  std::vector<st> &dimensions);

// write linear data in 'data' to named HDF5 dataset in given file
// - indicate actual dimensions in 'dimensions'
template<typename T> void write_HDF5_dataset(const std::string& filename,
  const std::string& data_set_name, const std::vector<T> &data,
  const std::vector<st> &dimensions);

// check whether attribute present
bool HDF5_attribute_present(const std::string& filename,
  const std::string& data_set_name, const std::string& attribute_name);

// read/write string attributes of given dataset
template<typename T> T read_HDF5_attribute(const std::string& filename,
  const std::string& data_set_name, const std::string& attribute_name);

template<> std::string read_HDF5_attribute(const std::string& filename,
  const std::string& data_set_name, const std::string& attribute_name);

template<typename T> void write_HDF5_attribute(const std::string& filename,
  const std::string& data_set_name, const std::string& attribute_name,
  const T &value);

template<> void write_HDF5_attribute(const std::string& filename,
  const std::string& data_set_name, const std::string& attribute_name,
  const std::string &value);

bool fileExists(const std::string& filename);
void removeIfExists(const std::string& filename);

} // end of namespace LEFTfield

#endif
