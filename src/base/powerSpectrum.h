/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef POWERSPECTRUM_H
#define POWERSPECTRUM_H

#include <stdio.h>
#include <string>
#include <vector>
#include <math.h>

#include "grid.h"
#include "error_handling.h"

namespace LEFTfield {

// implements a set of k bins and corresponding P(k) values
// - essentially a simple histogram class
// - linear and logarithmic bin versions
// - units of k vector refer to grid::Lbox (default 1)

class table;

// base class
// - implements everything except k bins
class powerSpectrumBase
{
 protected:
  std::vector<double> bounds, centers;
  std::vector<double> Pkvec, nvec;

 public:
  powerSpectrumBase(const std::vector<double> &bounds_,
    const std::vector<double> &centers_)
    : bounds(bounds_), centers(centers_), Pkvec(centers_.size()),
      nvec(centers_.size())
    {
    NT_assert(centers_.size()>0, "need at least one bin");
    NT_assert(bounds.size()==centers_.size()+1,
      "bound and center size mismatch");
    for (size_t i=0; i<centers.size(); ++i)
      NT_assert((centers[i]>=bounds[i]) && (centers[i]<=bounds[i+1]),
        "bound/center mismatch");
    }

  void zero() {
    std::fill(Pkvec.begin(), Pkvec.end(), 0);
    std::fill(nvec.begin(), nvec.end(), 0);
  }

  void add (const powerSpectrumBase &other) {
    NT_assert((bounds==other.bounds) && (centers==other.centers),
      "power spectra are incompatible");
    for (size_t i=0; i<Pkvec.size(); ++i) {
      Pkvec[i] += other.Pkvec[i];
      nvec[i] += other.nvec[i];
    }
  }

  const std::vector<double> &get_bounds() const { return bounds; }

  // fill contribution to P(k) at wavenumber k
  void fill(const double k, const double Asq, st weight=1) {
    st bin = find_bin(k);
    if (bin>=Pkvec.size())  // k value does not fall into any bin
      return;
    Pkvec[bin] += Asq*weight;
    nvec[bin] += weight;
  }

  // normalize: divide each bin by number of modes, and scale by Lbox^3
  // - also, multiply by 'N'
  std::vector<double> get_normalized(const double N = 1.) const;

  // scale power spectrum by c k^n
  void scale(const double c, const double n);

  // bin finder / getters
  // - for current functionality, only these have to be overloaded by derived
  //   classes
  // - convention: if k value does not fall into any bin, return a bin number
  //   that is larger than the number of bins.
  st find_bin(const double k) const
    {
    if ((k<bounds[0]) || (k>bounds.back())) // outside all bins
      return bounds.size()+1;
    return int(std::lower_bound(bounds.begin(), bounds.end(), k)-bounds.begin()) - 1;
    }
  double get_k_low(const st ibin) const { return bounds[ibin]; }
  double get_k_high(const st ibin) const { return bounds[ibin+1]; }
  double get_k_center(const st ibin) const { return centers[ibin]; }

  double get_normalized_Pk(const st ibin, const double N=1.) const {
    auto Lbox = grid::get_Lbox();
    double NN = N * Lbox*Lbox*Lbox;
    return (nvec[ibin] > 0.) ? (Pkvec[ibin] * NN / nvec[ibin]) : 0;
  }
  double get_Nmodes(const st ibin) const {
    return nvec[ibin];
  }

  // generate table from power spectrum values
  // (which can then be interpolated etc.)
  // - flags control whether table is filled with log k and log Pk values,
  //   and whether extrapolated beyond range
  std::shared_ptr<table> get_table(const bool log_k, const bool log_Pk,
		   const bool extrapolate = true) const;
  
  void print(FILE *fp) const;
};


// powerSpectrum - "standard" class
// - implements logarithmic bins
// - maybe in future would be better to rename this to 'powerSpectrumLog'...
class powerSpectrum : public powerSpectrumBase
{
 private:
  static std::vector<double> make_bounds(const double lgkmin, const double lgkmax, const st Nbin) {
    double dlk = (lgkmax - lgkmin) / double(Nbin);
    std::vector<double> res(Nbin+1);

    // define bins
    for (st i=0; i < res.size(); i++)
      res[i] = pow(10., lgkmin + double(i) * dlk);
    return res;
  }
  static std::vector<double> make_centers(const double lgkmin, const double lgkmax, const st Nbin) {
    double dlk = (lgkmax - lgkmin) / double(Nbin);
    std::vector<double> res(Nbin);

    // define bins
    for (st i=0; i < res.size(); i++)
      res[i] = pow(10., lgkmin + (i+0.5) * dlk);
    return res;
  }
 public:
  // construct Nbin logarithmic bins from log10 kmin to log10 kmax
  powerSpectrum(const double lgkmin, const double lgkmax, const st Nbin)
    : powerSpectrumBase(make_bounds(lgkmin, lgkmax, Nbin),
                        make_centers(lgkmin, lgkmax, Nbin)) {}
};

// powerSpectrumLin - implements linear bins
class powerSpectrumLin : public powerSpectrumBase
{
 private:
  static std::vector<double> make_bounds(const double kmin, const double kmax, const st Nbin) {
    double dlk = (kmax - kmin) / double(Nbin);
    std::vector<double> res(Nbin+1);

    // define bins
    for (st i=0; i < res.size(); i++)
      res[i] = kmin + double(i) * dlk;
    return res;
  }
  static std::vector<double> make_centers(const double kmin, const double kmax, const st Nbin) {
    double dlk = (kmax - kmin) / double(Nbin);
    std::vector<double> res(Nbin);

    // define bins
    for (st i=0; i < res.size(); i++)
      res[i] = kmin + (i+0.5) * dlk;
    return res;
  }

 public:
  // construct Nbin linear bins from k_min to k_max
  powerSpectrumLin(const double k_min, const double k_max, const st Nbin)
    : powerSpectrumBase(make_bounds(k_min, k_max, Nbin),
                        make_centers(k_min, k_max, Nbin)) {}
};


// ***********************************************************************
// implements a set of power spectra with the same k bins
// - for convenience for printing etc

// !!! right now, only implemented for logarithmic bins, and hence
// !!! class 'powerSpectrum'; no reason for this other than laziness...

class powerSpectrumSet
{
  st Nbins;
  double lkmin, lkmax;
  std::vector<sptr<powerSpectrum>> pkvec;
  std::vector<std::string> namevec;
 public:
  // construct Nbin logarithmic bins from log10 kmin to log10 kmax
  powerSpectrumSet(const double lgkmin, const double lgkmax,
		   const st Nbin = 20)
    : Nbins(Nbin), lkmin(lgkmin), lkmax(lgkmax)
  { }

  sptr<powerSpectrum> push_back(std::string name)
  {
    pkvec.push_back(make_sptr<powerSpectrum>(lkmin, lkmax, Nbins));
    namevec.push_back(name);
    return pkvec.back();
  }

  sptr<powerSpectrum> operator[](st i) {
    return pkvec[i];
  }
  const sptr<powerSpectrum> operator[](st i) const {
    return pkvec[i];
  }
  st size() const {
    return pkvec.size();
  }
  
  std::string get_name(st i) {
    return namevec[i];
  }

  void print(FILE *fp) const;  
};

} // end of namespace LEFTfield

#endif
