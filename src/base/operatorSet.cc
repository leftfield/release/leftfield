/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include "operatorSet.h"
#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>

#include "scalar_grid.h"
#include "vector_grid.h"

namespace LEFTfield {

// ********************************************************************
// class operatorSet
//
// (C) Fabian Schmidt, 2017-2021
// ********************************************************************

// write all operators to HDF5 file
void operatorSet::write(const std::string& filename,
			const std::string& dataset_prefix) const
{
  for (const auto &iter: objs) {
    logger.printf("operatorSet::write", INFO,
		  "Writing operator '%s%s' to '%s'.",
		  dataset_prefix.c_str(), iter.first.c_str(), filename.c_str());
    iter.second.grd->write_HDF5(filename, dataset_prefix + iter.first);
  }
}

// read operators from HDF5 file
// - NOTE: need existing set of Opdata with desired names
// - could add group name too, if added to scalar_grid::read_HDF5
void operatorSet::read(const std::string& filename,
		       const std::string& dataset_prefix) const
{
  for (const auto &iter: objs) {
    logger.printf("operatorSet::read", INFO,
		  "Attempting to read operator '%s%s' from '%s'.",
		  dataset_prefix.c_str(), iter.first.c_str(), filename.c_str());
    iter.second.grd->read_HDF5(filename, dataset_prefix + iter.first);
  }
}


// generate cross moments with given field
void operatorSet::print_cross_moments(const scalar_grid &Oprime,
				      const std::string &Oprimename) const
{
  // check grid sizes and reality
  NT_assert(getN() > 0, "operatorSet::print_cross_moments: empty operatorSet.");
  NT_assert(Oprime.getNG() == objs.begin()->second.grd->getNG(),
	    "operatorSet::print_cross_moments: grid sizes differ.");
  NT_assert(Oprime.is_real(), "operatorSet::print_cross_moments: Oprime has to be in real space.");

  printf("# N_G / ");
  for (const auto &iter: objs) {
    printf("< %s | %s > / ", Oprimename.c_str(), iter.first.c_str());
  }
  printf("\n");
  
  printf("%zu  ", Oprime.getNG());
  for (const auto &iter: objs) {
    if (! iter.second.grd->is_real())  {
      logger.printf("operatorSet::print_cross_moments", ESSENTIAL,
		    "WARNING: operator '%s' not in real space.", iter.first.c_str());
      printf("0.  ");
      continue;
    }
    
    scalar_grid gm(Oprime);
    gm *= *(iter.second.grd);
    double mom = gm.get_mean();
    fprintf(stderr, "< %s | %s > = %.6e\n", Oprimename.c_str(), iter.first.c_str(),
	    mom);
    printf("%.6e  ", mom);
  }
  printf("\n");
}
  
void operatorSet::print_stats(FILE *fp)
{
  for (const auto &iter: objs) {
    fprintf(fp, "'%s' stats:\n", iter.first.c_str());
    iter.second.grd->print_stats(fp);
  }
}
 

// fill auto- and cross-power spectra from grids
// - row by row
void operatorSet::fill_powerspectra(powerSpectrumSet &pks)
{
  for (auto i1=objs.begin(); i1!=objs.end(); ++i1) {
    for (auto i2=i1; i2!=objs.end(); ++i2) {
      std::string corrname = "<" + i1->first + " | " + i2->first + ">";
      logger.printf("operatorSet::fill_powerspectra", INFO,
		    "%s", corrname.c_str());
      if (i1 == i2)
	i1->second.grd->auto_powerspectrum(* pks.push_back(corrname));
      else
	i1->second.grd->cross_powerspectrum(*(i2->second.grd),
					  * pks.push_back(corrname));
    }
  }
}

// fill cross-power spectra from grids of given operatorSet
// - only fills diagonal and "upper triangle" ; for lower triangle,
//   use member routine of other operatorSet
void operatorSet::fill_powerspectra(operatorSet& O, powerSpectrumSet &pks)
{
  // check if types are the same
  NT_assert(getN() == O.getN(), "ERROR in operatorSet::fill_powerspectra: attempting to cross-correlate operatorSets of different size.");
  
  for (auto i1=objs.begin(); i1!=objs.end(); ++i1) {
    for (auto i2=i1; i2!=objs.end(); ++i2) {
      std::string corrname = "<" + i1->first + " | " + i2->first + "'>";
      logger.printf("operatorSet::fill_powerspectra(O)", INFO,
		    "%s", corrname.c_str());
      i1->second.grd->cross_powerspectrum(*(O[i2->first].grd),
					* pks.push_back(corrname));
    }
  }
}
 
// fill cross-power spectra of given scalar grid will all grids in the set
 void operatorSet::fill_powerspectra(scalar_grid& delta_h, powerSpectrumSet &pks)
{
  for (const auto &iter: objs) {
    std::string corrname = "<delta_h | " + iter.first + ">";
    logger.printf("operatorSet::fill_powerspectra(delta_h)", INFO,
		  "%s", corrname.c_str());
    iter.second.grd->cross_powerspectrum(delta_h, * pks.push_back(corrname));
  }
}
 
} // end of namespace LEFTfield
