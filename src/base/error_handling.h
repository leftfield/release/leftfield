/*
 *  This file is part of the MR utility library.
 *
 *  This code is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This code is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this code; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/* Copyright (C) 2019-2020 Max-Planck-Society
   Author: Martin Reinecke */

#ifndef DUCC0_ERROR_HANDLING_H
#define DUCC0_ERROR_HANDLING_H

#include <sstream>
#include <exception>

#include "useful_macros.h"
#include "log.h"

namespace LEFTfield {

#if defined (__GNUC__)
#define DUCC0_ERROR_HANDLING_LOC_ ::LEFTfield::CodeLocation(__FILE__, __LINE__, __PRETTY_FUNCTION__)
#else
#define DUCC0_ERROR_HANDLING_LOC_ ::LEFTfield::CodeLocation(__FILE__, __LINE__)
#endif

// to be replaced with std::source_location once generally available
class CodeLocation
  {
  private:
    const char *file, *func;
    int line;

  public:
    CodeLocation(const char *file_, int line_, const char *func_=nullptr)
      : file(file_), func(func_), line(line_) {}

    inline ::std::ostream &print(::std::ostream &os) const
      {
      os << "\n" << file <<  ": " <<  line;
      if (func) os << " (" << func << ")";
      os << ":\n";
      return os;
      }
  };

inline ::std::ostream &operator<<(::std::ostream &os, const CodeLocation &loc)
  { return loc.print(os); }

#if (__cplusplus>=201703L) // hyper-elegant C++2017 version
template<typename ...Args>
void streamDump__(::std::ostream &os, Args&&... args)
  { (os << ... << args); }
#else
template<typename T>
void streamDump__(::std::ostream &os, const T& value)
  { os << value; }

template<typename T, typename ... Args>
void streamDump__(::std::ostream &os, const T& value,
  const Args& ... args)
  {
  os << value;
  streamDump__(os, args...);
  }
#endif
template<typename ...Args>
[[noreturn]] void DUCC0_NOINLINE fail__(Args&&... args)
  {
  logger.printf(ESSENTIAL, "\n!!!! NT_fail occurred !!!!\n");
  ::std::ostringstream msg; \
  ::LEFTfield::streamDump__(msg, args...); \
    throw ::std::runtime_error(msg.str()); \
  }

#define NT_fail(...) \
  do { \
    ::LEFTfield::fail__(DUCC0_ERROR_HANDLING_LOC_, "\n", ##__VA_ARGS__, "\n"); \
    } while(0)

#define NT_assert(cond,...) \
  do { \
    if (cond); \
    else { NT_fail("Assertion failure\n", ##__VA_ARGS__); } \
    } while(0)

}

#endif
