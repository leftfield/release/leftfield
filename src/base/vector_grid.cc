/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <string.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <cassert>
#include "vector_grid.h"
#include "scalar_grid.h"

namespace LEFTfield {

// default constructor
vector_grid::vector_grid(const st N_grid, const bool isreal,
			 const double v)
  : grid(N_grid, isreal, VECTOR)
{
  resize(NG);
  fill(v);
  set_real(isreal);
  // otherwise empty grids
}


vector_grid& vector_grid::operator+=(const vector_grid& grid)
{
  NT_assert(grid.NG == NG, "ERROR in vector_grid::+=: grid sizes differ!");
  NT_assert(grid.real == real, "ERROR in vector_grid::+=: attempting to add real and Fourier space grids.");
  
  for (st I=0; I < 3; I++)  
    vg[I] += grid.vg[I];

  return *this;
}


vector_grid& vector_grid::operator*=(const double val)
{
  for (st I=0; I < 3; I++) {
    vg[I] *= val;
  }

  return *this;
}

// - contraction to scalar grid
// - scalar product with another vector field
// - note: only makes sense in real space; hence, error is issue if applied to Fourier space fields
scalar_grid operator*(const vector_grid& v1, const vector_grid& v2)
{
  NT_assert(v2.NG == v1.NG, "ERROR in vector_grid::*: grid sizes differ!");
  NT_assert(v2.real && v1.real, "ERROR in vector_grid::*: attempting to contract Fourier space grids.");
  
  scalar_grid prod(v1.vg[0]);
  prod *= v2.vg[0];  
  for (st I=1; I < 3; I++) 
    prod += v1.vg[I] * v2.vg[I];

  prod.set_real(true);

  return prod;
}

// - multiplication with scalar grid
// - note: only makes sense in real space; hence, error is issue if applied to Fourier space fields
vector_grid operator*(const scalar_grid& s, const vector_grid& v)
{
  NT_assert(s.getNG() == v.NG, "ERROR in vector_grid::*: grid sizes differ!");
  NT_assert(v.real && s.is_real(), "ERROR in vector_grid::*: attempting to contract Fourier space grids.");

  vector_grid prod(v);
  for (st I=0; I < 3; I++)
    prod.vg[I] *= s;

  prod.set_real(true);
  return prod;
}


vector_grid operator%(const vector_grid& v1, const vector_grid& v2)
{
  NT_assert(v2.NG == v1.NG, "ERROR in vector_grid::%: grid sizes differ!");
  NT_assert(v2.real && v1.real,
	    "ERROR in vector_grid::%: attempting to contract Fourier space grids.");

  // this probably does one unnecessary copy, but not sure how to avoid...
  vector_grid V(v1.NG, true);
  V.vg[0] = v1.vg[1] * v2.vg[2] - v1.vg[2] * v2.vg[1];
  V.vg[1] = v1.vg[2] * v2.vg[0] - v1.vg[0] * v2.vg[2];
  V.vg[2] = v1.vg[0] * v2.vg[1] - v1.vg[1] * v2.vg[0];
  
  return V;
}


// return \partial_i (grid)^i
void vector_grid::divergence(scalar_grid& div) const  
{
  NT_assert(!real, "ERROR in vector_grid::divergence: need to be in Fourier space!");

  div.resize(NG);
  div.set_real(false);
  div.iterateFourierHalfParallel([this,&div](st idx, st idxc, double kx, double ky, double kz, double)
    {
    auto vgx=hartley_load(vg[0][idx], vg[0][idxc]);
    auto vgy=hartley_load(vg[1][idx], vg[1][idxc]);
    auto vgz=hartley_load(vg[2][idx], vg[2][idxc]);
    auto res = cmplx(0., kx)*vgx
             + cmplx(0., ky)*vgy
             + cmplx(0., kz)*vgz;
    hartley_store(res, div[idx], div[idxc]);
    });
  // ensure k=0 mode is zero
  div[0] = 0.;
}




// curl, computed in Fourier space
vector_grid& vector_grid::curl()
{
  go_fourier();

  iterateFourierHalfParallel([this](st idx, st idxc, double kx, double ky, double kz, double)
    {
    cmplx Kq0(0.,kx), Kq1(0.,ky), Kq2(0.,kz);
    cmplx V[3];
    V[0] = hartley_load(vg[0][idx], vg[0][idxc]);
    V[1] = hartley_load(vg[1][idx], vg[1][idxc]);
    V[2] = hartley_load(vg[2][idx], vg[2][idxc]);
    hartley_store(Kq1 * V[2] - Kq2 * V[1], vg[0][idx], vg[0][idxc]);
    hartley_store(Kq2 * V[0] - Kq0 * V[2], vg[1][idx], vg[1][idxc]);
    hartley_store(Kq0 * V[1] - Kq1 * V[0], vg[2][idx], vg[2][idxc]);
    });
  // ensure k=0 mode is zero
  for (st I=0; I < 3; I++)
    vg[I][0] = 0.;

  return *this;
}


// generate from scalar field via
// \partial_i (grid)
// - note that derivatives are F.T. of i k^j in our convention
void vector_grid::generate_gradient(const scalar_grid& grid)
{
  // - simply resize as needed; no filling necessary
  resize(grid.getNG());
  set_real(false);
  NT_assert(!grid.is_real(), "ERROR in vector_grid::generate_gradient: need Fourier-space source grid!");

  iterateFourierHalfParallel([this,&grid](st idx, st idxc, double kx, double ky, double kz, double)
    {
    auto gridtmp=hartley_load(grid[idx],grid[idxc]);
    hartley_store(-kx*gridtmp.imag(), kx*gridtmp.real(), vg[0][idx],vg[0][idxc]);
    hartley_store(-ky*gridtmp.imag(), ky*gridtmp.real(), vg[1][idx],vg[1][idxc]);
    hartley_store(-kz*gridtmp.imag(), kz*gridtmp.real(), vg[2][idx],vg[2][idxc]);
    });
  // null out self-conjugate points on Nyquist planes
  for (st i0=0; i0 < NG; i0+=NG/2)  {
    for (st i1=0; i1 < NG; i1+=NG/2)  {
      for (st comp=0; comp < 3; comp++)  {
        vg[comp](i0,i1,NG/2) = 0.;
        vg[comp](i0,NG/2,i1) = 0.;
        vg[comp](NG/2,i0,i1) = 0.;
      }
    }
  }
}


// generate from scalar field via
// -\partial_j\/\laplace (grid) = - (i k_j) / (-k^2) = i k_j / k^2
void vector_grid::generate_displacement(const scalar_grid& grid, const double sign)
{
  // - simply resize as needed; no filling necessary
  resize(grid.getNG());
  set_real(false);
  NT_assert(!grid.is_real(), "ERROR in vector_grid::generate_displacement: need Fourier-space source grid!");
    
  iterateFourierHalfParallel([this,&grid,sign](st idx, st idxc, double kx, double ky, double kz, double K2)
    {
      double ilapl = -sign/K2;
      auto gridtmp= ilapl * hartley_load(grid[idx],grid[idxc]);
      hartley_store(-kx*gridtmp.imag(), kx*gridtmp.real(),
		    vg[0][idx], vg[0][idxc]);
      hartley_store(-ky*gridtmp.imag(), ky*gridtmp.real(),
		    vg[1][idx], vg[1][idxc]);
      hartley_store(-kz*gridtmp.imag(), kz*gridtmp.real(),
		    vg[2][idx], vg[2][idxc]);
    });
  
  // null out unphysical constant mode
  for (st I=0; I < 3; I++)  {
    vg[I][0] = 0.;
  }
  // null out self-conjugate points on Nyquist planes
  for (st i0=0; i0 < NG; i0+=NG/2)  {
    for (st i1=0; i1 < NG; i1+=NG/2)  {
      for (st comp=0; comp < 3; comp++)  {
        vg[comp](i0,i1,NG/2) = 0.;
        vg[comp](i0,NG/2,i1) = 0.;
        vg[comp](NG/2,i0,i1) = 0.;
      }
    }
  }
}



// returns CIC-interpolated value at x,y,z
// - grid has to be in real space
// - real value returned, obviously
// - trilinear interpolation if set, otherwise NGP
real_vec3 vector_grid::interpolate_to(const double xp, const double yp,
				      const double zp,
				      const bool trilinear) const
{
  return { vg[0].interpolate_to(xp, yp, zp, trilinear),
           vg[1].interpolate_to(xp, yp, zp, trilinear),
           vg[2].interpolate_to(xp, yp, zp, trilinear) }; 
}
  
// adds CIC-interpolated given (real) scalar value to x,y,z
// - grid has to be in real space
void vector_grid::interpolate_add(const real_vec3 val, const double xp,
				  const double yp, const double zp)
{
  for (st I=0; I < 3; I++)
    vg[I].interpolate_add(val[I], xp, yp, zp);
}

// return Fourier-space grid of size 3xNGd, for all modes
// with |k| <= kcut_phys, where kcut_phys refers to grid::Lbox
// (box size, i.e. fundamental modes of both grids are assumed to be the same)
// - no cut if kcut_phys=0.
// - Hermitianity of dest is ensured
// - returns number of Fourier modes that remain after cut in Nmodes
vector_grid vector_grid::copysharpk(const st NGd, const double kcut_phys,
				    const sharpkFilterType ftype,
				    st *N_modes) const
{
  // sanity check
  NT_assert(!real, "Error in vector_grid::copysharpk: source has to be in Fourier space.");

  vector_grid dest(NGd, false, 0.);  
  for (st i=0; i < 3; i++) {
    vg[i].copysharpk(dest.vg[i], kcut_phys, ftype, N_modes);
  }

  return dest;
}


// file I/O

void vector_grid::write_nbtl_s(std::ostream &file) const
{ for (const auto &g: vg) g.write_nbtl_s(file); }

void vector_grid::read_nbtl_s(std::istream &file)
{ for (auto &g: vg) g.read_nbtl_s(file); }


real_vec3 vector_grid::get_mean()
{
  return { vg[0].get_mean(), vg[1].get_mean(), vg[2].get_mean() };
}

real_vec3 vector_grid::get_variance(real_vec3 *mp)
{
  bool swtch = false;
  if (! is_real())  { // go to real space if necessary
    go_real();
    swtch = true;
  }
  real_vec3 mean, variance
    = { vg[0].get_variance(&mean[0]), vg[1].get_variance(&mean[1]),
	vg[2].get_variance(&mean[2]) };

  if (mp) {
    (*mp)[0] = mean[0];
    (*mp)[1] = mean[1];
    (*mp)[2] = mean[2];
  }
  
  if (swtch) go_fourier(); // go back to Fourier space if we were there before
  return variance;
}

void vector_grid::print_stats(FILE *fp)
{
  real_vec3 M, V = get_variance(&M);
  fprintf(fp, "vector_grid:\n");
  for (st I=0; I < 3; I++)  {
    fprintf(fp, "             mean^%zu = %.6e\n", I, M[I]);
    fprintf(fp, "          sqrt(V^%zu) = %.6e\n", I, sqrt(V[I]));
  }
}

} // end of namespace LEFTfield
