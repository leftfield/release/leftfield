/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef TYPES_H
#define TYPES_H

// ********************************************************************
// type definitions for nbody_tools/cubedfields
//
// (C) Fabian Schmidt, 2020
// ********************************************************************

#include <stddef.h>
#include <fstream>
#include <complex>
#include <array>
#include <memory>
#include <type_traits>

#include "log.h"

namespace LEFTfield {

// ****************************************************************
// useful and important types

// shorthand for size_t
typedef size_t st;

// complex numbers
typedef std::complex<double> cmplx;

// vectors
typedef std::array<cmplx, 3> vec3;
typedef std::array<double, 3> real_vec3;

// shared pointers
template<typename T> using sptr=std::shared_ptr<T>;
template<typename T, typename... Args> inline sptr<T> make_sptr(Args&&... args)
  { return std::make_shared<T>(args...); }

// grid type
enum grid_type {
  NONE=0, SCALAR, VECTOR, TENSOR, UNSUPPORTED,
  END_grid_type
};

// type of sharp-k filter:
// - NOFILTER: no cut
// - SPHERE: \Theta_H(kcut - |\vec k|)
// - CUBE:   \prod_i=1^3 \Theta_H(kcut - |k_i|)
enum sharpkFilterType {
  NOFILTER=0, SPHERE, CUBE,
  END_sharpkFilterType
};

// filter object for convenience
class sharpkFilter {
public:
  sharpkFilterType type;
  double kcut;

  sharpkFilter(sharpkFilterType type_=SPHERE, double kcut_=0.)
    : type(type_), kcut(kcut_)
  { }

  bool passes(const double kx, const double ky, const double kz,
	      const double K2) const
  {
    if (! (kcut > 0.)) return true; // no filter
    
    if (type==SPHERE)
      return K2 < kcut*kcut;
    if (type==CUBE)
      return fabs(kx) < kcut && fabs(ky) < kcut && fabs(kz) < kcut;
    
    return true; // default: no filter
  }
};
  
// struct describing order of given operator
// - order in perturbations
// - order in derivatives^2 (<=> k^2)
// - order in stochastic fields
// (other relevant order parameters in future)

struct op_order
{
  st pt;  // order in perturbations
  st deriv; // order in \partial_x^2
  st stoch; // order in epsilon
  // plus future order parameters ...

  op_order(const st pt_order=1, const st deriv_order=0,
     const st stoch_order=0)
  :  pt(pt_order), deriv(deriv_order), stoch(stoch_order)
  { }

  // adding orders
  // - don't implement subtraction to avoid issue with zero/negative orders...
  op_order& operator +=(const op_order& oo)
  {
    pt += oo.pt;
    deriv += oo.deriv;
    stoch += oo.stoch;
    return *this;
  }

  op_order operator+(const op_order& rhs) const
  {
    auto lhs = *this;
    lhs += rhs;
    return lhs;
  }

  std::string to_string() const {
    return std::string("op_order(") + std::to_string(pt)
      + "," + std::to_string(deriv) + "," + std::to_string(stoch) + ")";
  }
  
  // comparison depends on external information (slope of P(k), R_* scale, ...)
  // hence not implemented here;
  // use expansion_parameters::is_higher_order(  ) etc for this


};
  
} // end of namespace LEFTfield

#endif
