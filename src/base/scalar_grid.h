/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef SCALAR_GRID_H
#define SCALAR_GRID_H

#include <iostream>
#include <math.h>

#include "error_handling.h"
#include "grid.h"
#include "powerSpectrum.h"


namespace LEFTfield {

class scalar_grid : public grid
{
 protected:
  std::vector<double> rhog;

  template<typename Func> void iteratePS(powerSpectrumBase& Pk,
    Func func) const;

 public:
  explicit scalar_grid(const st N_grid=0, bool isreal=true, const double &val=0)
    : grid(N_grid, isreal, SCALAR), rhog(NG*NG*NG, val) {}

  // copy constructor
  scalar_grid(const scalar_grid& grd) = default;

  // copy assignment
  scalar_grid& operator=(const scalar_grid& grd) = default;

  // move constructor
  scalar_grid(scalar_grid&&) noexcept = default;
  
  virtual void resize(const st NGnew)
    {
    if (NGnew==NG) return;
    NG = NGnew;
    NG2 = NG*NG;
    rhog.resize(NG*NG*NG);
    rhog.shrink_to_fit();
    }

  void fill (const double &val)
    { for (auto &v:rhog) v=val; }

  // --------------------------------------------------------------------
  // array access
  // --------------------------------------------------------------------

  double &operator()(long i, long j, long k)
    { return rhog[i*NG2+j*NG+k]; }
  const double &operator()(long i, long j, long k) const
    { return rhog[i*NG2+j*NG+k]; }
  double &operator()(const std::array<st,3> &idx)
    { return rhog[idx[0]*NG2+idx[1]*NG+idx[2]]; }
  const double &operator()(const std::array<st,3> &idx) const
    { return rhog[idx[0]*NG2+idx[1]*NG+idx[2]]; }
  double &operator[](long idx)
    { return rhog[idx]; }
  const double &operator[](long idx) const
    { return rhog[idx]; }

  // --------------------------------------------------------------------
  // operations
  // --------------------------------------------------------------------
  // - note: for operations which only make sense in real space, error is
  //         issue if applied to Fourier space fields

  // - add grids element-by-element
  scalar_grid& operator+=(const scalar_grid &a)
    {
    assert_compatible(a, "+=");
    iterateParallel([this,&a](size_t idx){rhog[idx]+=a[idx];});
    return *this;
    }
  scalar_grid& operator-=(const scalar_grid &a)
    {
    assert_compatible(a, "-=");
    iterateParallel([this,&a](size_t idx){rhog[idx]-=a[idx];});
    return *this;
    }
  // - scale by real number
  scalar_grid& operator*=(const double fct)
    {
    iterateParallel([this,fct](size_t idx){rhog[idx]*=fct;});
    return *this;
    }
  // - add a  real number
  scalar_grid& operator+=(const double v)
    {
    iterateParallel([this,v](size_t idx){rhog[idx]+=v;});
    return *this;
    }
  scalar_grid& operator-=(const double v)
    {
    iterateParallel([this,v](size_t idx){rhog[idx]-=v;});
    return *this;
    }
  // - multiply element-by-element
  scalar_grid& operator*=(const scalar_grid &a)
    {
    assert_compatible(a, "*=");
    iterateParallel([this,&a](size_t idx){rhog[idx]*=a[idx];});
    return *this;
    }
  scalar_grid& operator/=(const scalar_grid &a)
    {
    assert_compatible(a, "/=");
    iterateParallel([this,&a](size_t idx){rhog[idx]/=a[idx];});
    return *this;
    }

  // add, element-wise, c * grid to grid
  scalar_grid& add(const double c, const scalar_grid &g)
    {
    assert_compatible(g, "add");
    if (c==0.) return *this; // triviality shortcut
    iterateParallel([this,&g,c](size_t idx){rhog[idx]+=c*g[idx];});
    return *this;
    }
  // multiply, element-wise, by c * grid
  scalar_grid& multiply(const double c, const scalar_grid &g)
    {
    assert_compatible(g, "multiply");
    iterateParallel([this,&g,c](size_t idx){rhog[idx]*=c*g[idx];});
    return *this;
    }
  
  // compute scalar product
  double dot(const scalar_grid& g) const;

  // invert cell by cell
  // - returned grid is limited to [-ceil, ceil]
  scalar_grid& inverse(const double ceil)
    {
    NT_assert(real, "need to be in real space");
    iterateParallel([this,ceil](size_t idx)
      { rhog[idx] = std::min(ceil, std::max(-ceil, 1./rhog[idx]));});
    return *this;
    }

  // apply function cell by cell (in real space)
  template<typename Func>
  scalar_grid& apply(Func func, bool serial=false)
  {
    // NT_assert(real, "need to be in real space");
    // allow for this cell-by-cell computation in Fourier space, but print
    // a warning.
    if (!real)
      logger.printf("scalar_grid::apply", INFO,
		    "WARNING: using 'apply' on a Fourier-space grid.");
    if (serial) {
      iterate([this,func](size_t idx)
              { rhog[idx] = func(rhog[idx]); });
    } else {
      iterateParallel([this,func](size_t idx)
              { rhog[idx] = func(rhog[idx]); });
    }
    return *this;
  }

  scalar_grid operator*(const double val) const
    {
    scalar_grid gnew(NG, real);
    iterateParallel([this,&gnew,val](size_t idx){gnew[idx]=rhog[idx]*val;});
    return gnew;
    }
  scalar_grid operator*(const scalar_grid& grid) const
    {
    assert_compatible(grid, "*");
    scalar_grid gnew(NG, real);
    iterateParallel([this,&gnew,&grid](size_t idx)
      {gnew[idx]=rhog[idx]*grid[idx];});
    return gnew;
    }
  scalar_grid operator+(const scalar_grid& grid) const
    {
    assert_compatible(grid, "+");
    scalar_grid gnew(NG, real);
    iterateParallel([this,&gnew,&grid](size_t idx)
      {gnew[idx]=rhog[idx]+grid[idx];});
    return gnew;
    }
  scalar_grid operator-(const scalar_grid& grid) const
    {
    assert_compatible(grid, "-");
    scalar_grid gnew(NG, real);
    iterateParallel([this,&gnew,&grid](size_t idx)
      {gnew[idx]=rhog[idx]-grid[idx];});
    return gnew;
    }


  // !!!!!
  /* // - binary versions referring to compound assignments */
  /* friend scalar_grid operator+(scalar_grid lhs, const scalar_grid& rhs) { */
  /*   lhs += rhs; */
  /*   return lhs; */
  /* } */
  /* friend scalar_grid operator-(scalar_grid lhs, const scalar_grid& rhs) { */
  /*   lhs -= rhs; */
  /*   return lhs; */
  /* } */
  /* friend scalar_grid operator*(scalar_grid lhs, const double c) { */
  /*   lhs *= c; */
  /*   return lhs; */
  /* } */
  friend scalar_grid operator*(const double c, scalar_grid lhs) {
    lhs *= c;
    return lhs;
  }
  /* friend scalar_grid operator*(scalar_grid lhs, const scalar_grid& rhs) { */
  /*   lhs *= rhs; */
  /*   return lhs; */
  /* } */

  // Laplace operator and inverse, applied in Fourier space
  // (note: derivatives refer to grid::Lbox)
  scalar_grid& Laplace();
  scalar_grid& inverseLaplace();

  // FFT
  scalar_grid& go_fourier();
  scalar_grid& go_real();

  // subtract mean to ensure <grid> = 0
  void subtract_mean();

  // smooth field (optionally take out CIC window function)
  // - R refers to grid::Lbox
  // !!! dividing by CIC window currently doesn't work (have to deal with
  //     zeros of kernel)
  void smooth_Gaussian(const double R, bool divide_CIC_window = false);

  // --------------------------------------------------------------------
  // further grid operations
  // --------------------------------------------------------------------

#if 0
  // copy grid contents from given array of real DFT
  // - assumes realDFTdata is array of dimensions NG*NG*(NG/2+1) !
  void copyDFTfourier(const fftw_complex *realDFTdata);
  void copyDFTreal(const fftw_complex *) {
    logger.printf("scalar_grid::copyDFTreal", ESSENTIAL,
      "copyDFTreal not yet implemented.");
  }
#endif

  // interpolate from given coarser grid (NGP)
  void interpolateNGP(const scalar_grid& grid);
  // --------------------------------------------------------------------
  // interpolation
  // --------------------------------------------------------------------

  // returns CIC-interpolated value at x,y,z
  // - grid has to be in real space
  // - real value returned, obviously
  // - trilinear interpolation if set, otherwise NGP
  double interpolate_to(const double x, const double y, const double z,
      const bool trilinear=true) const;

  // adds CIC-interpolated given (real) scalar value to x,y,z
  // - grid has to be in real space
  void interpolate_add(const double val,
           const double x, const double y, const double z);

  // returns gradient of CIC-interpolation in direction I=0,1,2
  double interpolate_to_gradient(const double xp, const double yp,
				 const double zp, const st I,
				 const bool trilinear=true) const;

  // - fill grid res with resized version of *this, where resizing is done in
  //   Fourier space
  // - either by zero-padding, in case of
  //   enlargement, or cutting of high-frequency modes in case of reduction
  void resizeFourier(scalar_grid &res) const;

  // - set to zero all Fourier modes in grid above kcut_phys,
  //   where kcut_phys refers to grid::Lbox
  // - returns number of Fourier modes that remain after cut
  // - does nothing if kcut_phys==0.
  // - Hermitianity is ensured
  st cutsharpk(const double kcut_phys,
	       const sharpkFilterType ftype = SPHERE);

  // - using sharpkFilter object
  // !!! should really be the other way around, using sharpkFilter in main function
  st cutsharpk(const sharpkFilter& filter)
  {
    return cutsharpk(filter.kcut, filter.type);
  }

  // return Fourier-space grid of size NGd, for all modes
  // with |k| <= kcut_phys, where kcut_phys refers to grid::Lbox
  // (box size, i.e. fundamental modes of both grids are assumed to be the same)
  // - no cut if kcut_phys=0.
  // - Hermitianity of dest is ensured
  // - returns number of Fourier modes that remain after cut in Nmodes
  void copysharpk(scalar_grid& dest, const double kcut_phys = 0.,
      const sharpkFilterType ftype = SPHERE,
      st *N_modes = 0) const;

  // - same, allocating grid of size NGd
  sptr<scalar_grid> copysharpk(const st NGd, const double kcut_phys = 0.,
        const sharpkFilterType ftype = SPHERE,
        st *N_modes = 0) const  {
    auto dest = make_sptr<scalar_grid>(NGd, false);
    copysharpk(*dest, kcut_phys, ftype, N_modes);
    return dest;
  }

  // File I/O

  // * nbtl: (use grid read_nbtl/write_nbtl)
  virtual void write_nbtl_s(std::ostream &file) const;
  // - if currently unallocate (NG==0), this allocates grid of the size
  //   found in file
  virtual void read_nbtl_s(std::istream &file);

  // * ASCII
  void read_density_from_text(const std::string &file);

  // * read in real slab from raw binary file (e.g. density output generated
  //   from ic_2lptngnonlocal)
  // - file is assumed to contain raw binary real grid with field size 'fsize'
  // - reads fsize * NG^2 * Nslab bytes
  // - if Nslab=0, then assumes Nslab = NG (i.e. full grid in file)
  void read_nbtl_slab_raw(const std::string &file, const st Nslab=0, const st istart=0,
         const st fsize = sizeof(double));

  // * HDF5
  // read in grid (NG^3 doubles) from given HDF5 file and data set name
  virtual void read_HDF5(const std::string& file,
                         const std::string& data_set_name);

  // write grid (NG^3 doubles) to given HDF5 file and data set name
  virtual void write_HDF5(const std::string& file,
                          const std::string& data_set_name) const;

  // --------------------------------------------------------------------
  // statistics
  // --------------------------------------------------------------------

  // get basic statistics
  // - mean
  double get_mean() const;
  // - variance (also computes mean and returns in pointer if given)
  double get_variance(double *mean = 0) const;

  // print mean and variance
  // (real space)
  void print_stats(FILE *fp);

  // compute sum of squares
  // - if in Fourier space, divides by NG^3
  double get_sumsquares() const;

  // power spectra
  // - fills the auto/cross power spectrum from the grid using grid::Lbox
  void auto_powerspectrum(powerSpectrumBase& Pk);
  void cross_powerspectrum(scalar_grid&, powerSpectrumBase& Pk);

  // real-space cross-correlation coefficient
  // - returns < (*this) g >_x /( < (*this)^2 >_x < g^2 >_x )^1/2 ,
  //   after means have been subtraced
  double crosscorrelation_coefficient(const scalar_grid& g) const;

  // === Tested the following, but they are actually noisier than    ===
  // === taking ratios of the binned P(k) returned by above routines ===

  /* // fill mode-by-mode P(k) ratio: P(k, *this) / P(k, grid) */
  /* // note: need to divide result by Lbox^3 */
  /* void powerspectrum_ratio(scalar_grid& grid, powerSpectrumBase&, */
  /*         bool normalize); */
  /* // fill mode-by-mode correlation coefficient: */
  /* //   P(k, cross) / sqrt[ P(k, *this) P(k, grid) ] */
  /* // note: need to divide result by Lbox^3 */
  /* void correlation_coefficient(scalar_grid& grid, powerSpectrumBase&, */
  /*             bool normalize); */
};

} // end of namespace LEFTfield

#endif
