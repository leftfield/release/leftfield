/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include "utilities/table.h"
#include "powerSpectrum.h"


namespace LEFTfield {

// normalize: divide each bin by number of modes, and scale by Lbox^3
// - also, multiply by 'N'
std::vector<double> powerSpectrumBase::get_normalized(const double N) const
{
  std::vector<double> res(nvec.size());
  auto Lbox = grid::get_Lbox();
  double NN = N * Lbox*Lbox*Lbox;
  for (st i=0; i < nvec.size(); i++)
    res[i] = (nvec[i] > 0.) ? (Pkvec[i] * NN / nvec[i]) : 0;
  return res;
}


// scale power spectrum by c k^n
void powerSpectrumBase::scale(const double c, const double n)
{
  for (st i=0; i < Pkvec.size(); i++)  {
    double k = get_k_center(i);
    Pkvec[i] *= c * pow(k, n);
  }
}

// generate table from power spectrum values
// (which can then be interpolated etc.)
// - flags control whether table is filled with log k and log Pk values,
//   and whether extrapolated beyond range
std::shared_ptr<table> powerSpectrumBase::get_table(const bool log_k, const bool log_Pk,
				    const bool extrapolate) const
{
  auto Pk = get_normalized();
  auto t = make_sptr<table>(Pk.size(), nullptr, extrapolate);
  for (st i=0; i < Pk.size(); i++)  {
    // bin center
    double k = get_k_center(i);
    t->push_back(log_k ? log(k) : k,
		 log_Pk ? log(Pk[i]) : Pk[i]);
  }

  return t;
}

void powerSpectrumBase::print(FILE *fp) const
{
  // alert user if Lbox has default value of 1
  if (grid::get_Lbox() == 1.) 
    logger.printf("powerSpectrumBase::print", ESSENTIAL, "NOTE: computing power spectrum for Lbox=1.");

  fprintf(fp, "# powerSpectrumBase::print: Lbox = %.1f h^-1 Mpc\n", grid::get_Lbox());
  fprintf(fp, "# k_low / k_high / k_mid [all h Mpc^-1] / P(k) [(h^-1 Mpc)^3] / N_modes\n");
  auto values = get_normalized();
  for (st i=0; i < Pkvec.size(); i++)  {
    fprintf(fp, "%.4e  %.4e  %.4e  %.7e  %.0f\n",
	    get_k_low(i), get_k_high(i), get_k_center(i),
	    values[i], nvec[i]);
  }
}

// ***********************************************************************

void powerSpectrumSet::print(FILE *fp) const
{
  // alert user if Lbox has default value of 1
  if (grid::get_Lbox() == 1.) 
    logger.printf("powerSpectrumBase::print", ESSENTIAL, "NOTE: computing power spectrum for Lbox=1.");

  fprintf(fp, "# powerSpectrumSet::print: Lbox = %.1f h^-1 Mpc\n", grid::get_Lbox());
  if (! size())  {
    fprintf(fp, "# no power spectra.\n");
    return;
  }
  fprintf(fp, "# k_low / k_high / k_mid [all h Mpc^-1] / ");
  for (st i=0; i < size(); i++)  {
    fprintf(fp, "P_%s(k) [(h^-1 Mpc)^3] / N_modes / ", namevec[i].c_str());
  }
  fprintf(fp, "(%zu power spectra)\n", size());
  for (st i=0; i < Nbins; i++)  {
    fprintf(fp, "%.4e  %.4e  %.4e",
	    pkvec[0]->get_k_low(i), pkvec[0]->get_k_high(i),
	    pkvec[0]->get_k_center(i) );
    for (st I=0; I < size(); I++)  
      fprintf(fp, "  %.8e  %.0f", pkvec[I]->get_normalized_Pk(i),
	      pkvec[I]->get_Nmodes(i));

    fprintf(fp, "\n");
  }
}

} // end of namespace LEFTfield
