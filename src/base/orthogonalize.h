/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef ORTHOGONALIZE_H
#define ORTHOGONALIZE_H

// ********************************************************************
// orthogonalize.h
// - functions to orthogonalize (~renormalize) fields with respect
//   to each other
// - options based on analytical fit (terms can be absorbed into higher-
//   derivative operators) or table interpolation
//
// (C) Fabian Schmidt, 2019-2021
// ********************************************************************

#include "scalar_grid.h"
#include "operatorSet.h"

namespace LEFTfield {

// -------------------------------------------------------------------
// orthogonalize given operator 'O' with respect to other operator 'ref',
// returning the result in given grid 'Oorth'
// - O, ref are non-constant, as power spectra are measured (FT if necessary)
// - non-in-place operation allows for parallelization

// - to enable debug output:
// #define WRITE_DEBUG_ORTHO

// Option 1: table interpolation
// - this works numerically precisely, but corresponds to adding nonlocal
//   terms in real space (i.e. not analytic in k^2)

void orthogonalize_table(scalar_grid& O, scalar_grid& ref,
			 scalar_grid& Oorth);

#ifdef ROOT_AVAILABLE
// Option 2: polynomial fit in k^2
// - uses ROOT fitting functionality
// - avoids adding nonlocal terms that are not absorbed by higher-derivative
//   coefficients
// - kminfit, kmaxfit specify fit range IN UNITS OF THE FUNDAMENTAL MODE
// - kmaxortho specifies maximum k at which subtraction is performed (to
//             avoid extrapolating beyond range of fit)

void orthogonalize_analyticalfit(scalar_grid& O, scalar_grid& ref,
				 scalar_grid& Oorth,
				 const double kminfit=0.,
				 const double kmaxfit=0.,
				 const double kmaxortho=0.);
#endif

// apply above to operator_set:

// orthogonalize all operators in oset w.r.t to operator with name refname
// - refname assumed to be contained in oset
// - is not orthogonalized w.r.t itself obviously
  void orthogonalize_table(operatorSet& oset, const std::string& refname = "delta");

#ifdef ROOT_AVAILABLE
// orthogonalize all operators in oset w.r.t to operator with name refname
// - refname assumed to be contained in oset
// - is not orthogonalized w.r.t itself obviously
// - automatically adds corresponding higher-derivative fields to
//   operator_set, if not already present
void orthogonalize_analyticalfit(operatorSet& oset,
				 const std::string& refname = "delta", 
				 const double kminfit=0.,
				 const double kmaxfit=0.,
				 const double kmaxortho=0.,
				 const bool add_hderiv_operators=true);
#endif

} // end of namespace LEFTfield

#endif
