/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <math.h>
#include <map>
#include <assert.h>
#include <stdlib.h>
#include <fftw3.h>
#include <unistd.h>
#include <numeric>
#ifdef _OPENMP
#include <omp.h>
#endif
#include "HDF5_IO.h"
#include "powerSpectrum.h"
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"

namespace LEFTfield {

static std::vector<cmplx> fft_tmp;

static fftw_plan get_plan(size_t ng, bool fwd)
  {
  static bool fftw_initialized=false;
  LogContext lc("get_plan");
  if (!fftw_initialized)
    {
    fftw_initialized=true;
#ifdef FFTW_THREADS
#ifdef _OPENMP
    fftw_init_threads();
    fftw_plan_with_nthreads(omp_get_max_threads());
#endif
#endif
    }
  static bool wisdom_loaded=false;
  if (!wisdom_loaded)
    {
    fftw_import_wisdom_from_filename("wisd");
    wisdom_loaded = true;
    }
  static std::map<size_t, fftw_plan> plandict_fwd, plandict_bwd;
  auto &plandict(fwd ? plandict_fwd : plandict_bwd);
  auto it = plandict.find(ng);
  if (it!=plandict.end()) return it->second;

  std::vector<cmplx> tmpc(ng*ng*(ng/2+1));
  std::vector<double> tmpr(ng*ng*ng);
  logger.printf(DEBUG, "Planning %zu %s.", ng, fwd ? "forward" : "backward");
  fftw_plan plan;
  if (fwd)
    {
    int ing = int(ng);
    int ingc = ing/2+1;
    fftw_iodim dims[3] = {{ing,ing*ing,ing*ingc},{ing,ing,ingc},{ing,1,1}};
    plan = fftw_plan_guru_dft_r2c(3, dims, 0, nullptr,
      tmpr.data(), (fftw_complex *)tmpc.data(), FFTW_MEASURE|FFTW_UNALIGNED);
    }
  else
    {
    int ing = int(ng);
    int ingc = ing/2+1;
    fftw_iodim dims[3] = {{ing,ing*ingc,ing*ing},{ing,ingc,ing},{ing,1,1}};
    plan = fftw_plan_guru_dft_c2r(3, dims, 0, nullptr,
      (fftw_complex *)tmpc.data(), tmpr.data(), FFTW_MEASURE|FFTW_UNALIGNED);
    }
  plandict[ng] = plan;
  logger.printf(DEBUG, "Done.", ng);

  fftw_export_wisdom_to_filename("wisd");
  return plan;
  }

// FFT
// - allocate plans only once needed

scalar_grid& scalar_grid::go_fourier()
{
  if (real)  {
    size_t NGH = NG/2+1;
    if (fft_tmp.size()<NG*NG*NGH)
      fft_tmp.resize(NG*NG*NGH);
    auto plan = get_plan(NG, true);
    fftw_execute_dft_r2c(plan, rhog.data(), (fftw_complex *)fft_tmp.data());
#pragma omp parallel for schedule(dynamic,1)
    for (st i=0; i<NG; ++i) {
      auto ic = (i==0) ? 0 : NG-i;
      for (st j=0; j<NG; ++j) {
        auto jc = (j==0) ? 0 : NG-j;
        for (st k=0; k<=NG/2; ++k)
          {
          auto kc = (k==0) ? 0 : NG-k;
          auto idx = i*NG2 + j*NG + k;
          auto idxc = ic*NG2 + jc*NG + kc;
          hartley_store(fft_tmp[k+NGH*j+NGH*NG*i], (*this)[idx], (*this)[idxc]);
          }
        }
      }
    real = false;
  }

  return *this;
}

scalar_grid& scalar_grid::go_real()
{
  if (!real)  {
    size_t NGH = NG/2+1;
    if (fft_tmp.size()<NG*NG*NGH)
      fft_tmp.resize(NG*NG*NGH);
    double norm = 1./double(NG*NG2);
#pragma omp parallel for schedule(dynamic,1)
    for (st i=0; i<NG; ++i)
      {
      auto ic = (i==0) ? 0 : NG-i;
      for (st j=0; j<NG; ++j) {
        auto jc = (j==0) ? 0 : NG-j;
        for (st k=0; k<=NG/2; ++k)
          {
          auto kc = (k==0) ? 0 : NG-k;
          auto idx = i*NG2 + j*NG + k;
          auto idxc = ic*NG2 + jc*NG + kc;
          fft_tmp[k+NGH*j+NGH*NG*i] = norm*hartley_load((*this)[idx], (*this)[idxc]);
          }
        }
      }
    auto plan = get_plan(NG, false);
    fftw_execute_dft_c2r(plan, (fftw_complex *)fft_tmp.data(), rhog.data());
    real = true;
  }

  return *this;
}


// operations

// subtract mean to ensure <grid> = 0
void scalar_grid::subtract_mean()
{
  if (real) { // subtract directly
    *this -= get_mean();
  } else { // if in Fourier space, remove k=0 mode
    rhog[0] = 0.;
  }
}


// apply Laplacian in Fourier space (note: derivatives refer to grid::Lbox)
scalar_grid& scalar_grid::Laplace()
{
  go_fourier();
  iterateFourierHalfParallel([this](st idx, st idxc, double, double, double, double K2)
    {
    rhog[idx]*=-K2;
    if (idxc!=idx)
      rhog[idxc]*=-K2;
    });
  return *this;
}

// apply inverse Laplacian in Fourier space (note: derivatives refer to grid::Lbox)
scalar_grid& scalar_grid::inverseLaplace()
{
  go_fourier();
  iterateFourierHalfParallel([this](st idx, st idxc, double, double, double, double K2)
    {
    rhog[idx] = (K2!=0) ? rhog[idx]/-K2 : 0;
    if (idxc!=idx)
      rhog[idxc] = (K2!=0) ? rhog[idxc]/-K2 : 0;
    });
  return *this;
}


// smooth field (optionally take out CIC window function)
// - R is in same units as Lbox
void scalar_grid::smooth_Gaussian(const double R, bool divide_CIC_window)
{
  NT_assert(!divide_CIC_window, "scalar_grid::smooth_Gaussian: ignoring flag divide_CIC_window (not yet implemented)");

  go_fourier();
  iterateFourierHalfParallel([this, R](st idx, st idxc, double, double, double, double K2)
    {
    rhog[idx] *= exp(-0.5*K2*R*R);
    if (idxc!=idx)
      rhog[idxc] *= exp(-0.5*K2*R*R);
    });
}


// returns CIC-interpolated value at x,y,z
// - grid has to be in real space
// - real value returned, obviously
// - trilinear interpolation if set, otherwise NGP
double scalar_grid::interpolate_to(const double xp, const double yp, const double zp, const bool trilinear) const
{
  NT_assert(is_real(), "ERROR: scalar_grid::interpolate_to requires grid in real space.");

  double xNG=1./NG;

  if (!trilinear)  { // do NGP
    double tmpi = (xp+0.5)*xNG+100;
    int i = int((tmpi-int(tmpi))*NG);
    double tmpj = (yp+0.5)*xNG+100;
    int j = int((tmpj-int(tmpj))*NG);
    double tmpk = (zp+0.5)*xNG+100;
    int k = int((tmpk-int(tmpk))*NG);
    return rhog[st(i)*NG2 + st(j)*NG + st(k)];
  }

  double x = ((xp*xNG)+100); x -= int(x); x*=NG;
  double y = ((yp*xNG)+100); y -= int(y); y*=NG;
  double z = ((zp*xNG)+100); z -= int(z); z*=NG;

  // do CIC
  st i = st(x), j = st(y), k = st(z);
  double dx = x-double(i), dy = y-double(j), dz = z-double(k);
  double tx = 1.-dx, ty = 1.-dy, tz = 1.-dz;

  double val=0;
  if ((i+1<NG) && (j+1<NG) && (k+1<NG)) // normal case, where we can take shortcuts
    {
    auto idx0 = i*NG2+j*NG+k;
    val = tx*(ty*(rhog[idx0] * tz +
                  rhog[idx0+1] * dz) +
              dy*(rhog[idx0+NG] * tz +
                  rhog[idx0+NG+1] * dz)) +
          dx*(ty*(rhog[idx0+NG2] * tz +
                  rhog[idx0+NG2+1] * dz) +
              dy*(rhog[idx0+NG2+NG] * tz +
                  rhog[idx0+NG2+NG+1] * dz));
    }
  else
    {
    st ip = (i+1)%NG, jp = (j+1)%NG, kp = (k+1)%NG;

    val += rhog[i*NG2+j*NG+k] * tx * ty * tz;
    val += rhog[i*NG2+j*NG+kp] * tx * ty * dz;
    val += rhog[i*NG2+jp*NG+k] * tx * dy * tz;
    val += rhog[i*NG2+jp*NG+kp] * tx * dy * dz;
    val += rhog[ip*NG2+j*NG+k] * dx * ty * tz;
    val += rhog[ip*NG2+j*NG+kp] * dx * ty * dz;
    val += rhog[ip*NG2+jp*NG+k] * dx * dy * tz;
    val += rhog[ip*NG2+jp*NG+kp] * dx * dy * dz;
    }
  return val;
}



// returns gradient of CIC-interpolation in direction I=0,1,2
double scalar_grid::interpolate_to_gradient(const double xp, const double yp,
					    const double zp, const st I,
					    const bool trilinear) const
{
  NT_assert(is_real(), "ERROR: scalar_grid::interpolate_to requires grid in real space.");

  double xNG=1./NG;

  if (!trilinear)  { // do NGP
    NT_fail("scalar_grid::interpolate_to_gradient: gradient of NGP not implemented.");
    double tmpi = (xp+0.5)*xNG+100;
    int i = int((tmpi-int(tmpi))*NG);
    double tmpj = (yp+0.5)*xNG+100;
    int j = int((tmpj-int(tmpj))*NG);
    double tmpk = (zp+0.5)*xNG+100;
    int k = int((tmpk-int(tmpk))*NG);
    return rhog[st(i)*NG2 + st(j)*NG + st(k)];
  }

  double x = ((xp*xNG)+100); x -= int(x); x*=NG;
  double y = ((yp*xNG)+100); y -= int(y); y*=NG;
  double z = ((zp*xNG)+100); z -= int(z); z*=NG;

  // do CIC
  st i = st(x), j = st(y), k = st(z);
  double dx = x-double(i), dy = y-double(j), dz = z-double(k);
  double tx = 1.-dx, ty = 1.-dy, tz = 1.-dz;
  switch (I) { // gradient in direction I
  case 0: {
    dx = 1.; tx = -1.;
    break; }
  case 1: {
    dy = 1.; ty = -1.;
    break;  }
  case 2: {
    dz = 1.; tz = -1.;
    break;  }
  default:
    NT_fail("scalar_grid::interpolate_to_gradient: invalid index I = ", I);
  }
  
  double val=0.;
  if ((i+1<NG) && (j+1<NG) && (k+1<NG)) // normal case, where we can take shortcuts
    {
    auto idx0 = i*NG2+j*NG+k;
    val = tx*(ty*(rhog[idx0] * tz +
                  rhog[idx0+1] * dz) +
              dy*(rhog[idx0+NG] * tz +
                  rhog[idx0+NG+1] * dz)) +
          dx*(ty*(rhog[idx0+NG2] * tz +
                  rhog[idx0+NG2+1] * dz) +
              dy*(rhog[idx0+NG2+NG] * tz +
                  rhog[idx0+NG2+NG+1] * dz));
    }
  else
    {
    st ip = (i+1)%NG, jp = (j+1)%NG, kp = (k+1)%NG;

    val += rhog[i*NG2+j*NG+k] * tx * ty * tz;
    val += rhog[i*NG2+j*NG+kp] * tx * ty * dz;
    val += rhog[i*NG2+jp*NG+k] * tx * dy * tz;
    val += rhog[i*NG2+jp*NG+kp] * tx * dy * dz;
    val += rhog[ip*NG2+j*NG+k] * dx * ty * tz;
    val += rhog[ip*NG2+j*NG+kp] * dx * ty * dz;
    val += rhog[ip*NG2+jp*NG+k] * dx * dy * tz;
    val += rhog[ip*NG2+jp*NG+kp] * dx * dy * dz;
    }
  return val;
}

// adds CIC-interpolated given (real) scalar value to x,y,z
// - grid has to be in real space
void scalar_grid::interpolate_add(const double val, const double xp, const double yp, const double zp)
{
  NT_assert(is_real(), "ERROR: scalar_grid::interpolate_add requires grid in real space.");

  // wrap around
  double x = fmod(xp, NG);
  if (x < 0.) x += double(NG);
  double y = fmod(yp, NG);
  if (y < 0.) y += double(NG);
  double z = fmod(zp, NG);
  if (z < 0.) z += double(NG);

  // do CIC
  st i = st(x), j = st(y), k = st(z);
  double dx = x-double(i), dy = y-double(j), dz = z-double(k);
  double tx = 1.-dx, ty = 1.-dy, tz = 1.-dz;
  st ip = (i+1)%NG, jp = (j+1)%NG, kp = (k+1)%NG;

  rhog[i*NG2+j*NG+k] += val * tx * ty * tz;
  rhog[i*NG2+j*NG+kp] += val * tx * ty * dz;
  rhog[i*NG2+jp*NG+k] += val * tx * dy * tz;
  rhog[i*NG2+jp*NG+kp] += val * tx * dy * dz;
  rhog[ip*NG2+j*NG+k] += val * dx * ty * tz;
  rhog[ip*NG2+j*NG+kp] += val * dx * ty * dz;
  rhog[ip*NG2+jp*NG+k] += val * dx * dy * tz;
  rhog[ip*NG2+jp*NG+kp] += val * dx * dy * dz;
}


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------

void scalar_grid::resizeFourier(scalar_grid &res) const
{
  NT_assert(!is_real(), "scalar_grid::resizeFourier: need to be in Fourier space!");
  
  auto NGnew = res.NG;
  NT_assert(NGnew > 0, "scalar_grid::resizeFourier: refusing to resize to grid size 0.");
  // short cut if the size dos not change 
  if (NG == NGnew) {
    res = *this;
    return;
  }
  logger.printf("scalar_grid::resizeFourier", WHATAMIDOING,
		"Resizing %zu -> %zu in Fourier space.", NG, NGnew);

  int lim = std::min(NGnew/2, NG/2); // CAUTION: we are assuming even NGs throughout this function!
  res.fill(0);
  res.set_real(false);
  // renormalization factor
  double F = pow(double(NGnew)/double(NG), 3.);
#pragma omp parallel for schedule(static)
  for (int i=-lim; i<lim; ++i) {
    int ii = (i<0) ? NG+i : i;
    int oi = (i<0) ? NGnew+i : i;
    for (int j=-lim; j<lim; ++j) {
      int ij = (j<0) ? NG+j : j;
      int oj = (j<0) ? NGnew+j : j;
      for (int k=-lim; k<lim; ++k) {
        int ik = (k<0) ? NG+k : k;
        int ok = (k<0) ? NGnew+k : k;
        res(oi,oj,ok) = F*(*this)(ii,ij,ik);
      }
    }
  }
}

// return Fourier-space grid of size NGd, for all modes
// with |k| <= kcut_phys, where kcut_phys refers to grid::Lbox
// (box size, i.e. fundamental modes of both grids are assumed to be the same)
// - no cut if kcut=0.
// - returns number of Fourier modes that remain after cut in Nmodes
void scalar_grid::copysharpk(scalar_grid& dest, const double kcut_phys,
			     const sharpkFilterType ftype,
			     st *N_modes) const
{
  LogContext lc("scalar_grid::copysharpk");
  resizeFourier(dest);
  st nmodes = dest.cutsharpk(kcut_phys, ftype);
  if (N_modes) *N_modes = nmodes;
}


// - set to zero all Fourier modes in grid above kcut_phys,
//   where kcut_phys refers to grid::Lbox
// - returns number of Fourier modes that remain after cut
// - does nothing if kcut_phys==0.
// - Hermitianity is ensured
st scalar_grid::cutsharpk(const double kcut_phys,
			  const sharpkFilterType ftype)
{
  if (kcut_phys==0.) // no cut requested
    return NG2*NG;
  // kcut in units of fundamental mode
  auto kcut = kcut_phys * grid::get_Lbox() / (2.*M_PI);

  if (( ftype == CUBE ) && ( kcut >= double(NG)/2. )) {
    // kcut is beyond Nyquist, so print a warning
    logger.printf("scalar_grid::cutsharpk", ESSENTIAL,
		  "WARNING: NG = %zu < 2kcut = %.1f. Nothing to cut.",
		  NG, 2.*kcut);
    return NG2*NG;
  }

  // k cut value in code units
  const double kcc = kcut, kcc2 = kcc*kcc;
  st Nmodes = 0; // number of modes after cut

  go_fourier();
#pragma omp parallel default(shared)
  {
#pragma omp for reduction(+:Nmodes)
  for (st i=0; i<NG; ++i) {
    int im = (i < NG/2 ? i: i-NG);
    double kxq = im;
    for (st j=0; j<NG; ++j) {
      int jm = (j < NG/2 ? j: j-NG);
      double kyq = jm;
      for (st k=0; k<NG; ++k) {
	int km = (k < NG/2 ? k: k-NG);
	double kzq = km;

	// apply sharp-k filter
	if ( ftype == CUBE )  { // cubic
	  // if the band limit is integer, we need to use this apparently
	  // asymmetric criterion. FFT does this as well.
	  if ((kxq<-kcc) || (kxq>=kcc) || (kyq<-kcc) || (kyq>=kcc) || (kzq<-kcc) || (kzq>=kcc)) {
	    operator()(i,j,k) = 0;
	    continue;
	  }
	} else { // spherical
	  double K2 = kxq*kxq + kyq*kyq + kzq*kzq;
	  if (! (K2 <= kcc2))  {
	    operator()(i,j,k) = 0;
	    continue;
	  }
	}

	// arrived here means mode below cut
	Nmodes++;
      }
    }
  }
  }

  return Nmodes;
}


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------


// power spectra
template<typename Func> void scalar_grid::iteratePS(powerSpectrumBase& Pk,
                            Func func) const {
  // fundamental frequency unit
  // note: to have k in units of k_fund, set grid::Lbox to 2pi
  const double kw = 2.*M_PI / grid::Lbox;

#pragma omp parallel
{
  powerSpectrumBase Pkloc(Pk);
  Pkloc.zero();
#pragma omp for schedule(dynamic,1)
  for (st i=0; i<=NG/2; ++i) {
    st ic = (i==0) ? 0: NG-i;
    int im = (i < NG/2 ? i: i-NG);
    double kxq = kw * im;
    for (st j=0; j<NG; ++j) {
      st jc = (j==0) ? 0: NG-j;
      int jm = (j < NG/2 ? j: j-NG);
      double kyq = kw * jm;
      for (st k=0; k<NG; ++k) {
        st kc = (k==0) ? 0: NG-k;
        st idx = i*NG2 + j*NG + k;
        st idxc = ic*NG2 + jc*NG + kc;
        if (idx<=idxc) {
          int km = (k < NG/2 ? k: k-NG);
          double kzq = kw * km;
          double K2 = kxq*kxq + kyq*kyq + kzq*kzq;
          func(Pkloc, idx, idxc, kxq, kyq, kzq, K2);
        }
      }
    }
  }
#pragma omp critical
  Pk.add(Pkloc);
}
  }

void scalar_grid::auto_powerspectrum(powerSpectrumBase& Pk)
{
  const double vf = pow(double(NG), -6.); // P(k) normalization
  const double kNycom = 0.5 * get_kNy(); // k_Ny/2 in units of grid::Lbox

  // info
  LogContext lc("scalar_grid::auto_powerspectrum");
  logger.printf(INFO, "Lbox = %.2f, k_Ny/2 = %.3e h/Mpc",
    grid::get_Lbox(), kNycom);

  go_fourier();
  iteratePS(Pk, [this, vf](powerSpectrumBase &Pkloc, st idx, st idxc, double, double, double, double K2)
    {
    double K = sqrt(K2);
    cmplx val= hartley_load(rhog[idx], rhog[idxc]);
    st weight = (idx==idxc) ? 1 : 2;
    Pkloc.fill(K, vf*std::norm(val), weight);
    });
}


void scalar_grid::cross_powerspectrum(scalar_grid& grid,
              powerSpectrumBase& Pk)
{
  const double vf = pow(double(NG), -6.); // P(k) normalization
  const double kNycom = 0.5 * get_kNy(); // k_Ny/2 in units of grid::Lbox

  // info
  LogContext lc("scalar_grid::cross_powerspectrum");
  logger.printf(INFO, "Lbox = %.2f, k_Ny/2 = %.3e h/Mpc",
    grid::get_Lbox(), kNycom);

  go_fourier();
  grid.go_fourier();
  NT_assert(compatible(grid), "grid mismatch");
  iteratePS(Pk, [this, &grid, vf](powerSpectrumBase &Pkloc, st idx, st idxc, double, double, double, double K2)
    {
    double K = sqrt(K2);
    cmplx v1 = hartley_load(rhog[idx],rhog[idxc]),
          v2 = hartley_load(grid[idx],grid[idxc]);
    st weight = (idx==idxc) ? 1 : 2;
    Pkloc.fill(K, vf*(v1.real()*v2.real() + v1.imag()*v2.imag()), weight);
    });
}

double scalar_grid::get_mean() const
{
  if (!real) // in Fourier space, simply return scaled 0-mode
    return rhog[0] / double(NG2*NG);
  
  double R=0.;
#pragma omp parallel if (NG > 64) default(shared)
  {
#pragma omp for reduction(+:R)
  for (st i=0; i < NG2*NG; i++)  {
    R += rhog[i];
  }
  }
  R /= double(NG2*NG);

  return R;
}


double scalar_grid::get_variance(double *mean) const
{
  NT_assert(is_real(), "ERROR in get_variance: need to be in real space.");

  double R=0.;
#pragma omp parallel if (NG > 64) default(shared)
  {
#pragma omp for reduction(+:R)
  for (st i=0; i < NG2*NG; i++)  {
    R += rhog[i]*rhog[i];
  }
  }
  double m = get_mean();
  double v( R / double(NG2*NG) - m*m);
  if (mean) *mean = m;

  return v;
}

void scalar_grid::print_stats(FILE *fp)
{
  bool swtch = false;
  if (! is_real())  { // go to real space if necessary
    go_real();
    swtch = true;
  }
  double m, v = get_variance(&m);
  fprintf(fp, "scalar_grid: mean = %.6e  (NG = %zu)\n", m, NG);
  fprintf(fp, "          sqrt(V) = %.6e\n", sqrt(fabs(v)));
  if (swtch) go_fourier(); // go back to Fourier space if we were there before
}

// scalar product
double scalar_grid::dot(const scalar_grid& g) const
{
  assert_compatible(g, "dot");
  if (real)
    return accumulateParallel([&](size_t idx) {
				return rhog[idx] * g[idx];} );
    
  return accumulateFourierParallel( [&](st idx, st idxc,
					double, double, double, double){
      st fct = (idxc>idx) ? 2 : 1;
      cmplx ab1 = hartley_load((*this)[idx], (*this)[idxc]);
      cmplx ab2 = hartley_load(g[idx], g[idxc]);      
      return fct * (ab1.real()*ab2.real() + ab1.imag()*ab2.imag());
				    });
}

// compute sum of squares
// - if in Fourier space, divides by NG^3
double scalar_grid::get_sumsquares() const
{
  double sumsq = 0.;
#pragma omp parallel for reduction(+:sumsq)
  for (st i=0; i<rhog.size(); ++i)
    sumsq += std::norm(rhog[i]);

  // if in Fourier space, divide by NG^3
  if (!is_real())
    sumsq /= double(NG2*NG);

  return sumsq;
}


// real-space cross-correlation coefficient
// - returns < (*this) g >_x /( < (*this)^2 >_x < g^2 >_x )^1/2 ,
//   after means have been subtraced
double scalar_grid::crosscorrelation_coefficient(const scalar_grid& g) const
{
  // idea: works in both Fourier and real space, and returns same number
  double num = dot(g); // asserts compatibility in space and NG
  if (!is_real()) 
    num /= double(NG2*NG);

  return num / sqrt( get_sumsquares() * g.get_sumsquares() );
}

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------

// read in density field in ASCII format
void scalar_grid::read_density_from_text(const std::string &file)
{
  std::ifstream densityFile(file);
  NT_assert(densityFile.is_open(), "scalar_grid::read_density_from_text: couldn't open density file '%s'.", file.c_str());

  st cell = 0;
  std::string line;
  while (std::getline(densityFile, line)) {
    std::istringstream iss(line);
    float density;
    iss >> density;
    rhog[cell] = density;
    ++cell;
  }
  assert(cell == (NG2*NG));
  logger.printf("scalar_grid::read_density_from_text", INFO,
    "Density was successfully load onto a %lux%lux%lu grid.", NG, NG, NG);
}

#if 0
// copy grid contents from given array of real DFT
// - assumes realDFTdata is array of dimensions NG*NG*(NG/2+1) !
// - WARNING: this function is still in debugging stage.
void scalar_grid::copyDFTfourier(const fftw_complex *realDFTdata)
{
  // print warning since still not sure this is fully functional
  logger.printf("scalar_grid::copyDFTfourier", ESSENTIAL,
    "WARNING: this function is still in debugging stage.");

  fill(0.);
  st NGp2 = NG/2 + 1; // number of elements in lowest direction in real DFT grid

  // run over real-DFT grid
  for (st i=0; i<NG; ++i) {
    int im = (i < NG/2 ? i: i-NG);
    for (st j=0; j<NG; ++j) {
      int jm = (j < NG/2 ? j: j-NG);
      for (st k=0; k <= NG/2; ++k) {
  st ind = i*NG*NGp2 + j*NGp2 + k;
  double ak = realDFTdata[ind][0], bk = realDFTdata[ind][1];

  // \vec{k} index from DFT grid index: im,jm,k
  // - invert to obtain complex FFT index
  st id = im >= 0 ? im : NG + im;
  st jd = jm >= 0 ? jm : NG + jm;
  // k trivially >= 0
  st kd = k ; //k >= 0 ? k : NG + k;
  rhog[id*NG2 + jd*NG + kd] = cmplx(ak, bk);

  // assign complex conjugate to -k
  im = -im;
  jm = -jm;
  st idM = im >= 0 ? im : NG + im;
  st jdM = jm >= 0 ? jm : NG + jm;
  // -k is <= 0
  st kdM = k == 0 ? 0 : NG - k;
  if (idM == id && jdM == jd && kdM == kd)  {
    fprintf(stderr, "self-conjugate mode (%zu/%zu/%zu)\n", id,jd,kd);
    rhog[id*NG2 + jd*NG + kd].imag(0.);
  }
  rhog[idM*NG2 + jdM*NG + kdM] = cmplx(ak, -bk);
      }
    }
  }

  fprintf(stderr, "copyDFTfourier: k=0 mode: %.5e + i %.5e\n", rhog[0].real(), rhog[0].imag());
  fprintf(stderr, "copyDFTfourier: next mode: %.5e + i %.5e\n", rhog[1].real(), rhog[1].imag());
}
#endif


// interpolate from given coarser grid (NGP)
void scalar_grid::interpolateNGP(const scalar_grid& grid)
{
  // check reality
  NT_assert(real && grid.real, "scalar_grid::interpolateNGP: need grids in real space.");

  // make sure given grid is coarser
  NT_assert(grid.NG <= NG, "scalar_grid::interpolateNGP: supplies grid resolution (%zu) is not lower than present grid (%zu).", grid.NG, NG);

  // make sure grids are commensurate
  NT_assert(fmod(double(NG), double(grid.NG)) == 0.,
      "scalar_grid::interpolateNGP: incommensurate grid resolutions (%zu, %zu).", grid.NG, NG);

  // if grid sizes same, simply copy
  if (grid.NG == NG) {
    *this = grid;
    return;
  }

  // now interpolate
  fill(0.);
  const st fac = NG / grid.NG;
  const st gNG = grid.NG, gNG2 = gNG*gNG;

  // run over fine grid, NGP interpolation
  for (st i=0; i<NG; ++i) {
    st I = i/fac;
    for (st j=0; j<NG; ++j) {
      st J = j/fac;
      for (st k=0; k<NG; ++k) {
  st K = k/fac;

  rhog[i*NG2 + j*NG + k] = grid.rhog[I*gNG2 + J*gNG + K];
      }
    }
  }

}



// file I/O

void scalar_grid::write_nbtl_s(std::ostream &file) const
{
  // write grid: 2 * NG**3 doubles
  std::vector<cmplx> tmp(NG*NG*NG);
  if (real)
    for (size_t i=0; i<tmp.size(); ++i)
      tmp[i] = rhog[i];
  else
    iterateFourierHalfParallel([this,&tmp](st idx, st idxc, double, double, double, double)
      {
      auto val = hartley_load(rhog[idx],rhog[idxc]);
      tmp[idx] = val;
      tmp[idxc] = std::conj(val);
      });
  file.write((const char*) tmp.data(), 2*NG2*NG * sizeof(double));
}

void scalar_grid::read_nbtl_s(std::istream &file)
{
  // read grid: 2 * NG**3 doubles
  std::vector<cmplx> tmp(NG*NG*NG);
  file.read((char*) tmp.data(), 2*NG2*NG * sizeof(double));
  if (real)
    for (size_t i=0; i<tmp.size(); ++i)
      rhog[i] = tmp[i].real();
  else
    iterateFourierHalfParallel([this,&tmp](st idx, st idxc, double, double, double, double)
      {
      hartley_store(tmp[idx],rhog[idx],rhog[idxc]);
      });
}



// read in real slab from raw binary file (e.g. density output generated
// from ic_2lptngnonlocal)
// - file is assumed to contain raw binary real grid with field size 'fsize'
// - reads fsize * NG^2 * Nslab bytes
void scalar_grid::read_nbtl_slab_raw(const std::string &file, const st Nslab,
        const st istart,
        const st fsize)
{
  NT_assert(fsize == sizeof(double),
      "scalar_grid::read_nbtl_slab_raw: unsupported field size %zu.", fsize);

  // in case of weird use...
  NT_assert(is_real(), "scalar_grid::read_nbtl_slab_raw: grid needs to be in real space (and erased before first assignment).");

  std::ifstream ifs(file, std::ifstream::binary);
  NT_assert(ifs.is_open(), "scalar_grid::read_nbtl_slab_raw: cannot open input file '%s'.", file.c_str());

  // allocate buffer
  logger.printf("scalar_grid::read_nbtl_slab_raw", INFO, "Allocating slab of %zu * %zu^2.", Nslab, NG);
  std::vector<double> buf(Nslab*NG2);

  // read slab
  ifs.read((char*) buf.data(), Nslab*NG2 * sizeof(double));
  NT_assert(ifs, "scalar_grid::read_nbtl_slab_raw: error reading from input file '%s'.", file.c_str());
  ifs.close();

  // if first slab, erase and swith to real space
  if (!istart)  {
    fill(0.);
    real = true;
  }

  // copy to relevant part of grid
  st I=0;
  for (st i=istart; i < istart+Nslab; ++i) {
    for (st j=0; j<NG; ++j) {
      for (st k=0; k<NG; ++k) {
  rhog[i*NG2 + j*NG + k] = buf[I++];
      }
    }
  }
}


// read in grid (NG^3 doubles) from given HDF5 file and data set name
void scalar_grid::read_HDF5(const std::string& filename,
                            const std::string& data_set_name)
{
  logger.printf("scalar_grid::read_HDF5", DEBUG, "Attempting to read field '%s' from file '%s'.",
		data_set_name.c_str(), filename.c_str());
  std::vector<st> dims;
  read_HDF5_dataset(filename, data_set_name, rhog, dims);
  resize(dims[0]);
  NT_assert((dims[1]==NG) && (dims[2]==NG),
    "all 3 dimensions must be equal");
  std::string space = "real";
  if (HDF5_attribute_present(filename, data_set_name, "space"))
    space = read_HDF5_attribute<std::string>(filename, data_set_name, "space");
  else
    logger.printf("scalar_grid::read_HDF5", ESSENTIAL, "'space' attribute not found. Assuming 'real'...");
  NT_assert((space=="real")||(space=="fourier"), "invalid domain type");
  set_real(space=="real");
}

// write grid (NG^3 doubles) to given HDF5 file and data set name
void scalar_grid::write_HDF5(const std::string& filename,
                             const std::string& data_set_name) const
{
  logger.printf("scalar_grid::write_HDF5", DEBUG, "Attempting to write field '%s' to file '%s'.",
		data_set_name.c_str(), filename.c_str());
  std::vector<st> dims{NG, NG, NG};
  write_HDF5_dataset(filename, data_set_name, rhog, dims);
  write_HDF5_attribute<std::string>(filename, data_set_name, "space", real ? "real" : "fourier");
}

} // end of namespace LEFTfield
