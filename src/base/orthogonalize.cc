/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */


// ********************************************************************
// orthogonalize
// - functions to orthogonalize (~renormalize) fields with respect
//   to each other
// - options based on analytical fit (terms can be absorbed into higher-
//   derivative operators) or table interpolation
//
// (C) Fabian Schmidt, 2019-2021
// ********************************************************************

#include "orthogonalize.h"
#include "utilities/table.h"

namespace LEFTfield {

// ******************************************************************
// orthogonalize given operator 'O' with respect to other operator 'ref',
// returning the result in given grid 'Oorth'
// - O, ref are non-constant, as power spectra are measured (FT if necessary)
// - non-in-place operation allows for parallelization

// - to enable debug output:
// #define WRITE_DEBUG_ORTHO

// Option 1: table interpolation
// - this works numerically precisely, but corresponds to adding nonlocal
//   terms in real space (i.e. not analytic in k^2)

void orthogonalize_table(scalar_grid& O, scalar_grid& ref,
			 scalar_grid& Oorth)
{
  assert(O.getNG() == ref.getNG());
  assert(O.getNG() == Oorth.getNG());

  O.go_fourier();
  ref.go_fourier();
  
  // // fictitious number for Lbox; drops out of all quantities
  // // - use true number to make debug output easier to interpret
  // const double Lbox=2000.;

  // linear bins with dk = k_f
  const st NG = ref.getNG();
  const double kf = grid::get_kfun(), kNy = ref.get_kNy();
  powerSpectrumLin Pc(kf, kNy, 2*NG);
  powerSpectrumLin Pref(kf, kNy, 2*NG);
  
  // measure power spectra
  O.go_fourier();
  ref.go_fourier();
  O.cross_powerspectrum(ref, Pc);
  ref.auto_powerspectrum(Pref);

  {
  // lin-lin interpolation
  auto tc = Pc.get_table(false, false, false);
  auto tref = Pref.get_table(false, false, false);
  // - have them set up arrays before entering into parallel loop
  tc->interpSpline(2.*kf);
  tref->interpSpline(2.*kf);

  // // conversion factor code units
  // const double kw = 2.*M_PI / double(NG); // fund. freq. in code units
  // const double bf = double(NG) / Lbox;

  logger.printf("orthogonalize_table", DEBUG, "Orthogonalizing operators...");
  Oorth.fill(0.);
  Oorth.set_real(false);
  Oorth.iterateFourierHalfParallel([O, tc, tref, ref, &Oorth](st idx, st idxc, double, double, double, double K2)
    {
    double K = sqrt(K2);
    double Sc = tc->interpSpline(K), Sref = tref->interpSpline(K);
    // skip if no reference power
    if (! (Sref > 0.)) return;
    Sc /= Sref;  // this is < delta O > / < delta delta >
    // orthogonalize
    auto oval = hartley_load (O[idx], O[idxc]);
    auto refval = hartley_load (ref[idx], ref[idxc]);
    hartley_store(oval - Sc*refval, Oorth[idx], Oorth[idxc]);
    });
  logger.printf("orthogonalize_table", DEBUG, "...done.");
  }

#ifdef WRITE_DEBUG_ORTHO
  // get cross power after renormalization, and write everything to file
  powerSpectrumLin Ptest(kf, kNy, 2*NG);
  Oorth.cross_powerspectrum(ref, Ptest);

  FILE *fp = fopen("Pk_orthogonalize_table.dat", "a");
  Pref.print(fp);
  fprintf(fp, "\n");
  Pc.print(fp);
  fprintf(fp, "\n");
  Ptest.print(fp);
  fprintf(fp, "\n");
  fclose(fp);
#endif
}

#ifdef ROOT_AVAILABLE

// Option 2: polynomial fit in k^2
// - avoids adding nonlocal terms that are not absorbed by higher-derivative
//   coefficients

} // end of namespace LEFTfield

#include <TGraphErrors.h>
#include <TF1.h>

namespace LEFTfield {

void orthogonalize_analyticalfit(scalar_grid& O, scalar_grid& ref,
				 scalar_grid& Oorth,
				 const double kminfit, const double kmaxfit,
				 const double kmaxortho)
{
  assert(O.getNG() == ref.getNG());
  assert(O.getNG() == Oorth.getNG());
  LogContext lc("orthogonalize_analyticalfit");
  
  O.go_fourier();
  ref.go_fourier();
  
  // linear bins with dk = k_f
  const st NG = ref.getNG();
  const double kf = grid::get_kfun(), kNy = ref.get_kNy();
  const st Nbin = 2*NG;
  powerSpectrumLin Pc(kf, kNy, Nbin);
  powerSpectrumLin Pref(kf, kNy, Nbin);
  
  // measure power spectra
  O.go_fourier();
  ref.go_fourier();
  O.cross_powerspectrum(ref, Pc);
  ref.auto_powerspectrum(Pref);
  logger.printf(DEBUG, "done with power spectra.");

  // collect data
  std::vector<double> X, Y, Yerr;
  const double kmax = kmaxfit > 0. ? kmaxfit : kNy;
  for (st i=0; i < Nbin; i++) {
    double k = Pc.get_k_center(i);
    if (k < kminfit || k >= kmax)  continue;
    double pr = Pref.get_normalized_Pk(i);
    if (! (pr > 0.) || Pref.get_Nmodes(i) == 0)  continue;

    // have valid point
    X.push_back(k);
    double pc = Pc.get_normalized_Pk(i);
    Y.push_back(pc/pr);
    Yerr.push_back(pc/pr / sqrt(Pref.get_Nmodes(i))); // rough error bar; ignores correlations
  }
  NT_assert(X.size() > 1, "not enough points to fit in orthogonalize_analyticalfit.");
  logger.printf(DEBUG, "have %zu points to fit; k in [%.3e, %.3e]",
		X.size(), X[0], X.back());

  {
  // fit ratio of power spectra  
  auto tg = make_sptr<TGraphErrors>(X.size(), X.data(), Y.data(), nullptr,
				      Yerr.data());
  auto fit = make_sptr<TF1>("fit", "[0] + [1]*x*x + [2]*x*x*x*x",
		     fmax(0., kminfit), kmax);
  fit->SetParameters(1., 1./kmax/kmax, pow(kmax, -4.));
  tg->Fit(fit.get(), "MNQ");
  // debug
  logger.printf(DEBUG, "fit parameters %.3e, %.3e, %.3e",
		fit->GetParameter(0), fit->GetParameter(1), fit->GetParameter(2));

  logger.printf(DEBUG, "Orthogonalizing operator...");
  Oorth.fill(0.);
  Oorth.set_real(false);
  Oorth.iterateFourierHalfParallel([O, ref, kmaxortho, fit, &Oorth](st idx, st idxc, double, double, double, double K2)
    {
    double K = sqrt(K2);
    double coeff = 0.;
    if (K < kmaxortho)
      coeff = fit->Eval(K);

    auto oval = hartley_load (O[idx], O[idxc]);
    auto refval = hartley_load (ref[idx], ref[idxc]);
    hartley_store(oval - coeff*refval, Oorth[idx], Oorth[idxc]);
    });
  logger.printf(DEBUG, "...done.");
  }

#ifdef WRITE_DEBUG_ORTHO
  // get cross power after renormalization, and write everything to file
  powerSpectrumLin Ptest(kf, kNy, Nbin);
  Oorth.cross_powerspectrum(ref, Ptest);

  FILE *fp = fopen("Pk_orthogonalize_analyticalfit.dat", "a");
  Pref.print(fp);
  fprintf(fp, "\n");
  Pc.print(fp);
  fprintf(fp, "\n");
  Ptest.print(fp);
  fprintf(fp, "\n");
  fclose(fp);
#endif
}

#endif


// apply above to operator_set:

// orthogonalize all operators in oset w.r.t to operator with name refname
// - refname assumed to be contained in oset
// - is not orthogonalized w.r.t itself obviously
void orthogonalize_table(operatorSet& oset, const std::string& refname)
{
  logger.printf("orthogonalize_table", ESSENTIAL,
		"WARNING in orthogonalize_table: expect minor shifts in sigma8 profile likelihood, as subtraction not guaranteed to be absorbed by bias terms.");

  auto ref = oset[refname].grd;

  for (auto& [name, op]: oset) {
    if (name == refname)  continue;

    logger.printf("orthogonalize_table", INFO,
		  "'%s'", name.c_str());
    auto OO = make_sptr<scalar_grid>(ref->getNG());
    orthogonalize_table(* op.grd, *ref, *OO);
    op = {OO, op.order};
  }
}

#ifdef ROOT_AVAILABLE

// orthogonalize all operators in oset w.r.t to operator with name refname
// - refname assumed to be contained in oset
// - is not orthogonalized w.r.t itself obviously
// - automatically adds corresponding higher-derivative fields to
//   operator_set, if not already present
void orthogonalize_analyticalfit(operatorSet& oset, const std::string& refname,
				 const double kminfit,
				 const double kmaxfit,
				 const double kmaxortho,
				 const bool add_hderiv_operators)
{
  LogContext lc("orthogonalize_analyticalfit(operator_set)");

  auto ref = oset[refname].grd;
  auto reforder = oset[refname].order;
  
  // 'lapl(refname)', 'lapl(lapl(refname))'
  std::string lname(std::string("lapl(") + std::string(refname)
		    + std::string(")"));
  std::string l2name(std::string("lapl(") + lname + std::string(")"));

  for (auto& [name, op]: oset) {
    // skip ref itself, and its derivatives
    if (name == refname || name == lname || name == l2name)
      continue;

    logger.printf(INFO, "'%s'", name.c_str());
    auto OO = make_sptr<scalar_grid>(ref->getNG());
    orthogonalize_analyticalfit(* op.grd, *ref, *OO,
				kminfit, kmaxfit, kmaxortho);
    op = {OO, op.order};
  }

  if (add_hderiv_operators)  {
    // now make sure that corresponding higher-derivative terms
    // 'lapl(ref)', 'lapl(lapl(ref))' are included
    if (oset.have(lname))  {
      // debug
      logger.printf(DEBUG, "have '%s'.", lname.c_str());
    } else {
      // add 'lapl(ref)'    
      auto laplref = make_sptr<scalar_grid>(*ref);
      laplref->go_fourier();
      laplref->Laplace();
      if (ref->is_real()) laplref->go_real();
      oset.add(lname, {laplref, reforder + op_order(0, 1)});
      logger.printf(INFO, "adding '%s' to operator_set.", lname.c_str());
    }
    
    if (oset.have(l2name))  {
      // debug
      logger.printf(DEBUG,  "have '%s'.", l2name.c_str());
    } else {
      // add 'lapl(lapl(ref))'    
      auto lapl2ref = make_sptr<scalar_grid>(*oset[lname].grd);
      lapl2ref->go_fourier();
      lapl2ref->Laplace();
      if (ref->is_real()) lapl2ref->go_real();
      oset.add(l2name, {lapl2ref, reforder + op_order(0, 2)});
      logger.printf(INFO, "adding '%s' to operator_set.", l2name.c_str());
    }
  }
}

#endif

} // end of namespace LEFTfield
