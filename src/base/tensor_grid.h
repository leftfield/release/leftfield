/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef TENSOR_GRID_H
#define TENSOR_GRID_H

#include <iostream>
#include <math.h>

#include "grid.h"
#include "scalar_grid.h"
#include "powerSpectrum.h"


namespace LEFTfield {

// implements trace-free tensor field on a grid
// - 6 complex arrays of size NG^3
//     0:  11
//     1:  22
//     2:  33
//     3:  12
//     4:  13
//     5:  23

class vector_grid;

class tensor_grid : public grid
{
 protected:
  scalar_grid kg[6];
  /* fftw_complex *kg[6]; */
  /* fftw_plan fftp_f[6], fftp_b[6]; */

  // array access by row and column, for convenience
  // - counting from 0
  // - protected for now, as using the pointers needs some care
  // - bounds check not implemented currently
  scalar_grid& operator()(int i, int j) {
    if (i == j)  return kg[i];   // 00, 11, 22
    if (i > j)  {
      const int d = j; j = i; i = d;
    }
    if (!i) // 01, 02
      return kg[j+2];
    // 12
    return kg[5];
  }
  const scalar_grid& operator()(int i, int j) const {
    if (i == j)  return kg[i];   // 00, 11, 22
    if (i > j)  {
      const int d = j; j = i; i = d;
    }
    if (!i) // 01, 02
      return kg[j+2];
    // 12
    return kg[5];
  }
  
 public:
  // default constructor
  // - allocate grid if N_grid > 0
  tensor_grid(const st N_grid=0, const bool isreal=true);
  // copy constructor
  tensor_grid(const tensor_grid&) = default;
  // move constructor
  tensor_grid(tensor_grid&&) noexcept = default;
  // copy assignment
  tensor_grid& operator=(const tensor_grid& grd) = default;
  
  // copy constructor using 'generate_tidal' below
  // - scalar_grid has to be in Fourier space!
  tensor_grid(const scalar_grid&, const bool subtract_trace=true);
  // copy constructor assigning the symmetrized matrix product of the two given
  // tensor grids given, specifically (*this)_{ab} = g1_{(ac} g2_{cb)}
  // - arguments can be the same grid
  // - implemented as constructor rather than operator to save memory 
  tensor_grid(const tensor_grid&, const tensor_grid&);

  scalar_grid &operator[](st comp) { return kg[comp]; }
  const scalar_grid &operator[](st comp) const { return kg[comp]; }

  virtual void resize(const st NGnew)
    {
    NG = NGnew;
    NG2 = NG*NG;
    for (st I=0; I < 6; I++)
      kg[I].resize(NG);
    }

  // fill grid with homogeneous value
  void fill(const double &v)
    { for (auto &vec: kg) vec.fill(v); }

  // to set real/Fourier status by hand (take care!)
  virtual void set_real(const bool r) {
    real = r;
    for (st I=0; I < 6; I++)
      kg[I].set_real(r);
  }

  // applies tensor to given vector element: (Re M)_mn(i,j,k) v^n
  real_vec3 Re(const st i, const st j, const st k,
		 const real_vec3& v) const;
  
  /* // take matrix product of self */
  // not implemented currently, as requires allocating temporary copy of self
  // use constructor instead, see above
  /* void square(); */

  // --------------------------------------------------------------------
  // operations
  // --------------------------------------------------------------------  
  // - note: for operations which only make sense in real space, error is
  //         issue if applied to Fourier space fields

  // generate from scalar field via,
  // subtract_trace=true:
  //   (\partial_i\partial_j/\laplace - 1/3 \delta_ij) grid
  // subtract_trace=false:
  //   (\partial_i\partial_j/\laplace) grid
  void generate_tidal(const scalar_grid& grid, const bool subtract_trace=true);

  // trace
  scalar_grid trace() const;

  // returns the result of \partial_i \partial_j T^ij, in Fourier space:  
  // - k_i k_j T_ij(\vec k)
  // where k refers to grid::Lbox
  scalar_grid divergence() const;
  
  // operations
  // - assignment
  // tensor_grid& operator=(const tensor_grid&);
  // - addition of two tensor grids
  tensor_grid& operator+=(const tensor_grid&);
  // - multiplication by C-number
  tensor_grid& operator*=(const double);
  // - multiplication by scalar_grid
  tensor_grid& operator*=(const scalar_grid&);
  // - add a real number to the diagonal (i.e., add val * identity)
  tensor_grid& add_diagonal(const double val) {
    for (st I=0; I < 3; I++)
      kg[I] += val;
    return *this;
  }

  // add, element-wise, c * grid to grid
  tensor_grid& add(const double c, const tensor_grid& grid) {
    for (st I=0; I < 6; I++)
      kg[I].add(c, grid.kg[I]);
    return *this;
  }    

  // - contraction to scalar grid
  scalar_grid operator*(const tensor_grid&) const;

  // - contraction with vector
  vector_grid operator*(const vector_grid&) const;

  // - vector product
  // - returns eps^ijk A_jl B_kl
  vector_grid operator%(const tensor_grid& B) const;

  /* // - binary versions referring to compound assignments */
  /* friend tensor_grid operator+(tensor_grid lhs, const tensor_grid& rhs) */
  /* { */
  /*   lhs += rhs; */
  /*   return lhs; */
  /* } */

  /* // - multiplication by C-number */
  /* friend tensor_grid operator*(tensor_grid lhs, const double v) */
  /* { */
  /*   lhs *= v; */
  /*   return lhs; */
  /* } */
  
  // - triple contraction to scalar grid = tr [ (*this) B F ]
  scalar_grid doubleContraction(const tensor_grid& B,
				const tensor_grid& F) const;
  
  // returns
  // \epsilon^ijk \epsilon^lmn (*this)_il B_jm F_kn
  // - B and F can be the same and equal to *this
  scalar_grid doubleEpsContraction(const tensor_grid& B,
				   const tensor_grid& F) const;

  // determinant
  scalar_grid determinant() const;
    
  // FFT
  tensor_grid& go_fourier() {
    // debug - sanity check
    if (real)  {
      logger.printf("tensor_grid::go_fourier", DEBUG,
		    "%d != %d/%d/%d/%d/%d/%d",
		    real, kg[0].is_real(), kg[1].is_real(), kg[2].is_real(),
		    kg[3].is_real(), kg[4].is_real(), kg[5].is_real() );
    }
    // scalar_grid prevents unnecessary actual Fourier transform
    for (int I=0; I < 6; I++)
      kg[I].go_fourier();
    real = false;
    return *this;
  }
  tensor_grid& go_real() {
    // debug - sanity check
    if (!real)  {
      logger.printf("tensor_grid::go_real", DEBUG,
		    "%d != %d/%d/%d/%d/%d/%d",
		    real, kg[0].is_real(), kg[1].is_real(), kg[2].is_real(),
		    kg[3].is_real(), kg[4].is_real(), kg[5].is_real() );
    }
    // scalar_grid prevents unnecessary actual Fourier transform
    for (int I=0; I < 6; I++)  
      kg[I].go_real();
    real = true;
    return *this;
  }

  // - set to zero all Fourier modes in grid above kcut_phys
  //   where kcut_phys refers to grid::Lbox
  // - returns number of Fourier modes that remain after cut
  // - Hermitianity is ensured
  st cutsharpk(const double kcut_phys,
	       const sharpkFilterType ftype = SPHERE)
  {
    st Nmodes=0;
    go_fourier();
    for (st I=0; I<6; I++)
      Nmodes = kg[I].cutsharpk(kcut_phys, ftype);
    return Nmodes;
  }
  
  // stats for individual grids (this is more or less for debugging)
  void print_stats(FILE* fp);
  
  // no smoothing for now
  
  // smooth field (optionally take out CIC window function)
  // - R is in units of grid cells
  // !!! dividing by CIC window currently doesn't work (have to deal with
  //     zeros of kernel)
  /* void smooth_Gaussian(const double R, bool divide_CIC_window = false); */


  // --------------------------------------------------------------------  
  // file I/O
  // --------------------------------------------------------------------

  // * nbtl format, see grid.cc for basic format
  virtual void write_nbtl_s(std::ostream &file) const;
  // - if currently unallocate (NG==0), this allocates grid of the size
  //   found in file
  virtual void read_nbtl_s(std::istream &file);  

  // * HDF5
  virtual void read_HDF5(const std::string& file,
                         const std::string& data_set_name)
  {
    kg[0].read_HDF5(file, data_set_name+"/0");
    kg[1].read_HDF5(file, data_set_name+"/1");
    kg[2].read_HDF5(file, data_set_name+"/2");
    kg[3].read_HDF5(file, data_set_name+"/3");
    kg[4].read_HDF5(file, data_set_name+"/4");
    kg[5].read_HDF5(file, data_set_name+"/5");
    NG = kg[0].getNG();
    real = kg[0].is_real();
    for (st i=1; i<6; ++i) {
      NT_assert(kg[i].getNG()==NG, "grid size mismatch");
      NT_assert(kg[i].is_real()==real, "grid space mismatch");
    }
  }

  virtual void write_HDF5(const std::string& file,
                          const std::string& data_set_name) const
  {
    kg[0].write_HDF5(file, data_set_name+"/0");
    kg[1].write_HDF5(file, data_set_name+"/1");
    kg[2].write_HDF5(file, data_set_name+"/2");
    kg[3].write_HDF5(file, data_set_name+"/3");
    kg[4].write_HDF5(file, data_set_name+"/4");
    kg[5].write_HDF5(file, data_set_name+"/5");
  }
};

} // end of namespace LEFTfield

#endif
