// code to read in Gadget particle file format

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <fstream>

// for debugging
// typedef unsigned int stdebug;
// - should be this:
typedef size_t stdebug;

namespace ns_assign_density_gadget {

  // some diagnostic output which is not needed normally
  //#define DEBUG

  struct gadget_header
  {
    unsigned int npart[6];
    double mass[6];
    double time;
    double redshift;
    int flag_sfr;
    int flag_feedback;
    unsigned int npartTotal[6];
    int flag_cooling;
    int num_files;
    double BoxSize;
    double Omega0;
    double OmegaLambda;
    double HubbleParam;
    char fill[256 - 6 * 4 - 6 * 8 - 2 * 8 - 2 * 4 - 6 * 4 - 2 * 4 - 4 * 8];	/* fills to 256 Bytes */

    void read_header(FILE *fp);

    stdebug get_num_part_file() const {
      stdebug N=0;
      for (int i=0; i < 6; i++)  N += (stdebug) npart[i];
      return N;
    }
    stdebug get_num_part_total() const {
      stdebug N=0;
      for (int i=0; i < 6; i++)  N += (stdebug) npartTotal[i];
      return N;
    }

    void print_info(FILE *fp) const
    {
      fprintf(fp, "--- Info ---\n");
      fprintf(fp, "Number of particles for each type: ");
      for (int i=0; i < 5; i++)  fprintf(fp, "%u / ", npartTotal[i]);
      fprintf(fp, "%u\n", npartTotal[5]);
      fprintf(fp, "box size: %.1f [Mpc/h] (!)\n", BoxSize);
      fprintf(fp, "redshift: %.1f\n", redshift);
      fprintf(fp, "Cosmology parameters: Om = %.4f, OL = %.4f, h_100 = %.4f\n",
	      Omega0, OmegaLambda, HubbleParam);
      fprintf(fp, "-------------\n");
    }

  };

  struct gadget_particle_data
  {
    float Pos[3];
    float Vel[3];
    float Mass;
    int Type;

    float Rho, U, Temp, Ne;
  };

  FILE* open_file(const std::string &fname, const int ifile,
		  const int Nfiles)
  {
    char buf[2000];

    if (Nfiles > 1) sprintf(buf, "%s.%d", fname.c_str(), ifile);
    else sprintf(buf, "%s", fname.c_str());

    FILE *fd = fopen(buf, "r");
    NT_assert(fd != NULL, "assign_density_gadget::open_file: can't open file `%s`.", buf);
    return fd;
  }

  float* allocate_block(const stdebug Npart)
  {
    logger.printf("assign_density_gadget::allocate_block", DEBUG,
		  "allocating space for %zu particles.", Npart);
    float *array = new float[3 * Npart];
    NT_assert(array != 0, "assign_density_gadget::allocate_block: cannot allocate memory.");
    return array;
  }

  void free_block(float *array)
  {
    logger.printf("assign_density_gadget::free_block", DEBUG,
		  "freeing memory.");
    if (array) delete[] array;
  }

  // read a single gadget file block into array
  // - e.g., particle positions or velocities
  // - array has to have room for 3 * header.num_part_file floats
  void read_block(FILE *fp, float* array, const gadget_header& header);

  // load positions from single particle file into array
  // - if array is 0, new array is allocated and returned (otherwise, array is returned)
  // - header is read into struct pointed to by pheader
  float* load_positions(float *array, gadget_header *pheader,
			const std::string &fname, const int ifile,
			const int Nfiles);

} // ns
// ---------------------------------------------------------------------


using namespace ns_assign_density_gadget;

// read particles in Gadget2 snapshot format
// - filenames are <base_filename>.<i>, where i = 0, 1, ... Nfiles-1
// - move each particle by shiftx/y/z grid cells
// - if nbar > 0, subsample particles to comoving number density nbar
// - returns box size specified in Gadget header
double assign_density_Gadget(ScalarGridAssigner &assigner,
			     const std::string &base_filename,
			     const int Nfiles,
			     const double shiftx,
			     const double shifty,
			     const double shiftz,
			     const double nbar)
{
  st NG = assigner.getNG();
  float *array = 0;
  gadget_header header;
  stdebug NP = 0;  // total particle number
  stdebug NPfile = 0;  // total particle number in file
  stdebug pCount = 0; // for checking consistency b/w header and files read
  double m = 0., fR = 0.;

  double Lbox = 0.;
  bool subsample = false;
  double fss = 1.; // subsampling fraction

  // === run over files ===
  for (int fi = 0; fi < Nfiles; fi++)  {
    logger.printf("assign_density_Gadget", INFO, 
		  "File %d / %d...", fi+1, Nfiles);
    array = load_positions(array, &header, base_filename, fi, Nfiles);

    NP = header.get_num_part_total();
    NPfile = header.get_num_part_file();
    if (!fi)  {
      // some header info
      header.print_info(stderr);

      // get box size - attempt to convert from kpc/h to Mpc/h
      Lbox = (header.BoxSize > 1.e4) ? header.BoxSize / 1000. : header.BoxSize;

      if (nbar > 0.)  {
	st Ntot = header.get_num_part_total();
	double nfull = double(Ntot) / (Lbox*Lbox*Lbox);
	fss = nbar/nfull;
	// debug
	logger.printf("assign_density_Gadget", DEBUG,
		      "n_full = %.3e, fss = %.3e", nfull, fss);
	if (fss >= 1.)  {
	  logger.printf("assign_density_Gadget", ESSENTIAL,
			"target density (%.3e) higher than actual particle density (%.3e). Not subsampling.", nbar, nfull);
	} else {      
	  logger.printf("assign_density_Gadget", INFO,
			"subsampling particles by factor %.3e.\n", fss);
	  subsample=true;
	}
      }
      
      // calculate m, fR
      m = double(NG*NG*NG) / double(NP);
      if (subsample)  m /= fss; // increase mass in proportion to subsampling
      fR = double(NG) / header.BoxSize;  // position conversion
      logger.printf("assign_density_Gadget", DEBUG,
		    "Assigning density... (m = %.3e, fR = %.3e)", m, fR);
    }
    logger.printf("assign_density_Gadget", INFO, "reading %zu particles total, %zu  in this file.", NP, NPfile);

    // === allocate position buffer ===
    
    std::vector<double> posvec(3*NPfile), mvec(NPfile);
  
    // === run over particles ===
    
    st c=0; // following actual count after subsampling
    for (stdebug I=0; I < NPfile; I++)  {
      // subsample, if desired
      if (subsample)  {
	if (fss < drand48())  continue;
      }
      
      // convert position to code units, add shift
      double x = fR * array[3*I]   + shiftx,
	y = fR * array[3*I+1] + shifty,
	z = fR * array[3*I+2] + shiftz;
      posvec[3*c]   = x;
      posvec[3*c+1] = y;
      posvec[3*c+2] = z;
      mvec[c] = m;
      c++;

      pCount++;
      if (!(pCount % 10000000))
	logger.printf("assign_density_Gadget", DEBUG,
		      "... read %zu particles...", pCount);
    }

    // debug
    logger.printf("assign_density_Gadget", DEBUG,
		  "assigned %zu particles from this file.", c);

    // now assign
    assigner.assign(posvec, mvec);    
  } // end file loop

  // === free up memory and finish ===
  free_block(array);
  
  // info
  if (logger.getLevel() >= INFO)  {
    logger.printf("assign_density_Gadget", INFO,
		  "Read in %zu (!= %zu) particles. Density stats:", pCount, NP);
    assigner.getGrid()->print_stats(stderr);
  }
  return Lbox;
}

// ---------------------------------------------------------------------
namespace ns_assign_density_gadget {
  st dummy_return;
  int dummy;
#ifdef DEBUG
#define SKIP { dummy_return=fread(&dummy, sizeof(dummy), 1, fp); fprintf(stderr, "SKIP: %d\n", dummy); }
#else
#define SKIP dummy_return=fread(&dummy, sizeof(dummy), 1, fp);
#endif


  void gadget_header::read_header(FILE *fp)
  {
    SKIP;
    dummy_return = fread(this, sizeof(gadget_header), 1, fp);
    SKIP;
  }

  void read_block(FILE *fp, float* array, const gadget_header& header)
  {
    stdebug count=0;
    SKIP;
    for(int k = 0; k < 6; k++) {
      stdebug nk = (stdebug) 3 * header.npart[k];
      dummy_return = fread(&array[count], sizeof(float), nk, fp);
      logger.printf("read_block", DEBUG, "%d. count = %lu, nk = %lu, dummy_return = %zu",
		    k, count, nk, dummy_return);
      count += nk;
    }
    SKIP;
  }

  // load positions from single particle file into array
  // - if array is 0, new array is allocated and returned (otherwise, array is returned)
  // - header is read into struct pointed to by pheader
  float* load_positions(float *array, gadget_header *pheader,
			const std::string &fname, const int ifile,
			const int Nfiles)
  {
    static stdebug arr_size = 0;

    // open file (exits if it cannot open)
    FILE* fp = open_file(fname, ifile, Nfiles);

    // read header
    pheader->read_header(fp);
#ifdef DEBUG
    pheader->print_info(stderr);
#endif

    // allocate memory if needed
    stdebug Npart = pheader->get_num_part_file();
    // check if at all reasonable
    NT_assert(Npart > 0 && Npart < 10000000000,
	      "load_positions: odd number of particles: %zu", Npart);

    if (!array)  { // first allocation
      array = allocate_block(Npart);
      arr_size = Npart;
    } else if (Npart > arr_size)  { // this file is bigger than previous ones
      free_block(array);
      array = allocate_block(Npart);
      arr_size = Npart;
    }

    // first block is position block; read it here
    read_block(fp, array, *pheader);

    fclose(fp);
    return array;
  }


} // ns
