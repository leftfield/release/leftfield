/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef OPERATORSET_H
#define OPERATORSET_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cassert>
#include <algorithm>

#include "types.h"
#include "powerSpectrum.h"
#include "scalar_grid.h"
#include "namedSet.h"

namespace LEFTfield {

// ********************************************************************
// class operatorSet
//
// encapsulate set of named scalar_grids that contain relevant operators
// - derived from namedSet
//
// (C) Fabian Schmidt, 2017-2021
// ********************************************************************

// ********************************************************************

struct Opdata
  {
  sptr<scalar_grid> grd;
  op_order order;
  };


// ********************************************************************

class operatorSet : public namedSet<Opdata>
{
 protected:
  
 public:
  operatorSet()
    : namedSet()
  { }
  virtual ~operatorSet() {}

  // get operator by name if exists, otherwise create one
  sptr<scalar_grid> getormake(const std::string& name,
			      const st NG, const op_order order) {
    if (have(name)) {
      return (*this)[name].grd;
    }
    
    // create new operator
    auto O = make_sptr<scalar_grid>(NG);
    add(name, Opdata{ O, order });
    return O;
  }

  // return 'true' if _all_ grids are in real/Fourier space
  bool is_real() const
    {
    for (const auto& [n, op]: objs)
      if (!op.grd->is_real()) return false;
    return true;
    }
  bool is_fourier() const
    {
    for (const auto &op: objs)
      if (!op.second.grd->is_fourier()) return false;
    return true;
    }

  st getNG() const {
    if (objs.empty()) return 0;
    return objs.begin()->second.grd->getNG();
  }

  // transform all operators to real/Fourier space
  void go_real()
    { for (auto &op: objs) op.second.grd->go_real(); }
  void go_fourier()
    { for (auto &op: objs) op.second.grd->go_fourier(); }

  void subtract_mean()
    { for (auto &op: objs) op.second.grd->subtract_mean(); }

  // // Fourier-resize all operators to target
  // // - if operators in target operatorSet do not exist, they are allocated
  // //   with given grid size
  // // - if they DO exist, resize is done to EXISTING grid size of target operatorSet
  // // - note: need to be in Fourier space!
  // void resizeFourier(operatorSet& target, st NGtarget,
  // 		     const double factor) const {
  //   for (const auto& [name, O]: objs) {
  //     O.grd->resizeFourier( * target.getormake(name, NGtarget, O.order) );
  //     if (factor != 1.)
  // 	* target[name].grd *= factor;
  //   }
  // }

  void cutsharpk(const sharpkFilter& filter) {
    for (auto &op: objs) op.second.grd->cutsharpk(filter);
  }
  
  // print stats for info
  void print_stats(FILE* fp);

  // !!! right now, power spectra in powerSpectrumSet only implemented for
  // !!! logarithmic bins; no reason for this other than laziness...

  // fill auto- and cross-power spectra from grids
  void fill_powerspectra(powerSpectrumSet &pks);

  // fill cross-power spectra from grids of given operator_set
  // - returns error if operator_sets are of different type
  //   (e.g., squared and cubed)
  // - only fills diagonal and "upper triangle" ; for lower triangle,
  //   use member routine of other operator_set_squared
  void fill_powerspectra(operatorSet& O, powerSpectrumSet &pks);

  // fill cross-power spectra of given scalar grid will all grids in the set
  void fill_powerspectra(scalar_grid& delta_h, powerSpectrumSet &pks);

  // generate cross moments with given field
  void print_cross_moments(const scalar_grid &Oprime,
			   const std::string &Oprimename) const;

  // write all operators to HDF5 file
  // - adds prefix to dataset name for each operator; e.g. if
  //   dataset_prefix = "group1/", then a group "group1" is created within
  //   which all fields are written
  void write(const std::string& filename,
	     const std::string& dataset_prefix = "") const;
  // read operators from HDF5 file
  // - NOTE: need existing set of Opdata with desired names
  // - prefix of dataset name see above
  void read(const std::string& filename,
	    const std::string& dataset_prefix = "") const;
};

} // end of namespace LEFTfield

#endif
