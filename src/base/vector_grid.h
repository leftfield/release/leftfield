/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef VECTOR_GRID_H
#define VECTOR_GRID_H

#include <iostream>
#include <math.h>

#include "grid.h"
#include "scalar_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"

namespace LEFTfield {

// implements vector field on a grid
// - 3 complex arrays of size NG^3
//     0:  1
//     1:  2
//     2:  3


class vector_grid : public grid
{ 
 protected:
  std::array<scalar_grid,3> vg;

 public:
  // default constructor
  // - allocate grid if N_grid > 0
  // - grid is filled value v
  vector_grid(const st N_grid=0, const bool isreal=true,
	      const double v=0.);

  // copy constructor
  vector_grid(const vector_grid&) = default;
  // move constructor
  vector_grid(vector_grid&&) noexcept = default;
  // copy assignment
  vector_grid& operator=(const vector_grid& grd) = default;

  scalar_grid &operator[](st comp) { return vg[comp]; }
  const scalar_grid &operator[](st comp) const { return vg[comp]; }

  // fill grid with homogeneous value
  void fill(const double &v)
    { for (auto &vec: vg) vec.fill(v); }

  virtual void resize(const st NGnew)
    {
    NG = NGnew;
    NG2 = NG*NG;
    for (st I=0; I < 3; I++)
      vg[I].resize(NG);
    }

  
  // to set real/Fourier status by hand (take care!)
  virtual void set_real(const bool r) {
    real = r;
    for (st I=0; I < 3; I++)
      vg[I].set_real(r);
  }

  // generate from scalar field via
  // (sign) \partial_i\/\laplace (grid)
  // - default sign=-1 applicable if grid == delta
  // - allocates grid of size grid.NG if empty
  void generate_displacement(const scalar_grid& grid, const double sign = -1.);
  // generate from scalar field via
  // \partial_i (grid)
  // - note that derivatives are F.T. of i k^j in our convention (see grid.h)
  // - allocates grid of size grid.NG if empty
  void generate_gradient(const scalar_grid& grid);

  // --------------------------------------------------------------------
  // operations
  // --------------------------------------------------------------------  

  // fill grid with \partial_i (grid)^i
  void divergence(scalar_grid& div) const;
  // return \partial_i (grid)^i
  scalar_grid divergence() const {
    scalar_grid div;
    divergence(div);
    return div;
  }
  
  // operations
  // - note: for operations which only make sense in real space, error is
  //         issue if applied to Fourier space fields
  // - addition of two vector grids
  vector_grid& operator+=(const vector_grid&);
  // - multiplication by C-number
  vector_grid& operator*=(const double);
  // - scalar product with another vector field
  friend scalar_grid operator*(const vector_grid&, const vector_grid&);
  // - vector product
  friend vector_grid operator%(const vector_grid&, const vector_grid&);

  // - multiplication with scalar field
  friend vector_grid operator*(const scalar_grid&, const vector_grid&);

  // - tensor contraction with vector
  friend vector_grid tensor_grid::operator*(const vector_grid&) const;

  // add, element-wise, c * grid to grid
  vector_grid& add(const double c, const vector_grid& grid) {
    for (st I=0; I < 3; I++)
      vg[I].add(c, grid.vg[I]);
    return *this;
  }

  
  /* // - binary versions referring to compound assignments */
  /* friend vector_grid operator+(vector_grid lhs, const vector_grid& rhs) */
  /* { */
  /*   lhs += rhs; */
  /*   return lhs; */
  /* } */
  
  /* friend vector_grid operator*(vector_grid lhs, const double c) */
  /* { */
  /*   lhs *= c; */
  /*   return lhs; */
  /* } */

  // curl, Laplace operator and inverse, applied in Fourier space
  // (note: derivatives in code units)
  vector_grid& curl();
  
  vector_grid& Laplace() {
    for (st i=0; i < 3; i++)
      vg[i].Laplace();
    return *this;
  }
    
  vector_grid& inverseLaplace() {
    for (st i=0; i < 3; i++)
      vg[i].inverseLaplace();
    return *this;
  }
  

  // FFT
  vector_grid& go_fourier() {
    if (!real) {
      logger.printf("vector_grid::go_fourier", DEBUG, "already in Fourier space");
      return *this;
    }
    // debug - sanity check
    logger.printf("vector_grid::go_fourier", DEBUG, "%d != %d/%d/%d",
		  real, vg[0].is_real(), vg[1].is_real(), vg[2].is_real() );
    // scalar_grid prevents unnecessary actual Fourier transform
    for (st i=0; i < 3; i++)
      vg[i].go_fourier();
    real = false;
    return *this;
  }
  vector_grid& go_real() {
    if (real) {
      logger.printf("vector_grid::go_real", DEBUG, "already in real space");
      return *this;
    }
    // debug - sanity check
    logger.printf("vector_grid::go_real", DEBUG, "%d != %d/%d/%d",
		  real, vg[0].is_real(), vg[1].is_real(), vg[2].is_real() );
    // scalar_grid prevents unnecessary actual Fourier transform
    for (st i=0; i < 3; i++)  
      vg[i].go_real();
    real = true;
    return *this;
  }

  // subtract mean to ensure <grid> = 0
  void subtract_mean() {
    for (st I=0; I < 3; I++)
      vg[I].subtract_mean();
  }

  // --------------------------------------------------------------------  
  // interpolation
  // --------------------------------------------------------------------

  // returns CIC-interpolated vector value at x,y,z
  // - real value returned, obviously
  // - grid has to be in real space
  // - trilinear interpolation if set, otherwise NGP
  real_vec3 interpolate_to(const double x, const double y, const double z,
  		           const bool trilinear = true) const;

  // adds CIC-interpolated given (real) vector value to x,y,z
  // - grid has to be in real space
  void interpolate_add(const real_vec3 val,
		       const double x, const double y, const double z);
  
  // - set to zero all Fourier modes in grid above kcut_phys
  //   where kcut_phys refers to grid::Lbox
  // - returns number of Fourier modes that remain after cut
  // - Hermitianity is ensured
  st cutsharpk(const double kcut_phys,
	       const sharpkFilterType ftype = SPHERE) {
    st Nmodes=0;
    for (st i=0; i < 3; i++)
      Nmodes = vg[i].cutsharpk(kcut_phys, ftype);
    return Nmodes;
  }
  
  // return Fourier-space grid of size 3xNGd, for all modes
  // with |k| <= kcut_phys, where kcut_phys refers to grid::Lbox
  // (box size, i.e. fundamental modes of both grids are assumed to be the same)
  // - no cut if kcut_phys=0.
  // - Hermitianity of dest is ensured
  // - returns number of Fourier modes that remain after cut in Nmodes
  vector_grid copysharpk(const st NGd, const double kcut_phys = 0.,
			 const sharpkFilterType ftype = SPHERE,
			 st *N_modes = 0) const;

  // - fill grid res with resized version of *this, where resizing is done in
  //   Fourier space
  // - either by zero-padding, in case of
  //   enlargement, or cutting of high-frequency modes in case of reduction
  void resizeFourier(vector_grid &res) const {
    res.set_real(false);
    for (st i=0; i < 3; i++)
      vg[i].resizeFourier(res.vg[i]);
  }

  // --------------------------------------------------------------------  
  // file I/O
  // --------------------------------------------------------------------

  // * nbtl format, see grid.cc for basic format
  virtual void write_nbtl_s(std::ostream &file) const;
  // - if currently unallocate (NG==0), this allocates grid of the size
  //   found in file
  virtual void read_nbtl_s(std::istream &file);  

  // * HDF5
  // read grid (NG^3 doubles) from given HDF5 file and data set name
  virtual void read_HDF5(const std::string& file,
                         const std::string& data_set_name)
  {
    vg[0].read_HDF5(file, data_set_name+"/0");
    vg[1].read_HDF5(file, data_set_name+"/1");
    vg[2].read_HDF5(file, data_set_name+"/2");
    NG = vg[0].getNG();
    real = vg[0].is_real();
    for (st i=1; i<3; ++i) {
      NT_assert(vg[i].getNG()==NG, "grid size mismatch");
      NT_assert(vg[i].is_real()==real, "grid space mismatch");
    }
  }

  // write grid (NG^3 doubles) to given HDF5 file and data set name
  virtual void write_HDF5(const std::string& file,
                          const std::string& data_set_name) const
  {
    vg[0].write_HDF5(file, data_set_name+"/0");
    vg[1].write_HDF5(file, data_set_name+"/1");
    vg[2].write_HDF5(file, data_set_name+"/2");
  }

  // --------------------------------------------------------------------
  // statistics
  // --------------------------------------------------------------------
  
  // get basic statistics
  // - note that these transform to real space!
  // - mean
  real_vec3 get_mean();
  // - variance (also computes mean and returns in pointer if given)
  real_vec3 get_variance(real_vec3 *mean = 0);

  // print mean and variance
  // (real space; real+imaginary parts)
  // - note that this transforms to real space!
  void print_stats(FILE *fp);
};

} // end of namespace LEFTfield

#endif
