/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <fstream>
#include <string.h>
#include "grid.h"
#include "error_handling.h"

namespace LEFTfield {

// bit mask used for encoding Fourier space flag
const int fourierID = 128; 

// static
double grid::Lbox=1.;

grid::grid(const st N_G, const bool Real, const grid_type t)
  : NG(N_G), NG2(N_G*N_G), real(Real), type(t) {}


// === nbody_tools::grid file format ===
//
// byte 0-3: IDstring, see above
// byte 4-8: (int) grid_type (including Fourier space flag)
// byte 8-...: (size_t) NG


std::ofstream grid::getOfstream(const std::string &file) const
{
  std::ofstream of(file.c_str(), std::ofstream::trunc | std::ofstream::binary);
  NT_assert(of, "grid::getOfstream: cannot open output file '",file,"'");

  int wtype = type | (real ? 0 : fourierID);
  // (debug)
  logger.printf("grid::getOfstream", DEBUG, "wtype = %d", wtype);
  of.write(IDstring, 4*sizeof(char));
  of.write(reinterpret_cast<const char*>(&wtype), sizeof(int));
  of.write(reinterpret_cast<const char*>(&NG), sizeof(st));
  return of;
}

std::ifstream grid::getIfstream(const std::string &file) 
{
  std::ifstream ifs(file, std::ifstream::binary);
  NT_assert(ifs, "grid::getIfstream: cannot open input file '",file,"'");

  char buf[4];
  ifs.read(&buf[0], 4*sizeof(char));
  NT_assert(!strncmp(buf, IDstring, 4), "grid::read: file type mismatch: bad ID");

  int rtype = 0;
  st NGf = 0;
  ifs.read((char*) &rtype, sizeof(int));
  ifs.read((char*) &NGf, sizeof(st));
  bool realf = ! ( (bool) (rtype & fourierID) );
  grid_type typef = grid_type( rtype - (rtype & fourierID) );

  // (debug)
  LogContext lc("grid::getIfstream");
  logger.printf(DEBUG, "%d / %d (%d) / %d", rtype, realf, real, typef);

  // (info)
  if (realf)  {
    logger.printf(INFO, "file for grid type %d (real space) with NG = %zu.",
		  typef, NGf);
  } else {
    logger.printf(INFO, "file for grid type %d (Fourier space) with NG = %zu.",
		  typef, NGf);
  }

  // sanity checks
  NT_assert(typef > NONE && typef < UNSUPPORTED, "grid::getIfstream: trivial or unsupported type.");

  // check for grid type mismatch
  NT_assert(type==typef, "grid::getIfstream: type mismatch: ", type, " != ", typef, "(file). Aborting read.");

  // set real/fourier flag
  real = realf;

  if (!NG) resize(NGf);
  NT_assert(NG==NGf, "grid::read: size mismatch: NG = ", NG, " != NG(file) = ", NGf, ". Aborting read.");

  return ifs;
}

} // end of namespace LEFTfield
