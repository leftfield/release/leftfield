/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef GRID_H
#define GRID_H

// ********************************************************************
// class grid
// - implements general grid settings etc
//
// A NOTE ON FOURIER CONVENTION
// (updated Nov 2019)
// - forward using FFTW_FORWARD:
//   delta(k) = \sum_j delta(x_j) exp(-i k x_j)
//
// - backward using FFTW_BACKWARD
//   delta(x) = \sum_k delta(k) exp(i k x)
//
// - derivatives are \partial/\partial x^j <--> i k_j
//
// (C) Fabian Schmidt, 2017
// ********************************************************************

#include "types.h"
#include "error_handling.h"

namespace LEFTfield {

// ****************************************************************
// utility functions

// returns permutation of 3-element set of indices a
// - permutation labeled by I = 0, 1, 2
// - example usage:
//   st is = 1, js = 8, ks = 18;
//   for (st I=0; I < 3; I++)  {
//      auto ap = permute(I, {is,js,ks});
//      // ...
template<typename T>
std::array<T, 3> permute(st I, const std::array<T, 3>& a)
{
  if (I==1)
    return { a[1], a[2], a[0] };
  else if (I==2)
    return { a[2], a[0], a[1] };

  return { a[0], a[1], a[2] };
}

inline void hartley_load (double h1, double h2, double &re, double &im)
  {
  re = .5*(h1+h2);
  im = .5*(h2-h1);
  }
inline void hartley_load (double h1, double h2, cmplx &c)
  { c = {.5*(h1+h2), .5*(h2-h1)}; }
inline cmplx hartley_load (double h1, double h2)
  { return {.5*(h1+h2), .5*(h2-h1)}; }
inline void hartley_store (double re, double im, double &h1, double &h2)
  {
  if (&h1==&h2)
    {
#ifdef CHECK_NYQUIST_IMAGINARY
    if (fabs(im)>1e-10*fabs(re))
      logger.printf(INFO,
        "Warning: Hartley symmetry violation on Nyquist plane. "
        "Setting imaginary part to zero.");
#endif
    im=0;
    }
  h1 = re - im;
  h2 = re + im;
  }
inline void hartley_store (const cmplx &c, double &h1, double &h2)
  { hartley_store(c.real(), c.imag(), h1, h2); }

inline bool is_nbtl_file(const std::string &name)
  {
  std::string suffix=".nbtl";
  if (suffix.size() > name.size()) return false;
  return std::equal(suffix.rbegin(), suffix.rend(), name.rbegin());
  }

// ****************************************************************

class grid
{
 protected:
  // ID string: first 4 bytes in files written/read by all grids
  constexpr static const char *IDstring="NBTL";
  static double Lbox;

  st NG, NG2;
  bool real;
  grid_type type;

  // write/read file header
  // - to be called by write/read functions of each derived class
  std::ofstream getOfstream(const std::string &file) const;
  // - if unallocated, i.e. NG==0, calls allocate() function with NG
  //   read from file
  std::ifstream getIfstream(const std::string &file);

  void assert_compatible(const grid& g, const char *where) const {
    NT_assert(compatible(g), "grid mismatch in ", where, ": ",
	      real, ',', NG, " vs. ", g.real, ',', g.NG);
  }

 public:
  // defined in grid.cc; performs global initializations
  grid(const st N_G=0, const bool Real=true, const grid_type t = NONE);
  virtual ~grid()
    {}

  static void set_Lbox(double Lbox_) {
    NT_assert(Lbox==1. || Lbox_ == Lbox, "Lbox cannot be changed more than once");
    if (Lbox_ != Lbox)  {
      logger.printf("grid::set_Lbox", INFO, "Setting Lbox = %.4e", Lbox_); 
      Lbox = Lbox_;
    }
  }
  static double get_Lbox() {
    return Lbox;
  }

  // return wavenumber of fundamental mode
  // - referring to grid::Lbox
  static double get_kfun() {
    return 2.*M_PI / Lbox;
  }
  // return Nyquist wavenumber for given grid size
  // - referring to grid::Lbox
  static double get_kNy(const st NG_) {
    return M_PI / Lbox * double(NG_);
  }
  
  // return grid size that has given Nyquist frequency
  // - kNy = k_Ny refers to grid::Lbox
  // - next larger even number
  static st get_NGofkNy_ceil(const double kNy) {
    return 2 * (st) ceil(kNy * Lbox / (2.*M_PI));
  }
  // return grid size that has given Nyquist frequency
  // - kNy = k_Ny refers to grid::Lbox
  // - next smaller even number
  static st get_NGofkNy_floor(const double kNy) {
    return 2 * (st) floor(kNy * Lbox / (2.*M_PI));
  }

  // return Nyquist frequency of grid
  // - referring to grid::Lbox
  double get_kNy() const {
    return grid::get_kNy(NG);
  }
  
  // resize = free & allocate, if NG different from current NG
  virtual void resize(const st NG) = 0;

  // to set real/Fourier status by hand (take care!)
  virtual void set_real(const bool r) {
    real = r;
  }

  bool is_real() const {
    return real;
  }
  bool is_fourier() const {
    return !real;
  }

  st getNG() const {
    return NG;
  }

  bool compatible(const grid &other) const {
    return ((NG==other.NG) && (real==other.real));
  }

  // **********************************************************************
  // FFT
  // Update Aug. 2021: no more need for virtual FFT functions
  // virtual void go_real() = 0;
  // virtual void go_fourier() = 0;

  // **********************************************************************
  // File I/O
  //
  // A word on formats:
  // * 'nbtl' writes NG^3 std::complex<double> grids to proprietary format, either
  //    real or Fourier space
  // * HDF5 writes NG^3 double grids to HDF5 format, either real or Fourier space

  // 1) specific nbtl format (see grid.cc for basic format)
  virtual void write_nbtl(const std::string &file) const {
    auto stream = getOfstream(file);
    write_nbtl_s(stream);
  }
  virtual void read_nbtl(const std::string &file) {
    auto stream = getIfstream(file);
    read_nbtl_s(stream);
  }
  virtual void write_nbtl_s(std::ostream &file) const = 0;
  virtual void read_nbtl_s(std::istream &file) = 0;

  // 2) HDF5 format
  virtual void read_HDF5(const std::string &file,
                         const std::string& data_set_name) = 0;
  virtual void write_HDF5(const std::string &file,
                         const std::string& data_set_name) const = 0;

  // 3) Flexible routines: either nbtl or HDF5, based on file name ending
  void read(const std::string &file, const std::string& data_set_name="delta")
    { is_nbtl_file(file) ? read_nbtl(file) : read_HDF5(file, data_set_name); }
  void write(const std::string &file, const std::string& data_set_name="delta") const
    { is_nbtl_file(file) ? write_nbtl(file) : write_HDF5(file, data_set_name); }

  // **********************************************************************
  // Grid helpers

  // Helper iterating over the grid indices and calling the specified
  // function, passing 1D grid index.
  template<typename Func> void iterate(Func func) const {
    for (size_t i=0; i<NG*NG*NG; ++i)
      func(i);
  }
  template<typename Func> void iterateParallel(Func func) const {
    #pragma omp parallel for
    for (size_t i=0; i<NG*NG*NG; ++i)
      func(i);
  }
  template<typename Func, typename T=double>
  T accumulateParallel(Func func) const {
    NT_assert(real==true, "grid::accumulate: need to be in real space.");
    T sum(0.);
    #pragma omp parallel for reduction(+:sum)
    for (size_t i=0; i<NG*NG*NG; ++i)
      sum += func(i);

    return sum;
  }

  template<typename Func> void iterateFourierHalf(Func func) const {
    NT_assert(real==false, "grid::iterateFourierHalf: need to be in Fourier space.");
    // fundamental frequency unit
    // note: to have k in units of k_fund, set grid::Lbox to 2pi
    const double kw = 2.*M_PI / grid::Lbox; 

    for (st i=0; i<=NG/2; ++i) {
      st ic = (i==0) ? 0: NG-i;
      int im = (i < NG/2 ? i: i-NG);
      double kxq = kw * im;
      for (st j=0; j<NG; ++j) {
        st jc = (j==0) ? 0: NG-j;
        int jm = (j < NG/2 ? j: j-NG);
        double kyq = kw * jm;
        for (st k=0; k<NG; ++k) {
          st kc = (k==0) ? 0: NG-k;
          st idx = i*NG2 + j*NG + k;
          st idxc = ic*NG2 + jc*NG + kc;
          if (idx<=idxc) {
            int km = (k < NG/2 ? k: k-NG);
            double kzq = kw * km;
            double K2 = kxq*kxq + kyq*kyq + kzq*kzq;
            func(idx, idxc, kxq, kyq, kzq, K2);
          }
        }
      }
    }
  }

  template<typename Func> void iterateFourierHalfParallel(Func func) const {
    NT_assert(real==false, "grid::iterateFourierHalf: need to be in Fourier space.");
    // fundamental frequency unit
    // note: to have k in units of k_fund, set grid::Lbox to 2pi
    const double kw = 2.*M_PI / grid::Lbox; 

#pragma omp parallel for schedule(dynamic,1)
    for (st i=0; i<=NG/2; ++i) {
      st ic = (i==0) ? 0: NG-i;
      int im = (i < NG/2 ? i: i-NG);
      double kxq = kw * im;
      for (st j=0; j<NG; ++j) {
        st jc = (j==0) ? 0: NG-j;
        int jm = (j < NG/2 ? j: j-NG);
        double kyq = kw * jm;
        for (st k=0; k<NG; ++k) {
          st kc = (k==0) ? 0: NG-k;
          st idx = i*NG2 + j*NG + k;
          st idxc = ic*NG2 + jc*NG + kc;
          if (idx<=idxc) {
            int km = (k < NG/2 ? k: k-NG);
            double kzq = kw * km;
            double K2 = kxq*kxq + kyq*kyq + kzq*kzq;
            func(idx, idxc, kxq, kyq, kzq, K2);
          }
        }
      }
    }
  }

  template<typename Func, typename T=double>
  T accumulateFourierParallel(Func func) const {
    NT_assert(real==false, "grid::accumulateFourier: need to be in Fourier space.");
    
    T sum(0.);
    // fundamental frequency unit
    // note: to have k in units of k_fund, set grid::Lbox to 2pi
    const double kw = 2.*M_PI / grid::Lbox; 

#pragma omp parallel for schedule(dynamic,1) reduction(+:sum)
    for (st i=0; i<=NG/2; ++i) {
      st ic = (i==0) ? 0: NG-i;
      int im = (i < NG/2 ? i: i-NG);
      double kxq = kw * im;
      for (st j=0; j<NG; ++j) {
        st jc = (j==0) ? 0: NG-j;
        int jm = (j < NG/2 ? j: j-NG);
        double kyq = kw * jm;
        for (st k=0; k<NG; ++k) {
          st kc = (k==0) ? 0: NG-k;
          st idx = i*NG2 + j*NG + k;
          st idxc = ic*NG2 + jc*NG + kc;
          if (idx<=idxc) {
            int km = (k < NG/2 ? k: k-NG);
            double kzq = kw * km;
            double K2 = kxq*kxq + kyq*kyq + kzq*kzq;
            sum += func(idx, idxc, kxq, kyq, kzq, K2);
          }
        }
      }
    }

    return sum;
  }

};

} // end of namespace LEFTfield

#endif
