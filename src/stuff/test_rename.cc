/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <map>
#include <string>
#include <iostream>

typedef size_t st;

std::map<std::string, st> names; // names, index
 


// rename an object
void rename(const std::string& name, const std::string& newname) {
  auto nodeHandler = names.extract(name);
  nodeHandler.key() = newname;
  names.insert(std::move(nodeHandler));
}

void rename_slow(const std::string& name, const std::string& newname) {
  auto i = names.find(name);

  if (i != names.end())  {
    st value = i->second;
    names.erase(i);
    names[newname] = value;
  }
}


int main()
{
  names["A"] = 1;
  names["B"] = 2;

  printf("A = %zu, B = %zu, R = %zu\n",
	 names["A"], names["B"], names["R"]);

  rename("B", "R");
  
  printf("A = %zu, B = %zu, R = %zu\n",
	 names["A"], names["B"], names["R"]);

  rename_slow("B", "R");
  
  printf("A = %zu, B = %zu, R = %zu\n",
	 names["A"], names["B"], names["R"]);

  return 0;
}
