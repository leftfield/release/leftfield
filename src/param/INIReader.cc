/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

// Read an INI file into easy-to-access name/value pairs.

// inih and INIReader are released under the New BSD license (see LICENSE.txt).
// Go to the project home page for more info:
//
// https://github.com/benhoyt/inih
/* inih -- simple .INI file parser

inih is released under the New BSD license (see LICENSE.txt). Go to the project
home page for more info:

https://github.com/benhoyt/inih

https://github.com/jtilly/inih
*/

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cctype>
#include <cstdlib>
#include <sstream>
#include <iomanip>
#include <cstring>
#include "param/INIReader.h"
#include "base/error_handling.h"

namespace {

using namespace std;

/* Typedef for prototype of handler function. */
typedef int (*ini_handler)(void* user, const char* section,
                           const char* name, const char* value);

/* Typedef for prototype of fgets-style reader function. */
typedef char* (*ini_reader)(char* str, int num, void* stream);

/* Parse given INI-style file. May have [section]s, name=value pairs
   (whitespace stripped), and comments starting with ';' (semicolon). Section
   is "" if name=value pair parsed before any section heading. name:value
   pairs are also supported as a concession to Python's configparser.

   For each name=value pair parsed, call handler function with given user
   pointer as well as section, name, and value (data only valid for duration
   of handler call). Handler should return nonzero on success, zero on error.

   Returns 0 on success, line number of first error on parse error (doesn't
   stop on first error), -1 on file open error, or -2 on memory allocation
   error (only when INI_USE_STACK is zero).
*/
int ini_parse(const char* filename, ini_handler handler, void* user);

/* Same as ini_parse(), but takes a FILE* instead of filename. This doesn't
   close the file when it's finished -- the caller must do that. */
int ini_parse_file(FILE* file, ini_handler handler, void* user);

/* Same as ini_parse(), but takes an ini_reader function pointer instead of
   filename. Used for implementing custom or string-based I/O. */
int ini_parse_stream(ini_reader reader, void* stream, ini_handler handler,
                     void* user);

/* Nonzero to allow multi-line value parsing, in the style of Python's
   configparser. If allowed, ini_parse() will call the handler with the same
   name for each subsequent line parsed. */
#ifndef INI_ALLOW_MULTILINE
#define INI_ALLOW_MULTILINE 1
#endif

/* Nonzero to allow a UTF-8 BOM sequence (0xEF 0xBB 0xBF) at the start of
   the file. See http://code.google.com/p/inih/issues/detail?id=21 */
#ifndef INI_ALLOW_BOM
#define INI_ALLOW_BOM 1
#endif

/* Nonzero to allow inline comments (with valid inline comment characters
   specified by INI_INLINE_COMMENT_PREFIXES). Set to 0 to turn off and match
   Python 3.2+ configparser behaviour. */
#ifndef INI_ALLOW_INLINE_COMMENTS
#define INI_ALLOW_INLINE_COMMENTS 1
#endif
#ifndef INI_INLINE_COMMENT_PREFIXES
#define INI_INLINE_COMMENT_PREFIXES "#"
#endif

/* Nonzero to use stack, zero to use heap (malloc/free). */
#ifndef INI_USE_STACK
#define INI_USE_STACK 1
#endif

/* Stop parsing on first error (default is to keep parsing). */
#ifndef INI_STOP_ON_FIRST_ERROR
#define INI_STOP_ON_FIRST_ERROR 0
#endif

/* Maximum line length for any line in INI file. */
#ifndef INI_MAX_LINE
#define INI_MAX_LINE 1024
#endif

/* inih -- simple .INI file parser

inih is released under the New BSD license (see LICENSE.txt). Go to the project
home page for more info:

https://github.com/benhoyt/inih

*/

#if defined(_MSC_VER) && !defined(_CRT_SECURE_NO_WARNINGS)
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#if !INI_USE_STACK
#include <stdlib.h>
#endif

#define MAX_SECTION 256
#define MAX_NAME 256

/* Strip whitespace chars off end of given string, in place. Return s. */
inline static char* rstrip(char* s)
{
    char* p = s + strlen(s);
    while (p > s && isspace((unsigned char)(*--p)))
        *p = '\0';
    return s;
}

/* Return pointer to first non-whitespace char in given string. */
inline static char* lskip(char* s)
{
    while (*s && isspace((unsigned char)(*s)))
        s++;
    return s;
}

/* Return pointer to first char (of chars) or inline comment in given string,
   or pointer to null at end of string if neither found. Inline comment must
   be prefixed by a whitespace character to register as a comment. */
inline static char* find_chars_or_comment(char* s, const char* chars)
{
#if INI_ALLOW_INLINE_COMMENTS
    int was_space = 0;
    while (*s && (!chars || !strchr(chars, *s)) &&
           !(was_space && strchr(INI_INLINE_COMMENT_PREFIXES, *s))) {
        was_space = isspace((unsigned char)(*s));
        s++;
    }
#else
    while (*s && (!chars || !strchr(chars, *s))) {
        s++;
    }
#endif
    return s;
}

/* Version of strncpy that ensures dest (size bytes) is null-terminated. */
inline static char* strncpy0(char* dest, const char* src, size_t size)
{
    strncpy(dest, src, size);
    dest[size - 1] = '\0';
    return dest;
}

/* See documentation in header file. */
inline int ini_parse_stream(ini_reader reader, void* stream, ini_handler handler,
                     void* user)
{
    /* Uses a fair bit of stack (use heap instead if you need to) */
#if INI_USE_STACK
    char line[INI_MAX_LINE];
#else
    char* line;
#endif
    char section[MAX_SECTION] = "";
    char prev_name[MAX_NAME] = "";

    char* start;
    char* name;
    char* value;
    int lineno = 0;
    int error = 0;

#if !INI_USE_STACK
    line = (char*)malloc(INI_MAX_LINE);
    if (!line) {
        return -2;
    }
#endif

    /* Scan through stream line by line */
    while (reader(line, INI_MAX_LINE, stream) != NULL) {
        lineno++;

        start = line;
#if INI_ALLOW_BOM
        if (lineno == 1 && (unsigned char)start[0] == 0xEF &&
                           (unsigned char)start[1] == 0xBB &&
                           (unsigned char)start[2] == 0xBF) {
            start += 3;
        }
#endif
        start = lskip(rstrip(start));

        if (*start == ';' || *start == '#') {
            /* Per Python configparser, allow both ; and # comments at the
               start of a line */
        }
#if INI_ALLOW_MULTILINE
        else if (*prev_name && *start && start > line) {

#if INI_ALLOW_INLINE_COMMENTS
        auto end = find_chars_or_comment(start, NULL);
        if (*end)
            *end = '\0';
        rstrip(start);
#endif

            /* Non-blank line with leading whitespace, treat as continuation
               of previous name's value (as per Python configparser). */
            if (!handler(user, section, prev_name, start) && !error)
                error = lineno;
        }
#endif
        else if (*start == '[') {
            /* A "[section]" line */
            auto end = find_chars_or_comment(start + 1, "]");
            if (*end == ']') {
                *end = '\0';
                strncpy0(section, start + 1, sizeof(section));
                *prev_name = '\0';
            }
            else if (!error) {
                /* No ']' found on section line */
                error = lineno;
            }
        }
        else if (*start) {
            /* Not a comment, must be a name[=:]value pair */
            auto end = find_chars_or_comment(start, "=:");
            if (*end == '=' || *end == ':') {
                *end = '\0';
                name = rstrip(start);
                value = lskip(end + 1);
#if INI_ALLOW_INLINE_COMMENTS
                end = find_chars_or_comment(value, NULL);
                if (*end)
                    *end = '\0';
#endif
                rstrip(value);

                /* Valid name[=:]value pair found, call handler */
                strncpy0(prev_name, name, sizeof(prev_name));
                if (!handler(user, section, name, value) && !error)
                    error = lineno;
            }
            else if (!error) {
                /* No '=' or ':' found on name[=:]value line */
                error = lineno;
            }
        }

#if INI_STOP_ON_FIRST_ERROR
        if (error)
            break;
#endif
    }

#if !INI_USE_STACK
    free(line);
#endif

    return error;
}

/* See documentation in header file. */
inline int ini_parse_file(FILE* file, ini_handler handler, void* user)
{
    return ini_parse_stream((ini_reader)fgets, file, handler, user);
}

/* See documentation in header file. */
inline int ini_parse(const char* filename, ini_handler handler, void* user)
{
    FILE* file;
    int error;

    file = fopen(filename, "r");
    if (!file)
        return -1;
    error = ini_parse_file(file, handler, user);
    fclose(file);
    return error;
}

// string <-> datatype conversions from Planck LevelS

template<typename T> inline const char *type2typename ()
  { NT_fail("unsupported data type"); }
template<> inline const char *type2typename<signed char> ()
  { return "signed char"; }
template<> inline const char *type2typename<unsigned char> ()
  { return "unsigned char"; }
template<> inline const char *type2typename<short> ()
  { return "short"; }
template<> inline const char *type2typename<unsigned short> ()
  { return "unsigned short"; }
template<> inline const char *type2typename<int> ()
  { return "int"; }
template<> inline const char *type2typename<unsigned int> ()
  { return "unsigned int"; }
template<> inline const char *type2typename<long> ()
  { return "long"; }
template<> inline const char *type2typename<unsigned long> ()
  { return "unsigned long"; }
template<> inline const char *type2typename<long long> ()
  { return "long long"; }
template<> inline const char *type2typename<unsigned long long> ()
  { return "unsigned long long"; }
template<> inline const char *type2typename<float> ()
  { return "float"; }
template<> inline const char *type2typename<double> ()
  { return "double"; }
template<> inline const char *type2typename<long double> ()
  { return "long double"; }
//template<> inline const char *type2typename<bool> ()
//  { return "bool"; }
//template<> inline const char *type2typename<std::string> ()
//  { return "std::string"; }

bool equal_nocase (const string &a, const string &b)
  {
  if (a.size()!=b.size()) return false;
  for (size_t m=0; m<a.size(); ++m)
    if (tolower(a[m])!=tolower(b[m])) return false;
  return true;
  }

string trim (const string &orig)
  {
  string::size_type p1=orig.find_first_not_of(" \t");
  if (p1==string::npos) return "";
  string::size_type p2=orig.find_last_not_of(" \t");
  return orig.substr(p1,p2-p1+1);
  }

template<typename T> string dataToString (const T &x)
  {
  ostringstream strstrm;
  strstrm << x;
  return trim(strstrm.str());
  }

template<> string dataToString (const bool &x)
  { return x ? "T" : "F"; }
template<> string dataToString (const string &x)
  { return trim(x); }
template<> string dataToString (const float &x)
  {
  ostringstream strstrm;
  strstrm << setprecision(8) << x;
  return trim(strstrm.str());
  }
template<> string dataToString (const double &x)
  {
  ostringstream strstrm;
  strstrm << setprecision(16) << x;
  return trim(strstrm.str());
  }
//template<> string dataToString (const long double &x)
  //{
  //ostringstream strstrm;
  //strstrm << setprecision(25) << x;
  //return trim(strstrm.str());
  //}

//template string dataToString (const signed char &x);
//template string dataToString (const unsigned char &x);
//template string dataToString (const short &x);
//template string dataToString (const unsigned short &x);
template string dataToString (const int &x);
//template string dataToString (const unsigned int &x);
//template string dataToString (const long &x);
//template string dataToString (const unsigned long &x);
//template string dataToString (const long long &x);
//template string dataToString (const unsigned long long &x);
template string dataToString (const size_t &x);

void end_stringToData (const string &x, const char *tn, istringstream &strstrm)
  {
  string error = string("conversion error in stringToData<")+tn+">(\""+x+"\")";
  NT_assert (strstrm,error);
  string rest;
  strstrm >> rest;
//  rest=trim(rest);
  NT_assert (rest.length()==0,error);
  }

template<typename T> void stringToData (const string &x, T &value)
  {
  istringstream strstrm(x);
  strstrm >> value;
  end_stringToData (x,type2typename<T>(),strstrm);
  }

template<> void stringToData (const string &x, string &value)
  { value = trim(x); }

template<> void stringToData (const string &x, bool &value)
  {
  const char *fval[] = {"f","n","false",".false.","off","no","0"};
  const char *tval[] = {"t","y","true",".true.","on","yes","1"};
  for (size_t i=0; i< sizeof(fval)/sizeof(fval[0]); ++i)
    if (equal_nocase(x,fval[i])) { value=false; return; }
  for (size_t i=0; i< sizeof(tval)/sizeof(tval[0]); ++i)
    if (equal_nocase(x,tval[i])) { value=true; return; }
  NT_fail("conversion error in stringToData<bool>(\""+x+"\")");
  }

//template void stringToData (const string &x, signed char &value);
//template void stringToData (const string &x, unsigned char &value);
//template void stringToData (const string &x, short &value);
//template void stringToData (const string &x, unsigned short &value);
template void stringToData (const string &x, int &value);
//template void stringToData (const string &x, unsigned int &value);
//template void stringToData (const string &x, long &value);
//template void stringToData (const string &x, unsigned long &value);
//template void stringToData (const string &x, long long &value);
//template void stringToData (const string &x, unsigned long long &value);
template void stringToData (const string &x, float &value);
template void stringToData (const string &x, double &value);
//template void stringToData (const string &x, long double &value);
template void stringToData (const string &x, size_t &value);

/*! Reads a value of a given datatype from a string */
template<typename T> inline T stringToData (const std::string &x)
  { T result; stringToData(x,result); return result; }

} // end of unnamed namespace

INIReader::INIReader(const std::string &filename)
{
    NT_assert(ini_parse(filename.c_str(), ValueHandler, this)==0,
      "error reading INI file");
}

INIReader::INIReader(FILE *file)
{
    NT_assert(ini_parse_file(file, ValueHandler, this)==0,
      "error reading INI file");
}

bool INIReader::have(const std::string &name, const std::string &section) const
{
    return _values.count(MakeKey(name, section)) > 0;
}

std::string INIReader::Get(const std::string &name, const std::string &section, const std::string &default_value)
{
    std::string key = MakeKey(name, section);
    if (_values.count(key))
      return _values.at(key);
    _values[key] = default_value;
    return default_value;
}
std::string INIReader::Get(const std::string &name, const std::string &section) const
{
    std::string key = MakeKey(name, section);
    NT_assert(_values.count(key), "Key ", section, ":", name, " not found");
    return _values.at(key);
}

template<typename T> T INIReader::get(const std::string &name, const std::string &section) const
{
    return stringToData<T>(Get(name, section));
}
template float INIReader::get(const std::string &name, const std::string &section) const;
template double INIReader::get(const std::string &name, const std::string &section) const;
template int INIReader::get(const std::string &name, const std::string &section) const;
template long INIReader::get(const std::string &name, const std::string &section) const;
template std::size_t INIReader::get(const std::string &name, const std::string &section) const;
template bool INIReader::get(const std::string &name, const std::string &section) const;
template std::string INIReader::get(const std::string &name, const std::string &section) const;

template<typename T> T INIReader::get(const std::string &name, const std::string &section, const T &default_value)
{
    return stringToData<T>(Get(name, section, dataToString(default_value)));
}
template float INIReader::get(const std::string &name, const std::string &section, const float &default_value);
template double INIReader::get(const std::string &name, const std::string &section, const double &default_value);
template int INIReader::get(const std::string &name, const std::string &section, const int &default_value);
template long INIReader::get(const std::string &name, const std::string &section, const long &default_value);
template std::size_t INIReader::get(const std::string &name, const std::string &section, const std::size_t &default_value);
template bool INIReader::get(const std::string &name, const std::string &section, const bool &default_value);
template std::string INIReader::get(const std::string &name, const std::string &section, const std::string &default_value);

template<typename T> void INIReader::set(const std::string &name, const std::string &section, const T &value)
{
    std::string key = MakeKey(name, section);
    _values[key] = dataToString(value);
}
template void INIReader::set(const std::string &name, const std::string &section, const float &value);
template void INIReader::set(const std::string &name, const std::string &section, const double &value);
template void INIReader::set(const std::string &name, const std::string &section, const int &value);
template void INIReader::set(const std::string &name, const std::string &section, const long &value);
template void INIReader::set(const std::string &name, const std::string &section, const std::size_t &value);
template void INIReader::set(const std::string &name, const std::string &section, const bool &value);
template void INIReader::set(const std::string &name, const std::string &section, const std::string &value);


std::string INIReader::MakeKey(const std::string &name, const std::string &section)
{
    std::string key = section + "=" + name;
    // Convert to lower case to make section/name lookups case-insensitive
    std::transform(key.begin(), key.end(), key.begin(), ::tolower);
    return key;
}

int INIReader::ValueHandler(void* user, const char* section, const char* name,
                            const char* value)
{
    INIReader* reader = (INIReader*)user;
    std::string key = MakeKey(name, section);
    if (reader->_values[key].size() > 0)
        reader->_values[key] += "\n";
    reader->_values[key] += value;
    reader->_sections.insert(section);
    return 1;
}
