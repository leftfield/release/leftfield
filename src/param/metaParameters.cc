/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

// *********************************************************************
// eftlike
//
// metaParameters:
// - access to all meta parameters (i.e. settings) needed by forward models,
//   likelihoods, and the like
// - currently, just reads in parameter field based on INIReader; in future,
//   reading from command line arguments could be added
//
// Notes:
// - intended to supersede 'likeparam' class which hard-codes all metaparameters
//
// (C) Fabian Schmidt, 2021
// *********************************************************************

#include "metaParameters.h"

namespace LEFTfield {

// print values of given list of parameter names from given section to
// given stream
// - in one line
void metaParameters::print(const std::vector<std::string>& names,
			   const std::string& section,
			   FILE *fp) const
{
  fprintf(fp, "[%s] ", section.c_str());
  for (const auto& name : names) {
    if (have(name, section))
      fprintf(fp, "%s = %s; ", name.c_str(),
	       get<std::string>(name, section).c_str());
    else
      fprintf(fp, "%s = n/a ", name.c_str());
  }
  fprintf(fp, "\n");  
}
  

// *********************************************************************

// utility function: print most relevant parameters(
// (taken over from previous likeparam::print)

void print_param(const metaParameters& param, FILE *fp)
{
  fprintf(fp, "=== metaParameters from ini file '%s': ===\n",
	  param.get_filename().c_str());
  
  param.print({ "Omegam", "OmegaL", "hubble", "ns", "knl_today" },
	      "cosmology", fp);
  param.print({ "matterfile", "matter_redshift", "tracerfile", "tracer_redshift", "Rstar", "knl3Peps" }, "data", fp);
  param.print({ "outputfile", "prefix" }, "output", fp);
  param.print({ "alpha", "Lbox", "Lambda", "Lambda_scaling", "kmax", "kNyLambda", "tolerance" }, "metaparameters", fp);
  param.print({ "lptorder", "lptEdS", "NGeul", "supersampling" }, "gravity", fp);
  param.print({ "type", "biasorder", "renorm", "filtertype" }, "bias", fp);
  param.print({ "type", "filtertype" }, "likelihood", fp);
  
  fprintf(fp, "======================================\n");
}


} // end of namespace LEFTfield
