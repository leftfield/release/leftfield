/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef METAPARAMETERS_H
#define METAPARAMETERS_H

#include <stdio.h>
#include <string>
#include <vector>
#include <cassert>
#include <algorithm>
#include <map>

// *********************************************************************
// eftlike
//
// metaParameters:
// - access to all meta parameters (i.e. settings) needed by forward models,
//   likelihoods, and the like
// - currently, just reads in parameter field based on INIReader; in future,
//   reading from command line arguments could be added
// - usage:
//     metaParameters param("inifile.ini");
//     double alpha = param.get<double>("alpha", "section", 1.);
//     // template argument <double> is optional in this case
//
// Notes:
// - supersedes 'likeparam' class which hard-codes all metaparameters
// - to do: functionality to add parameters in code; requires modification of
//          INIReader
//
// (C) Fabian Schmidt, 2021
// *********************************************************************

#include <iostream>
#include "INIReader.h"
#include "parameter_base.h"

namespace LEFTfield {
	    
// *********************************************************************

class metaParameters {
 protected:
  std::string fn;
  sptr<INIReader> reader;

 public:
  metaParameters(const std::string &filename)
    : fn(filename), reader(make_sptr<INIReader>(filename))
  {  }

  ~metaParameters()
  { }

  std::string get_filename() const {
    return fn;
  }

  bool have(const std::string& name, const std::string &section) const
  {
    return reader->have(name, section);
  }

  // get parameter of type T given name, section name and default value
  template<typename T>
  T get(const std::string& name, const std::string &section, const T &defvalue)
  {
    return reader->get<T>(name, section, defvalue);
  }
  // get parameter of enum type T given name, section name and default value
  template<typename T>
  T getEnum(const std::string& name, const std::string &section, const T &defvalue)
  {
  return type_from_string<T>(
	  reader->get<std::string>(name, section, enum_to_string<T>(defvalue)) );
  }
  // get parameter of type T given name and section name
  template<typename T>
  T get(const std::string& name, const std::string section) const
  {
    return reader->get<T>(name, section);
  }
  // get parameter of enum type T given name and section name
  template<typename T>
  T getEnum(const std::string& name, const std::string &section) const
  {
  return type_from_string<T>(
		  reader->get<std::string>(name, section));
  }

  // print values of given list of parameter names from given section to
  // given stream
  // - in one line
  void print(const std::vector<std::string>& names,
	     const std::string& section,
	     FILE *fp) const;
  
};

// *********************************************************************

// utility function: print most relevant parameters
// (taken over from previous likeparam::print)

void print_param(const metaParameters&, FILE *fp);

// *********************************************************************
  
} // end of namespace LEFTfield

#endif
