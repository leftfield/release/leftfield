/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include "parameter_base.h"
#include <math.h>


  



namespace LEFTfield {

// **********************************************************************

// convert enum types from/to string
// - for printing and reading from ini file
// - unfortunately, these need to be updated whenever the enums change
std::string stringEnum<opset_type>::to_string(const opset_type& type)
{
  switch (type) {
  case NOPTYPE: return "NOPTYPE";
  case LINEAR: return "LINEAR";
  case SECOND: return "SECOND";
  case THIRD: return "THIRD";
  case LPT: return "LPT";
  case LPTGENERAL: return "LPTGENERAL";
  default: {
    logger.printf("to_string(enum)", ESSENTIAL, "unknown type %d", type);
    return "[UNKNOWN TYPE]";
  }
  }
}

std::string stringEnum<like_type>::to_string(const like_type& type)
{
  switch (type) {
  case NOLTYPE: return "NOLTYPE";
  case FOURIER: return "FOURIER";
  case REAL: return "REAL";
  case MARGFOURIER: return "MARGFOURIER";
  case MARGREAL: return "MARGREAL";
  case MARGREALGENERAL: return "MARGREALGENERAL";
  case REALSIMPLE: return "REALSIMPLE";
  default: {
    logger.printf("to_string(enum)", ESSENTIAL, "unknown type %d", type);
    return "[UNKNOWN TYPE]";
  }
  }
}


std::string stringEnum<sharpkFilterType>::to_string(const sharpkFilterType& type)
{
  switch (type) {
  case NOFILTER: return "NOFILTER";
  case SPHERE: return "SPHERE";
  case CUBE: return "CUBE";
  default: {
    logger.printf("to_string(enum)", ESSENTIAL, "unknown type %d", type);
    return "[UNKNOWN TYPE]";
  }
  }
}

// the following don't need to be updated when the enums change, as long
// as END_* items remain
  
opset_type stringEnum<opset_type>::type_from_string(const std::string& str)
{
  for (st it=0; it < END_opset_type; ++it)  {
    opset_type type = static_cast<opset_type>(it);
    if (str == std::string(to_string(type)))
      return type;
  }

  NT_fail("ERROR in opset_type_from_string: cannot convert '%s' to known type.", str.c_str());
  return END_opset_type;
}

like_type stringEnum<like_type>::type_from_string(const std::string& str)
{
  for (st it=0; it < END_like_type; ++it)  {
    like_type type = static_cast<like_type>(it);
    if (str == std::string(to_string(type)))
      return type;
  }

  NT_fail("ERROR in like_type_from_string: cannot convert '%s' to known type.", str.c_str());
  return END_like_type;
}

sharpkFilterType stringEnum<sharpkFilterType>::type_from_string(const std::string& str)
{
  for (st it=0; it < END_sharpkFilterType; ++it)  {
    sharpkFilterType type = static_cast<sharpkFilterType>(it);
    if (str == std::string(to_string(type)))
      return type;
  }

  NT_fail("ERROR in sharpkFilterType_from_string: cannot convert '%s' to known type.", str.c_str());
  return END_sharpkFilterType;
}

// **********************************************************************

} // end of namespace LEFTfield
