/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef PARAMETER_BASE_H
#define PARAMETER_BASE_H

#include <stdio.h>
#include <string>
#include <vector>
#include <cassert>
#include <algorithm>

// *********************************************************************
// eftlike
//
// parameter_base:
// - define some relevant types and enums
// - struct likeparam
//
// FIXME: find better name...
//
// (C) Fabian Schmidt, 2019
// *********************************************************************

#include "grid.h" // for types

// *********************************************************************

namespace LEFTfield {

// type of operator_set:
enum opset_type {
  NOPTYPE=0, LINEAR=1,
  SECOND, // second-order Eulerian
  THIRD,  // third-order Eulerian
  LPT,    // Lagrangian bias expansion using total M_ij^(m) at each order
  LPTGENERAL,    // Lagrangian bias expansion using all shapes
  END_opset_type
};

// type of likelihood:
enum like_type {
  NOLTYPE=0, FOURIER=1, REAL, MARGFOURIER, MARGREAL, MARGREALGENERAL, REALSIMPLE, MIXED,
  END_like_type
};


// convert enum types from/to string
// - for printing and reading from ini file
// - use class template specialization rather than function template specialization (see e.g. http://www.gotw.ca/publications/mill17.htm)
// - unfortunately, the stringEnum<T>::to_string functions need to be updated whenever the enums change

template<typename enumT>
struct stringEnum; // no unspecialized definition; compile-time error in case of attempted use on other type than those defined below

template<typename enumT>
inline std::string enum_to_string(const enumT& T)
{
  return stringEnum<enumT>::to_string(T);
}

template<typename enumT>
inline enumT type_from_string(const std::string& str)
{
  return stringEnum<enumT>::type_from_string(str);
}

// specializations
template<>
struct stringEnum<sharpkFilterType> {
  static std::string to_string(const sharpkFilterType& T);
  static sharpkFilterType type_from_string(const std::string&);
};

template<>
struct stringEnum<opset_type> {
  static std::string to_string(const opset_type& T);
  static opset_type type_from_string(const std::string&);
};

template<>
struct stringEnum<like_type> {
  static std::string to_string(const like_type& T);
  static like_type type_from_string(const std::string&);
};

// *********************************************************************

} // end of namespace LEFTfield

#endif
