/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef __INIREADER_H__
#define __INIREADER_H__

#include <map>
#include <set>
#include <string>

// Read an INI file into easy-to-access name/value pairs. (Note that I've gone
// for simplicity here rather than speed, but it should be pretty decent.)
class INIReader
{
private:
    std::map<std::string, std::string> _values;
    std::set<std::string> _sections;
    static std::string MakeKey(const std::string &name, const std::string &section);
    static int ValueHandler(void* user, const char* section, const char* name,
                            const char* value);

    // Get a string value from INI file, throwing an exception if not found.
    std::string Get(const std::string &name, const std::string &section) const;
    // Get a string value from INI file, returning default_value if not found.
    std::string Get(const std::string &name, const std::string &section,
                    const std::string &default_value);

public:
    // Empty Constructor
    INIReader() {};

    // Construct INIReader and parse given filename. See ini.h for more info
    // about the parsing. Throws an exception on error.
    INIReader(const std::string &filename);

    // Construct INIReader and parse given file. See ini.h for more info
    // about the parsing. Throws an exception on error.
    INIReader(FILE *file);

    // Return the list of sections found in ini file
    const std::set<std::string>& Sections() const
      { return _sections; }

    // Return true iff the parameter "name" is present in "section"
    bool have(const std::string &name, const std::string &section) const;

    // generic "getter" functions
    template<typename T> T get(const std::string &name, const std::string &secion) const;
    template<typename T> T get(const std::string &name, const std::string &section, const T &default_value);
    // generic "setter" function
    template<typename T> void set(const std::string &name, const std::string &section, const T &value);
};

#endif  // __INIREADER_H__
