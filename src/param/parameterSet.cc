/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include "parameterSet.h"
#include <math.h>


#include "INIReader.h"


namespace LEFTfield {

// evaluate prior for current parameter values
double parameterSet::getPriorPseudoChi2() const
{
  double f=0.;
  for (auto &p: objs)
    f += p.second.getPriorPseudoChi2();
  
  return f;
}



// *** read parameters from given file ***
void parameterSet::readIniFile(const std::string &file,			       
			       const std::string &parsection,
			       const std::string &priorsection)
{
  INIReader reader(file);

  for (auto &p: objs) {
    // ignore parameters without names
    auto name = p.first;
    if (! name.length() )  continue;

    // 1) search for parameter values and limits
    if (reader.have(name, parsection))  {
      std::istringstream iss( reader.get<std::string>(name, parsection) );

      std::vector<double> val;
      std::string item;
      while (std::getline(iss, item, ',')) {
	val.push_back(std::stod(item));
      }

      // value
      if (val.size() > 0)  p.second.val = val[0];
      // limits
      if (val.size() > 2) {
	p.second.lowlim = val[1];
	p.second.uplim = val[2];
      }
      if (val.size() > 3)  p.second.step = val[3];
    }

    // 2) search for fixed flag (with existing value as default)
    p.second.fixed = reader.get<bool>(name + std::string("_fix"),
				      parsection,
				      p.second.fixed);

    // 3) search for priors
    if (reader.have(name, priorsection)) {
      std::istringstream iss2( reader.get<std::string>(name, priorsection) );

      std::vector<double> val;
      std::string item;
      while (std::getline(iss2, item, ',')) {
	val.push_back(std::stod(item));
      }

      // limits
      if (val.size() >= 2) {
	p.second.priormean = val[0];
	p.second.priorsigma = val[1];
      }
    }
  }
  
  // info: print out result
  if (logger.getLevel() >= INFO) {
    logger.printf("parameterSet::readIniFile", INFO, "=== Parameter values, ranges and priors after reading section '%s' from '%s': ===",
		  parsection.c_str(), file.c_str());
    print(stderr, true);
  }
}



// print out parameters with names
// - if full==true, also print limits, step size, and priors
void parameterSet::print(FILE *fp, const bool full) const
{
  if (!full)  { // digest
    for (const auto &p: objs) {
      fprintf(fp, "%s = %.5e +- %.5e\n", p.first.c_str(),
	      p.second.val, p.second.error);
    }
    return;
  }

  for (const auto &p: objs) {
    fprintf(fp, "%s = %.5e +- %.5e (limits: [%.5e, %.5e], step size %.5e), %s, prior %.5e +- %.5e\n", p.first.c_str(),
	    p.second.val, p.second.error, p.second.lowlim, p.second.uplim, p.second.step,
	    p.second.fixed ? "fixed" : "free", p.second.priormean, p.second.priorsigma);
  }
}

} // end of namespace LEFTfield
