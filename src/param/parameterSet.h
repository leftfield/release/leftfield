/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef PARAMETER_SET_H
#define PARAMETER_SET_H

#include <stdio.h>
#include <string>
#include <vector>
#include <cassert>
#include <algorithm>
#include <map>

// *********************************************************************
// eftlike
//
// parameterSet: encapsulate set of parameters
//
// Notes:
// - attempted different implementations, this one seems the least bureaucratic
//
// (C) Fabian Schmidt, 2021
// *********************************************************************

#include "namedSet.h"
#include "operatorSet.h"

namespace LEFTfield {

// *********************************************************************

class parameter {
public:
  double val; // parameter value
  double lowlim, uplim; // upper and lower limits
  double error, step; // inferred error, step for samplers etc. 
  bool fixed; // whether fixed or not
  double priormean, priorsigma; // mean and sqrt(variance) of Gaussian prior

  parameter(double val_=0., double lowlim_=-1.e30,
	    double uplim_=1.e30, double step_=0.1, double error_=0.,
	    bool fixed_=false, double priormean_=0., double priorsigma_=-1.)
    : val(val_), lowlim(lowlim_), uplim(uplim_), error(error_), step(step_),
      fixed(fixed_), priormean(priormean_), priorsigma(priorsigma_)
  { }

  // operator double() const { // let's see if this does what is desired...
  //   return val;
  // } // it does not...

  double getPriorPseudoChi2() const
  {
    if (fixed || !(priorsigma > 0.)) return 0.;
    return (val - priormean)*(val - priormean) / priorsigma * priorsigma
      + log(2.*M_PI * priorsigma * priorsigma); // normalization
  }
  
  double getPriorPseudoChi2Gradient() const
  {
    if (fixed || !(priorsigma > 0.)) return 0.;
    return 2.*(val - priormean)/(priorsigma*priorsigma);
  }
};
	    
// *********************************************************************

class parameterSet : public namedSet<parameter> {
 protected:
  
 public:
  parameterSet()
    : namedSet<parameter>()
  { }

  parameterSet(const std::map<std::string, parameter>& nameval)
    : namedSet<parameter>(nameval)
  { }

  virtual ~parameterSet()
  { }

  // initialize bias parameter array from operator set, by prepending basename
  // to operator name
  // - e.g., basename = "b_" -> standard bias naming convention
  //                  = "sigma_" -> standard stochastic parameter convention
  parameterSet(const operatorSet& opset,
	       const std::string& basename = "b_")
  {
    for (const auto &op: opset)
      add(basename+op.first, parameter(0.));
  }

  
  // get values themselves
  std::vector<double> get_values() const {
    std::vector<double> v;
    for (const auto &p: objs)  v.push_back(p.second.val);
    return v;
  }
  
  // evaluate prior for current parameter values
  double getPriorPseudoChi2() const;
  
  double getPriorLogLikelihood() const {
    return -0.5 * getPriorPseudoChi2();
  }

  // compute gradient of prior for parameter i and current parameter values
  // - called by likelihood::maximize
  double getPriorPseudoChi2Gradient(const std::string &name) const {
    return objs.at(name).getPriorPseudoChi2Gradient();
  }


  // *** read parameters from given ini file ***
  // - optionally specify section from which to read parameter values, limits
  //   and step sizes, and from which to read priors
  void readIniFile(const std::string &file,
		   const std::string &parsection = "parameter_values",
		   const std::string &priorsection = "parameter_priors");
  
  // *** print out parameters ***
  // - if full==true, also print limits, step size, and priors
  void print(FILE *fp, const bool full=false)  const;
  
};

// *********************************************************************

} // end of namespace LEFTfield

#endif
