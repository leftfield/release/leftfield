/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <math.h>
#include <cassert>

#include "cosmology/cosmological.h"
#include "utilities/integrate.h"

// ********************************************************************

namespace LEFTfield {

// - return slope of linear P(k) from fit
// -- smoothes over BAO wiggles
double exppar_LCDM_scaling::dlnPLdlnk(const double k) const
{
  static const double ninf = -2.4, cfit = 2.166, afit = 0.2825;

  double x = k / (0.073*Om*h); // x = k/k_eq
  return ninf + (ns-ninf) * pow(1. + cfit*x*x, -afit);
}


// print interesting info on which operators are relevant up to given
// order in delta
// - for diagnostic purposes
void exppar_LCDM_scaling::print_relevant_indices(const double Lambda, const st order_in_delta) const
{
  double nL = dlnPLdlnk(Lambda);
  // scaling index of deterministic contribution at 'order_in_delta':
  double indexDet = double(order_in_delta) * (3.+nL)/2.;
  // max. corresponding order in stochastic terms (for single power of epsilon):
  double indexStoch = indexDet - 1.5;
  double order_in_stoch = indexStoch / ((3.+nL)/2.);

  fprintf(stderr, "Lambda = %.2f, nL = %.2f, order_in_delta = %zu: indexDet = %.2f, order of O for eps_O O: %.1f\n",
	  Lambda, nL, order_in_delta, indexDet, order_in_stoch);
    
  for (st order=1; order < order_in_delta; order++)  {      
    // max. higher-derivative order for terms at order 'order' in delta,
    // assuming 1/R_* ~ k_NL:
    double order_in_deriv = double(order_in_delta - order) * (3.+nL)/2. / 2.;
    fprintf(stderr, "At order %zu, order in deriv. = %.1f\n", order,
	    order_in_deriv);
  }
}

// returns order of O to be included in eps_O given deterministic order
// considered
st exppar_LCDM_scaling::get_stochastic_order(const double Lambda, const st order_in_delta) const
{
  double nL = dlnPLdlnk(Lambda);
  // scaling index of deterministic contribution at 'order_in_delta':
  double indexDet = double(order_in_delta) * (3.+nL)/2.;
  // max. corresponding order in stochastic terms (for single power of epsilon):
  double indexStoch = indexDet - 1.5;
  double order_in_stoch = indexStoch / ((3.+nL)/2.);
  
  return st(round(order_in_stoch));
}

// returns order of derivatives to be included in operator of order 'order_O'
// given deterministic order considered
st exppar_LCDM_scaling::get_derivative_order(const double Lambda, const st order_in_delta,
			  const st order_O) const
{
  if (order_O >= order_in_delta) return 0;
  
  double nL = dlnPLdlnk(Lambda);
  
  // max. higher-derivative order for terms at order 'order' in delta,
  // assuming 1/R_* ~ k_NL:
  double order_in_deriv = double(order_in_delta - order_O) * (3.+nL)/2. / 2.;
  
  return st(round(order_in_deriv));
}

// compare given op_order objects based on pre-set value of Lambda
bool exppar_LCDM_scaling::is_higher_order(const op_order& oo,
				 const op_order& compare_to) const
{
  // make sure Lambda is set
  NT_assert(Lambda > 0., "exppar_LCDM_scaling::is_higher_order: need to set cutoff Lambda to nonzero value.");
  double nL = dlnPLdlnk(Lambda);
  double ind1 = compute_index(oo, nL), ind2 = compute_index(compare_to, nL);

  // allow for tolerance
  return ind1 > ind2 + tolerance;
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

// compute order of operator with given 'op_order'
double exppar_LCDM_Rstar::compute_order(const op_order& oo) const
{
  // compute expansion parameters
  double nL = dlnPLdlnk(Lambda);
  double epsnl = pow(Lambda/knl, 0.5*(3.+nL));
  double epsstoch = sqrt(knl3Peps) * pow(Lambda/knl, 1.5);
  double epsderiv = pow(Lambda*Rstar, 2.);
  
  // info
  static double LambdaP = -1.;
  if (LambdaP != Lambda) {
    logger.printf("exppar_LCDM_Rstar::compute_order", INFO,
		  "Lambda = %.2f, knl = %.2f, knl3Peps = %.3e, epsnl = %.3e, epsstoch = %.3e, epsderiv = %.3e",
		  Lambda, knl, knl3Peps, epsnl, epsstoch, epsderiv);
    LambdaP = Lambda;
  }
  
  // multiply expansion parameters with appropriate powers
  double order = pow(epsnl, double(oo.pt));
  if (epsderiv > 0.)
    order *= pow(epsderiv, double(oo.deriv));
  if (epsstoch > 0.)
    order *= pow(epsstoch, double(oo.stoch));

  return order;
}


// compare given op_order objects based on pre-set value of Lambda
bool exppar_LCDM_Rstar::is_higher_order(const op_order& oo,
					const op_order& compare_to) const
{
  // make sure Lambda is set
  NT_assert(Lambda > 0., "exppar_LCDM_Rstar::is_higher_order: need to set cutoff Lambda to nonzero value.");
  double ord1 = compute_order(oo), ord2 = compute_order(compare_to);

  // allow for tolerance (defined here as 2^-tolerance)
  return ord1 < ord2 * pow(0.5, tolerance);
}




} // end of namespace LEFTfield
