/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef COSMOLOGICAL_H
#define COSMOLOGICAL_H

// ********************************************************************
// classes/structures providing relevant information on background
// cosmology
//
// IN PROGRESS
//
// class growthLCDM:
//
// - calculates growth factor, growth rate, and related quantities
//   for different cosmologies
// - right now, just growthLCDM
//
// (C) Fabian Schmidt, 2020
// ********************************************************************

#include <array>
#include "types.h"
#include "log.h"
#include "param/metaParameters.h"
#include "cosmology/expansion_parameters.h"

namespace LEFTfield {


// ********************************************************************
// abstract growth class
// - interface/functionality TBD


// ********************************************************************

namespace ns_growth {
  void Dint (double a, const std::array<double,2> &y, std::array<double,2> &f);
}

class growthLCDM
{

 protected:
  double Om, OL, OK, ain;
  double D0;

  double lnaprev;
  std::array<double,2> y;

  // E2 = H^2(a) / H_0^2
  // HpH = dln H/da
  // Omofa = Omega_m(a)
  void calc_exp_rate(const double a, double& E2, double& HpH,
		     double& Omofa) const    
  {
    double a2 = a*a;
    double oma3 = Om/(a2*a);
    double oka2 = OK/a2;
    // future: wCDM
    /* double fx = wCDM ? pow(a, wexp) : 1.; // a^-3(1+w) */
    //(1.-Om-OK) * fx;
    E2 = oma3 + oka2 + OL; 
    HpH = (-1.5*oma3 - oka2) / (a*E2);
    Omofa = oma3 / E2;
  }

  friend void ns_growth::Dint(double a, const std::array<double,2> &y, std::array<double,2> &f);

 public:
  // parameters needed for growth as well as scaling index calculation
  growthLCDM(const double Omega_m, const double Omega_Lambda,
	     const double a_in = 1.e-5);

  ~growthLCDM()
    { }

  // *** expansion history ***
  
  double HubbleNormOfa(const double a) const
  {
    double E2, HpH, Omofa;
    calc_exp_rate(a, E2, HpH, Omofa);
    return sqrt(E2);
  }

  double OmegamOfa(const double a) const
  {
    double E2, HpH, Omofa;
    calc_exp_rate(a, E2, HpH, Omofa);
    return Omofa;
  }
  
  
  // *** linear growth factors ***

  double growthFactorOfa(const double a, double *dlnDdlna=0);

  double growthFactorOfz(const double z, double *dlnDdlna=0)
  {
    return growthFactorOfa(1./(1.+z), dlnDdlna);
  }

  double growthFactorNormOfa(const double a, double *dlnDdlna=0)
  {
    return growthFactorOfa(a, dlnDdlna) / D0;
  }

  double growthFactorNormOfz(const double z, double *dlnDdlna=0)
  {
    return growthFactorOfz(z, dlnDdlna) / D0;
  }
  
  // in future: higher-order growth factors

};



// ********************************************************************
// setup growthLCDM and expansion_parameters objects given settings in param

struct cosmo_info {
  sptr<growthLCDM> g;
  sptr<expansion_parameters> ep;
};

cosmo_info get_cosmo_info(metaParameters &param);

} // end of namespace LEFTfield

#endif
