/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef EXPANSION_PARAMETERS_H
#define EXPANSION_PARAMETERS_H

// ********************************************************************
// expansion parameters:
// - allow for comparison of operator orders (op_order)
//
// (C) Fabian Schmidt, 2020
// ********************************************************************

#include "types.h"
#include "log.h"
#include "param/metaParameters.h"

namespace LEFTfield {

// ********************************************************************
// abstract expansion_parameters class
// - main functionality is to allow to compare two op_order objects
// - this comparison can be done based on scaling indices, information
//   on higher-derivative scale, etc.

class expansion_parameters
{
 protected:
  double tolerance; // add tolerance when deciding on higher order
  
 public:
  expansion_parameters(const double tol=0.)
    : tolerance(tol)
    {  }

  virtual ~expansion_parameters()
    { }

  void set_tolerance(const double tol)
  {
    tolerance = tol;
  }
  double get_tolerance() const
  {
    return tolerance;
  }

  virtual bool is_higher_order(const op_order& oo,
			       const op_order& compare_to) const
    = 0;

  bool is_lowerequal_order(const op_order& oo,
			   const op_order& compare_to) const
  {
    return ! is_higher_order(oo, compare_to);
  }

};

// ********************************************************************
// expansion parameters for LCDM

// - based on scaling
class exppar_LCDM_scaling : public expansion_parameters
{
 protected:
  double Om, h, ns;
  double Lambda; // cutoff to use for index evaluation

  // compute scaling index of operator with given 'op_order'
  // - nL = dln P_L/dln k
  double compute_index(const op_order& oo, const double nL) const {
    double ind = double(oo.pt) * 0.5 * (3.+nL);
    ind += double(oo.deriv) * 2.;
    ind += double(oo.stoch) * 1.5;
    return ind;
  }
  
 public:
  exppar_LCDM_scaling(const double Omega_m, const double hubble,
		      const double n_s, const double Lambdas,
		      const double tol = 0.)
    : expansion_parameters(tol), Om(Omega_m), h(hubble), ns(n_s), Lambda(Lambdas)
  { }

  virtual ~exppar_LCDM_scaling()
    { }
  
  // set cutoff value
  void setLambda(const double L) {
    Lambda = L;
  }
  double getLambda() const {
    return Lambda;
  }  

  // *** scaling indices and relevant orders ***

  // - return slope of linear P(k) from fit
  // -- smoothes over BAO wiggles
  double dlnPLdlnk(const double k) const;

  // print interesting info on which operators are relevant up to given
  // order in delta
  // - for diagnostic purposes
  void print_relevant_indices(const double Lambda,
			      const st order_in_delta) const;

  // returns order of O to be included in eps_O given deterministic order
  // considered
  st get_stochastic_order(const double Lambda, const st order_in_delta) const;

  // returns order of derivatives to be included in operator of order 'order_O'
  // given deterministic order considered
  st get_derivative_order(const double Lambda, const st order_in_delta,
			  const st order_O) const;

  // compare given op_order objects based on pre-set value of Lambda
  virtual bool is_higher_order(const op_order& oo,
			       const op_order& compare_to) const;
};

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

// - based on values of k_NL and R_*
class exppar_LCDM_Rstar : public exppar_LCDM_scaling
{
  double knl, Rstar; // knl, R_*
  double knl3Peps;   // knl^3 P_eps to estimate stochastic expansion parameter

  // compute order of operator with given 'op_order'
  double compute_order(const op_order& oo) const;

 public:
  // construct given cosmological parameters, Lambda (for index), k_NL,
  // and R_star
  // - if Dnorm != 1, rescale k_nl assuming P_L,target/P_L,ref = Dnorm^2
  exppar_LCDM_Rstar(const double Omega_m, const double hubble,
		    const double n_s, const double Lambdas,
		    const double k_nl, const double R_star,
		    const double kNL3Peps,
		    const double Dnorm = 1., const double tol = 0.)
    : exppar_LCDM_scaling(Omega_m, hubble, n_s, Lambdas, tol),
    knl(k_nl), Rstar(R_star), knl3Peps(kNL3Peps)
  {
    if (Dnorm != 1.)  { // rescale k_nl by given growth ratio
      double nn = dlnPLdlnk(knl)+3.;
      knl *= pow(Dnorm, -2./nn);
      // correspondingly rescale knl3Peps
      knl3Peps *= pow(Dnorm, 3. * -2./nn);
      
      // debug
      logger.printf("exppar_LCDM_Rstar", INFO,
		    "Dnorm = %.2f, k_nl = %.4f h/Mpc\n", Dnorm, knl);
    }      
  }

  virtual ~exppar_LCDM_Rstar()
    { }

  // compare given op_order objects based on pre-set value of Lambda
  // - tolerance defined here as including orders suppressed by 2^-tolerance
  virtual bool is_higher_order(const op_order& oo,
			       const op_order& compare_to) const;
};




} // end of namespace LEFTfield

#endif
