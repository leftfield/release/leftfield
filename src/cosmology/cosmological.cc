/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <math.h>
#include <cassert>

#include "cosmology/cosmological.h"
#include "utilities/integrate.h"

// ********************************************************************

namespace LEFTfield {

// ********************************************************************

using namespace ns_growth;

namespace ns_growth {

  const growthLCDM *growthInt = 0;

  // vs ln a
  // - copied from flatLCDMcosmology.cc
  void Dint (double lna, const std::array<double,2> &y, std::array<double,2> &f)
  {
    double a = exp(lna), a2 = a*a;
    // (aH)^2 / H_0^2
    double aH2 = growthInt->Om / a + growthInt->OL * a2 + growthInt->OK;
    // d(aH)^2 / dln a / H_0^2
    double aH2p = - growthInt->Om / a + 2.*growthInt->OL * a2;

    f[0] = y[1];
    f[1] = -(0.5*aH2p/aH2 + 1.)*y[1] + 1.5 * growthInt->Om / (a*aH2) * y[0];
  }


}



growthLCDM::growthLCDM(const double Omega_m, const double Omega_Lambda,
		       const double a_in)
  : Om(Omega_m), OL(Omega_Lambda), OK(1.-Om-OL),
    ain(a_in), D0(0.)
{
  D0 = growthFactorOfa(1.);
  logger.printf("growthLCDM", DEBUG, "done initializing. D(a=1) = %.5f.", D0);
}



double growthLCDM::growthFactorOfa(const double a, double *dlnDdlna)
{
  double lna = log(a);
  if (!(D0 > 0.) || lnaprev-lna > 0.1)  {
    // integrate from beginning
    // D(a_in), dD/da(a_in): EdS solution
    y[0] = ain;
    y[1] = ain;
    lnaprev = log(ain);
  }

  // otherwise, integrate from previous point
  growthInt = this;
  double h = 1.e-3;
  odeint(y, lnaprev, lna, 1.e-8, h, 1.e-10, 1.e-3, Dint);
  lnaprev = lna;
  growthInt = 0;

  if (dlnDdlna)  *dlnDdlna = y[1]/y[0];
  
  return y[0];
}


// ********************************************************************

cosmo_info get_cosmo_info(metaParameters &param)
{
  cosmo_info ci;
  const double Om = param.get("Omegam", "cosmology", 0.3),
    OL = param.get("OmegaL", "cosmology", 0.7),
    h = param.get("hubble", "cosmology", 0.7),
    ns = param.get("ns", "cosmology", 0.967);
  const double Rstar = param.get("Rstar", "data", 0.);
  // default value for Lambda_s...?
  const double Lambda_s = param.get("Lambda_scaling", "metaparameters", 0.1);
  
  // allocate growthLCDM object with cosmology from likeparam
  ci.g = make_sptr<growthLCDM>(Om, OL);

  // expansion_parameters object, depending on whether Rstar is set
  if (Rstar > 0.)  {
    logger.printf("get_cosmo_info", INFO, "setting up exppar_LCDM_Rstar with Rstar = %.2f.", Rstar);
    // const double Dnorm = ci.g->growthFactorOfz(param.z)
    //   / ci.g->growthFactorOfz(0.);
    // - always compute scaling dimensions for z=0
    const double Dnorm = 1.;
    ci.ep = make_sptr<exppar_LCDM_Rstar>(Om, h, ns,
					 Lambda_s,
					 param.get("knl_today", "cosmology", 0.25),
					 Rstar,
					 param.get("knl3Peps", "data", 0.),
					 Dnorm,
					 param.get("tolerance", "metaparameters", 0.));
  } else {
    logger.printf("get_cosmo_info", INFO, "setting up exppar_LCDM_scaling with Lambda_scaling = %.2f.", Lambda_s);
    ci.ep = make_sptr<exppar_LCDM_scaling>(Om, h, ns, Lambda_s,
					   param.get("tolerance", "metaparameters", 0.));
  }
  
  return ci;
}

} // end of namespace LEFTfield
