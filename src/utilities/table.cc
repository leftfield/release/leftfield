/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include "utilities/table.h"
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <string.h>


namespace LEFTfield {

void polint(const double xa[], const double ya[], 
	    const int n, const double x, double *y, double *dy);

void splint(const double xa[], const double ya[], const double y2a[], 
	    const int n, const double x, double *y);

void spline(const double x[], const double y[], const int n, 
	    const double yp1, const double ypn, double y2[]);


// ***************************************************************
// table member functions

table::table(const std::string &filename, const int col_x, const int col_y,
	     const bool log_x, const bool log_y,
	     const bool extrapolate)
  : N(0), ii(0), xx(0), yy(0), y2(0), extrapol(extrapolate)
{
  std::ifstream ifs(filename.c_str());
  NT_assert(ifs.is_open(), "table::table: couldn't open file: ", filename.c_str());

  const int Nc = col_x > col_y ? col_x : col_y;
  double val[Nc];
  char str[1024];
  while (!ifs.eof()) {
    ifs.getline(str, 1024);
    if (str[0] == '#' || strlen(str) < 5)  continue;
    std::istringstream iss(str);
    for (int i=0; i < Nc; i++)  iss >> val[i];
    // make |val| an option?
    // if (log_x && ! (val[col_x-1] > 0.))  continue;
    // if (log_y && ! (val[col_y-1] > 0.))  continue;
    xx.push_back(log_x ? log(fabs(val[col_x-1])) : val[col_x-1]);
    yy.push_back(log_y ? log(fabs(val[col_y-1])) : val[col_y-1]);
  }

  logger.printf("table::table", INFO, "read in %zu lines from '%s'.",
		xx.size(), filename.c_str());
  NT_assert(xx.size() > 0, "table::table: failed to read any lines from file %s", filename.c_str());

  N = xx.size();
}

// copy constructor
table::table(const table& t)
  : N(t.N), ii(0), xx(t.xx), yy(t.yy), y2(0), extrapol(t.extrapol),
    writefile(t.writefile)
{}
  

// linear interpolation
double table::interpLin(const double x) const
{
  st i=0;
  while (i < N && x > xx[i]) i++;
  if (!extrapol && (!i || i == N))  return 0.;
  i--;
#ifdef INTERP_BEYOND_TABLE
  if (!i) i++;
  else if (i == N)  i -= 2;
#else
  if (!i || i == N) {
    logger.printf("table::interpLin", ESSENTIAL,
		  "x = %.3e beyond range of table ! Returning 0.", x);
    return 0.;
  }
#endif

  i--;
  return yy[i] + (x-xx[i]) / (xx[i+1]-xx[i]) * (yy[i+1]-yy[i]);
}



// polynomial interpolation with polynomial of degree npol-1
double table::interpPol(const double x, const st npol) const
{
  NT_assert(npol <= N, "table::interpPol: requested npol is greater than array dimension.");

  st i=0;
  while (i < N && x > xx[i]) i++;
#ifndef INTERP_BEYOND_TABLE
  if (!i || i == N) {
    logger.printf("table::interpPol", ESSENTIAL,
		  "x = %.3e beyond range of table ! Returning 0.", x);
    return 0.;
  }
#endif
  if (!extrapol && (!i || i == N))  return 0.;
  i--;
  // now x is between x[i] and x[i+1]
  i = (i<(npol-1)/2) ? 0 : i-(npol-1)/2;
  if (i+npol > N)  i = N-npol;
  // polint takes array values x[1]...x[npol]
  i--;

  double y, dy;
  polint(&xx[i], &yy[i], npol, x, &y, &dy);
  return y;
}

// * deriving *
// uses polynomial interpolation
double table::derivePol(const double x, const double dx, 
			const int npol) const
{
  return 0.5/dx * (interpPol(x+dx, npol) - interpPol(x-dx, npol));
}

double table::dderivePol(const double x, const double dx, 
			 const int npol) const
{
  double dx2 = 2.*dx;
  return (interpPol(x+dx2, npol) + interpPol(x-dx2, npol)
	  - 2.*interpPol(x, npol)) / (dx2*dx2);
}


// spline interpolation 
double table::interpSpline(const double x) const
{
  if (y2.empty()) {  // setup vector of 2nd derivatives
    y2.resize(N);
    spline(xx.data()-1, yy.data()-1, N, 1.e30, 1.e30, y2.data()-1);
  }

  // check whether extrapolating
  if (!extrapol && (x < xmin() || x >= xmax()))  return 0.;

  // interpolate
  double y=0.;
  splint(xx.data()-1, yy.data()-1, y2.data()-1, N, x, &y);
  return y;
}

// * deriving with spline *
double table::deriveSpline(const double x, const double dx) const
{
  return 0.5/dx * (interpSpline(x+dx) - interpSpline(x-dx));
}

double table::dderiveSpline(const double x, const double dx) const
{
  double dx2 = 2.*dx;
  return (interpSpline(x+dx2) + interpSpline(x-dx2)
	  - 2.*interpSpline(x)) / (dx2*dx2);
}


void table::writeToFile(const std::string &filename) const
{
  FILE *fp = fopen(filename.c_str(), "w");
  NT_assert(fp != NULL, "table::writeToFile: couldn't open file.");

  fprintf(fp, "# Table output, N = %zu\n", N);
  for (st i=0; i < N; i++)
    fprintf(fp, "%.12e  %.12e\n", xx[i], yy[i]);
  fclose(fp);
}
 
void table::readFromFile(const std::string &filename)
{
  FILE *fp = fopen(filename.c_str(), "r");
  NT_assert(fp != NULL, "table::readFromFile: couldn't open file.");

  int ret = fscanf(fp, "# Table output, N = %zu\n", &N);
  NT_assert(ret != 0, "table::readFromFile: read failure.");

  logger.printf("table::readFromFile", INFO, "reading %zu lines.\n", N);
  xx.resize(N);
  yy.resize(N);
  for (st i=0; i < N; i++)  {
    ret = fscanf(fp, "%lf  %lf\n", &xx[i], &yy[i]);
    NT_assert(ret == 2, "table::readFromFile: read failure.");
  }

  fclose(fp);
  ii = N;
}


void table::readFromFile(const std::string &filename, const std::string &format)
{
  FILE *fp = fopen(filename.c_str(), "r");
  NT_assert(fp != NULL, "table::readFromFile: couldn't open file.");

  int ret = fscanf(fp, "# Table output, N = %zu\n", &N);
  NT_assert(ret != 0, "table::readFromFile: read failure.");

  logger.printf("table::readFromFile", INFO, "reading %zu lines.\n", N);
  xx.resize(N);
  yy.resize(N);
  for (st i=0; i < N; i++)  {
    ret = fscanf(fp, format.c_str(), &xx[i], &yy[i]);
    NT_assert(ret == 2, "table::readFromFile: read failure.");
  }
  
  fclose(fp);
  ii = N;
}



// ********************************************************************
// Polynomial interpolation (NR)

void polint(const double xa[], const double ya[], 
	    const int n, const double x, double *y, double *dy)
{

	int i,m,ns=1;
	double den,dif,dift,ho,hp,w;
	double c[n+1], d[n+1];

	dif=fabs(x-xa[1]);
	for (i=1;i<=n;i++) {
		if ( (dift=fabs(x-xa[i])) < dif) {
			ns=i;
			dif=dift;
		}
		c[i]=ya[i];
		d[i]=ya[i];
	}
	*y=ya[ns--];
	for (m=1;m<n;m++) {
		for (i=1;i<=n-m;i++) {
			ho=xa[i]-x;
			hp=xa[i+m]-x;
			w=c[i+1]-d[i];
			NT_assert( (den=ho-hp) != 0.0, "Error in polint.");
			den=w/den;
			d[i]=hp*den;
			c[i]=ho*den;
		}
		*y += (*dy=(2*ns < (n-m) ? c[ns+1] : d[ns--]));
	}
}

// *********************************************************************
// Spline interpolation (NR)

  
void splint(const double xa[], const double ya[], const double y2a[], 
	    const int n, const double x, double *y)
{
	int klo,khi,k;
	double h,b,a;

	klo=1;
	khi=n;
	while (khi-klo > 1) {
		k=(khi+klo) >> 1;
		if (xa[k] > x) khi=k;
		else klo=k;
	}
	h=xa[khi]-xa[klo];
	NT_assert(h != 0.0, "splint: Bad xa input to routine splint.");
	a=(xa[khi]-x)/h;
	b=(x-xa[klo])/h;
	*y=a*ya[klo]+b*ya[khi]+((a*a*a-a)*y2a[klo]+(b*b*b-b)*y2a[khi])*(h*h)/6.0;
}

void spline(const double x[], const double y[], const int n, 
	    const double yp1, const double ypn, double y2[])
{
	int i,k;
	double p,qn,sig,un;
	std::vector<double> u(n);

	if (yp1 > 0.99e30)
		y2[1]=u[1]=0.0;
	else {
		y2[1] = -0.5;
		u[1]=(3.0/(x[2]-x[1]))*((y[2]-y[1])/(x[2]-x[1])-yp1);
	}
	for (i=2;i<=n-1;i++) {
		sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);
		p=sig*y2[i-1]+2.0;
		y2[i]=(sig-1.0)/p;
		u[i]=(y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
		u[i]=(6.0*u[i]/(x[i+1]-x[i-1])-sig*u[i-1])/p;
	}
	if (ypn > 0.99e30)
		qn=un=0.0;
	else {
		qn=0.5;
		un=(3.0/(x[n]-x[n-1]))*(ypn-(y[n]-y[n-1])/(x[n]-x[n-1]));
	}
	y2[n]=(un-qn*u[n-1])/(qn*y2[n-1]+1.0);
	for (k=n-1;k>=1;k--)
		y2[k]=y2[k]*y2[k+1]+u[k];
}

} // end of namespace LEFTfield
