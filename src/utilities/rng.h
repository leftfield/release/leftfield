/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef RNG_H
#define RNG_H

// *********************************************************************
// eftlike
//
// rng.h
// - wrappers for random number generators
//
// (C) Fabian Schmidt, 2021
// *********************************************************************

#include <random>
#include "scalar_grid.h"

namespace LEFTfield {

// *********************************************************************
// class RNG
//
// - encapsulates random number generator
// - can use different algorithms; hard-coded via typedef right now

typedef std::mt19937_64 generator_type;

class RNG
{
  generator_type generator;
  std::normal_distribution<double> dgauss;
  std::uniform_real_distribution<double> duniform;
  std::exponential_distribution<double> dexp;

 public:
  RNG(st seed)
    : generator(seed),
      dgauss(0., 1.),
      duniform(0., 1.),
      dexp(1.)
  { }
  
  // get a single draw from a uniform distribution
  double get_uniform(const double min=0., const double max=1.) {
    return min + (max-min) * duniform(generator);
  }
	   
  // get a single draw from a Gaussian distribution
  double get_Gauss(const double mean=0., const double sigma=1.) {
    return mean + sigma * dgauss(generator);
  }

  // get a single draw from an exponential distribution
  double get_exponential(const double lambda=1.) {
    return lambda * dexp(generator);
  }

  // fill grid with independent draws from a Gaussian distribution
  // - take care of whether grid is in real or Fourier space
  void fill_Gauss(scalar_grid& grid,
		  const double mean=0., const double sigma=1.);
};

} // end of namespace LEFTfield

#endif
