/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

// *********************************************************************
// eftlike
//
// rng.h
// - wrappers for random number generators
//
// (C) Fabian Schmidt, 2021
// *********************************************************************

#include "scalar_grid.h"
#include "rng.h"

namespace LEFTfield {

// *********************************************************************

// fill grid with independent draws from a Gaussian distribution
// - take care of whether grid is in real or Fourier space
// - current implementation ensures same realization for given RNG state for
//   both real- and Fourier-space grids
// - alternative version saves one FFT, but yields different realization than
//   real space
void RNG::fill_Gauss(scalar_grid& grd,
		     const double mean, const double sigma)
{
  bool fourier = ! grd.is_real();
  
  // fill in real space
  grd.set_real(true);
  grd.iterate([&](size_t idx) {
    grd[idx] = mean + sigma * dgauss(generator);
  	  } );

  // go to Fourier if desired
  if (fourier) grd.go_fourier();

  // // alternative: fill directly in Fourier space
  // //   - saves one FFT, but yields different realization
  // const st NG = grd.getNG();
  // double sigF = pow(double(NG), 1.5) / sqrt(2.) * sigma;
  // grd.iterateFourierHalf([&](st idx, st idxc, double, double, double, double) {
  // 	   cmplx val(get_Gauss(0., sigF), get_Gauss(0., sigF));
  // 	   hartley_store(val, grd[idx], grd[idxc]);
  // 				 });
  // // add mean to \vec k = 0 mode.
  // grd[0] = mean * double(NG*NG*NG);

}

} // end of namespace LEFTfield
