/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef INTEGRATE_H
#define INTEGRATE_H

// ********************************************************************
// utilities/integrate.h
//
// - adaptive step size Runge-Kutta 4th order ODE integrator
//   - functions take function object
//       derivs(double x, double y[N], double& yprime[N])
//     to derive solution y(x)
//   - see codes/tests/test_odeint for an example
// - Romberg integration for definite integrals
//   - see codes/tests/test_growth for an example
//
// (C) Fabian Schmidt, 2020
// ********************************************************************

#include <cmath>
#include <vector>
#include <algorithm>
#include <array>
#include "base/error_handling.h"

namespace LEFTfield {

template<std::size_t N, typename Derivs> void rkck(
  const std::array<double, N> &y, const std::array<double, N> &dydx,
  double x, double h, std::array<double, N> &yout, std::array<double, N> &yerr,
  Derivs derivs)
  {
  using namespace std;
  constexpr const double
    a2=.2, a3=.3, a4=.6, a5=1., a6=.875,
    b21=.2,
    b31=3./40., b32=9./40.,
    b41=.3, b42=-.9, b43=1.2,
    b51=-11./54., b52=2.5, b53=-70./27., b54=35./27.,
    b61=1631./55296., b62=175./512., b63=575./13824., b64=44275./110592., b65=253./4096.,
    c1=37./378., c3=250./621., c4=125./594., c6=512./1771.,
    dc1=c1-2825./27648., dc3=c3-18575./48384.,
    dc4=c4-13525./55296., dc5=-277./14336., dc6=c6-0.25;

  array<double, N> ak2, ak3, ak4, ak5, ak6, ytemp;
	
  for (size_t i=0; i<N; ++i)
    ytemp[i]=y[i]+b21*h*dydx[i];
  derivs(x+a2*h,ytemp,ak2);
  for (size_t i=0; i<N; ++i)
    ytemp[i]=y[i]+h*(b31*dydx[i]+b32*ak2[i]);
  derivs(x+a3*h,ytemp,ak3);
  for (size_t i=0; i<N; ++i)
    ytemp[i]=y[i]+h*(b41*dydx[i]+b42*ak2[i]+b43*ak3[i]);
  derivs(x+a4*h,ytemp,ak4);
  for (size_t i=0; i<N; ++i)
    ytemp[i]=y[i]+h*(b51*dydx[i]+b52*ak2[i]+b53*ak3[i]+b54*ak4[i]);
  derivs(x+a5*h,ytemp,ak5);
  for (size_t i=0; i<N; ++i)
    ytemp[i]=y[i]+h*(b61*dydx[i]+b62*ak2[i]+b63*ak3[i]+b64*ak4[i]+b65*ak5[i]);
  derivs(x+a6*h,ytemp,ak6);
  for (size_t i=0; i<N; ++i)
    yout[i]=y[i]+h*(c1*dydx[i]+c3*ak3[i]+c4*ak4[i]+c6*ak6[i]);
  for (size_t i=0; i<N; ++i)
    yerr[i]=h*(dc1*dydx[i]+dc3*ak3[i]+dc4*ak4[i]+dc5*ak5[i]+dc6*ak6[i]);
  }

template<std::size_t N, typename Derivs> void rkqs(
  std::array<double, N> &y, const std::array<double, N> &dydx,
  double &x, double htry, double eps, const std::array<double, N> &yscal,
  double &hnext, Derivs derivs)
  {
  using namespace std;
  constexpr double SAFETY=0.9;
  constexpr double PSHRNK=-0.25;
  constexpr double PGROW=-0.2;
  const double ERRCON=pow(5./SAFETY,1./PGROW);

  auto h=htry;
  while (true) {
    array<double,N> ytemp, yerr;
    rkck(y,dydx,x,h,ytemp,yerr,derivs);
    double errmax=0;
    for (size_t i=0; i<N; ++i)
      errmax=max(errmax,abs(yerr[i]/yscal[i]));
    errmax /= eps;
    if (errmax>1) {
      auto htemp=SAFETY*h*pow(errmax,PSHRNK);
      h=((h>=0) ? max(htemp,0.1*h) : min(htemp,0.1*h));
      auto xnew=x+h;
      NT_assert(xnew!=x, "stepsize underflow in rkqs");
      continue;
    } else {
      if (errmax>ERRCON)
        hnext=SAFETY*h*pow(errmax,PGROW);
      else hnext=5.0*h;
        x += h;
      y=ytemp;
      break;
    }
  }
  }

template<std::size_t N, typename Derivs> void odeint(
  std::array<double,N> &y0, double x0, double x1, double eps, double h,
  double hmin, double hmax, Derivs derivs)
  {
  using namespace std;
  constexpr size_t MAXSTP=25000000;
  constexpr double TINY=1.0e-30;

  auto x=x0;
  h=copysign(h, x1-x0);
  array<double,N> y(y0), dydx, yscal;
  for (size_t nstp=0; nstp<MAXSTP; ++nstp) {
    derivs(x,y,dydx);
    for (size_t i=0; i<N; ++i)
      yscal[i]=abs(y[i])+abs(dydx[i]*h)+TINY;
    if ((x+h-x1)*(x+h-x0) > 0.0)
      h=x1-x;
    double hnext;
    rkqs(y,dydx,x,h,eps,yscal,hnext,derivs);
    if ((x-x1)*(x1-x0) >= 0.0)
      { y0=y; return; }
    NT_assert(abs(hnext)>=hmin, "Step size too small in odeint");
    h=min(hnext,hmax); // respect max step size
    }
  }

template<typename Func> double romberg(Func func, double a, double b,
				       double acc=1.e-5, std::size_t max_steps=26) {
  using namespace std;
  vector<double> R1(max_steps), R2(max_steps); // buffers
  double *Rp = &R1[0], *Rc = &R2[0]; // Rp is previous row, Rc is current row
  double h = (b-a); //step size
  Rp[0] = (func(a) + func(b))*h*.5; // first trapezoidal step

  for (size_t i=1; i<max_steps; ++i) {
    h /= 2.;
    double c=0;
    size_t ep=1<<(i-1); //2^(n-1)
    for (size_t j=1; j<=ep; ++j)
      c += func(a+(2*j-1)*h);
    Rc[0] = h*c + .5*Rp[0]; //R(i,0)

    for (size_t j = 1; j <= i; ++j) {
      auto n_k = 1<<(2*j);
      Rc[j] = (n_k*Rc[j-1] - Rp[j-1])/(n_k-1); // compute R(i,j)
    }

    if (i > 1 && abs(Rp[i-1]-Rc[i]) < acc)
      return Rc[i-1];

    // swap Rp and Rc as we only need the last row
    swap(Rp, Rc);
  }
  return Rp[max_steps-1]; // return our best guess
}

} // end of namespace LEFTfield

#endif
