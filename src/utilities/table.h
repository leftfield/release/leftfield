/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef TABLE_H
#define TABLE_H

#include <stdio.h>
#include <vector>
#include <string>

// ************************************************
// Nbody_tools
//
// ordered table classes for interpolating and deriving 
// functions quickly
// - imported from cosmology code package
//
// NOTE: ASCENDING order in x is required for interpolation !
//
// class table
// - linear interpolation
// - polynomial interpolation
// * to be done: quick index search via bisection
//
// (C) Fabian Schmidt, 2007, 2019
// ************************************************

// whether to do bounds check
/* #define BOUNDS_CHECK_TABLE */
// whether to extend interpolation beyond table
#define INTERP_BEYOND_TABLE

#include "types.h"
#include "log.h"
#include "error_handling.h"

// ***************************************************************

namespace LEFTfield {

class table
{
  st N, ii;
  std::vector<double> xx, yy;
  mutable std::vector<double> y2; // for spline interpolation
  bool extrapol;  // whether to extrapolate beyond range
  const std::string writefile;

public:
  table()
    : N(0), ii(0), xx(0), yy(0), y2(0), writefile("")
    { }
  // Try to read from file, if not present, file is written once table
  // is filled
  table(const st Nval, const std::string &filename = "",
        const bool extrapolate = true)
    : N(Nval), ii(0), xx(0), yy(0), y2(0), extrapol(extrapolate),
      writefile(filename)
  { 
    if (filename != "")
      readFromFile(filename);
    else {
      xx.resize(N);
      yy.resize(N);
    }
  }

  // copy constructor
  table(const table& t);
  
  // Read table from file, using given column numbers
  // - arbitrary size
  // - ignores lines starting with '#'
  // - column numbers from from 1
  // - fills log of value if specified
  table(const std::string &filename, const int col_x, const int col_y,
	const bool log_x = false, const bool log_y = false,
        const bool extrapolate = true);

  // Fill table from vectors
  table(const std::vector<double>& xvec,
        const std::vector<double>& yvec,
	const bool extrapolate = true)
    : N(xvec.size()), ii(0), xx(xvec), yy(yvec), y2(0), extrapol(extrapolate)
  {
    NT_assert(xvec.size() == yvec.size(), "table::table: x and y vectors have different size !");
  }

  // * setting values *
  void push_back(const double x, const double y) {
#ifdef BOUNDS_CHECK_TABLE
    NT_assert(ii < N, "table::push_back: beyond end of table.");
#endif
    xx[ii] = x;  yy[ii] = y;
    ii++;
    if (writefile!="" && ii == N) {
      logger.printf("table::push_back", INFO, "table filled, writing to file '%s'.", writefile.c_str());
      writeToFile(writefile);
    }
  }

  void setValue(const st i, const double x, const double y) {
#ifdef BOUNDS_CHECK_TABLE
    NT_assert(i < N, "table::setValue: index out of range.");
#endif
    // delete spline information since values changed
    y2.resize(0);

    xx[i] = x;  yy[i] = y;
  }

  void setx(const st i, const double x) {
#ifdef BOUNDS_CHECK_TABLE
    NT_assert(i < N, "table::setx: index out of range.");
#endif
    // delete spline information since values changed
    y2.resize(0);

    xx[i] = x;
  }

  void sety(const st i, const double y) {
#ifdef BOUNDS_CHECK_TABLE
    NT_assert(i < N, "table::sety: index out of range.");
#endif
    // delete spline information since values changed
    y2.resize(0);

    yy[i] = y;
  }

  void addy(const st i, const double y) {
#ifdef BOUNDS_CHECK_TABLE
    NT_assert(i < N, "table::addy: index out of range.");
#endif
    // delete spline information since values changed
    y2.resize(0);

    yy[i] += y;
  }

  bool isFilled() const {
    return ii == N;
  }

  st getN() const {
    return N;
  }

  // * getting values *
  double getx(const st i) const {
#ifdef BOUNDS_CHECK_TABLE
    NT_assert(i < N, "table::getx: index out of range.");
#endif
    return xx[i];
  }

  double gety(const st i) const {
#ifdef BOUNDS_CHECK_TABLE
    NT_assert(i < N, "table::gety: index out of range.");
#endif
    return yy[i];
  }

  // * x range *
  // be independent of direction of ordering
  double xmin() const {
    return xx[0] < xx[N-1] ? xx[0] : xx[N-1];
  }
  double xmax() const {
    return xx[0] > xx[N-1] ? xx[0] : xx[N-1];
  }

  // * operations *
  // scale y values
  table& operator*=(const double ys) {
    for (st i=0; i < N; i++)  yy[i] *= ys;
    return *this;
  }
  // add to y values
  table& operator+=(const double yo) {
    for (st i=0; i < N; i++)  yy[i] += yo;
    return *this;
  }

  // * print out *
  void print(FILE *fp = stdout) const {
    fprintf(fp, "Table of size %zu.\n", N);
    for (st i=0; i < N; i++)
      fprintf(fp, "%zu. x[i] = %.4e, y[i] = %.4e\n", i, xx[i], yy[i]);
  }

  // * interpolating *
  // linear interpolation
  double interpLin(const double x) const;
  // polynomial interpolation with polynomial of degree npol-1
  double interpPol(const double x, const st npol) const;
  // cubic spline interpolation
  double interpSpline(const double x) const;

  // * deriving *
  // uses polynomial interpolation
  double derivePol(const double x, const double dx, const int npol) const;
  double dderivePol(const double x, const double dx, const int npol) const;
  // deriving via spline interpolation
  double deriveSpline(const double x, const double dx) const;
  double dderiveSpline(const double x, const double dx) const;

  // * read/write from/to file *
  void writeToFile(const std::string &filename) const;
  void readFromFile(const std::string &filename);
  void readFromFile(const std::string &filename, const std::string &format);
};

// ***************************************************************

} // end of namespace LEFTfield

#endif


