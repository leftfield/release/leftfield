/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include "forward/generate_operatorSet.h"
#include "vector_grid.h"
#include "tensor_grid.h"

namespace LEFTfield {

// ********************************************************************
// forward/generate_operatorSet.h
//
// various routines to construct or add to operatorSets given linear density
// field
//
// (C) Fabian Schmidt, 2017-2021
// ********************************************************************


// *******************************************************************
// quadratic operators: delta^2, K_ij^2, s^k del_k delta

void generate_operatorSet_squared(operatorSet& opset, const scalar_grid& delta_,
				  const bool with_displacement,
				  const bool fourier)
{
  // -- construct quadratic operators --
  scalar_grid delta(delta_);
  delta.go_real();

  // delta^2
  auto delta2 = make_sptr<scalar_grid>(delta);
  *delta2 *= delta;
  opset.add("delta^2", {delta2, 2});

  delta.go_fourier();
  sptr<scalar_grid> K2;
  // construct tidal field K_ij from delta and square
  {
    tensor_grid Kij(delta);
    Kij.go_real();
    K2 = make_sptr<scalar_grid>(Kij*Kij);
    opset.add("K^2", {K2, 2});
  } // can de-allocate tidal field at this point to save memory

  if (with_displacement)  {
    // displacement term
    vector_grid sdisp(delta.getNG());
    sdisp.generate_displacement(delta);
    sdisp.go_real();

    vector_grid gradd(delta.getNG());
    gradd.generate_gradient(delta);
    gradd.go_real();

    auto sdeld = make_sptr<scalar_grid>(sdisp * gradd);
    sdeld->subtract_mean();
    opset.add("s.del delta", {sdeld, 2});
  }
  
  // -- go to Fourier space if desired --
  if (fourier)  opset.go_fourier();
  else  opset.go_real();

  // -- subtract means --
  delta2->subtract_mean();
  K2->subtract_mean();
}

// *******************************************************************
// cubic operators: delta^3, delta K^2, K^3, O_td, s^k del_k delta^2,
//                  s^k del_k K^2

sptr<scalar_grid> construct_K3(const tensor_grid& Kij)
{
  return make_sptr<scalar_grid>(Kij.doubleContraction(Kij, Kij));
}


sptr<scalar_grid> construct_Otd(const scalar_grid& deltasq,
				const scalar_grid& K2,
				const tensor_grid& Kij)
{
  // -- Vij = Del_ij V, V = (delta^2 - 3/2 K^2)
  scalar_grid V(deltasq);
  V += -1.5 * K2;
  V.go_fourier();
  tensor_grid Vij(V);
  Vij.go_real();
  auto otd = make_sptr<scalar_grid>(Vij * Kij);
  *otd *= 8./21.;

  return otd;
}


// generate cubic displacement term
sptr<scalar_grid> construct_sdelO2(vector_grid& sdisp,
				   scalar_grid& O2)
{
  vector_grid gradO2(O2.getNG());
  O2.go_fourier();
  gradO2.generate_gradient(O2);
  gradO2.go_real();
  O2.go_real();
  sdisp.go_real();

  return make_sptr<scalar_grid>(sdisp * gradO2);
}

void generate_operatorSet_cubed(operatorSet& opset, const scalar_grid& delta_,
				const bool with_displacement,
				const bool fourier)
{
  // ---------------------------------------------------------------------
  // I. construct quadratic operators (same as above, but we are keeping
  //    some intermediate grids too)
  
  scalar_grid delta(delta_);
  delta.go_real(); 
  
  // delta^2
  auto delta2 = make_sptr<scalar_grid>(delta);
  *delta2 *= delta;
  opset.add("delta^2", {delta2, 2});
  
  // subtract means
  double sigmasqtemp = delta2->get_mean();
  delta2->subtract_mean();

  // delta^3
  auto delta3 = make_sptr<scalar_grid>(*delta2);
  *delta3 *= delta;
  // - renormalization
  *delta3 += -2. * sigmasqtemp * delta;
  
  // construct tidal field K_ij from delta and square  
  scalar_grid deltaF(delta);
  deltaF.go_fourier();
  tensor_grid Kij(deltaF);
  Kij.go_real();
  auto K2 = make_sptr<scalar_grid>(Kij*Kij);
  opset.add("K^2", {K2, 2});
  K2->subtract_mean();

  // ---------------------------------------------------------------------
  // II. construct cubic operators

  // - delta K^2
  auto dK2 = make_sptr<scalar_grid>(*K2);
  *dK2 *= delta;
  
  // - K^3
  auto K3 = construct_K3(Kij);
  // - O_td
  auto Otd = construct_Otd(*delta2, *K2, Kij);

  // -- fill pointers and names --
  opset.add("delta^3", {delta3, 3});
  opset.add("delta K^2", {dK2, 3});
  opset.add("K^3", {K3, 3});
  opset.add("Otd", {Otd, 3});

  // can de-allocate tidal field at this point to save memory
  Kij.resize(0);

  // ---------------------------------------------------------------------
  // III. displacements

  if (with_displacement)  {
    // displacement term
    vector_grid sdisp(deltaF.getNG());
    sdisp.generate_displacement(deltaF);
    sdisp.go_real();
    
    vector_grid gradd(deltaF.getNG());
    gradd.generate_gradient(deltaF);
    gradd.go_real();

    auto sdeld = make_sptr<scalar_grid>(sdisp * gradd);
    sdeld->subtract_mean();
    auto sdeld2 = construct_sdelO2(sdisp, *delta2);
    // - renormalization
    *sdeld2 += -2. * sigmasqtemp * delta; 
    auto sdelK2 = construct_sdelO2(sdisp, *K2);
  
    // -- fill pointers and names --
    opset.add("s.del delta", {sdeld, 2});
    opset.add("s.del delta^2", {sdeld2, 3});
    opset.add("s.del K^2", {sdelK2, 3});
  }

  // -- go to Fourier space if desired --
  if (fourier)  opset.go_fourier();
  else  opset.go_real();
}






// ******************************************************************


// construct new scalar_grid with (\partial_k g1) (\partial^k g2)
// - returns real-space grid
sptr<scalar_grid> construct_partialOpartialO(const scalar_grid& g1,
					     const scalar_grid& g2)
{
  NT_assert(g1.getNG() == g2.getNG(), "ERROR in construct_partialOpartialO: grid sizes differ.");

  auto g = make_sptr<scalar_grid>(g1);
  g->go_fourier();
  vector_grid v1;
  v1.generate_gradient(*g);
  v1.go_real();
  *g = g2;
  g->go_fourier();
  vector_grid v2;
  v2.generate_gradient(*g);
  v2.go_real();
  *g = v1 * v2;
  
  return g;
}

// construct higher derivative operators based on given operator_set
// - construct "all" (see below) that are of lesser or equal order to
// 'maxorder' according to given expansion_parameters object
// - returns number of fields added
//
// specifically, for given operators { O_i }, this constructs all products
// (\lapl O_{i_1}) O_{i_2} ... and
// (\partial_k O_{i_1}) (\partial^k O_{i_2}) O_{i_3} ...
// that are relevant according to 'maxorder' and expansion_parameters.
//
// NOT included are terms such as \partial_k M_{ij} \partial^j M^k_i ;
// these require more work to implement

st add_derivatives_full(operatorSet& opset,
			const op_order& maxorder,
			const expansion_parameters& ep,
			const st max_derivatives)
{
  opset.go_real(); // construction in real space by default
  
  std::vector<std::string> names;
  for (const auto &op : opset)
    names.push_back(op.first);

  st nadd=0;
  // notice we also run over newly added operators, so that e.g.
  // (\lapl O) (\lapl O') is also added if relevant
  for (st I=0; I<names.size(); ++I)  {
    // get order of given operator once Laplace is applied
    const auto &opI = opset[names[I]];
    op_order order_lO(opI.order);
    order_lO += op_order(0, 1);

    // skip if already higher order
    if (ep.is_higher_order(order_lO, maxorder))  continue;

    // skip if higher than max order in derivatives
    if (order_lO.deriv > max_derivatives)  continue;

    // == I. \lapl O ==
    // assemble name
    std::string newname(std::string("lapl(") + names[I]
			+ std::string(")"));
    // - if already exists, skip
    if (opset.have(newname)) continue;

    auto laplO = make_sptr<scalar_grid>(*opI.grd);
    laplO->go_fourier();
    laplO->Laplace();
    laplO->go_real();

    opset.add(newname, Opdata{laplO, order_lO});
    names.push_back(newname);
    nadd++;
    // info
    logger.printf("operatorSet::add_derivatives_full", INFO,
		  "adding '%s'.", newname.c_str());

    // II. (\lapl O) O' and (\partial O) (\partial O')
    for (st J=I; J < names.size(); J++)  {
      op_order order_lOO(order_lO);
      order_lOO += opset[names[J]].order;
      if (ep.is_higher_order(order_lOO, maxorder))  continue;

      // order(lapl O) + order(O') is relevant
      if (J != I)  { // avoid redundancy
	// -> include (lapl O) O'

	std::string newnameOO(newname + std::string(" ") + names[J]);
	// - if already exists, skip
	if (opset.have(newnameOO)) continue;

	auto laplOO = make_sptr<scalar_grid>(*laplO);
	*laplOO *= *opset[names[J]].grd;
	opset.add(newnameOO, Opdata{laplOO, order_lOO});
        names.push_back(newnameOO);
	nadd++;
	// info
	logger.printf("operatorSet::add_derivatives_full", INFO,
		      "adding '%s'.", newnameOO.c_str());
      }

      // -> include (\partial O) (\partial O')
      std::string newnamedelOdelO(std::string("(partial ") + names[I]
				  + std::string(")(partial ") + names[J]
				  + std::string(")"));
      // - if already exists, skip
      if (opset.have(newnamedelOdelO)) continue;
      
      auto delOdelO = construct_partialOpartialO(*opset[names[I]].grd,
						 *opset[names[J]].grd);
      opset.add(newnamedelOdelO, Opdata{delOdelO, order_lOO});
      names.push_back(newnamedelOdelO);
      nadd++;
      // info
      logger.printf("operatorSet::add_derivatives_full", INFO,
		    "adding '%s'.", newnamedelOdelO.c_str());
    }      
  }      

  return nadd;  
}


// add laplace of fields until they are of lesser or equal order to
// 'maxorder' according to given expansion_parameters object
// - returns number of fields added
// - note this is only small subset of derivative operators; left here for
//  legacy
st add_derivatives_laplace(operatorSet& opset,
			   const op_order& maxorder, const expansion_parameters& ep,
			   const st max_derivatives)
{
  std::vector<std::string> names;
  for (const auto &op : opset)
    names.push_back(op.first);

  st nadd=0;
  // notice we also run over newly added operators, so that e.g. lapl^2 O
  // is also added if relevant
  for (st I=0; I < names.size(); I++)  {
    const auto &opI = opset[names[I]];
    // get order of given operator once Laplace is applied
    op_order neworder(opI.order);
    neworder += op_order(0, 1);
    
    // skip if higher than maxorder
    if (ep.is_higher_order(neworder, maxorder))  continue;
    
    // skip if higher than max order in derivatives
    if (neworder.deriv > max_derivatives)  continue;

    // assemble name
    std::string newname(std::string("lapl(") + names[I]
			+ std::string(")"));
    // - if already exists, skip
    if (opset.have(newname)) continue;
    
    auto O = make_sptr<scalar_grid>(*opI.grd);
    O->go_fourier();
    O->Laplace();

    opset.add(newname, Opdata{O, neworder});
    names.push_back(newname);

    // debug
    logger.printf("operatorSet::add_derivatives_laplace", INFO,
		  "adding '%s'.", newname.c_str());
    
    nadd++;
  }

  return nadd;  
}

} // end of namespace LEFTfield
