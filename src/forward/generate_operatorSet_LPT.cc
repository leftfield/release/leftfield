/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include "forward/generate_operatorSet_LPT.h"
#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>

#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "forward/nLPT.h"

namespace LEFTfield {

// ********************************************************************
// LPT bias operators
//
// constructed from set of Lagrangian longitudinal displacement contributions
// sigma^(n,p)
// - constructs all invariants and combinations thereof; fully general for
//   any expansion history, as long as appropriate nLPT object is given
// - as discussed in bias review, no need to include curl component in bias
//   construction
// - construct at any given value of Dnorm (where Dnorm=1 is epoch of linear
//   density field passed to nLPT object)
//
// (C) Fabian Schmidt, 2020
// ********************************************************************

// ********************************************************************
// class LPTinvariants
//
// encapsulate all invariants up to some order n, out of which all operators
// are constructed by operator_set_LPT
// maybe: base class implements interface and push_invariant functions;
//        derived classes implement actual construction of invariants
//        two derived classes: _general and _EdS
// ********************************************************************


// base class constructs linear invariant = tr[ M^(1) ]
LPTinvariants::LPTinvariants(const st order,
			     const nLPT &lptobject,
			     const double Dnorm,
			     const double /*kcut*/,
			     const sharpkFilterType /*ftype*/)
  : y(log(Dnorm)), vM(lptobject.get_divergence_shapes())
{
  if (order > lptobject.getOrder()+1)  {
    logger.printf("LPTinvariants", ESSENTIAL,
		  "WARNING: requested order = %zu > n_max+1 = %zu. Bias set will not be complete.",
		  order, lptobject.getOrder()+1);
  }

  // sanity checks
  for (const auto& M: vM)  {
    NT_assert(M.sigma->is_real(),
	      "ERROR in LPTinvariants: M^(%zu,%zu) has to be in real space.", M.order, M.p);
  }
  
  vOrder.resize(order);

  // 1. Linear invariants: only tr[M^(1)]
  LPTshape d1(1, 0, 1.), d2(0, 0, 1.); // d2 is trivially constant=1
  // - always at first position in vM vector
  push_invariant(make_sptr<scalar_grid>(*vM[0].sigma), d1, d2, true);
}


// fill given grid to relevant arrays, including name and order
void LPTinvariants::push_invariant(sptr<scalar_grid> O,
				   const LPTshape& M1, const LPTshape& M2,
				   const bool add_time_dep)
{
  st N = M1.order + M2.order; // order
  std::string name("NOP");
  
  // assemble name
  if (N==1)  {
    // only one linear order operator    
    name = "tr[M^(1)]";
  } else {
    std::string M1str = M1.p > 0
      ? std::to_string(M1.order) + "," + std::to_string(M1.p)
      : std::to_string(M1.order);
    std::string M2str = M2.p > 0
      ? std::to_string(M2.order) + "," + std::to_string(M2.p)
      : std::to_string(M2.order);
    
    name = std::string("tr[M^(") + M1str + std::string(") M^(") + M2str
      + std::string(")]");
  }

  if (add_time_dep)  {
    // apply time dependence
    *O *= M1.aty(y) * M2.aty(y);
  }
  
  vInv.add(name, {O, N});  // add to list of invariants
  vOrder[N-1].push_back(name); // save index of operator
}


// fill given grid to relevant arrays, including name and order
void LPTinvariants::push_invariant(sptr<scalar_grid> O,
				   const LPTshape& M1, const LPTshape& M2,
				   const LPTshape& M3,
				   const bool add_time_dep)
{
  st N = M1.order + M2.order + M3.order; // order
  std::string name("NOP");

  std::string M1str = M1.p > 0
    ? std::to_string(M1.order) + "," + std::to_string(M1.p)
    : std::to_string(M1.order);
  std::string M2str = M2.p > 0
    ? std::to_string(M2.order) + "," + std::to_string(M2.p)
    : std::to_string(M2.order);
  std::string M3str = M3.p > 0
    ? std::to_string(M3.order) + "," + std::to_string(M3.p)
    : std::to_string(M3.order);
    
  name = std::string("tr[M^(") + M1str + std::string(") M^(") + M2str
    + std::string(") M^(") + M3str + std::string(")]");

  if (add_time_dep)  {
    // apply time dependence
    *O *= M1.aty(y) * M2.aty(y) * M3.aty(y);
  }

  vInv.add(name, {O, N});  // add to list of invariants
  vOrder[N-1].push_back(name); // save index of operator
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// Derived classes

// general: include all shapes individually
LPTinvariants_general::LPTinvariants_general(const st order, // = order in PT up to which to construct
					     const nLPT &lptobject,
					     // normalized growth factor for which to construct
					     const double Dnorm, 
					     const double kcut,
					     const sharpkFilterType ftype)
  : LPTinvariants(order, lptobject, Dnorm, kcut, ftype)
{  
  // construct all basic invariants of the form tr[ M M' ], tr[ M M' M'' ]
  // up to order 'order'
  // - linear invariant already constructed by base class
    
  // 2. Quadratic invariants: tr[ M^(n,p) M^(m,q) ]
  for (const auto& M1: vM) {
    for (const auto& M2: vM) {
      if (M1.order + M2.order > order)  continue;
      // - m >= n to avoid duplication
      // - if m == n, q >= p to avoid duplication
      if (M2 < M1)  continue;
  
      auto OI = make_sptr<scalar_grid>( *M1.get_tensor() * *M2.get_tensor() );
      // - fill this invariant, including name and order
      push_invariant(OI, M1, M2);
    }
  }

  // 3. Cubic invariants: tr[ M^(m1,p1) M^(m2,p2) M^(m3,p3) ]
  for (const auto& M1: vM) {
    for (const auto& M2: vM) {
      for (const auto& M3: vM) {
	if (M1.order + M2.order + M3.order > order)  continue;
	// - m3 >= m2 >= m1 to avoid duplication
	// - if m3 == m2, p3 >= p2, etc., to avoid duplication
	if (M2 < M1)  continue;
	if (M3 < M2)  continue;

	auto OI = make_sptr<scalar_grid>(
	  M1.get_tensor()->doubleContraction(*M1.get_tensor(), *M2.get_tensor()) );
	push_invariant(OI, M1, M2, M3);
      }
    }
  }

  // now apply sharp-k filters
  for (auto &op : vInv)  {
    if (kcut > 0.)  op.second.grd->cutsharpk(kcut, ftype);
    op.second.grd->go_real();
  }
  
  // info (or debug?)
  if (logger.getLevel() >= INFO)  {
    logger.printf("LPTinvariants_general", INFO, "names of operators at each order (%zu total):", vInv.getN());
    for (st n=1; n <= order; n++)  {
      fprintf(stderr, "order %zu: ", n);
      for (st i=0; i < getNumInvariants(n); i++)
	std::cerr << getInvariantName(n,i) << ", ";
      std::cerr << "\n";
    }
  }

  // free tensor grid, as not needed anymore
  lptobject.free_tensors();
}

// EdS: include only total M at each order
LPTinvariants_EdS::LPTinvariants_EdS(const st order, // = order in PT up to which to construct
				     const nLPT &lptobject,
				     // normalized growth factor for which to construct
				     const double Dnorm, 
				     const double kcut,
				     const sharpkFilterType ftype)
  : LPTinvariants(order, lptobject, Dnorm, kcut, ftype)
{  
  // construct all basic invariants of the form tr[ M M' ], tr[ M M' M'' ]
  // up to order 'order'
  // - linear invariant already constructed by base class

  // first construct temporary set of sigma^(m) shapes
  std::vector<LPTsigma> sigmavec;
  sigmavec.push_back(LPTsigma(0)); // so that index = order
  for (st n=1; n < order; n++) {
    LPTsigma M(n, 0, 1.,
	       make_sptr<scalar_grid>(lptobject.total_trace_at_order(n, Dnorm, false)));
    sigmavec.push_back(M);
  }
  
  // 2. Quadratic invariants: tr[ M^(n) M^(m) ]
  for (st n=1; n < order; n++)  {
    for (st m=n; m < order; m++)  { // m >= n
      if (n+m > order)  continue;
        
      auto OI = make_sptr<scalar_grid>( (*sigmavec[n].get_tensor())
					 * (*sigmavec[m].get_tensor()) );
      // - fill this invariant, including name and order ;
      // - do not add time dependence
      push_invariant(OI, sigmavec[n], sigmavec[m], false);
    }
  }

  // 3. Cubic invariants: tr[ M^(m1) M^(m2) M^(m3) ]
  for (st m1=1; m1 < order; m1++)  {
    for (st m2=m1; m2 < order; m2++)  { // m2 >= m1
      for (st m3=m2; m3 < order; m3++)  { // m2 >= m1
	if (m1+m2+m3 > order)  continue;

	auto OI = make_sptr<scalar_grid>(
		  sigmavec[m1].get_tensor()->doubleContraction(*sigmavec[m2].get_tensor(),
							       *sigmavec[m3].get_tensor()) );

	// - fill this invariant, including name and order ;
	// - do not add time dependence
	push_invariant(OI, sigmavec[m1], sigmavec[m2], sigmavec[m3], false);
      }
    }
  }

  // now apply sharp-k filters
  for (auto &op : vInv)  {
    if (kcut > 0.)  op.second.grd->cutsharpk(kcut, ftype);
    op.second.grd->go_real();
  }

  // delete temporary array
  for (auto& M: sigmavec)
    M.sigma=nullptr;

  // info (or debug?)
  if (logger.getLevel() >= INFO)  {
    logger.printf("LPTinvariants_EdS", INFO, "names of operators at each order (%zu total):", vInv.getN());
    for (st n=1; n <= order; n++)  {
      fprintf(stderr, "order %zu: ", n);
      for (st i=0; i < getNumInvariants(n); i++)
	std::cerr << getInvariantName(n,i) << ", ";
      std::cerr << "\n";
    }
  }

  // free tensor grid, as not needed anymore
  lptobject.free_tensors();
}


// ********************************************************************
// generate_operatorSet_LPT
//
// - Lagrangian-space operators constructed from set of Lagrangian
//   longitudinal displacement contributions
// - optional sharp-k filter to avoid aliasing
//   (MUCH better option is to ensure grid is large enough that aliasing cannot
//    occur, i.e. k_Ny > n k_max(delta) )
// ********************************************************************



// construct all combinations of invariants for a given partition
// - uses vOrder to run over all operators at given order
void construct_operators(operatorSet& opset,
			 const LPTinvariants& Linv,
			 const st n_order,
			 const std::vector<st>& v,
			 const st N, const st level,
			 sptr<scalar_grid> O,
			 const double kcut,
			 const sharpkFilterType ftype)
{
  static std::string name;
  
  // operator in the end looks like
  // O^[v[0]] O^[v[1]] ... O^[v[N]] ,
  // and we have to loop over all invariants at given order;
  // do this recursively

  st n = v[level]; // index of invariant list = order
  // - loop over invariants
  for (st o=0; o < Linv.getNumInvariants(n); o++)  {
    auto O1 = make_sptr<scalar_grid>(* Linv.getInvariant(n, o));
    if (level)  // multiply to previous operator
      *O1 *= *O;
    else { // clear name
      name.clear();
    }
    
    if (kcut > 0.)  O1->cutsharpk(kcut, ftype);
    O1->go_real();

    auto invname = Linv.getInvariantName(n,o);
    if (level < N)  { // continue iteration
      name += invname + " ";
      construct_operators(opset, Linv, n_order, v, N, level+1, O1, kcut, ftype);
    } else {
      // last element of partition: save operator and name
      std::string oname(name + invname );
      opset.add(oname, {O1, n_order});
      logger.printf("operator_set_LPT::construct_operators", DEBUG,
		    "adding %s (%zu'th order)", oname.c_str(), n_order);
    }
  }
}




// construct all combinations of invariants at order 'n':
// - run over all partitions of 'n'
// - recursively calls itself 
void construct_partitions(operatorSet& opset,
			  const LPTinvariants& Linv,
			  const st n_order,
			  const st n,
			  std::vector<st>& v,
			  const st level,
			  const double kcut,
			  const sharpkFilterType ftype)
{
  if (n<1) return; // we are done
  
  v[level] = n;

  if (level)  {
    // construct operators corresponding to partition
    // - we already have the trivial partitions with level==0
    // - this in turn calls itself recursively
    construct_operators(opset, Linv, n_order, v, level, 0, 0, kcut, ftype);
  }

  st first = (level==0) ? 1 : v[level-1];

  for (st i=first; i<=n/2; i++){
    v[level]=i; /* replace last */
    construct_partitions(opset, Linv, n_order, n-i, v, level+1, kcut, ftype);
  }
}


// construct all grids from set of tensor grids
void generate_operatorSet_LPT(operatorSet& opset,
			      const st order,
			      const LPTinvariants& Linv,
			      const double kcut,
			      const sharpkFilterType ftype)
//  : n_order(0), inv(Linv)
{
  // first, fill invariants into operator set
  Linv.fill_operatorSet(opset);

  // now run over nontrivial partitions up to order 'order'
  if (order > 1) {
    for (st n_order=2; n_order <= order; n_order++) {
      // info (or debug?)
      logger.printf("operator_set_LPT", INFO,
		    "=== Partitions at order %zu / %zu ===", n_order, order);
      
      std::vector<st> vpart(n_order);
      construct_partitions(opset, Linv, n_order, n_order, vpart, 0, kcut, ftype);
    }
  }

  logger.printf("operator_set_LPT", INFO, "=== have %zu operators in total ===",
		opset.getN());
}





// ************************************************************************
// class operator_set_LPTEulerian
//
// - derives from operator_set_LPT
// - displaces all operators to Eulerian space using functionality
//   provided by nLPT
// - optional sharp-k filter to avoid aliasing
//   (MUCH better option is to ensure grid is large enough that aliasing cannot
//    occur, i.e. k_Ny > n k_max(delta) )
// ********************************************************************


// construct all operators from LPTinvariants object, up to including order 'order',
// and use nLPT object to displace to Eulerian frame
// - supersampling, NGdelta passed on to nLPT for displacement operation
// - if 'reduce' is set, reduce grids from NGdelta to lptobject->NG in
//   Fourier space
// - kcutLPT, ftype passed on to operator_set_LPT
// - kcutS is applied to bias operators before displacement to Eulerian space
void generate_operatorSet_LPTEulerian(operatorSet& opset,
				      const st order,
				      const LPTinvariants& Linv,
				      const nLPT &lptobject,
				      const double Dnorm,
				      const double supersampling,
				      const st NGdelta,
				      const bool reduce,
				      const double kcutLPT,
				      const double kcutS,
				      const sharpkFilterType ftype)
{
  // fill Lagrangian operators
  generate_operatorSet_LPT(opset, order, Linv, kcutLPT, ftype);
  
  std::vector<std::string> names;
  for (const auto &op: opset)
    names.push_back(op.first);
 
  const st myNG = opset[names[0]].grd->getNG();
  std::vector<sptr<scalar_grid>> wvec;

  // assemble vector of weight grids, including 1 for density
  for (st I=0; I < opset.getN(); I++)  {
    // check for linear order tr[ M^(1) ] operator and replace this
    // with Eulerian density
    if (opset[names[I]].order.pt == 1)  {
      opset[names[I]].grd->fill(1.);
      wvec.push_back(opset[names[I]].grd);
      fprintf(stderr, "renamin %s -> delta\n", names[I].c_str());
      opset.rename(names[I], "delta");
      names[I] = "delta";
      continue;
    }

    if (kcutS!=0.)  { // apply sharp-k cut
      opset[names[I]].grd->cutsharpk(kcutS, ftype);
    }
    
    // subtract mean (mean would contribute to 1+\delta)
    opset[names[I]].grd->subtract_mean();
    opset[names[I]].grd->go_real();
    wvec.push_back(opset[names[I]].grd);
  }
  
  // displace set of operators (in parallel)
  auto rvec =
    lptobject.weighted_Eulerian_density(wvec, Dnorm,
					supersampling, NGdelta,
					reduce ? myNG : NGdelta, true);

  // replace operators
  NT_assert(rvec.size() == wvec.size(),
	    "ERROR with weighted_Eulerian_density in generate_operatorSet_LPTEulerian:",
	    rvec.size(), wvec.size());

  // debug
  logger.printf("generate_operatorSet_LPTEulerian", DEBUG, "Done with weighted_Eulerian_density (%zu/%zu/%zu).", opset.getN(), wvec.size(), rvec.size());
  
  for (st I=0; I < rvec.size(); I++)  {
    if (opset[names[I]].order.pt == 1)  {
      rvec[I]->subtract_mean();
      opset[names[I]] = Opdata{rvec[I], op_order(1,0)};
      // rename the field here
      opset.rename(names[I], "delta");
      names[I] = "delta";
      continue;
    }
         
    // replace Lagrangian operator with this
    // - order unchanged
    // - keep Lagrangian names apart from "delta" for matter
    opset[names[I]].grd = rvec[I];
  }
}

} // end of namespace LEFTfield
