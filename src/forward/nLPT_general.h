/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef NLPT_GENERAL_H
#define NLPT_GENERAL_H

// ***********************************************************************
// cubedfields
//
// nLPT_general
// - implements n-th order LPT expansion for general expansion history,
//   as implemented by growthLCDM object (although it does not have to be LCDM)
// - initial conditions from EdS solution
// - derived from nLPT
// - new shapes are generated starting at sigma^(3) --> sigma^(3,0), sigma^(3,1)
//
// (C) Fabian Schmidt, 2020
// ***********************************************************************

#include <vector>
#include "forward/nLPT.h"

namespace LEFTfield {

class nLPT_general : public nLPT
{
  // settings for table interpolation
  static const st Ntable;
  static const double yin;
  table *gammatable;

  // integrate ODE for quadratic source terms to longitudinal component,
  // and add to tables
  void make_time_dependence_Q(LPTshape& Mout,
			      const LPTshape& M1, const LPTshape& M2) const;
  // integrate ODE for cubic source terms to longitudinal component
  // and add to tables
  // - weight takes into account different permutations with the same time
  //   dependence
  void make_time_dependence_C(LPTshape& Mout, const LPTshape& M1, const LPTshape& M2,
			      const LPTshape& M3, const double weight) const;
  // integrate ODE for quadratic source terms to transverse (curl) component
  // and add to tables
  // - sign takes into account different permutations with the same time
  //   dependence
  void make_time_dependence_curl(LPTshape& Mout,
				 const LPTshape& M1, const LPTshape& M2,
				 const double sign) const;

 protected:
  // construct next higher order from existing set of LPTshapes
  void constructM_general(const st n, const double kcut=0.,
			  const sharpkFilterType ftype=SPHERE);

 public:
  // construct distortion tensors up to including order n from given
  // density field
  nLPT_general(const scalar_grid& delta, const st n,
	       growthLCDM& gL,
	       const double kcut=0., const sharpkFilterType ftype=SPHERE);
  virtual ~nLPT_general()
    { }

  // returns Omega_m / f^2 - 1 at y = log(Dnorm)
  double gammaOfy(const double y) const {
    if (y <= yin)  return 0.;
    return gammatable->interpSpline(y);
  }
};

} // end of namespace LEFTfield

#endif
