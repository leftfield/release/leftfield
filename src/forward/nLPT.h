/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef NLPT_H
#define NLPT_H

// ***********************************************************************
// cubedfields
//
// nLPT: classes implementing n-th order LPT distortion tensor and
//       displacement field, as well as associated bias fields
//
// - contains vector of sigma^(n,p) (divergence of displacement contributions)
//   and curl s contributions, where n is order in perturbations and p labels
//   shapes at each order (only 1 in case of EdS approximation)
// -- encapsulated respectively by LPTsigma and LPTcurl structures
// - fields are constructed at time of given linear density field and
//   then scaled to desired time when evaluating total displacement, Eulerian
//   density, etc; time of given linear density defines Dnorm=1
// - general philosophy: keep grids in real space, return
//   Fourier-space total trace and displacement if desired
//
// regarding curl of displacement <-> antisymmetric part of M^(n):
// - does not have to be included explicitly in bias expansion
// - begins at third order, and only sources (anit)symmetric M^(n) at
//   sixth (fourth) order
// - included in LPT_EdS if macro LPT_WITH_CURL is defined in nLPT.cc;
//   equations see Appendix of paper_nLPT
// - fractional contribution to P(k) less than 3.e-5 in all cases considered,
//   hence ignore by default
//
// (C) Fabian Schmidt, 2020
// ***********************************************************************

#include <vector>
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "utilities/table.h"
#include "cosmology/cosmological.h"

namespace LEFTfield {

// ***********************************************************************
// structure encapsulating contributions to LPT displacement
// - shape and time dependence
// - note in general have several (2 or 3) time dependences with the same shape
// - parametrization of time dependence (y = log(Dnorm)):
// f(y) = w exp(n y) + alpha_t->interpSpline(y)
// f'(y) = n w exp(n y) + alphaprime_t->interpSpline(y)
// f''(y) = n^2 w exp(n y) + alphadprime_t->interpSpline(y) ,
//   (to make interpolation more accurate),
//   where alpha_* tables are only needed beyond EdS approximation

class LPTshape
{
 public:
  st order; // order in LPT
  st p; // index at fixed order
  double w; // coefficient of zero-order time dependence
  // table with beyond-EdS time dependence
  // - note time coordinate is y = log( Dnorm )
  sptr<table> alpha_t, alphaprime_t, alphadprime_t;

 LPTshape(st Order=0, st P=0, const double W=0.,
	  const double yin=0., const st Ntable=0)
    : order(Order), p(P), w(W)
   {
     if (Ntable)  make_tables(yin, Ntable, false);
   }

  // create tables with linear spacing y \in [yin, 0] and Ntable elements
  // - if reallocate==true, any existing tables are deleted
  void make_tables(const double yin, const st Ntable,
		   const bool reallocate = false);

  // evaluate time dependence and derivatives
  double aty(const double y) const;
  double prime_aty(const double y) const;
  void get_derivatives_aty(const double y,
			   double& f,
			   double& fprime,
			   double& fdprime) const;
  
  // inverse of above: given w, f, f', f'', add to w and alpha tables, specifically
  // w += w_1
  // alpha_t += f_1(y) - w_1 exp(n y)
  // alphaprime_t += f_1'(y) - n w_1 exp(n y)
  // alphadprime_t += f_1''(y) - n^2 w_1 exp(n y)
  void add_time_dependence(const st i, const double w,
			   const double f, const double fprime,
			   const double fdprime);
  
  // comparison operators based on order and shape index
  bool operator==(const LPTshape& LM) const {
    if (LM.order != order)  return false;
    if (LM.p != p) return false;
    return true;
  }

  bool operator<(const LPTshape& LM) const {
    return order < LM.order
      || (order == LM.order && p < LM.p);
  }
  bool operator>=(const LPTshape& LM) const {
    return ! (*this < LM);
  }
    
};


// structure encapsulating contributions to symmetric part of LPT distortion
// tensor M_ij:
//
// sigma_tot(q, y) = \sum_{LPTsigma M} M.aty(y) * M.sigma(q)
//
// where M.aty(y) returns M.w * exp(M.order * y) + M.alpha(y)

class LPTsigma : public LPTshape
{
  mutable sptr<tensor_grid> Mij;
  
 public:
  sptr<scalar_grid> sigma; // pointer to grid

  LPTsigma(st Order=0, st P=0, const double W=0.,
       const sptr<scalar_grid> &s=nullptr, const double yin=0., const st Ntable=0)
    : LPTshape(Order, P, W, yin, Ntable), sigma(s)
  { }
  ~LPTsigma()
    {
    }

  // return by pointer
  sptr<tensor_grid> get_tensor() const
  {
    if (Mij)  {
      if (sigma->is_real())  Mij->go_real();
      else  Mij->go_fourier();
      return Mij;
    }

    // debug
    logger.printf("LPTsigma::get_tensor", DEBUG,
		  "constructing new tensor grid (order %zu, NG=%zu).",
		  order, sigma->getNG());
    
    bool real = sigma->is_real();
    if (!real) { // construct tensor directly
      Mij = make_sptr<tensor_grid>(*sigma, false);
      return Mij;
    }

    // make copy of sigma grid and go to Fourier space
    scalar_grid sigmac(*sigma);
    sigmac.go_fourier();

    Mij = make_sptr<tensor_grid>(sigmac, false);
    Mij->go_real();
    return Mij;
  }

  void free_tensor() const {
    Mij = nullptr;
  }
};

// structure encapsulating contributions to curl of displacement
//
// \nabla \times s_tot(q, y) = \sum_{LPTcurl C} C.aty(y) * C.cs(q)
//
// where C.aty(y) returns C.w * exp(C.order * y + C.alpha)

class LPTcurl : public LPTshape
{
 public:
  sptr<vector_grid> cs; // pointer to grid

  LPTcurl(st Order=0, st P=0, const double W=0.,
	  const sptr<vector_grid> &CS=nullptr, const double yin=0., const st Ntable=0)
    : LPTshape(Order, P, W, yin, Ntable), cs(CS)
  { }
};



// ***********************************************************************
// nLPT
// - base class: provides total_trace, total_displacement functionalities
//   as well as (weighted) Eulerian fields
// - only constructs linear contribution to sigma
// - derived classes construct vM and vcs fields
// - base class handles cleanup though

class nLPT
{
 protected:
  st NG; // grid size
  st order; // maximum order which we work at
  std::vector<LPTsigma> vM; // vector of divergence constributions
  std::vector<LPTcurl> vcs; // vector of curl contributions
  
 public:
  // constructs linear distortion tensor from given density field
  nLPT(const scalar_grid& delta);
  virtual ~nLPT() {}

  const std::vector<LPTsigma>& get_divergence_shapes() const {
    return vM;
  }
  const std::vector<LPTcurl>& get_curl_shapes() const {
    return vcs;
  }

  void free_tensors() const;
  
  st getNG() const {
    return NG;
  }
  st getOrder() const {
    return order;
  }

  // generate total trace
  //  sigmatot = \sum_m=1^n \sum_p tr[M^(m,p)_ij]
  // - in Fourier space if desired
  scalar_grid total_trace(const double Dnorm,
			  const bool in_fourier = false) const;

  // generate trace contribution at fixed order
  //  sigma = \sum_p tr[M^(n,p)_ij] ; n = order
  // - in Fourier space if desired
  scalar_grid total_trace_at_order(const st order,
				   const double Dnorm,
				   const bool in_fourier = false) const;

  // generate total _symmetric_ distortion tensor
  //  Mtot_ij = \sum_m=1^n \sum_p M^(m,p)_ij
  // - allocates tensor
  // - in Fourier space if desired
  tensor_grid total_M(const double Dnorm,
		      const bool in_fourier = false) const;

  // generate displacement field
  //  vs^i = (\partial^i / \lapl) sigmatot
  // - in Fourier space if desired
  // - incorporates curl component for n >= 3
  vector_grid total_displacement(const double Dnorm,
				 const bool in_fourier = true) const;

  // get curl contribution to displacement
  vector_grid curl_displacement(const double Dnorm,
				const bool in_fourier = true) const;

  // !!!! would be nice to remove some of the code duplication in the
  //      following functions...
  
  // generate density field by displacing "particles"
  // - NGdelta is resolution of final density grid; if set to zero,
  //   use own NG
  // - supersampling is 1/(Lagrangian step size) in code units referring to
  //   density grid NGdelta, aka "particle grid size over Eulerian grid size"
  // - displacement field is upgraded to NGdelta in Fourier space
  // - interpolation: currently CIC (appears to perform best)
  // - assignment: currently CIC (appears to perform best)
  scalar_grid Eulerian_density(const double Dnorm,
			       const double supersampling=1.,
			       const st NGdelta=0,
			       const bool fourier=false) const;
  
  // generate density field by displacing "particles", weighted by
  // given scalar_field interpolated to particle position
  // - NGdelta is resolution of final density grid; if set to zero,
  //   use own NG
  // - supersampling is 1/(Lagrangian step size) in code units referring to
  //   density grid NGdelta, aka "particle grid size over Eulerian grid size"
  // - displacement field is upgraded to NGdelta in Fourier space
  // - interpolation: currently CIC (appears to perform best)
  // - assignment: currently CIC (appears to perform best)
  scalar_grid weighted_Eulerian_density(scalar_grid& wgrid,
					const double Dnorm,
					const double supersampling=1.,
					const st NGdelta=0,
					const bool fourier=false) const;

  // same as above, but for a whole set of operators
  // - parallelized
  // - uses grid size NGCIC for assignment, and reduces to NGfinal at the
  //   end, if NGfinal > 0
  // - needs memory space for (OMP_NUM_THREADS) * NGdelta^3 grids
  std::vector<sptr<scalar_grid>>
    weighted_Eulerian_density(const std::vector<sptr<scalar_grid>>& wvec,
			      const double Dnorm,
			      const double supersampling=1.,
			      const st NGCIC=0, const st NGfinal=0,
			      const bool fourier=false) const;

  // TO BE ADDED:
  // - (weighted) density in redshift space
  // - (weighted) density on lightcone
  
};



// ***********************************************************************
// nLPT_EdS
//
// - derived class using EdS approximation

class nLPT_EdS : public nLPT
{

 protected:
  // construct next higher order from existing set of LPTsigma
  void constructM_EdS(const st n, const double kcut=0.,
		      const sharpkFilterType ftype=SPHERE);

  public:
  // construct distortion tensors up to including order n from given
  // density field
  nLPT_EdS(const scalar_grid& delta,
	   const st n=1,
	   const double kcut=0., const sharpkFilterType ftype=SPHERE);
  virtual ~nLPT_EdS()
    { }
};

} // end of namespace LEFTfield

#endif
