/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef GENERATE_OPERATORSET_H
#define GENERATE_OPERATORSET_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cassert>
#include <algorithm>

#include "types.h"
#include "powerSpectrum.h"
#include "scalar_grid.h"
#include "operatorSet.h"
#include "cosmology/cosmological.h"

namespace LEFTfield {

// ********************************************************************
// forward/generate_operatorSet.h
//
// various routines to construct or add to operatorSets given linear density
// field
//
// (C) Fabian Schmidt, 2017-2021
// ********************************************************************


// *******************************************************************
// quadratic operators: delta^2, K_ij^2, s^k del_k delta

void generate_operatorSet_squared(operatorSet& opset, const scalar_grid& delta,
				  const bool with_displacement=true,
				  const bool fourier=true);

// *******************************************************************
// cubic operators: delta^3, delta K^2, K^3, O_td, s^k del_k delta^2,
//                  s^k del_k K^2

void generate_operatorSet_cubed(operatorSet& opset, const scalar_grid& delta,
				const bool with_displacement=true,
				const bool fourier=true);

// ******************************************************************
// Add higher derivatives according to relevance


// construct new scalar_grid with (\partial_k g1) (\partial^k g2)
// - returns real-space grid
sptr<scalar_grid> construct_partialOpartialO(const scalar_grid& g1,
					const scalar_grid& g2);

// construct higher derivative operators based on given operator_set
// - units refer to grid::Lbox
// - construct "all" (see below) that are of lesser or equal order to
// 'maxorder' according to given expansion_parameters object
// - returns number of fields added
// - max_derivatives is maximum number of derivative operations applied
//   (i.e. include terms with up to including \partial^{2 max_derivatives}
//    order); included because, depending on expansion_parameters, higher
//   derivatives might not be suppressed
//
// specifically, for given operators { O_i }, this constructs all products
// (\lapl O_{i_1}) O_{i_2} ... and
// (\partial_k O_{i_1}) (\partial^k O_{i_2}) O_{i_3} ...
// that are relevant according to 'maxorder' and expansion_parameters.
//
// NOT included are terms such as \partial_k M_{ij} \partial^j M^k_i ;
// these require more work to implement

st add_derivatives_full(operatorSet& opset,
			const op_order& maxorder,
			const expansion_parameters& ep,
			const st max_derivatives=2);


// add laplace of fields until they are of lesser or equal order to
// 'maxorder' according to given expansion_parameters object
// - units refer to grid::Lbox
// - returns number of fields added
// - max_derivatives: see above
// - note this is only small subset of derivative operators; left here for
//  legacy
st add_derivatives_laplace(operatorSet& opset,
			   const op_order& maxorder,
			   const expansion_parameters& ep,
			   const st max_derivatives=2);

} // end of namespace LEFTfield

#endif
