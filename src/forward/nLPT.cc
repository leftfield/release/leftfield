/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */


// ***********************************************************************
// cubedfields
//
// nLPT: classes implementing n-th order LPT distortion tensor and
//       displacement field, as well as associated bias fields
//
// - contains vector of sigma^(n,p) (divergence of displacement contributions)
//   and curl s contributions, where n is order in perturbations and p labels
//   shapes at each order (only 1 in case of EdS approximation)
// -- encapsulated respectively by LPTsigma and LPTcurl structures
// - fields are constructed at time of given linear density field and
//   then scaled to desired time when evaluating total displacement, Eulerian
//   density, etc; time of given linear density defines Dnorm=1
// - general philosophy: keep grids in real space, return
//   Fourier-space total trace and displacement if desired
//
// regarding curl of displacement <-> antisymmetric part of M^(n):
// - does not have to be included explicitly in bias expansion
// - begins at third order, and only sources (anit)symmetric M^(n) at
//   sixth (fourth) order
// - included in LPT_EdS if macro LPT_WITH_CURL is defined in nLPT.cc;
//   equations see Appendix of paper_nLPT
// - fractional contribution to P(k) less than 3.e-5 in all cases considered,
//   hence ignore by default
//
// (C) Fabian Schmidt, 2020
// ***********************************************************************

// whether to consistently include transverse (curl) displacement in nLPT
// construction:
// #define LPT_WITH_CURL

#include <cassert>
#ifdef WITH_OMP
#include <omp.h>
#endif

#include "forward/nLPT.h"
#include "assigners.h"

namespace LEFTfield {

// ***********************************************************************
// structure encapsulating contributions to LPT displacement
// - shape and time dependence

void LPTshape::make_tables(const double yin, const st Ntable,
			   const bool reallocate)
{
  // if tables exist and reallocate is not set, do nothing here
  if (alpha_t && !reallocate) return;
  
  alpha_t=make_sptr<table>(Ntable);
  alphaprime_t=make_sptr<table>(Ntable);
  alphadprime_t=make_sptr<table>(Ntable);

  const double dy = -yin / double(Ntable-1);

  double y = yin;
  for (st i=0; i < Ntable; i++)  {
    alpha_t->push_back(y, 0.);
    alphaprime_t->push_back(y, 0.);
    alphadprime_t->push_back(y, 0.);
    y += dy;
  }
}

  
double LPTshape::aty(const double y) const
{
  double ret = w * exp(double(order) * y);
  if (alpha_t)
    ret += alpha_t->interpSpline(y);
  return ret;
}

double LPTshape::prime_aty(const double y) const
{
  double ret = double(order) * w * exp(double(order) * y);    
  if (alphaprime_t)
    ret += alphaprime_t->interpSpline(y);

  return ret;
}

void LPTshape::get_derivatives_aty(const double y,
				   double& f,
				   double& fprime,
				   double& fdprime) const
{
  f = w * exp(double(order) * y);
  fprime = double(order) * f;
  fdprime = double(order*order) * f;
  if (!alpha_t)  return;

  f += alpha_t->interpSpline(y);
  fprime += alphaprime_t->interpSpline(y);
  fdprime += alphadprime_t->interpSpline(y);
}

// inverse of above: given w, f, f', f'', add to w and alpha tables, specifically
// w += w_1
// alpha_t += f_1(y) - w_1 exp(n y)
// alphaprime_t += f_1'(y) - n w_1 exp(n y)
// alphadprime_t += f_1''(y) - n^2 w_1 exp(n y)
void LPTshape::add_time_dependence(const st i, const double wp,
				   const double f, const double fprime,
				   const double fdprime)
{
  assert(alpha_t != 0);
  assert(i < alpha_t->getN());

  if (!i)  w += wp; // only add weight once!
  const double y = alpha_t->getx(i), f0 = wp*exp(double(order)*y);
  double alphap = f - f0;
  double alphaprimep = fprime - double(order) * f0;
  double alphadprimep = fdprime - double(order*order) * f0;
  if (logger.getLevel() >= DEBUG && ! isfinite(alphap) )  {
    logger.printf("LPTshape::add_time_dependence", DEBUG, "f = %.5e, alpha = %.5e", f, alphap);
  }
  // add values to table
  alpha_t->addy(i, alphap);
  alphaprime_t->addy(i, alphaprimep);
  alphadprime_t->addy(i, alphadprimep);

  // // debug
  // logger.printf("LPTshape::add_time_dependence", DEBUG "y = %.1e, %.3e / %.3e / %.3e (%.3e/%.3e/%.3e)",
  // 	  y, alpha_t->gety(i), alphaprime_t->gety(i), alphadprime_t->gety(i),
  // 	  f, fprime, fdprime);
}

// ************************************************************************
// nLPT
// - base class: provides total_trace, total_displacement functionalities
//   as well as (weighted) Eulerian fields
// - only constructs linear contribution to sigma
// - derived classes construct vM and vcs fields

// construct linear distortion tensor from given density field
nLPT::nLPT(const scalar_grid& delta)
  : NG(delta.getNG()), order(1)
{
  // n=1, w=1.
  LPTsigma M1(1, 0, 1., make_sptr<scalar_grid>(delta));
  *M1.sigma *= -1.; // sigma^(1) = -delta
  M1.sigma->go_real();

  // info
  if (logger.getLevel() >= INFO)  {
    logger.printf("nLPT::nLPT", INFO, "sigma^(1):");
    M1.sigma->print_stats(stderr);
  }

  // M^(1)
  vM.push_back(M1);
}

// get total trace
scalar_grid nLPT::total_trace(const double Dnorm,
			      const bool in_fourier) const
{
  scalar_grid tr(NG, true, 0.);
  
  for (auto& M : vM)  {
    tr.add(M.aty(log(Dnorm)), *M.sigma);
  }

  if (in_fourier)  tr.go_fourier();
  return tr;
}

// generate trace contribution at fixed order
//  sigma = \sum_p tr[M^(n,p)_ij] ; n = order
// - in Fourier space if desired
scalar_grid nLPT::total_trace_at_order(const st order,
				       const double Dnorm,
				       const bool in_fourier) const
{
  scalar_grid tr(NG, true, 0.);
  
  for (auto& M : vM)  {
    if (M.order != order)  continue;
    tr.add(M.aty(log(Dnorm)), *M.sigma);
  }

  if (in_fourier)  tr.go_fourier();
  return tr;
}


// generate total _symmetric_ distortion tensor
//  Mtot_ij = \sum_m=0^n M^(m)_ij
// - allocates tensor
// - in Fourier space if desired
tensor_grid nLPT::total_M(const double Dnorm,
			  const bool in_fourier) const
{
  scalar_grid sigma(total_trace(Dnorm, in_fourier));
  sigma.go_fourier();
  return tensor_grid(sigma, false);
}

// get curl contribution to displacement
vector_grid nLPT::curl_displacement(const double Dnorm,
				    const bool in_fourier) const
{
  vector_grid cs(NG, true);

  // add contributions
  for (auto& csn : vcs)
    cs.add(csn.aty(log(Dnorm)), *csn.cs);

  // invert curl
  cs.go_fourier();
  cs *= -1.;
  cs.curl().inverseLaplace();

  // info (additional FFT)
  if (logger.getLevel() >= INFO)  {
    logger.printf("nLPT::curl_displacement", INFO, "curl stats:");
    cs.print_stats(stderr);
  }
  
  if (in_fourier)  cs.go_fourier();
  else  cs.go_real();
  return cs;
}


// generate total displacement field
vector_grid nLPT::total_displacement(const double Dnorm,
				     const bool in_fourier) const
{
  vector_grid vs;

  // 1) Longitudinal part
  // note that sigma = \nabla.\v{s}, hence the + sign
  vs.generate_displacement(total_trace(Dnorm, true), 1.);

  if (!in_fourier)  vs.go_real();
  else vs.go_fourier();

  // 2) Curl part, if order is >= 3
  if (order >= 3)
    vs += curl_displacement(Dnorm, in_fourier);

  return vs;
}


// generate density field by displacing "particles"
// - NGdelta is resolution of final density grid; if set to zero,
//   use own NG
// - supersampling is 1/(Lagrangian step size) in code units referring to
//   density grid NGdelta, aka "particle grid size over Eulerian grid size"
// - displacement field is upgraded to NGdelta in Fourier space
// - interpolation: currently CIC (appears to perform best)
// - assignment: currently CIC (appears to perform best)
scalar_grid nLPT::Eulerian_density(const double Dnorm,
				   const double supersampling,
				   const st NGdelta,
				   const bool fourier) const
{
  const st NGd = NGdelta ? NGdelta : NG;
  
  // allocate array
  CICAssigner assigner(NGd);

  LogContext lc("nLPT::Eulerian_density");
  logger.printf(INFO, "generating displacement.");
  vector_grid vs(total_displacement(Dnorm, true));

  // upgrade to NGdelta grid
  logger.printf(INFO, "copysharpk.");
  vector_grid vsL(vs.copysharpk(NGd, 0., CUBE));
  vsL.go_real();
  // convert to grid units
  vsL *= double(vsL.getNG()) / grid::get_Lbox();

  // info
  if (logger.getLevel() >= INFO) {
    logger.printf(INFO, "displacement on CIC grid after unit conversion:");
    vsL.print_stats(stderr);
  }
  
  // run over grid, stepping with 1/supersampling
  // - NGss = particle grid size; ensure we have integer number of "particles"
  const st NGss = st( round(supersampling*double(NGd)) );
  // - step size in units of displacement grid
  const double f = double(NGd) / double(NGss);
  // - grid mass
  std::vector<double> m(1);
  m[0] = f*f*f; 
  // debug
  logger.printf(DEBUG, "NG = %zu, NG(s) = %zu, NGss = %zu, f = %.5f, m = %.3e",
		NG, vsL.getNG(), NGss, f, m[0]);

  std::vector<double> pos(3); // for assignment function
  const  bool trilinear = !(f==1.); // if f==1, then can use NGP instead of trilinear interpolation for speedup
  double qx=0.;
  for (st i=0; i < NGss; ++i) {
    double qy=0.;
    for (st j=0; j < NGss; ++j) {
      double qz=0.;
      for (st k=0; k < NGss; ++k) {
	// debug
	// logger.printf(DEBUG, "Eulerian_density: q = %.2f, %.2f, %.2f", qx, qy, qz);

	// get interpolated displacement and position,
	// taking into account rescaled coordinates
	real_vec3 s = vsL.interpolate_to(qx, qy, qz, trilinear);
	pos[0] = qx+s[0];
	pos[1] = qy+s[1];
	pos[2] = qz+s[2];
	
	// add to density, taking into account grid mass
        assigner.assign(pos, m);

	qz += f;
      }
      qy += f;
    }
    qx += f;
  }
  auto d = *assigner.getGrid();

  // info
  if (logger.getLevel() >= INFO)  {
    logger.printf(INFO, "Eulerian delta:");
    d.print_stats(stderr);
  }

  if (fourier) d.go_fourier();
  return d;
}

// generate density field by displacing "particles", weighted by
// given scalar_field interpolated to particle position
// - NGdelta is resolution of final density grid; if set to zero,
//   use own NG
// - supersampling is 1/(Lagrangian step size) in code units referring to
//   density grid NGdelta, aka "particle grid size over Eulerian grid size"
// - displacement field is upgraded to NGdelta in Fourier space
// - interpolation: currently CIC (appears to perform best)
// - assignment: currently CIC (appears to perform best)
scalar_grid nLPT::weighted_Eulerian_density(scalar_grid& wgrid,
					    const double Dnorm,
					    const double supersampling,
					    const st NGdelta,
					    const bool fourier) const
{
  const st NGd = NGdelta ? NGdelta : NG;

  // allocate array
  CICAssigner assigner(NGd);

  LogContext lc("nLPT::weighted_Eulerian_density");
  logger.printf(INFO, "generating displacement.");
  vector_grid vs(total_displacement(Dnorm, true));

  // upgrade to NGdelta grid
  logger.printf(INFO, "copysharpk.");
  vector_grid vsL(vs.copysharpk(NGd, 0., CUBE));
  vsL.go_real();
  // convert to grid units
  vsL *= double(vsL.getNG()) / grid::get_Lbox();

  // info
  if (logger.getLevel() >= INFO) {
    logger.printf(INFO, "displacement on CIC grid after unit conversion:");
    vsL.print_stats(stderr);
  }

  // get weight field on NGdelta grid
  wgrid.go_fourier();
  auto wgridL = wgrid.copysharpk(NGd, 0.);
  wgridL->go_real();
  
  // run over grid, stepping with 1/supersampling
  // - NGss = particle grid size; ensure we have integer number of "particles"
  const st NGss = st( round(supersampling*double(NGd)) );
  // - step size in units of displacement grid
  const double f = double(vsL.getNG()) / double(NGss);
  // - grid mass
  const double m = f*f*f; 
  // debug
  logger.printf(DEBUG, "NG = %zu, NG(s) = %zu, NGss = %zu, f = %.5f, m = %.3e",
		NG, vsL.getNG(), NGss, f, m);

  std::vector<double> mw(1), pos(3); // for assignment function
  const  bool trilinear = !(f==1.); // if f==1, then can use NGP instead of trilinear interpolation for speedup
  double qx=0.;
  for (st i=0; i < NGss; ++i) {
    double qy=0.;
    for (st j=0; j < NGss; ++j) {
      double qz=0.;
      for (st k=0; k < NGss; ++k) {

	// get interpolated displacement and position,
	// taking into account rescaled coordinates
	// - option 1: trilinear interpolation
	real_vec3 s = vsL.interpolate_to(qx, qy, qz, trilinear);
	pos[0] = qx+s[0];
	pos[1] = qy+s[1];
	pos[2] = qz+s[2];

	// get interpolated weight
	mw[0] = m * wgridL->interpolate_to(qx, qy, qz, trilinear);

	// add to density, taking into account grid mass and weight
	assigner.assign(pos, mw);

	qz += f;
      }
      qy += f;
    }
    qx += f;
  }
  auto d = *(assigner.getGrid());

  // info
  if (logger.getLevel() >= INFO) {
    logger.printf(INFO, "Eulerian field:");
    d.print_stats(stderr);
  }

  if (fourier) d.go_fourier();
  return d;
}



// same as above, but for a whole set of operators
// - parallelized
// - uses grid size NGCIC for assignment, and reduces to NGfinal at the
//   end, if NGfinal > 0
// - needs memory space for (OMP_NUM_THREADS) * NGdelta^3 grids
std::vector<sptr<scalar_grid>>
   nLPT::weighted_Eulerian_density(const std::vector<sptr<scalar_grid>>& wvec,
				   const double Dnorm,
				   const double supersampling,
				   const st NGCIC, const st NGfinal,
				   const bool fourier) const
{
  const st NGd = NGCIC ? NGCIC : NG;
  const st NGf = NGfinal ? NGfinal : NGCIC;

  LogContext lc("nLPT::weighted_Eulerian_density<vector>");
  logger.printf(INFO, "generating displacement.");
  vector_grid vs(total_displacement(Dnorm, true));

  // upgrade to NGdelta grid
  logger.printf(INFO, "copysharpk.");
  vector_grid vsL(vs.copysharpk(NGd, 0., CUBE));
  vsL.go_real();
  // convert to grid units
  vsL *= double(vsL.getNG()) / grid::get_Lbox();

  // info
  if (logger.getLevel() >= INFO) {
    logger.printf(INFO, "displacement on CIC grid after unit conversion:");
    vsL.print_stats(stderr);
  }

  // allocate return and weight fields
  logger.printf(INFO, "allocating.");
  std::vector<sptr<scalar_grid>> wLvec;
  std::vector<CICAssigner> assigners;
  for (st I=0; I < wvec.size(); I++)  {
    assigners.emplace_back(NGd);

    // get weight field on NGdelta grid
    wvec[I]->go_fourier();
    wLvec.push_back( wvec[I]->copysharpk(NGd, 0.) );
    wLvec.back()->go_real();  
  }

  // run over grid, stepping with 1/supersampling
  // - NGss = particle grid size; ensure we have integer number of "particles"
  const st NGss = st( round(supersampling*double(NGd)) );
  // - step size in units of displacement grid
  const double f = double(vsL.getNG()) / double(NGss);
  // - grid mass
  const double m = f*f*f; 
  const  bool trilinear = !(f==1.); // if f==1, then can use NGP instead of trilinear interpolation for speedup
  // debug
  logger.printf(DEBUG, "NG = %zu, NG(s) = %zu, NGss = %zu, f = %.5f, m = %.3e",
		NG, vsL.getNG(), NGss, f, m);
  // info
  logger.printf(INFO, "running over %zu weight fields.", wvec.size());
  
#if 0
// #pragma omp parallel default(shared)
//   {
// #pragma omp for 
#pragma omp parallel for schedule(dynamic,1)
    for (st I=0; I < wvec.size(); I++)  {
#ifdef WITH_OMP
      // debug
      logger.printf(DEBUG, "Thread %d/%d:  assigning.",
		    omp_get_thread_num(), omp_get_num_threads());
#endif

      std::vector<double> mw(1), pos(3); // for assignment function
      double qx=0.;
      for (st i=0; i < NGss; ++i) {
	double qy=0.;
	for (st j=0; j < NGss; ++j) {
	  double qz=0.;
	  for (st k=0; k < NGss; ++k) {

	    // get interpolated displacement and position,
	    // taking into account rescaled coordinates
	    // - option 1: trilinear interpolation
	    real_vec3 s = vsL.interpolate_to(qx, qy, qz, trilinear);
	    pos[0] = qx+s[0];
	    pos[1] = qy+s[1];
	    pos[2] = qz+s[2];

	    // get interpolated weight
	    mw[0] = m * wLvec[I]->interpolate_to(qx, qy, qz, trilinear);
	    
	    // add to density, taking into account grid mass and weight
	    assigners[I].assign(pos, mw);

	    qz += f;
	  }
	  qy += f;
	}
	qx += f;
      }

#ifdef WITH_OMP
      // debug
      logger.printf(DEBUG, "Thread %d/%d: done assigning.",
		    omp_get_thread_num(), omp_get_num_threads());
#endif
    } // end for
  // }

#else
// new approach: process arrays in slabs; each slab is processed in parallel
// first parallel block: determine pos and mw
// second parallel block: assign the computed pos and mw
// blocks must be separated to avoid concurrent writes to the assigner fields
  std::vector<double> pos(3*NGss*NGss);
  std::vector<std::vector<double>> mw(wvec.size());
  for (auto &v: mw) v.resize(NGss*NGss);

  for (st i=0; i < NGss; ++i) {
    double qx = i*f;
#pragma omp parallel
{
#pragma omp for schedule(static)
    for (st j=0; j < NGss; ++j) {
      double qy = j*f;
      for (st k=0; k < NGss; ++k) {
        double qz = k*f;
        size_t idx = k+j*NGss;

        // get interpolated displacement and position,
        // taking into account rescaled coordinates
        // - option 1: trilinear interpolation
        real_vec3 s = vsL.interpolate_to(qx, qy, qz, trilinear);
        pos[3*idx  ] = qx+s[0];
        pos[3*idx+1] = qy+s[1];
        pos[3*idx+2] = qz+s[2];

        // get interpolated weight
        for (st I=0; I < wvec.size(); I++)  {
          mw[I][idx] = m * wLvec[I]->interpolate_to(qx, qy, qz, trilinear);
        }
      }
    }

#pragma omp for schedule(dynamic,1)
    // add to density, taking into account grid mass and weight
    for (st I=0; I < wvec.size(); I++)
      assigners[I].assign(pos, mw[I]);
}
    }
#endif

  // now do copysharpk and/or FFT
  std::vector<sptr<scalar_grid>> rvec;
  for (st I=0; I < assigners.size(); I++)  {
    rvec.push_back(assigners[I].getGrid());
    // if different resolution wanted, do copysharpk and delete temporary
    if (NGf != NGd)  {
      auto d = rvec[I];
      d->go_fourier();
      auto df = d->copysharpk(NGf, 0.);
      rvec[I] = df;
    }
    
    if (fourier) rvec[I]->go_fourier();
  }

  return rvec;
}


void nLPT::free_tensors() const
{
  // info
  logger.printf("nLPT::free_tensors", INFO,
		"freeing up to %zu tensors.", vM.size());
  
  for (const auto& M : vM)
    M.free_tensor();
}



// *************************************************************************
// nLPT with EdS approximation

nLPT_EdS::nLPT_EdS(const scalar_grid& delta, const st n, const double kcut,
		   const sharpkFilterType ftype)
  : nLPT(delta)
{
  order = n;

  // recursively construct higher M^(m), m = 2, ... n
  for (st m=2; m <= n; m++)  {
    constructM_EdS(m, kcut, ftype);
  }
}



// construct tensor grid from set of lower-order tensors in nLPT
// - curl part ignored as mentioned above
// - sharp-k cut at "kcut" (in code units) is applied to each quadratic
//   and cubic combination of lower-order fields
void nLPT_EdS::constructM_EdS(const st n, const double kcut,
			      const sharpkFilterType ftype)
{
  LogContext lc("nLPT_EdS::constructM_EdS");
  logger.printf(INFO, "constructing n = %zu'th order displacement tensor.", n);
  
  // first, check if all grid sizes match, and go to real space in all grids
  for (auto& M : vM)  {
    assert(M.sigma->getNG() == NG);
    M.sigma->go_real();
  }
  for (auto& t : vcs)  {
    assert(t.cs->getNG() == NG);
    t.cs->go_real();
  }
  
  // ==== I. Longitudinal part ====

  // 1. quadratic terms
  auto sigma = make_sptr<scalar_grid>(NG, true, 0.);
  const double nminus3 = n-3.;
  double denom = double(n*n) + 0.5*nminus3;   
  for (st m=1; m < n; m++)  {
    double num = double(m*m) + (double(n)-double(m))*(double(n)-double(m))
      + 0.5*nminus3;
    if (num == 0.)  continue;
    double w = 0.5 * num / denom;
    // for fun/check
    logger.printf(DEBUG, "m = %zu, w = (1/2) %.1f / %.1f = %.5f",
		  m, num, denom, w);
    
    sigma->add(w, (*vM[m-1].get_tensor()) * (*vM[n-m-1].get_tensor()) );
    sigma->add(-w, (*vM[m-1].sigma) * (*vM[n-m-1].sigma) );

#ifdef LPT_WITH_CURL      
    // curl part: starts at order 3;
    //   so only contributes here if m >= 3 and n-m >= 3
    int cind1 = m-3, cind2 = n-m-3;
    if (cind1 >= 0 && cind2 >= 0)  {
      // debug
      logger.printf(DEBUG, "m = %zu, n-m = %zu, adding curl contribution.", m, n-m);

      // 1/2 times dot product of curls
      sigma->add(0.5*w, (*vcs[cind1].cs) * (*vcs[cind2].cs));
    }
#endif
  }

  // 2. cubic terms
  if (n >= 3)  {
    for (st m1=1; m1 < n; m1++)  {
      for (st m2=1; m2 < n; m2++)  {
	const st m3 = n-m1-m2;
	if (m3 <= 0 || m3 >= n)  continue;
	
	double num = double(m1*m1+m2*m2+m3*m3) + 0.5*nminus3;
	if (num == 0.)  continue;
	double w = -1./6. * num/denom;
	logger.printf(DEBUG, "m_i = %zu/%zu/%zu, w = -(1/6) %.1f / %.1f = %.5f",
		      m1, m2, m3, num, denom, w);
	vM[m1-1].sigma->go_fourier();
	tensor_grid M(*vM[m1-1].sigma, false);
	vM[m1-1].sigma->go_real();

	sigma->add(w,
          vM[m1-1].get_tensor()->doubleEpsContraction(*vM[m2-1].get_tensor(),
   					              *vM[m3-1].get_tensor()) );
#ifdef LPT_WITH_CURL
	if (n < 7)  continue;
	// curl part: M C C'
	// - run over permutations
	for (st ip=0; ip<3; ip++)  {
	  auto ind = permute<int>(ip, {int(m1)-3, int(m2)-3, int(m3)-3});
	  if (! (ind[1] >= 0 && ind[2] >= 0))  continue;
	  // M corresponds to ind[0]
	  st mind = ind[0]+2;
	  // debug
	  logger.printf(DEBUG, "m1 = %d, m2 = %d, m3 = %d, adding curl contribution.", ind[0], ind[1], ind[2]);

	  // add t M t' 
	  sigma->add(0.5*w, (*vcs[ind[1]].cs) *
		     ( (*vM[mind].get_tensor()) * (*vcs[ind[2]].cs) ) );
	}
#endif
      }
    }
  }

  // info (note: is before sharp-k cut)
  if (logger.getLevel() >= INFO)  {
    logger.printf(INFO, "sigma^(%zu), before sharp-k cut:", n);
    sigma->print_stats(stderr);
  }
  
  // construct tensor, go back to real space
  // - apply sharp-k cut here
  if (kcut > 0.)  {
    sigma->go_fourier();
    sigma->cutsharpk(kcut, ftype);
    sigma->go_real();
  }

  // w=1, since sigma already contains weight factors
  // p=0, since only one shape at each order
  LPTsigma Mp(n, 0, 1., sigma);
  Mp.sigma->go_real();
  vM.push_back(Mp);

  // ==== II. Curl part ====
  
  if (n < 3)  return;
  auto cs = make_sptr<vector_grid>(NG, true);
  denom = n;
  // sum is symmetric in m <-> n-m; term with m = n/2 vanishes
  // --> sum_{m=1}^{n/2} and multiply by 2 (no 1/2 in w)
  for (st m=1; m <= n/2; m++)  {
    if (2*m == n)  continue;
    double num = double(n - 2*m);
    double w = num / denom;
    logger.printf(DEBUG, "curl: n = %zu, m = %zu, w = %.1f / %.1f = %.5f",
		  n, m, num, denom, w);
    
    // '%' = vector product
    cs->add(w, (*vM[m-1].get_tensor()) % (*vM[n-m-1].get_tensor()) );

#ifdef LPT_WITH_CURL
    // curl part:
    int cind1 = m-3, cind2 = n-m-3;
    // debug
    if (cind1 >= 0 || cind2 >= 0)
      logger.printf(DEBUG, "m = %zu, n-m = %zu, adding curl contribution.", m, n-m);
    
    // 'cross'
    if (cind1 >= 0)  { // C^(a) M^(b)
      // tr(M) t^i
      cs->add(0.5*w, (*vM[n-m-1].sigma) * (*vcs[cind1].cs) );
      // - M_ij t^j
      cs->add(-0.5*w, (*vM[n-m-1].get_tensor()) * (*vcs[cind1].cs) );
    }
    if (cind2 >= 0)  { // M^(a) C^(b)
      // same as above, but opposite sign
      cs->add(-0.5*w, (*vM[m-1].sigma) * (*vcs[cind2].cs) );
      cs->add(0.5*w, (*vM[m-1].get_tensor()) * (*vcs[cind2].cs) );
    }
    
    // 'auto': only contributes here if m >= 3 and n-m >= 3
    if (cind1 >= 0 && cind2 >= 0)  {
      // 1/4 times vector product of curls
      cs->add(0.25*w, (*vcs[cind1].cs) % (*vcs[cind2].cs) );
    }
#endif
  }
  
  // info (note: is before sharp-k cut)
  if (logger.getLevel() >= INFO)  {
    logger.printf(INFO, "(curl s)^(%zu), before sharp-k cut:", n);
    cs->print_stats(stderr);
  }

  // - apply sharp-k cut here
  if (kcut > 0.)  {
    cs->go_fourier();
    cs->cutsharpk(kcut, ftype);
    cs->go_real();
  }
  
  // w=1, since cs field already contains weight factors
  // p=0, since only one shape at each order
  LPTcurl csp(n, 0, 1., cs);
  vcs.push_back(csp);
}

} // end of namespace LEFTfield
