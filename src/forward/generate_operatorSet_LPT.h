/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#ifndef GENERATE_OPERATORSET_LPT_H
#define GENERATE_OPERATORSET_LPT_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cassert>
#include <algorithm>

#include "operatorSet.h"
#include "scalar_grid.h"
#include "forward/nLPT.h"

namespace LEFTfield {

// ********************************************************************
// LPT bias operators
//
// constructed from set of Lagrangian longitudinal displacement contributions
// sigma^(n,p)
// - constructs all invariants and combinations thereof; fully general for
//   any expansion history, as long as appropriate nLPT object is given
// - also derived version that only includes total shape at each order, i.e.
//     M_ij^(n)(y) = \sum M_ij^(n,p) f^(n,p)(y)
// -- only differs starting at n=4
// - as discussed in bias review, no need to include curl component in bias
//   construction
// - construct at any given value of Dnorm (where Dnorm=1 is epoch of linear
//   density field passed to nLPT object)
//
// (C) Fabian Schmidt, 2020
// ********************************************************************

// ********************************************************************
// class LPTinvariants
//
// encapsulate all invariants up to some order n, out of which all operators
// are constructed by operator_set_LPT
// - base class implements interface and push_invariant functions;
//     derived classes implement actual construction of invariants
// - two derived classes: _general and _EdS
// ********************************************************************


// base class: only constructs linear invariant
class LPTinvariants
{
 protected:
  double y; // log(Dnorm) at which construction is done
  // shapes from LPTobject
  const std::vector<LPTsigma>& vM;
  // invariants, keep as operator_set for convenience
  operatorSet vInv;
  // vector of vectors holding the names of operators at each order
  std::vector< std::vector<std::string> > vOrder;

  // maybe just make part of constructor...
  void construct_invariants(const st order,
			    const std::vector<LPTsigma>& vM,
			    const double kcut,
			    const sharpkFilterType ftype);

  // fill given grid to relevant arrays, including name and order
  // - if flag set, also applies time dependence obtained by multiplying M1, M2, (, M3)
  void push_invariant(sptr<scalar_grid> O,
		      const LPTshape& M1, const LPTshape& M2,
		      const bool add_time_dep = true);
  void push_invariant(sptr<scalar_grid> O,
		      const LPTshape& M1, const LPTshape& M2,
		      const LPTshape& M3, const bool add_time_dep = true);

 public:
  LPTinvariants(const st order, // = order in PT up to which to construct
		const nLPT &lptobject,
		// normalized growth factor for which to construct
		const double Dnorm, 
		const double kcut=0.,
		const sharpkFilterType ftype=SPHERE);

  // fill invariants into given operator_set
  void fill_operatorSet(operatorSet& opset) const {
    opset.append(vInv);
  }
  
  // - return number of invariants at each order (via vOrder)
  st getNumInvariants(st order) const {
    assert(order >= 1 && order <= vOrder.size());
    return vOrder[order-1].size();
  }

  // - return name for invariant 'index' at order 'order'
  std::string getInvariantName(st order, st index) const {
    assert(order >= 1 && order <= vOrder.size());
    assert(index < vOrder[order-1].size());
    return vOrder[order-1][index];
  }

  // - get pointer to invariant 'index' at order 'order'
  const sptr<scalar_grid> getInvariant(st order, st index) const {
    return vInv[getInvariantName(order, index)].grd;
  }
  sptr<scalar_grid> getInvariant(st order, st index) {
    return vInv[getInvariantName(order, index)].grd;
  }
};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
// derived classes

// general: include all shapes individually
class LPTinvariants_general : public LPTinvariants
{
 public:
  LPTinvariants_general(const st order, // = order in PT up to which to construct
			const nLPT& lptobject,
			// normalized growth factor for which to construct
			const double Dnorm, 
			const double kcut=0.,
			const sharpkFilterType ftype=SPHERE);

};

// EdS: include only total M at each order
class LPTinvariants_EdS : public LPTinvariants
{
 public:
  LPTinvariants_EdS(const st order, // = order in PT up to which to construct
		    const nLPT& lptobject,
		    // normalized growth factor for which to construct
		    const double Dnorm, 
		    const double kcut=0.,
		    const sharpkFilterType ftype=SPHERE);

};

// ********************************************************************
// generate_operatorSet_LPT
//
// - build Lagrangian-space operators constructed from set of Lagrangian
//   longitudinal displacement contributions
// - optional sharp-k filter to avoid aliasing
//   (MUCH better option is to ensure grid is large enough that aliasing cannot
//    occur, i.e. k_Ny > n k_max(delta) )
// ********************************************************************

void generate_operatorSet_LPT(operatorSet& opset,
			      const st order, // = order in PT up to which to construct
			      const LPTinvariants& Linv,
			      const double kcut=0.,
			      const sharpkFilterType ftype=SPHERE);


// ********************************************************************
// generate_operatorSet_LPTEulerian
//
// - takes operatorSet with Lagrangian operators
// - displaces all operators to Eulerian space using functionality
//   provided by nLPT
// - optional sharp-k filter to avoid aliasing
//   (MUCH better option is to ensure grid is large enough that aliasing cannot
//    occur, i.e. k_Ny > n k_max(delta) )
// ********************************************************************

// construct all operators from LPTinvariants object, up to including order 'order',
// and use nLPT object to displace to Eulerian frame
// - supersampling, NGdelta passed on to nLPT for displacement operation
// - if 'reduce' is set, reduce grids from NGdelta to lptobject->NG in
//   Fourier space
// - kcutLPT, ftype passed on to operator_set_LPT
// - kcutS is applied to bias operators before displacement to Eulerian space
void generate_operatorSet_LPTEulerian(operatorSet& opset,
				      const st order,
				      const LPTinvariants& Linv,
				      const nLPT& lptobject,
				      // normalized growth factor for which to construct
				      const double Dnorm, 
				      const double supersampling=1.,
				      const st NGdelta=0.,
				      const bool reduce=false,
				      const double kcutLPT=0.,
				      const double kcutS=0.,
				      const sharpkFilterType ftype=SPHERE);

} // end of namespace LEFTfield

#endif
