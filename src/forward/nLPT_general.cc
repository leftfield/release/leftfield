/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */


// ***********************************************************************
// cubedfields
//
// nLPT_general
// - implements n-th order LPT expansion for general expansion history,
//   as implemented by growthLCDM object (although it does not have to be LCDM)
// - derived from nLPT
// - new shapes are generated starting at sigma^(3) --> sigma^(3,0), sigma^(3,1)
//
// (C) Fabian Schmidt, 2020
// ***********************************************************************

#include <cassert>
#ifdef WITH_OMP
#include <omp.h>
#endif

#include "forward/nLPT_general.h"
#include "utilities/integrate.h"

namespace LEFTfield {

// *************************************************************************
// nLPT with general expansion history

const double nLPT_general::yin = log(0.1);
const st nLPT_general::Ntable = 100;
// // tested, entirely negligible differences:
// const double nLPT_general::yin = log(0.03);
// const st nLPT_general::Ntable = 500;


nLPT_general::nLPT_general(const scalar_grid& delta, const st n,
			   growthLCDM& gL, 
			   const double kcut,
			   const sharpkFilterType ftype)
  : nLPT(delta)
{
  order = n;
  
  // make table with gamma vs y
  const double amin = 0.1 * exp(yin); // safely below time integration range
  const double dlna = -log(amin) / double(Ntable-1);

  gammatable = new table(Ntable);
  double lna = log(amin);
  for (st i=0; i < Ntable; i++)  {
    double a = exp(lna);
    double f, D = gL.growthFactorNormOfa(a, &f), Om = gL.OmegamOfa(a);
    double gamma = Om/(f*f) - 1.;
    gammatable->push_back(log(D), gamma); // y, gamma

    // debug
    if (i == Ntable-1)  {
      logger.printf("nLPT_general", DEBUG, "a = %.3e, D = %.3e, y = %.3e, f = %.3e, gamma = %.5e",
		    a, D, log(D), f, gamma);
    }
    
    lna += dlna;
  }
  
  // recursively construct higher M^(m), m = 2, ... n
  for (st m=2; m <= n; m++)  {
    constructM_general(m, kcut, ftype);
  }
}


namespace ns_nLPT { }

using namespace ns_nLPT;

namespace ns_nLPT {

  const LPTshape *M1int, *M2int, *M3int; // note these can be longitudinal or transverse
  const nLPT_general *lptInt;
  bool printint = false;

  // integrand of second-order source term to longitudinal component
  void QnLPT_int (double y, const std::array<double,2> &x, std::array<double,2> &f)
  {
    // get gamma and time evolution of source term
    double gamma = lptInt->gammaOfy(y);
    double m1 = M1int->aty(y);
    double m2, m2p, m2pp;
    M2int->get_derivatives_aty(y, m2, m2p, m2pp);

    f[0] = x[1];
    f[1] = -0.5 * (1.+3.*gamma) * x[1] + 1.5*(1.+gamma) * x[0]
      + m1 * (m2pp + 0.5 * (1.+3.*gamma) * m2p - 0.75*(1.+gamma)*m2);
  }

  // integrand of third-order source term to longitudinal component
  void CnLPT_int (double y, const std::array<double,2> &x, std::array<double,2> &f)
  {
    // get gamma and time evolution of source term
    double gamma = lptInt->gammaOfy(y);
    double m1 = M1int->aty(y), m2 = M2int->aty(y);
    double m3, m3p, m3pp;
    M3int->get_derivatives_aty(y, m3, m3p, m3pp);

    f[0] = x[1];
    f[1] = -0.5 * (1.+3.*gamma) * x[1] + 1.5*(1.+gamma) * x[0]
      -0.5 * m1*m2 * (m3pp + 0.5 * (1.+3.*gamma) * m3p - 0.5*(1.+gamma)*m3);
  }

  // integrand of second-order source term to transverse component
  void curlnLPT_int (double y, const std::array<double,2> &x, std::array<double,2> &f)
  {
    // get gamma and time evolution of source term
    double gamma = lptInt->gammaOfy(y);
    double m1 = M1int->aty(y);
    double m2, m2p, m2pp;
    M2int->get_derivatives_aty(y, m2, m2p, m2pp);

    f[0] = x[1];
    f[1] = -0.5 * (1.+3.*gamma) * x[1]
      + m1 * (m2pp + 0.5 * (1.+3.*gamma) * m2p );
  }

}



// integrate ODE for quadratic source terms to longitudinal component,
// and add to tables
void nLPT_general::make_time_dependence_Q(LPTshape& Mout,
					  const LPTshape& M1,
					  const LPTshape& M2) const
{
  lptInt = this;
  M1int = &M1; M2int = &M2;

  // EdS solution -> prefactor
  const double n = Mout.order, m = M2.order; // m: order of part with time derivative
  double w = 0.5*(4.*m*m + 2.*m-3.) / (2.*n*n + n - 3.);
  // include factors from M1, M2
  w *= M1.w * M2.w;
  
  if (w == 0.)  return; // nothing to add
  
  // IC (EdS)
  std::array<double,2> a, f;
  a[0] = w * exp(n*yin);
  a[1] = n * a[0];

  // integrate and fill tables
  double yprev = yin;
  for (st i=0; i < Ntable; i++)  {    
    double h = 1.e-3, y = Mout.alpha_t->getx(i);
    odeint(a, yprev, y, 1.e-8, h, 1.e-10, 1.e-3, QnLPT_int);
    QnLPT_int(y, a, f); // get second time derivative

    // fill tables
    Mout.add_time_dependence(i, w, a[0], a[1], f[1]);

    yprev = y;
  }

  // debug
  LogContext lc("nLPT_general::make_time_dependence_Q");
  logger.printf(DEBUG, "n = %.0f, m = %.0f, w_EdS += %.5f = %.5f",
		n, m, w/M1.w/M2.w, Mout.w/M1.w/M2.w);
  logger.printf(DEBUG, "alpha, alpha', alpha'' (final time) = %.5e, %.5e, %.5e",
		Mout.alpha_t->gety(Ntable-1),	Mout.alphaprime_t->gety(Ntable-1),
		Mout.alphadprime_t->gety(Ntable-1));
  // a[0], a[1], f[1]);
}

// integrate ODE for cubic source terms to longitudinal component
// and add to tables
// - weight takes into account different permutations with the same time
//   dependence
void nLPT_general::make_time_dependence_C(LPTshape& Mout,
					  const LPTshape& M1,
					  const LPTshape& M2,
					  const LPTshape& M3,
					  const double weight) const
{
  lptInt = this;
  M1int = &M1; M2int = &M2; M3int = &M3;

  // EdS solution -> prefactor
  const double n = Mout.order, m = M3.order; // m: order of part with time derivative
  double w = 0.5*(1. - m - 2.*m*m) / (2.*n*n + n - 3.);
  // include factors from M1, M2, M3
  w *= M1.w * M2.w * M3.w;
  
  if (w == 0.)  return; // nothing to add
  
  // IC (EdS)
  std::array<double,2> a, f;
  a[0] = w * exp(n*yin);
  a[1] = n * a[0];
    
  // integrate
  double yprev = yin;
  for (st i=0; i < Ntable; i++)  {    
    double h = 1.e-3, y = Mout.alpha_t->getx(i);
    odeint(a, yprev, y, 1.e-8, h, 1.e-10, 1.e-3, CnLPT_int);
    CnLPT_int(y, a, f); // get second time derivative

    // fill tables (with weight depending on permutations)
    Mout.add_time_dependence(i, weight*w, weight*a[0], weight*a[1],
			     weight*f[1]);

    yprev = y;
  }

  // debug
  LogContext lc("nLPT_general::make_time_dependence_C");
  logger.printf(DEBUG, "n = %.0f, m_i = %zu/%zu/%zu, w += %.5f = %.5f",
		n, M1.order, M2.order, M3.order, weight*w/M1.w/M2.w/M3.w,
		Mout.w/M1.w/M2.w/M3.w);
  logger.printf(DEBUG, "alpha, alpha', alpha'' (final time) = %.5e, %.5e, %.5e",
		Mout.alpha_t->gety(Ntable-1),	Mout.alphaprime_t->gety(Ntable-1),
		Mout.alphadprime_t->gety(Ntable-1));
  // a[0], a[1], f[1]);
}


// integrate ODE for quadratic source terms to transverse (curl) component
// and add to tables
// - sign takes into account different permutations with the same time
//   dependence
void nLPT_general::make_time_dependence_curl(LPTshape& Mout,
					     const LPTshape& M1,
					     const LPTshape& M2,
					     const double sign) const
{
  lptInt = this;
  M1int = &M1; M2int = &M2;

  // EdS solution -> prefactor
  const double n = Mout.order, m = M2.order; // m: order of part with time derivative
  double w = m*(1. + 2.*m) / n / (1.+2.*n);
  // include factors from M1, M2
  w *= M1.w * M2.w;
  
  if (w == 0.)  return; // nothing to add

  // IC (EdS)
  std::array<double,2> a, f;
  a[0] = w * exp(n*yin);
  a[1] = n * a[0];
    
  // integrate
  double yprev = yin;
  for (st i=0; i < Ntable; i++)  {    
    double h = 1.e-3, y = Mout.alpha_t->getx(i);
    odeint(a, yprev, y, 1.e-8, h, 1.e-10, 1.e-3, curlnLPT_int);
    curlnLPT_int(y, a, f); // get second time derivative

    // fill tables (with sign depending on permutation)
    Mout.add_time_dependence(i, sign*w, sign*a[0], sign*a[1], sign*f[1]);

    yprev = y;
  }

  // debug
  LogContext lc("nLPT_general::make_time_dependence_curl");
  logger.printf(DEBUG, "n = %.0f, m = %.0f, w += %.5f = %.5f",
		n, m, sign*w/M1.w/M2.w, Mout.w/M1.w/M2.w);
  logger.printf(DEBUG, "alpha, alpha', alpha'' (final time) = %.5e, %.5e, %.5e",
		Mout.alpha_t->gety(Ntable-1),	Mout.alphaprime_t->gety(Ntable-1),
		Mout.alphadprime_t->gety(Ntable-1));
  // a[0], a[1], f[1]);
}



// construct tensor grid from set of lower-order tensors in nLPT
// - curl part ignored as mentioned above
// - sharp-k cut at "kcut" (in code units) is applied to each quadratic
//   and cubic combination of lower-order fields
void nLPT_general::constructM_general(const st n,
				      const double kcut,
				      const sharpkFilterType ftype)
{
  LogContext lc("nLPT_general::constructM_general");
  logger.printf(INFO, "constructing n = %zu'th order displacement tensor.", n);
  if (n > 5)  {
    logger.printf(ESSENTIAL, "WARNING: (curl)^2 components missing for n > 5.");
  }
  
  // first, check if all grid sizes match, and go to real space in all grids
  for (auto& M : vM)  {
    assert(M.sigma->getNG() == NG);
    M.sigma->go_real();
  }

  // construct contributions to sigma^(n) = tr M^(n)
  std::vector<LPTsigma> vMp;
  
  // 1. quadratic terms: run over all M^(m,p), M^(n-m,p')
  st p=0; // shape index at fixed order
  for (auto& M1: vM) { // not 'const' as we switch M1.sigma b/w real/Fourier
    for (auto& M2: vM) {
      if (M1.order + M2.order != n)  continue;

      // for shape, only need to consider "M2 >= M1":
      if (M2 < M1) continue;
      
      // shape
      auto sigma = make_sptr<scalar_grid>(NG, true);	  
      sigma->add(1., *M1.get_tensor() * *M2.get_tensor());
      sigma->add(-1., (*M1.sigma) * (*M2.sigma) );

      // info (note: is before sharp-k cut)
      if (logger.getLevel() >= INFO) {
	logger.printf(INFO, "sigma^(%zu,%zu) (%zu/%zu), before sharp-k cut:",
		      n, p, M1.order, M2.order);
	sigma->print_stats(stderr);
      }

      // - apply sharp-k cut here
      if (kcut > 0.)  {
	sigma->go_fourier();
	sigma->cutsharpk(kcut, ftype);
	sigma->go_real();
      }
      
      // get weight and time dependence
      LPTsigma Mp(n, p, 0., sigma, yin, Ntable);
      make_time_dependence_Q(Mp, M1, M2);
      if (! (M2 == M1) ) //  add both permutations!
	make_time_dependence_Q(Mp, M2, M1);

      vMp.push_back(Mp);
      p++;
    }
  }

  // 2. cubic terms
  if (n >= 3)  {
    for (auto& M1: vM) {  // not 'const' as we switch M1.sigma b/w real/Fourier
      for (auto& M2: vM) {
	for (auto& M3: vM) {
	  if (M1.order + M2.order + M3.order != n)  continue;
	  
	  // for shape, only need to consider "M3 >= M2 >= M1":
	  if (M2 < M1) continue;
	  if (M3 < M2) continue;
	  
	  // shape
	  auto sigma = make_sptr<scalar_grid>(
		       M1.get_tensor()->doubleEpsContraction(*M2.get_tensor(),
							     *M3.get_tensor() ) );
	  
	  // info (note: is before sharp-k cut)
	  if (logger.getLevel() >= INFO) {
	    logger.printf(INFO, "sigma^(%zu,%zu) (%zu/%zu/%zu), before sharp-k cut:",
			  n, p, M1.order, M2.order, M3.order);
	    sigma->print_stats(stderr);
	  }

	  // - apply sharp-k cut here
	  if (kcut > 0.)  {
	    sigma->go_fourier();
	    sigma->cutsharpk(kcut, ftype);
	    sigma->go_real();
	  }
	
	  // get weight and time dependence
	  // - pay attention to number of independent permutations...
	  LPTsigma Mp(n, p, 0., sigma, yin, Ntable);
	  make_time_dependence_C(Mp, M1, M2, M3,
				 (M1==M2) ? 1. : 2.);
	  if (! (M1 == M3) )
	    make_time_dependence_C(Mp, M2, M3, M1, (M2==M3) ? 1. : 2.);

	  if (!(M2 == M3) && !(M2 == M1) )
	    make_time_dependence_C(Mp, M3, M1, M2, (M1==M3) ? 1. : 2.);

	  vMp.push_back(Mp);
	  p++;
	}
      }
    }
  }

  // append new shapes to vector
  vM.insert(std::end(vM), std::begin(vMp), std::end(vMp));
  
  // info
  logger.printf(INFO, "order=%zu, have %zu shapes for longitudinal component.", n, p);
  
  // II. Curl part
  
  if (n < 3)  return;
  p = 0; // reset shape counter
  std::vector<LPTcurl> vcsp;
  // sum is symmetric in m <-> n-m; term with M1 == M2 vanishes
  // --> run over all M^(m,p), M^(n-m,p') with p' != p
  for (auto& M1: vM) {  // not 'const' as we switch M1.sigma b/w real/Fourier
    for (auto& M2: vM) {
      if (M1.order + M2.order != n)  continue;
      if (M1 == M2)  continue; // would vanish

      // for shape, only need to consider "M2 >= M1":
      if (M2 < M1) continue;

      // shape
      // '%' = vector product
      auto cs = make_sptr<vector_grid>(*M1.get_tensor() % *M2.get_tensor() );

      // note that vM fields are already k cut; do not apply k cut here

      // get weight and time dependence - add both permutations!
      LPTcurl csp(n, p, 0., cs, yin, Ntable);
      //  add both permutations with opposite signs; M1==M2 already excluded above
      make_time_dependence_curl(csp, M1, M2, 1.);
      make_time_dependence_curl(csp, M2, M1, -1.);

      vcsp.push_back(csp);
      p++;
    }
  }

  // append new shapes to vector
  vcs.insert(std::end(vcs), std::begin(vcsp), std::end(vcsp));
  

  // info
  logger.printf(INFO, "order=%zu, have %zu shapes for transverse component.", n, p);

}

} // end of namespace LEFTfield
