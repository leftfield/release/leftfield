# LEFTfield / release

**Lagrangian, effective-field-theory based forward modeling of cosmological density fields.**

**Please cite [this paper](https://inspirehep.net/literature/1837166)** if you publish results based on this code.

A **list of related papers** is maintained [here](https://ui.adsabs.harvard.edu/public-libraries/Y35wKu4rQwuIYJDg3dzUHQ).

LEFTfield is released under GNU GPLv3.

## Brief overview

```src``` contains the library source, while ```codes``` contains the executables:

 - _correlations:_ codes for measuring statistics of fields.
 - _forward:_ forward-modeling code proper; see below.
 - _io:_ I/O; convert Gadget particle and halo finder outputs to HDF5 fields.
 - _ROOT:_ codes using ROOT package (histograms, Ntuples).
 - _tests:_ various small code tests; useful also for learning basic ingredients of LEFTfield.
 - _utilities:_ various utility codes.

**Forward codes:**
 - _forward_nLPT:_ take input linear matter density field and produce n-th order LPT output density field at desired redshift (used [here](https://inspirehep.net/literature/1837166)).
 - _forward_operatorLPT:_ take input linear matter density field and produce all bias operators relevant at n-th order in LPT, at desired redshift (used [here](https://inspirehep.net/literature/1820161)).

## Installation and building

**Prerequisites:** FFTW3 (non-MPI) and HDF5 (C bindings). The code is written in C++17.

 1. Clone repository and ```cd leftfield```.
 
 2. Create ```Makefile.local``` with compiler, flags, and library settings. See ```Makefile.local.default``` for an example based on ```$FFTW_HOME, $HDF5_HOME``` environment variables. The package ROOT is optional.

 3. Run ```make```.

 4. Add library paths to your ```LD_LIBRARY_PATH``` if necessary; e.g., ```export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$FFTW_HOME/lib:$HDF5_HOME/lib```.


You should be all set.



(C) 2017-2021 Fabian Schmidt, Martin Reinecke
