/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "cosmology/cosmological.h"

using namespace LEFTfield;

// *********************************************************************
// operator_scaling.cc
//
// given parameters max_order and Rstar computes
// how many total bias and stochastic terms will appear in the likelihood
// as a function of Lambda_scaling
//
// - if Rstar=0, uses exppar_LCDM_scaling, otherwise exppar_LCDM_Rstar
//
// (C) Fabian Schmidt, 2020
// *********************************************************************

const double Om=0.3, OL=1.-Om, h=0.7, ns=0.967;
const double knl0 = 0.25;

// number of operators at leading order inderivatives at each order
const st Ocount[] =
  { 0, 1, 2, 4, 8,
    0, 0, 0, 0, 0 // don't have those numbers right now...
  };

// Lambda values to run over
const double Lambdas[] =
  { 0.1, 0.14, 0.2, 0.25
  };
const st NLambda = 4;


void count_operators(exppar_LCDM_scaling *ep, const double Lambda,
		     const st max_order,
		     const st max_deriv = 2)
{
  static const char *labels[] = {
    "deterministic", "stochastic"
  };
  
  ep->setLambda(Lambda);

  op_order ref(max_order, 0, 0); // reference order to compare to
  if (max_order > 4)  {
    fprintf(stderr, "WARNING in count_operators: operator count will be wrong above fourth order.\n");
  }

  // - run over deterministic and stochastic operators
  for (st stoch=0; stoch <= 1; stoch++)  {
    st opcount=0;
    for (st deriv=0; deriv <= max_deriv; deriv++)  {
      for (st pt=1; pt <= max_order; pt++)  {
	op_order oo(pt, deriv, stoch);
	
	if (ep->is_lowerequal_order(oo, ref))  { // operators are relevant
	  // linear h. deriv. term: just lapl^n delta, n=0,1,2...	
	  st count = 1;
	  // higher-order h. deriv terms:
	  // roughly, nO = Ocount[pt] terms just from laplace acting on O,
	  // plus quadratic combinations (\lapl O) O', (\del O) (\del O').
	  // assume that number of quadratic combinations is of order nO
	  // itself --> 3^deriv * nO higher-derivative terms (very rough!)
	  if (pt > 1)  {
	    count = (st) pow(3., double(deriv)) * Ocount[pt];
	  }

	  fprintf(stderr, "pt order = %zu, deriv order = %zu: %s operators relevant. Adding %zu to operator count.\n",
		  pt, deriv, labels[stoch], count);
	  opcount += count;
	}
      }
    }
    fprintf(stderr, "Total %s operator count (very approximate!): %zu\n\n",
	    labels[stoch], opcount);
  }

}


int main(int argc, char **argv)
{
  // default settings
  st max_order = 3;
  double Rstar = 0.;
  double z = 0.;
  double knl3Peps = 100.;
  double tolerance = 0.;
  
  // read in arguments
  if (argc > 1) max_order = (st) atoi(argv[1]);
  if (argc > 2) Rstar = atof(argv[2]);
  if (argc > 3) z = atof(argv[3]);
  if (argc > 4) knl3Peps = atof(argv[4]);
  if (argc > 5) tolerance = atof(argv[5]);

  fprintf(stderr, "Running for max_order = %zu, Rstar = %.2f, z = %.2f, knl3Peps = %.2f, tolerance = %.2f\n",
	  max_order, Rstar, z, knl3Peps, tolerance);
  
  growthLCDM g(Om, OL);
  const double Dnorm = g.growthFactorOfz(z) / g.growthFactorOfz(0.);

  exppar_LCDM_scaling *ep = 0;

  // expansion_parameters object, depending on whether Rstar is set
  if (Rstar > 0.)  {
    ep = new exppar_LCDM_Rstar(Om, h, ns, 0.1, knl0, Rstar, knl3Peps, Dnorm,
			       tolerance);
  } else {
    ep = new exppar_LCDM_scaling(Om, h, ns, 0.1, tolerance);    
  }

  // now count operators
  for (st I=0; I < NLambda; I++)  {
    double Lambda = Lambdas[I];
    fprintf(stderr, "\n");
    fprintf(stderr, "=== Lambda = %.2f ===\n", Lambda);
    count_operators(ep, Lambda, max_order);
  }

  return 0;
}
