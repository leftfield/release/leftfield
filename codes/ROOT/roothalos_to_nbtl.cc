/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "grid.h"
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"
#include "assigners.h"

#include <TFile.h>
#include <TNtupleD.h>

using namespace LEFTfield;

// *********************************************************************
// roothalos_to_nbtl
//
// - read in halos from root file (generated using AHF_to_root for example)
// - select given general selection in ROOT format
// - assign to grid of given size
// - write grid to nbtl file (optionally in Fourier space)
// - !! currently, no weighting implemented !!
//
// (C) Fabian Schmidt, 2020
// *********************************************************************

// choose assignment scheme here:
#define ASSIGN_CIC
//#define ASSIGN_FT

// size of TTree buffer - set to maximum expected number of halos
const st Nselmax = 10000000;

double LNmean = 0., LNsigma = 1.;

// log-normal mass weighting
// - note that we don't need normalizing factor, since assign_density_halos
//   takes care of normalization
double fweight(const double M)
{
  double x = log10(M);
  return exp(-0.5 * (x-LNmean)*(x-LNmean) / (LNsigma*LNsigma));
}

int main(int argc, char **argv)
{
  if (argc < 5) {
    std::cerr << "\n Usage: " << argv[0] << " < root file >  < select string >  < NG >  <output file>  [< real or fourier (default real) >]  [< Lbox = 2000 Mpc/h>]\n\n";
    std::cerr << " Variable list for selection: "
	      << "ID:hostHalo:numSubStruct:Mvir:npart:Xc:Yc:Zc:VXc:VYc:VZc:Rvir:Rmax:r2:mbp_offset:com_offset:Vmax:v_esc:sigV:lambda:lambdaE:Lx:Ly:Lz:b:c:Eax:Eay:Eaz:Ebx:Eby:Ebz:Ecx:Ecy:Ecz:ovdens:nbins:fMhires:Ekin:Epot:SurfP:Phi0:cNFW\n\n";
    return 0;
  }

  // bool LNW = false;
  // const double lMmin = atof(argv[2]), lMmax = atof(argv[3]);
  // if (lMmax < 0.)  {
  //   LNW = true;
  //   LNmean = lMmin;  LNsigma = fabs(lMmax);
  //   std::cerr << "Employing log-normal weighting with mean " << LNmean
  // 	      << " and RMS " << LNsigma << ".\n";
  // }

  std::string select(argv[2]);  
  st NG = atoi(argv[3]);
  const char *output = argv[4];
  double Lbox = 2000.;
  if (argc > 6)  Lbox = atof(argv[6]);
  
  int real = 1;
  if (argc > 5)  {
    if (! strncasecmp(argv[5], "r", 1))  {
      real = 1;
      fprintf(stderr, "Writing real-space grid to '%s'.\n", output);
    } else if (! strncasecmp(argv[5], "f", 1))  {
      real = 0;
      fprintf(stderr, "Writing Fourier-space grid to '%s'.\n", output);
    } else {
      fprintf(stderr, "Wrong flag '%s'.\n", argv[5]);
      return 0;
    }
  }

  // read Ntuple from file
  TFile f(argv[1], "READ");
  TNtupleD* nt = (TNtupleD*) f.Get("nthalo");
  if (! nt )  {
    std::cerr << "Could not get Ntuple from file '" << argv[1] << "'.\n";
    return -1;
  }
  nt->SetEstimate(Nselmax);
  
  st Nselect = nt->Draw("Xc:Yc:Zc", select.c_str(), "goff");
  std::cerr << Nselect << " halos after selection.\n";
  if (Nselect == 0)  return 0;
  if (Nselect >= Nselmax)  {
    std::cerr << "ERROR: selected number > Nselmax = " << Nselmax << ". Increase that number.\n";
    return -1;
  }

  // assemble position vector
  const double *xvec = nt->GetV1();
  const double *yvec = nt->GetV2();
  const double *zvec = nt->GetV3();

  if (!xvec || !yvec || !zvec)  {
    fprintf(stderr, "Could not get vectors from Ntuple (%p/%p/%p)\n",
	    xvec, yvec, zvec);
    return -1;
  }
  
  std::vector<double> posvec, wvec;
  // fixed weight for now
  // can be generalized by adding fourth variable to Draw( )
  const double w = 1.; 

  // conversion to code units; AHF is in kpc
  const double conv = double(NG) / (1000.*Lbox); 
  for (st I=0; I < Nselect; I++)  {
    double x = xvec[I] * conv;
    if (x > double(NG)) x -= double(NG);
    if (x < 0.) x += double(NG);
    posvec.push_back(x);
    double y = yvec[I] * conv;
    if (y > double(NG)) y -= double(NG);
    if (y < 0.) y += double(NG);
    posvec.push_back(y);
    double z = zvec[I] * conv;
    if (z > double(NG)) z -= double(NG);
    if (z < 0.) z += double(NG);
    posvec.push_back(z);

    // // debug
    // if (! (I%10000) )  {
    //   fprintf(stderr, "%.5e / %.5e / %.5e\n", x,y,z);
    // }

    wvec.push_back(w);
  }

  // get sum of weights and normalizing factor (to ensure mean 0)
  double sow = 0.;
  for (st i = 0; i < wvec.size(); i++)  sow += wvec[i];
  const double fn = double(NG*NG*NG) / sow;
  for (st i = 0; i < wvec.size(); i++)  wvec[i] *= fn;
  fprintf(stderr, "Halo grid mass: %.5e\n", fn);

#ifdef ASSIGN_CIC
  CICAssigner assigner(NG);
#elif defined(ASSIGN_FT)
  FTAssigner assigner(NG);
#endif
  
  // assign
  assigner.assign(posvec, wvec);

  // get grid
  auto deltaCIC = assigner.getGrid();

  // some helpful info
  deltaCIC->print_stats(stderr);
  
  // go to Fourier space if desired
  if (!real)
    deltaCIC->go_fourier();

  // write output
  deltaCIC->write(output);

  return 0;
}
