/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>
#include <algorithm>

#include "grid.h"
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"

#include <TH1D.h>
#include <TFile.h>

using namespace LEFTfield;

// *********************************************************************
// histograms
//
// - read in two fields in nbtl format (have to be equal size)
// - fill histograms of cell-by-cell grid values and their difference
//
// (C) Fabian Schmidt, 2020
// *********************************************************************

const sharpkFilterType ftype = SPHERE; // cut type
const int Nbins = 100; // histogram bins

int main(int argc, char **argv)
{
  if (argc < 4) {
    std::cerr << "\n Usage: " << argv[0] << " < nbtl file 1 > < nbtl file 2 > <output root file> [<Lambda = sharp-k cut; default none>] [<Lbox = 2000>]\n\n";
    return 0;
  }

  // read in grids
  scalar_grid g1, g2;
  g1.read(argv[1]);
  g2.read(argv[2]);

  if (g1.getNG() != g2.getNG())  {
    std::cerr << "Grids have to have same size.\n";
    return -1;
  }

  // apply k cut if present  
  const double Lambda = (argc > 4) ? atof(argv[4]) : 0.;
  const double Lbox = (argc > 5) ? atof(argv[5]) : 2000.;
  grid::set_Lbox(Lbox);
  if (Lambda > 0.)  {
    // determine k_cut = Lambda divided by the fundamental of the box
    double kcut = Lambda;
    fprintf(stderr, "Lambda = %.2f = kcut\n", Lambda);
    g1.cutsharpk(kcut, ftype);
    g2.cutsharpk(kcut, ftype);
  }
  
  g1.go_real();
  g2.go_real();
  const st NG = g1.getNG();

  // histogram ranges
  double m1, m2;
  double D1 = 5.*sqrt( g1.get_variance(&m1) );
  double D2 = 5.*sqrt( g2.get_variance(&m2) );

  // allocate histograms
  TH1D *h1 = new TH1D("h1", "", Nbins, - D1, D1);
  TH1D *h2 = new TH1D("h2", "", Nbins, - D2, D2);
  TH1D *hdiff = new TH1D("hdiff", "", Nbins,
			 - fmax(D1, D2), fmax(D1,D2));
      
  // run over grid and fill histograms
  // - subtract mean in process, to make difference more meaningful
  // (NOT parallel; if needed could be parallelized as in
  //  https://root.cern/doc/master/mt201__parallelHistoFill_8C.html)
  for (st ix = 0; ix < NG; ix++) {
    for (st iy = 0; iy < NG; iy++) {
      for (st iz = 0; iz < NG; iz++) {
	double v1 = g1(ix,iy,iz) - m1;
	double v2 = g2(ix,iy,iz) - m2;
	h1->Fill(v1);
	h2->Fill(v2);
	hdiff->Fill(v1-v2);
      }
    }
  }

  // some diagnostic printout
  fprintf(stderr, "h1: mean = %.5e, RMS = %.5e\n", h1->GetMean(), h1->GetRMS());
  fprintf(stderr, "h2: mean = %.5e, RMS = %.5e\n", h2->GetMean(), h2->GetRMS());
  fprintf(stderr, "hdiff: mean = %.5e, RMS = %.5e\n",
	  hdiff->GetMean(), hdiff->GetRMS());
  
  // write histograms to output file
  TFile f(argv[3], "RECREATE");
  h1->Write();
  h2->Write();
  hdiff->Write();
  f.Close();

  return 0;
}
