/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "grid.h"
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"

#include <TFile.h>
#include <TNtupleD.h>

using namespace LEFTfield;

// *********************************************************************
// AHF_to_root
//
// - read in halos in AHF format
// - convert all fields to ROOT TNtuple for further processing
//
// (C) Fabian Schmidt, 2020
// *********************************************************************

int main(int argc, char **argv)
{
  if (argc < 3) {
    std::cerr << "\n Usage: " << argv[0] << " < AHF file >  <output root file>\n\n";
    std::cerr << " Resulting variable list in TNtuple: "
	      << "ID:hostHalo:numSubStruct:Mvir:npart:Xc:Yc:Zc:VXc:VYc:VZc:Rvir:Rmax:r2:mbp_offset:com_offset:Vmax:v_esc:sigV:lambda:lambdaE:Lx:Ly:Lz:b:c:Eax:Eay:Eaz:Ebx:Eby:Ebz:Ecx:Ecy:Ecz:ovdens:nbins:fMhires:Ekin:Epot:SurfP:Phi0:cNFW\n\n";
    return 0;
  }

  const char *output = argv[2];

  // create ROOT Ntuple from AHF file and select halos
  std::ifstream ifs(argv[1]);
  if (! ifs.is_open() )  {
    std::cerr << "Cannot open halo file '" << argv[1] << "'.\n";
    return -1;
  }

  // #ID(1)	hostHalo(2)	numSubStruct(3)	Mvir(4)	npart(5)	Xc(6)	Yc(7)	Zc(8)	VXc(9)	VYc(10)	VZc(11)	Rvir(12)	Rmax(13)	r2(14)	mbp_offset(15)	com_offset(16)	Vmax(17)	v_esc(18)	sigV(19)	lambda(20)	lambdaE(21)	Lx(22)	Ly(23)	Lz(24)	b(25)	c(26)	Eax(27)	Eay(28)	Eaz(29)	Ebx(30)	Eby(31)	Ebz(32)Ecx(33)	Ecy(34)	Ecz(35)	ovdens(36)	nbins(37)	fMhires(38)	Ekin(39)	Epot(40)	SurfP(41)	Phi0(42)	cNFW(43)
  TFile f(output, "RECREATE");
  TNtupleD nt("nthalo", "nthalo",
	      "ID:hostHalo:numSubStruct:Mvir:npart:Xc:Yc:Zc:VXc:VYc:VZc:Rvir:Rmax:r2:mbp_offset:com_offset:Vmax:v_esc:sigV:lambda:lambdaE:Lx:Ly:Lz:b:c:Eax:Eay:Eaz:Ebx:Eby:Ebz:Ecx:Ecy:Ecz:ovdens:nbins:fMhires:Ekin:Epot:SurfP:Phi0:cNFW");
  st Nh = nt.ReadStream(ifs);
  std::cerr << "Read " << Nh << " halos into Ntuple.\n";
  st Nbytes = f.Write();
  std::cerr << "Writing " << Nbytes << " to " << output << ".\n";
  f.Close();
  return 0;
}
