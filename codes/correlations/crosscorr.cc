/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>
#include <algorithm>

#include "scalar_grid.h"
#include "powerSpectrum.h"

using namespace LEFTfield;

// *********************************************************************
// crosscorr
//
// - read in two fields in HDF5 or nbtl format
// - print out auto- and cross power spectra
//
// (C) Fabian Schmidt, 2019
// *********************************************************************

// default number of k bins
const int Nbins_def = 264;

// default HDF5 field name
const std::string deffield("delta");

int main(int argc, char **argv)
{
  if (argc < 3) {
    std::cerr << "\n Usage: " << argv[0] << " < nbtl file 1 > < nbtl file 2 > [< Lbox = 2000 Mpc/h, for scaling power spectra >] [<field name = delta>] [<field name 2>] [<N_kbins = 264>]\n\n";
    return 0;
  }

  // grid size for final grid sharp-k filtered grids
  // const st NG = atoi(argv[3]);
  const int Nbins = argc > 6 ? atoi(argv[6]) : Nbins_def;

  double Lbox = 2000.;
  if (argc > 3)  Lbox = atof(argv[3]);
  grid::set_Lbox(Lbox);

  // read in grids
  scalar_grid rg1;
  scalar_grid rg2;
  rg1.read(argv[1], argc > 4 ? std::string(argv[4]) : deffield);
  rg2.read(argv[2], argc > 5 ? std::string(argv[5]) : deffield);

  // copy sharp k to destination grids
  // determine minimum grid size
  st NGmin = std::min(rg1.getNG(), rg2.getNG());
  scalar_grid g1(NGmin);
  scalar_grid g2(NGmin);
  fprintf(stderr, "Grid sizes: NG1 = %zu, NG2 = %zu, P(k) grid: %zu\n",
	  rg1.getNG(), rg2.getNG(), NGmin);
  double kcut = grid::get_kNy(NGmin);
  fprintf(stderr, "Doing copysharpk to Nyquist of NG = %zu, kcut = %.3e h/Mpc\n", NGmin, kcut);
  rg1.copysharpk(g1, kcut);
  rg2.copysharpk(g2, kcut);
  
  // compute power spectra
  const double lkMin = log10(0.003), lkMax = 2.4597;
  powerSpectrumSet pks(lkMin, lkMax, Nbins);

  g1.auto_powerspectrum(* pks.push_back("CIC-auto"));
  g2.auto_powerspectrum(* pks.push_back("CIC-auto"));
  g1.cross_powerspectrum(g2, * pks.push_back("cross"));

  printf("# Grid sizes: NG1 = %zu, NG2 = %zu, P(k) grid: %zu\n",
	 rg1.getNG(), rg2.getNG(), NGmin);
  pks.print(stdout);

  return 0;
}
