/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <stdlib.h>
#include <math.h>

// obviously should all be in library
#include "grid.h"
#include "scalar_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"
#include "assigners.h"

using namespace LEFTfield;

// *********************************************************************
// simplePk
//
// Measure P(k) on a single file
// - right now, read in particles in HPM format
// - mostly for cross-checking
//
// (C) Fabian Schmidt, 2017
// *********************************************************************

// whether to average two shifted grids for anti-aliasing
#define ANTIALIAS

// whether to just print out lowest Fourier modes
// #define MODEOUT

// *********************************************************************
// main - driver routine for any of the above

int main(int argc, char **argv)
{  
  if (argc < 5) {
    std::cerr << "\n Usage: simplePk < file > <NG_1d> <Np_1d> <Lbox> [<NG for P(k)>]\n\n";
    return 0;
  }

  size_t NG_HPM = (size_t) atoi(argv[2]);
  size_t Np = atoi(argv[3]);
  size_t NP = Np*Np*Np;
  double Lbox = atof(argv[4]);
  grid::set_Lbox(Lbox);
  size_t NG = NG_HPM;
  if (argc > 5)  NG = atoi(argv[5]);
  fprintf(stderr, "Running for Lbox = %.2e Mpc/h, NG(HPM) = %ld, power spectrum grid: %ld\n",
	  Lbox, NG_HPM, NG);
  CICAssigner assigner(NG);
  assign_density_HPM(assigner, argv[1], NG_HPM, NP);
  auto g = *assigner.getGrid();
  g.print_stats(stderr);

#ifdef ANTIALIAS  
  fprintf(stderr, "Reading in second, shifted grid for anti-aliasing...\n");
  
  assign_density_HPM(assigner, argv[1], NG_HPM, NP,
			0.5, 0.5, 0.5);
  auto g2 = *assigner.getGrid();
  g2.print_stats(stderr);
  
  g += g2;
  g *= 0.5;
#endif

  g.print_stats(stderr);

#ifdef MODEOUT
  // print out Fourier modes up to same maximum scale, for checking
  // growth mode by mode
  g.go_fourier();

  const double kmax = 0.2;
  const double kw = 2.*M_PI / Lbox; // fund. freq. in h Mpc^-1
  const int Imax = kmax / kw;
  
  // run over grid
  for(int i=0; i<Imax; ++i) {
    for(int j=0; j<Imax; ++j) {
      for(int k=0; k<Imax; ++k) {
	int im = (i < NG/2 ? i: i-NG);
	int jm = (j < NG/2 ? j: j-NG);
	int km = (k < NG/2 ? k: k-NG);

	printf("%d %d %d %.6e %.6e %.6e   %.6e %.6e\n",
	       im, jm, km, kw*double(im), kw*double(jm), kw*double(km),
	       g.Re(i,j,k), g.Im(i,j,k) );
      }
    }
  }
  return 0;
#endif
  
  // powerSpectrum Pk(-3., 1., 20);
  // to match HPMpowerspectrum.cc:
  const double lkMin = log10(0.003), lkMax = 2.4597;
  const int Nbins = 66;
  powerSpectrum Pk(lkMin, lkMax, Nbins);

  // optionally smooth
  // g.smooth_Gaussian(5., false);
  
  g.auto_powerspectrum(Pk);
  Pk.print(stdout);

  return 0;
}
