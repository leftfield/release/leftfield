/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <stdlib.h>
#include <math.h>

// obviously should all be in library
#include "grid.h"
#include "scalar_grid.h"
#include "powerSpectrum.h"
#include "tensor_grid.h"
#include "forward/generate_operatorSet.h"
#include "assigners.h"

using namespace LEFTfield;

// *********************************************************************
// squaredfield_estimator
//
// Calculate auto- and cross-power spectra of squared fields:
// delta^2, (K_ij)^2
//
// - warmup and check for cubed field
// - checked on initial conditions and compared with exact prediction (see calc_truth.cc and plot_check.gnu)
//
// (C) Fabian Schmidt, 2017
// *********************************************************************

// whether to read in Gadget files (otherwise HPM)
#define READ_GADGET

// whether to average two shifted grids for anti-aliasing
//#define ANTIALIAS

#ifdef ANTIALIAS
#include "assign_Gadget_antialias.cc"
#endif

// *********************************************************************
// main - driver routine for any of the above

int main(int argc, char **argv)
{
  // ---------------------------------------------------------------------
  // O. read in density field
  
#ifdef READ_GADGET
  if (argc < 4) {
    std::cerr << "\n Usage: " << argv[0] << " < base file > < N_files > <NG_1d>  [<smoothing scale>] [<factor to scale density>]\n\n";
    return 0;
  }

  const int Nfiles = atoi(argv[2]);
  const size_t NG = atoi(argv[3]);
  double D = 1., R=0.;;
  if (argc > 4)  R = atof(argv[4]);
  if (argc > 5)  D = atof(argv[5]);

  CICAssigner assigner(NG);
#ifdef ANTIALIAS  
  scalar_grid delta(NG);
  const double Lbox = assign_Gadget_antialias(delta, argv[1], Nfiles);
#else
  const double Lbox = assign_density_Gadget(assigner, argv[1], Nfiles);
  auto delta = *assigner.getGrid();
#endif
  fprintf(stderr, "Lbox = %.2f h^-1 Mpc\n", Lbox);

#else
  if (argc < 5) {
    std::cerr << "\n Usage: " << argv[0] << " < file > <NG_1d> <Np_1d> <Lbox> [<smoothing scale>] [<factor to scale density>]\n\n";
    return 0;
  }

  const size_t NG = atoi(argv[2]);
  const size_t Np = atoi(argv[3]);
  const size_t NP = Np*Np*Np;
  const double Lbox = atof(argv[4]);
  double D = 1., R=0.;;
  if (argc > 5)  R = atof(argv[5]);
  if (argc > 6)  D = atof(argv[6]);

  scalar_grid delta(NG);
  delta.assign_density_HPM(argv[1], NP);
#endif
  grid::set_Lbox(Lbox);

  // ---------------------------------------------------------------------
  // I. scale and smooth density
  
  fprintf(stderr, "Smoothing scale: R = %.3f h^-1 Mpc = %.3f grid cells.\n",
	  R, R/Lbox*double(NG));
  delta *= D;

  // smooth
  scalar_grid deltaR(delta);
  deltaR.smooth_Gaussian(R, false);
  
  // ---------------------------------------------------------------------
  // II. construct quadratic operators

  fprintf(stderr, "--- constructing unsmoothed fields ---\n");
  operatorSet Oset, OsetR;
  generate_operatorSet_squared(Oset, delta);
  // print stats for check
  Oset.print_stats(stderr);
  
  fprintf(stderr, "--- constructing smoothed fields ---\n");
  generate_operatorSet_squared(OsetR, deltaR);
  // print stats for check
  OsetR.print_stats(stderr);

  // ---------------------------------------------------------------------
  // III. measure power spectra
  
  // power spectra
  // log k bins to match HPMpowerspectrum.cc:
  const double lkMin = log10(0.003), lkMax = 2.4597;
  const int Nbins = 66;
  powerSpectrumSet pks(lkMin, lkMax, Nbins);
  powerSpectrumSet pksR(lkMin, lkMax, Nbins);
  powerSpectrumSet pkscross(lkMin, lkMax, Nbins);
  powerSpectrumSet pkscross2(lkMin, lkMax, Nbins);

  // unsmoothed:
  Oset.fill_powerspectra(pks);
  // smoothed:
  OsetR.fill_powerspectra(pksR);
  // cross between smoothed and unsmoothed (two permutations: equal at tree level):
  OsetR.fill_powerspectra(Oset, pkscross);
  Oset.fill_powerspectra(OsetR, pkscross2);
  
  // print cross power spectra (relevant for bias estimation)
  FILE *fpc = fopen("Pksquared_init_up.dat","w");
  pkscross.print(fpc);
  fclose(fpc);
  //pkscross.print(stdout);
  //printf("\n");
  //fflush(stdout);

  FILE *fpc2 = fopen("Pksquared_init_low.dat","w");
  pkscross2.print(fpc2);
  fclose(fpc2);
  //pkscross2.print(stdout);
  //fflush(stdout);
  
  // print auto power spectra for comparison / check
  FILE *fp = fopen("Pksquared_unsmoothed.dat", "w");
  pks.print(fp);
  fclose(fp);

  fp = fopen("Pksquared_smoothed.dat", "w");
  pksR.print(fp);
  fclose(fp);

  return 0;
}
