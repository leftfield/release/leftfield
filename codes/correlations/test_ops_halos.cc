/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <stdlib.h>
#include <math.h>

#include "grid.h"
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"
#include "forward/generate_operatorSet.h"
#include "param/parameter_base.h"

using namespace LEFTfield;

// *********************************************************************
// test_ops_cross
//
// - for testing construction of sharp-k filtered operators
//
// (C) Fabian Schmidt, 2019
// *********************************************************************

// *********************************************************************
// settings

// grid size for sharp-k filtered grids
// - should be large enough so that k_Ny > n_PT Lambda
const st NG = 768;//384;

// *********************************************************************
// global variables

double Lbox=2000., Lambda=0.1, kmax=0.;
std::string outfile, prefix;
sharpkFilterType ftypeO(SPHERE);

// --------------------------------------------------------------------------
// setup: parse 'eftlike' command line, read in grids, apply sharp-k cut
// and restrict to given NG
// - returns 0 if successful
int setup(int argc, char **argv, scalar_grid& deltaLambda,
	  scalar_grid& deltah, const st /*NG*/)
{
  if (argc < 3) {
    std::cerr << "\n Usage: " << argv[0] << " < matter nbtl file > < halo nbtl file >  [<Lbox = 2000 Mpc/h>]  [<Lambda = 0.1 h/Mpc>] [<output file>]  [<prefix (e.g. other parameter values) for output]  [<kmax/Lambda=0.5>]\n\n";
    return 1;
  }

  if (argc > 3)  Lbox = atof(argv[3]);
  if (argc > 4)  Lambda = atof(argv[4]);
  if (argc > 5)  {
    outfile = argv[5];
    fprintf(stderr, "Writing results to '%s'.\n", outfile.c_str());
  }
  if (argc > 6)  {
    prefix = argv[6];
    fprintf(stderr, "Prefix: '%s'.\n", prefix.c_str());
  }
  kmax = 0.5 * Lambda;
  if (argc > 7)  kmax = atof(argv[7]) * Lambda;
  grid::set_Lbox(Lbox);

  // determine k_cut
  double kcut = Lambda;
  fprintf(stderr, "Lbox = %.2f h^-1 Mpc, Lambda = %.3f h Mpc^-1, kmax = %.2f Lambda; sharpkFilterType = %d\n",
	  Lbox, Lambda, kmax/Lambda, ftypeO);

  // I. matter
  {
  scalar_grid deltaCIC;
  deltaCIC.read(argv[1]);
  // debug
  //  deltaCIC->print_stats(stderr);
  
  // copy grid in Fourier space
  deltaCIC.go_fourier();
  st N_modes=0;
  deltaCIC.copysharpk(deltaLambda, kcut, ftypeO, &N_modes);
  fprintf(stderr, "Setup: working with %zu modes after Lambda cut.\n", N_modes);
  }

  // II. halos
  {
  scalar_grid deltaCIC;
  deltaCIC.read(argv[2]);
  
  // copy grid in Fourier space
  deltaCIC.go_fourier();
  deltaCIC.copysharpk(deltah, kcut, ftypeO);
  }

  return 0;
}


// *********************************************************************
// main - driver routine for any of the above

int main(int argc, char **argv)
{
  // ---------------------------------------------------------------------
  // I. allocate and read in density fields

  scalar_grid deltaLambda(NG);
  scalar_grid deltah(NG);

  if (setup(argc, argv, deltaLambda, deltah, NG)) return -1;

  // check if grid large enough
  double kNy = deltaLambda.get_kNy();
  fprintf(stderr, "kNy = %.3f h/Mpc = %.3f Lambda\n", kNy, kNy/Lambda);
  if (kNy / Lambda < 2.) {
    // for checking of convergence in high-Lambda only!
    fprintf(stderr, "WARNING! Aliasing might occur.\n");
  }
  
  // ---------------------------------------------------------------------
  // II. Construct operators

  // - quadratic
  operatorSet opsmarg;
  generate_operatorSet_squared(opsmarg, deltaLambda, false, true);
  opsmarg.add("delta", Opdata{make_sptr<scalar_grid>(deltaLambda), 1});

  opsmarg.print_stats(stderr);
  opsmarg.go_fourier();

  // ---------------------------------------------------------------------
  // III. Power spectra for diagnostics
  
  const double lkMin = log10(0.003), lkMax = 2.4597;
  const int Nbins = 132;
  
  // < O O' >
  char str[1024];
  powerSpectrumSet pks1(lkMin, lkMax, Nbins);
  opsmarg.fill_powerspectra(pks1);
  sprintf(str, "PkOO_%s_L%.2f.dat", outfile.length() > 0 ? outfile.c_str() : "",
	  Lambda);
  FILE *fpd = fopen(str, "w");  
  pks1.print(fpd);
  fclose(fpd);

  // < delta_h O >
  powerSpectrumSet pks2(lkMin, lkMax, Nbins);
  opsmarg.fill_powerspectra(deltah, pks2);
  sprintf(str, "PkOh_%s_L%.2f.dat", outfile.length() > 0 ? outfile.c_str() : "",
	  Lambda);
  fpd = fopen(str, "w");  
  pks2.print(fpd);
  fclose(fpd);
  
  return 0;
}
