/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <stdlib.h>
#include <math.h>

// obviously should all be in library
#include "grid.h"
#include "scalar_grid.h"
#include "powerSpectrum.h"
#include "tensor_grid.h"
#include "forward/generate_operatorSet.h"
#include "assigners.h"

using namespace LEFTfield;

// *********************************************************************
// squaredfield_estimator_halos
//
// Calculate cross-power spectrum of halos with squared fields:
// delta^2, (K_ij)^2
//
// - warmup and check for cubed field
// - checked on initial conditions and compared with exact prediction (see calc_truth.cc and plot_check.gnu)
//
// (C) Fabian Schmidt, 2017
// *********************************************************************

// whether to average two shifted grids for anti-aliasing
// #define ANTIALIAS

#ifdef ANTIALIAS
#include "assign_Gadget_antialias.cc"
#endif


// *********************************************************************
// main - driver routine for any of the above

int main(int argc, char **argv)
{
  // ---------------------------------------------------------------------
  // O. read in density field (Gadget)
  
  if (argc < 4) {
    std::cerr << "\n Usage: " << argv[0] << " < base file matter > < N_files > < halo file > <NG_1d>  [<smoothing scale>] [<factor to scale density>]\n\n";
    return 0;
  }

  const int Nfiles = atoi(argv[2]);
  const size_t NG = atoi(argv[4]);
  double D = 1., R=0.;;
  if (argc > 5)  R = atof(argv[5]);
  if (argc > 6)  D = atof(argv[6]);

  CICAssigner assigner(NG);
#ifdef ANTIALIAS  
  scalar_grid delta(NG);
  const double Lbox = assign_Gadget_antialias(delta, argv[1], Nfiles);
#else
  const double Lbox = assign_density_Gadget(assigner, argv[1], Nfiles);
  auto delta = *assigner.getGrid();
#endif
  grid::set_Lbox(Lbox);
  fprintf(stderr, "Lbox = %.2f h^-1 Mpc\n", Lbox);

  // ... and halos
  assign_density_Titouan_halos(assigner, argv[3], Lbox);
  auto delta_h = *assigner.getGrid();

  // ---------------------------------------------------------------------
  // I. scale and smooth density
  
  fprintf(stderr, "Smoothing scale: R = %.3f h^-1 Mpc = %.3f grid cells.\n",
	  R, R/Lbox*double(NG));
  delta *= D;
  
  // smooth density
  delta.smooth_Gaussian(R, false);

  // ---------------------------------------------------------------------
  // II. construct cubic operators

  fprintf(stderr, "--- constructing smoothed fields ---\n");
  operatorSet OsetR;
  generate_operatorSet_squared(OsetR, delta);
  // print stats for check
  OsetR.print_stats(stderr);
 
  // ---------------------------------------------------------------------
  // III. measure power spectra
  
  // power spectra
  // log k bins to match HPMpowerspectrum.cc:
  const double lkMin = log10(0.003), lkMax = 2.4597;
  const int Nbins = 66;
  powerSpectrumSet pks(lkMin, lkMax, Nbins);

  // P_mm W_R^2
  delta.auto_powerspectrum( * pks.push_back("<delta delta>"));
  // P_hm W_R
  delta.cross_powerspectrum(delta_h, * pks.push_back("<delta_h delta>"));

  // < delta_h O^(3)[\delta_R] >
  OsetR.fill_powerspectra(delta_h, pks);
  
  // print
  pks.print(stdout);
  fflush(stdout);
  
  return 0;
}
