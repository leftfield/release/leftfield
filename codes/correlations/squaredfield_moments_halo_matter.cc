/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

// enable VERBOSE
#ifndef VERBOSE
#define VERBOSE
#endif

#include <iostream>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <assert.h>

// obviously should all be in library
#include "grid.h"
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"
#include "assigners.h"

using namespace LEFTfield;

// *********************************************************************
// squaredfield_moments_halos
//
// Calculate cross-power spectra and moments of halos with squared fields:
// delta^2, (K_ij)^2, s^kd_k delta
//
// - both linear and nonlinear density field
// - for cross-checking with BORG likelihood
//
// (C) Minh Nguyen & Fabian Schmidt, 2018
// *********************************************************************

// halo settings
const double lgMbins[] = {
  13.0, 13.5, 14.0, 14.5
};
const double b1vec[] = { // values of b_1 for subtraction
  1.1993, 1.6041, 2.3609
};
const int Nbins = 3;

// files & matter settings
std::string IC_base;
size_t Nfiles_IC;
std::string snapshot_base;
size_t Nfiles_snapshot;
std::string halo_file;

// factor to scale density
const double D = 76.001; // D = D(z_final) / D(z_ini)

// output file for moments
const std::string moment_prefix ("/freya/ptmp/mpa/minh/moments/");
std::string moment_file;

// *********************************************************************

// class of moments of operators constructed out of two different fields
class DualFieldMoments {
public:

  cmplx dd; // quadratic moment
  cmplx ddd, dKK, dsdeld, dlapd; // cubic moments

  DualFieldMoments()
  { }
  DualFieldMoments(const DualFieldMoments& m)
    : dd(m.dd), ddd(m.ddd), dKK(m.dKK), dsdeld(m.dsdeld), dlapd(m.dlapd)
  { }
  DualFieldMoments &operator=(const DualFieldMoments &) = default;

  void print(FILE *fp)
  {
    // print only real parts by default; could print imaginary if necessary for debugging
    fprintf(fp, "%.8e  %.8e  %.8e  %.8e  %.8e  ", dd.real(), ddd.real(),
	    dKK.real(), dsdeld.real(), dlapd.real());
  }
};


DualFieldMoments get_moments(scalar_grid& delta_halo,
		    scalar_grid& delta_m)
{
  DualFieldMoments m;
  delta_halo.go_real();
  delta_m.go_real();

  // ---------------------------------------------------------------------
  // construct fields

  // construct squared density operator deltasq
  scalar_grid deltasq(delta_m);
  deltasq *= delta_m;

  // construct tidal operator K2
  tensor_grid Kij(delta_m);
  Kij.go_real();
  scalar_grid K2 = Kij*Kij;

  // construct displacement operator sdeld
  vector_grid sk(delta_m.getNG());
  sk.generate_displacement(delta_m);
  sk.go_real();
  vector_grid dk(delta_m.getNG());
  dk.generate_gradient(delta_m);
  dk.go_real();
  scalar_grid sdeld = sk * dk;

  // construct laplace operator lapd
  scalar_grid lapd(delta_m);
  lapd.Laplace();
  lapd.go_real();

  // subtract means
#ifdef VERBOSE
  fprintf(stdout, "delta^2 stats BEFORE subtracting the mean:\n");
  deltasq.print_stats(stdout);
#endif
  deltasq.subtract_mean();
#ifdef VERBOSE
  fprintf(stdout, "delta^2 stats AFTER subtracting the mean:\n");
  deltasq.print_stats(stdout);
#endif

#ifdef VERBOSE
  fprintf(stdout, "K^2 stats BEFORE subtracting the mean:\n");
  K2.print_stats(stdout);
#endif
  K2.subtract_mean();
#ifdef VERBOSE
  fprintf(stdout, "K^2 stats AFTER subtracting the mean:\n");
  K2.print_stats(stdout);
#endif

#ifdef VERBOSE
  fprintf(stdout, "s^kd_k delta stats BEFORE subtracting the mean:\n");
  sdeld.print_stats(stdout);
#endif
  sdeld.subtract_mean();
#ifdef VERBOSE
  fprintf(stdout, "s^kd_k delta stats AFTER subtracting the mean:\n");
  sdeld.print_stats(stdout);
#endif

#ifdef VERBOSE
  fprintf(stdout, "laplace delta stats BEFORE subtracting the mean:\n");
  lapd.print_stats(stdout);
#endif
  lapd.subtract_mean();
#ifdef VERBOSE
  fprintf(stdout, "laplace delta stats AFTER subtracting the mean:\n");
  lapd.print_stats(stdout);
#endif
  // ---------------------------------------------------------------------
  // measure matter-halo cross moments

  scalar_grid delta_copy(delta_m);
  delta_copy *= delta_halo;
  m.dd = delta_copy.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< delta_m delta_h > stats:\n");
  delta_copy.print_stats(stdout);
#endif

  deltasq *= delta_halo; // save memory by overwriting deltasq with (deltasq_m * delta_h)
  m.ddd = deltasq.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< delta_m^2 delta_h > stats:\n");
  deltasq.print_stats(stdout);
#endif

  K2 *= delta_halo; // save memory by overwriting K2 with (K2_m * delta_h)
  m.dKK = K2.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< K_m^2 delta_h > stats:\n");
  K2.print_stats(stdout);
#endif

  sdeld *= delta_halo; // save memory by overwriting sdeld with (sdeld_m * delta_h)
  m.dsdeld = sdeld.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< (s^kd_k delta_m) delta_h > stats:\n");
  sdeld.print_stats(stdout);
#endif

  lapd *= delta_halo; // save memory by overwriting lapd with (lapd_m * delta_h)
  m.dlapd = lapd.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< (laplace delta_m) delta_h > stats:\n");
  lapd.print_stats(stdout);
#endif

  return m;
}


// *********************************************************************
// main - driver routine for reading in and scaling density fields, computing and printing the moments

int main(int argc, char **argv)
{
  // ---------------------------------------------------------------------
  // I. read in fields

  // read in command line arguments
  if (argc < 8) {
    std::cerr << "\n Too few arguments. Usage: " << argv[0] << " <NG_1d> <IC_filename> <IC_filenumber> <snapshot_filename> <snapshot_filenumber> <halo_file> <moment_file>\n\n";
    return 0;
  }
  const size_t NG = std::stoul(argv[1]);
  IC_base = std::string(argv[2]);
  Nfiles_IC = std::stoul(argv[3]);
  snapshot_base = std::string(argv[4]);
  Nfiles_snapshot = std::stoul(argv[5]);
  halo_file = std::string(argv[6]);
  moment_file = moment_prefix + std::string(argv[7]);

  // read in initial density at z_ini
  CICAssigner assigner(NG);
  double LboxIC = assign_density_Gadget(assigner, IC_base.c_str(), Nfiles_IC);
  auto delta = *assigner.getGrid();

  // scale initial density field at z_ini to get the linear evolved density field at z_final
  delta *= D;

  // read in non-linear evolved density
  double LboxNL = assign_density_Gadget(assigner, snapshot_base.c_str(), Nfiles_snapshot);
  auto deltaNL = *assigner.getGrid();
  assert (LboxIC == LboxNL);
  grid::set_Lbox(LboxIC);
  fprintf(stderr, "Lbox of IC files = %.2f h^-1 Mpc and matched with Lbox of snapshot files\n", LboxIC);

  // moments file
  FILE *fp = fopen(moment_file.c_str(), "w");
  fprintf(fp, "# NG = %lu\n", NG);
  fprintf(fp, "# NG / lg M_min / lg M_max / b_1 (used in subtr.) / moments (dh-d, dh-d^2, dh-K^2, dh-sdeld, dh-lapd) for delta_h-delta_lin / delta_h-delta_nl / (delta_h-b_1 delta_m)-delta_lin / (delta_h-b_1 delta_m)-delta_nl\n");

  // loop over halo mass bins
  for (auto bin = 0; bin < Nbins; bin++)  {
    const double lgMmin = lgMbins[bin], lgMmax = lgMbins[bin+1];
    const double b1 = b1vec[bin];
    fprintf(stdout, "====== lg M = %.3f - %.3f, b_1 = %.4f ======\n",
	    lgMmin, lgMmax, b1);

    delta.go_real();
    deltaNL.go_real();

    // read in halos
    assign_density_AHF_halos(assigner,halo_file.c_str(),
				     pow(10., lgMmin), pow(10., lgMmax));
    auto delta_h = *assigner.getGrid();

    fprintf(stdout, "Subtracting b_1 delta_m ...\n");
    scalar_grid delta_h_sub(delta_h);
    scalar_grid deltacopy(deltaNL);
    deltacopy *= -b1;
    delta_h_sub += deltacopy;

    // ---------------------------------------------------------------------
    // II. measure moments

    fprintf(fp, "%lu %.4e %.4e %.6e  ", NG, lgMmin, lgMmax, b1);
    DualFieldMoments mom;

	// linear matter field - halo field moments
    mom = get_moments(delta_h, delta);
    mom.print(fp);
    // non-linear matter field - halo field moments
    mom = get_moments(delta_h, deltaNL);
    mom.print(fp);
    // linear matter field - stochastic halo field moments
	mom = get_moments(delta_h_sub, delta);
	mom.print(fp);
    // non-linear matter field - stochastic halo field moments
	mom = get_moments(delta_h_sub, deltaNL);
	mom.print(fp);

    fprintf(fp, "\n");

  } // end of loop over mass bins

  // close moments file
  fclose(fp);

  return 0;
}
