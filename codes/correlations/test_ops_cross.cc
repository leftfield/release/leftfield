/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <stdlib.h>
#include <math.h>

#include "grid.h"
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"
#include "forward/generate_operatorSet.h"
#include "orthogonalize.h"

using namespace LEFTfield;

// *********************************************************************
// test_ops_cross
//
// - for testing construction of sharp-k filtered operators
// - this version for cross-correlating different smoothing scales
//
// !!! TO BE ADAPTED TO NEW INTERFACE !!!
//
// (C) Fabian Schmidt, 2019
// *********************************************************************

// *********************************************************************
// settings

// grid size for sharp-k filtered grids
// - should be large enough so that k_Ny > n_PT Lambda
const st NG = 384;
// sufficient for small boxes:
// const st NG = 256;

// *********************************************************************
// global variables

double Lbox;
double Lambda1=0.1, Lambda2=0.1;
double kmax;
bool renorm;
const char *outfile = 0;
const char *prefix = 0;



// *********************************************************************
// setup: read in grids, apply sharp-k cut and restrict to NG defined above 

int mod_setup(int argc, char **argv, scalar_grid& deltaLambda1,
	  scalar_grid& deltaLambda2)
{
  if (argc < 3) {
    std::cerr << "\n Usage: " << argv[0] << " < matter nbtl file >  [<Lbox = 2000 Mpc/h>]  [<Lambda_1 = 0.1 h/Mpc>] [<Lambda_2 = 0.1 h/Mpc>] [<output file root>] [<renorm. flag, default 1>]\n\n";
    return 1;
  }

  if (argc > 2)  Lbox = atof(argv[2]);
  if (argc > 3)  Lambda1 = atof(argv[3]);
  if (argc > 4)  Lambda2 = atof(argv[4]);
  if (argc > 5)  {
    outfile = argv[5];
    fprintf(stderr, "Writing results to '%s'.\n", outfile);
  }

  renorm = true;
  if (argc > 6 && atoi(argv[6]) <= 0)  {
    renorm = false;
    fprintf(stderr, "Not renormalizing.\n");
  }
  grid::set_Lbox(Lbox);
  
  // determine k_cut = Lambda divided by the fundamental of the box
  double kcut1 = Lambda1;  
  fprintf(stderr, "Lbox = %.2f h^-1 Mpc, Lambda1 = %.3f h Mpc^-1\n", Lbox, Lambda1);
  double kcut2 = Lambda2;  
  fprintf(stderr, "Lbox = %.2f h^-1 Mpc, Lambda2 = %.3f h Mpc^-1\n", Lbox, Lambda2);

  // I. matter  
  scalar_grid deltaCIC; 
  deltaCIC.read(argv[1]);
  
  // copy grid in Fourier space
  deltaCIC.go_fourier();
  deltaCIC.copysharpk(deltaLambda1, kcut1);
  deltaCIC.copysharpk(deltaLambda2, kcut2);

  return 0;
}



// *********************************************************************
// main - driver routine for any of the above

int main(int argc, char **argv)
{
  // ---------------------------------------------------------------------
  // I. allocate and read in density fields

  scalar_grid deltaLambda1(NG), deltaLambda2(NG);

  Lambda1 = 0.1; // default
  Lambda2 = 0.2; // default
  Lbox = 2000.; // default
  if (mod_setup(argc, argv, deltaLambda1, deltaLambda2)) return -1;

  // check if grid large enough
  double kNy = deltaLambda1.get_kNy();
  fprintf(stderr, "kNy = %.3f h/Mpc = %.3f Lambda\n", kNy,
	  kNy/fmax(Lambda1, Lambda2));
  
  // ---------------------------------------------------------------------
  // II. Construct operators

  // - quadratic
  operatorSet opsmarg1, opsmarg2;
  generate_operatorSet_squared(opsmarg1, deltaLambda1, true);
  generate_operatorSet_squared(opsmarg2, deltaLambda2, true);
  opsmarg1.add("delta", Opdata{make_sptr<scalar_grid>(deltaLambda1), 1});
  opsmarg2.add("delta", Opdata{make_sptr<scalar_grid>(deltaLambda2), 1});

  fprintf(stderr, "test_ops: delta1:\n");
  deltaLambda1.print_stats(stderr);
  deltaLambda2.print_stats(stderr);
  opsmarg1.print_stats(stderr);

  if (renorm) {
    // renormalize ?
    orthogonalize_table(opsmarg1, "delta");
    orthogonalize_table(opsmarg2, "delta");
  }
  
  // power spectra for diagnostics
  const double lkMin = log10(0.003), lkMax = 2.4597;
  const int Nbins = 66;
  powerSpectrumSet pks(lkMin, lkMax, Nbins);
  
  // (a)
  char str[1024];
  powerSpectrumSet pks1(lkMin, lkMax, Nbins);
  opsmarg1.fill_powerspectra(pks1);
  sprintf(str, "PkcrossOO_%s_L%.2f_L%.2f.dat", outfile, Lambda1, Lambda1);
  FILE *fpd = fopen(str, "w");  
  pks1.print(fpd);
  fclose(fpd);

  // (b)
  powerSpectrumSet pks2(lkMin, lkMax, Nbins);
  opsmarg2.fill_powerspectra(pks2);
  sprintf(str, "PkcrossOO_%s_L%.2f_L%.2f.dat", outfile, Lambda2, Lambda2);
  fpd = fopen(str, "w");  
  pks2.print(fpd);
  fclose(fpd);

  // (c)
  powerSpectrumSet pks3(lkMin, lkMax, Nbins);
  opsmarg1.fill_powerspectra(opsmarg2, pks3);
  sprintf(str, "PkcrossOO_%s_L%.2f_L%.2f.dat", outfile, Lambda1, Lambda2);
  fpd = fopen(str, "w");  
  pks3.print(fpd);
  fclose(fpd);

  // (d)
  powerSpectrumSet pks4(lkMin, lkMax, Nbins);
  opsmarg2.fill_powerspectra(opsmarg1, pks4);
  sprintf(str, "PkcrossOO_%s_L%.2f_L%.2f.dat", outfile, Lambda2, Lambda1);
  fpd = fopen(str, "w");  
  pks4.print(fpd);
  fclose(fpd);
  
  return 0;
}
