/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

// enable VERBOSE
#ifndef VERBOSE
#define VERBOSE
#endif

#include <iostream>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <assert.h>

// obviously should all be in library
#include "grid.h"
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"
#include "assigners.h"

using namespace LEFTfield;

// *********************************************************************
// squaredfield_moments_matter
//
// Calculate cross-power spectra and moments of matter squared fields:
// delta^2, (K_ij)^2, s^kd_k delta
//
// - both linear and nonlinear density field
//
// (C) Minh Nguyen & Fabian Schmidt, 2018
// *********************************************************************

// files & matter settings
std::string IC_base;
size_t Nfiles_IC;
std::string snapshot_base;
size_t Nfiles_snapshot;

// factor to scale density
const double D = 76.001;

// output file for moments
const std::string moment_prefix ("/freya/ptmp/mpa/minh/moments/");
std::string moment_file;

// *********************************************************************

// class of moments of operators constructed out of one single field
class SingleFieldMoments {
public:

  cmplx dd;
  cmplx ddd, dKK, dsdeld, dlapd;
  cmplx dddd, ddKK, ddsdeld, ddlapd, KKKK, KKsdeld, KKlapd, sdeldsdeld, sdeldlapd, lapdlapd;

  SingleFieldMoments()
  { }
  SingleFieldMoments(const SingleFieldMoments& m)
    : dd(m.dd), ddd(m.ddd), dKK(m.dKK), dsdeld(m.dsdeld), dlapd(m.dlapd), dddd(m.dddd), ddKK(m.ddKK), ddsdeld(m.ddsdeld), ddlapd(m.ddlapd), KKKK(m.KKKK), KKsdeld(m.KKsdeld), KKlapd(m.KKlapd), sdeldsdeld(m.sdeldsdeld), sdeldlapd(m.sdeldlapd), lapdlapd(m.lapdlapd)
  { }
  SingleFieldMoments &operator=(const SingleFieldMoments &) = default;

  void print(FILE *fp)
  {
    // print only real parts by default; could print imaginary if necessary for debugging
    fprintf(fp, "%.8e  %.8e  %.8e  %.8e  %.8e  %.8e  %.8e  %.8e  %.8e  %.8e  %.8e  %.8e  %.8e  %.8e  %.8e  "  , dd.real(), ddd.real(), dKK.real(), dsdeld.real(), dlapd.real(), dddd.real(), ddKK.real(), ddsdeld.real(), ddlapd.real(), KKKK.real(), KKsdeld.real(), KKlapd.real(), sdeldsdeld.real(), sdeldlapd.real(), lapdlapd.real());

  }
};


SingleFieldMoments get_moments(scalar_grid& delta)
{
  SingleFieldMoments m;
  delta.go_real();

  // ---------------------------------------------------------------------
  // construct fields

  // construct squared density operator delta^2
  scalar_grid deltasq(delta);
  deltasq *= delta;

  // construct tidal operator K2
  tensor_grid Kij(delta);
  Kij.go_real();
  scalar_grid K2 = Kij*Kij;

  // construct displacement operator sdeld
  vector_grid sk(delta.getNG());
  sk.generate_displacement(delta);
  sk.go_real();
  vector_grid dk(delta.getNG());
  dk.generate_gradient(delta);
  dk.go_real();
  scalar_grid sdeld = sk * dk;

  // construct laplace operator lapd
  scalar_grid lapd(delta);
  lapd.Laplace();
  lapd.go_real();

  // subtract means
#ifdef VERBOSE
  fprintf(stdout, "delta^2 stats BEFORE subtracting the mean:\n");
  deltasq.print_stats(stdout);
#endif
  deltasq.subtract_mean();
#ifdef VERBOSE
  fprintf(stdout, "delta^2 stats AFTER subtracting the mean:\n");
  deltasq.print_stats(stdout);
#endif

#ifdef VERBOSE
  fprintf(stdout, "K^2 stats BEFORE subtracting the mean:\n");
  K2.print_stats(stdout);
#endif
  K2.subtract_mean();
#ifdef VERBOSE
  fprintf(stdout, "K^2 stats AFTER subtracting the mean:\n");
  K2.print_stats(stdout);
#endif

#ifdef VERBOSE
  fprintf(stdout, "s^kd_k delta stats BEFORE subtracting the mean:\n");
  sdeld.print_stats(stdout);
#endif
  sdeld.subtract_mean();
#ifdef VERBOSE
  fprintf(stdout, "s^kd_k delta stats AFTER subtracting the mean:\n");
  sdeld.print_stats(stdout);
#endif

#ifdef VERBOSE
  fprintf(stdout, "laplace delta stats BEFORE subtracting the mean:\n");
  lapd.print_stats(stdout);
#endif
  lapd.subtract_mean();
#ifdef VERBOSE
  fprintf(stdout, "laplace delta stats AFTER subtracting the mean:\n");
  lapd.print_stats(stdout);
#endif
  // ---------------------------------------------------------------------
  // measure auto and cross moments of operators constructed above

  // < delta delta >
  auto delta_delta = delta * delta;
  m.dd = delta_delta.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< delta delta > stats:\n");
  delta_delta.print_stats(stdout);
#endif

  // < delta deltasq >
  auto delta_deltasq = delta * deltasq;
  m.ddd = delta_deltasq.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< delta delta^2 > stats:\n");
  delta_deltasq.print_stats(stdout);
#endif

  // < delta K2 >
  auto delta_K2 = delta * K2;
  m.dKK = delta_K2.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< delta K^2 > stats:\n");
  delta_K2.print_stats(stdout);
#endif

  // < delta sdeld >
  auto delta_sdeld = delta * sdeld;
  m.dsdeld = delta_sdeld.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< delta (s^kd_k delta) > stats:\n");
  delta_sdeld.print_stats(stdout);
#endif

  // < delta lapd >
  delta *= lapd; // save memory by overwriting delta grid with (delta * lapd)
  m.dlapd = delta.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< delta (laplace delta) > stats:\n");
  delta.print_stats(stdout);
#endif

  // < deltasq deltasq >
  auto deltasq_deltasq = deltasq * deltasq;
  m.dddd = deltasq_deltasq.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< delta^2 delta^2 > stats:\n");
  deltasq_deltasq.print_stats(stdout);
#endif

  // < deltasq K2 >
  auto deltasq_K2 = deltasq * K2;
  m.ddKK = deltasq_K2.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< delta K^2 > stats:\n");
  deltasq_K2.print_stats(stdout);
#endif

  // < deltasq sdeld >
  auto deltasq_sdeld = deltasq * sdeld;
  m.ddsdeld = deltasq_sdeld.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< delta^2 (s^kd_k delta) > stats:\n");
  deltasq_sdeld.print_stats(stdout);
#endif

  // < deltasq lapd >
  deltasq *= lapd; // save memory by overwriting deltasq grid with (deltasq * lapd)
  m.ddlapd = deltasq.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< delta^2 (laplace delta) > stats:\n");
  deltasq.print_stats(stdout);
#endif

  // < K2 K2 >
  auto K2_K2 = K2 * K2;
  m.KKKK = K2_K2.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< K^2 K^2 > stats:\n");
  K2_K2.print_stats(stdout);
#endif

  // < K2 sdeld >
  auto K2_sdeld = K2 * sdeld;
  m.KKsdeld = K2_sdeld.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< K^2 (s^kd_k delta) > stats:\n");
  K2_sdeld.print_stats(stdout);
#endif

  // < K2 lapd >
  K2 *= lapd; // save memory by overwriting K2 grid with (K2 * lapd)
  m.KKlapd = K2.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< K^2 (laplace delta) > stats:\n");
  K2.print_stats(stdout);
#endif

  // < sdeld sdeld >
  auto sdeld_sdeld = sdeld * sdeld;
  m.sdeldsdeld = sdeld_sdeld.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< (s^kd_k delta) (s^kd_k delta) > stats:\n");
  sdeld_sdeld.print_stats(stdout);
#endif

  // < sdeld lapd>
  sdeld *= lapd; // save memory by overwriting sdeld grid with (sdeld * lapd)
  m.sdeldlapd = sdeld.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< (s^kd_k delta) (laplace delta) > stats:\n");
  sdeld.print_stats(stdout);
#endif

  // < lapd lapd >
  lapd *= lapd; // save memory by overwriting lapd grid with (lapd * lapd)
  m.lapdlapd = lapd.get_mean();
#ifdef VERBOSE
  fprintf(stdout, "< (laplace delta) (laplace delta) > stats:\n");
  lapd.print_stats(stdout);
#endif


  return m;
}


// *********************************************************************
// main - driver routine for reading in and scaling density fields, computing and printing the moments

int main(int argc, char **argv)
{
  // ---------------------------------------------------------------------
  // I. read in fields

  // read in command line arguments
  if (argc < 7) {
    std::cerr << "\n Too few arguments. Usage: " << argv[0] << " <NG_1d> <IC_filename> <IC_filenumber> <snapshot_filename> <snapshot_filenumber> [<smoothing_scale>] <moment_file>\n\n";
    return 0;
  }
  const size_t NG = std::stoul(argv[1]);
  IC_base = std::string(argv[2]);
  Nfiles_IC = std::stoul(argv[3]);
  snapshot_base = std::string(argv[4]);
  Nfiles_snapshot = std::stoul(argv[5]);
  if (argc == 7) {
  	  moment_file = moment_prefix + std::string(argv[6]);
	} else if (argc == 8) {
  	  moment_file = moment_prefix + std::string(argv[7]);
	} else {
    std::cerr << "\n Too many arguments. Usage: " << argv[0] << " <NG_1d> <IC_filename> <IC_filenumber> <snapshot_filename> <snapshot_filenumber> [<smoothing_scale>] <moment_file>\n\n";
    return 0;
	}

  // read in initial density at z_ini
  CICAssigner assigner(NG);
  double LboxIC = assign_density_Gadget(assigner, IC_base.c_str(), Nfiles_IC);
  auto delta = *assigner.getGrid();

  // scale initial density field to get the linear evolved density field at z0
  delta *= D;

  // read in non-linear evolved density
  double LboxNL = assign_density_Gadget(assigner, snapshot_base.c_str(), Nfiles_snapshot);
  auto deltaNL = *assigner.getGrid();
  assert (LboxIC == LboxNL);
  fprintf(stderr, "Lbox of IC files = %.2f h^-1 Mpc and matched with Lbox of snapshot files\n", LboxIC);
  grid::set_Lbox(LboxIC);

  // moments file
  FILE *fp = fopen(moment_file.c_str(), "w");
  fprintf(fp, "# NG = %lu\n", NG);
  fprintf(fp, "# NG / moments (d-d, d-d^2, d-K^2, d-sdeld, d-lapd, d^2-d^2, d^2-K^2, d^2-sdeld, d^2-lapd, K^2-K^2, K^2-sdeld, K^2-lapd, sdeld-sdeld, sdeld-lapd, lapd-lapd) for delta_lin / delta_nl\n");

    delta.go_real();
    deltaNL.go_real();

    // ---------------------------------------------------------------------
    // II. measure moments

    fprintf(fp, "%lu  ", NG);
    SingleFieldMoments mom;

	// linear matter field moments
    mom = get_moments(delta);
    mom.print(fp);
    // non-linear matter field moments
    mom = get_moments(deltaNL);
    mom.print(fp);

    fprintf(fp, "\n");

  // close moments file
  fclose(fp);

  return 0;
}
