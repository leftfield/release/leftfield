/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <stdlib.h>
#include <math.h>

// obviously should all be in library
#include "grid.h"
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"
#include "forward/generate_operatorSet.h"
#include "assigners.h"

using namespace LEFTfield;

// *********************************************************************
// cubedfield_estimator
//
// Calculate cross-power spectra of halos with cubed fields:
// delta^3, delta (K_ij)^2, (K_ij)^3, O_td
//
// (C) Fabian Schmidt, 2017
// *********************************************************************

// whether to average two shifted grids for anti-aliasing
// #define ANTIALIAS


#ifdef ANTIALIAS
#include "assign_Gadget_antialias.cc"
#endif


// *********************************************************************
// main - driver routine for any of the above

int main(int argc, char **argv)
{
  // ---------------------------------------------------------------------
  // O. read in density field (Gadget)
  
  if (argc < 4) {
    std::cerr << "\n Usage: " << argv[0] << " < base file initial matter > < N_files(initial matter) > < halo file > <NG_1d>  [<b_1>] [<smoothing scale>] [<factor to scale density>] [< base file final matter > <N_files(final matter)>]\n\n";
    return 0;
  }

  const int Nfiles = atoi(argv[2]);
  const st NG = atoi(argv[4]);
  double D = 1., R=0., b1=0.;
  if (argc > 5)  b1 = atof(argv[5]);
  if (argc > 6)  R = atof(argv[6]);
  if (argc > 7)  D = atof(argv[7]);

  CICAssigner assigner(NG);
#ifdef ANTIALIAS  
  scalar_grid delta(NG);
  const double Lbox = assign_Gadget_antialias(delta, argv[1], Nfiles);
#else
  const double Lbox = assign_density_Gadget(assigner, argv[1], Nfiles);
  auto delta = *assigner.getGrid();
#endif
  fprintf(stderr, "Lbox = %.2f h^-1 Mpc\n", Lbox);
  grid::set_Lbox(Lbox);

  // ... and halos
  assign_density_Titouan_halos(assigner, argv[3], Lbox);
  auto delta_h = *assigner.getGrid();
  sptr<scalar_grid> deltafinal;
  if (argc > 9)  {
    const int Nfilesf = atoi(argv[9]);
    fprintf(stderr, "Reading in final density field from '%s' (%d files).\n",
	    argv[8], Nfilesf);
    assign_density_Gadget(assigner, argv[8], Nfilesf);
    deltafinal = assigner.getGrid();
  }
  
  // ---------------------------------------------------------------------
  // I. scale and smooth density
  
  // convert R to code units
  fprintf(stderr, "Smoothing scale: R = %.3f h^-1 Mpc = %.3f grid cells.\n",
	  R, R/Lbox*double(NG));
  delta *= D;

  // if b_1 given, subtract b_1 delta from halos to subtract lower-order
  // contribution
  if ( b1 != 0. )  {
    // - subtract nonlinear matter field if given
    if (deltafinal)  {
      fprintf(stderr, "Subtracting b_1 delta_final from delta_h, with b_1 = %.3f.\n", b1);
      delta_h += -b1 * (*deltafinal);
      deltafinal.reset(); // final matter density no longer needed      
    } else { // otherwise linear field
      fprintf(stderr, "Subtracting b_1 delta_lin from delta_h, with b_1 = %.3f.\n", b1);
      delta_h += -b1 * delta;
    }
  }
  
  // smooth density
  delta.smooth_Gaussian(R, false);

  // ---------------------------------------------------------------------
  // II. construct cubic operators

  fprintf(stderr, "--- constructing smoothed fields ---\n");
  operatorSet OsetR;
  generate_operatorSet_cubed(OsetR, delta);
  // print stats for check
  OsetR.print_stats(stderr);

  // Laplace delta field
  //scalar_grid lapldelta(delta);
  //lapldelta.Laplace();
  
  // ---------------------------------------------------------------------
  // III. measure power spectra
  
  // power spectra
  // log k bins to match HPMpowerspectrum.cc:
  const double lkMin = log10(0.003), lkMax = 2.4597;
  const int Nbins = 66;
  powerSpectrumSet pks(lkMin, lkMax, Nbins);

  // P_mm W_R^2
  delta.auto_powerspectrum( * pks.push_back("<delta delta>"));
  // P_hm W_R
  delta.cross_powerspectrum(delta_h, * pks.push_back("<delta_h delta>"));

  // < [delta_h - b_1 delta_m] O^(3)[\delta_R] >
  OsetR.fill_powerspectra(delta_h, pks);

  // < [delta_h - b_1 delta_m] \nabla^2\delta_R >
  //delta_h.cross_powerspectrum(lapldelta,
//			      * pks.push_back("<delta_h nabla^2 delta>"), true);
  
  // print
  pks.print(stdout);
  fflush(stdout);

  return 0;
}
