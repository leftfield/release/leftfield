/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<iostream>
#include <stdlib.h>
#include <math.h>

#include "scalar_grid.h"

using namespace LEFTfield;

// *********************************************************************
// test_copysharpk
//
// - read in field in nbtl format
// - cutsharpk with given kcut
// - do copysharpk to desired grid size
// -- if kcut corresponds to k_Ny of reduced grid, should have the same power
//    as original grid
// - print stats
//
// (C) Fabian Schmidt, 2020
// *********************************************************************

const sharpkFilterType ftype = CUBE;
std::string ds_name("delta");
double Lbox = 2000.;


// print cross-correlation coefficient for individual modes
// - all indices <= NGd/2
void print_r(const scalar_grid &rg1, const scalar_grid &out,
       std::array<st, 3> a)
{
  const st i = std::min<st>(a[0], out.getNG()/2);
  const st j = std::min<st>(a[1], out.getNG()/2);
  const st k = std::min<st>(a[2], out.getNG()/2);

  cmplx Cs = rg1(i,j,k), Cd = out(i,j,k);
  double r = (Cs*Cd).real() / sqrt(std::norm(Cs) * std::norm(Cd));
  if (std::norm(Cs) * std::norm(Cd) > 0.)
    fprintf(stderr,  "r(%zu, %zu, %zu) = %.4f\n", i,j,k, r);
  else
    fprintf(stderr, "r(%zu, %zu, %zu) ill-defined (vanishing denominator)\n", i,j,k);
}


int main(int argc, char **argv)
{
  LogContext lc("test_copysharpk.cc");

  if (argc < 3) {
    std::cerr << "\n Usage: " << argv[0] << " < input field file > < NG_target > [<k_cut: default correspond to k_Ny,target>] [<Lbox = " << Lbox <<
      "] [<data set name = " << ds_name << ">]\n\n";
    return 0;
  }

  const st NG = (st) atoi(argv[2]);
  if (!(NG > 0) || (NG%2))  {
    fprintf(stderr, "Invalid NG = %zu. Must be positive integer number.\n",
      NG);
    return -1;
  }
  if (argc > 4) Lbox = atof(argv[4]);
  if (argc > 5) ds_name = argv[5];
  grid::set_Lbox(Lbox);

  // read in grid
  scalar_grid rg1;
  rg1.read(argv[1], ds_name);
  logger.printf(ESSENTIAL, "Input grid:");
  rg1.print_stats(stderr);

  // apply cut and print stats
  const double EPS = 0.;//1.e-5;
  double kcut = grid::get_kNy(NG) + EPS;
  if (argc > 3)  kcut = atof(argv[3]);
  logger.printf(ESSENTIAL, "Input grid after filtering with k_cut = %.4e:", kcut);
  rg1.go_fourier();
  rg1.cutsharpk(kcut, ftype);

  // MR FIXME: this seems to be a bug. Both values will always be identical,
  //           because print_stats will go back to Fourier space.
  // FS: doesn't the explicit go_real() fix this? Of course it's unnecessary,
  //     since we know that sumsquares returns the same in real and Fourier space...
  double sumsq_sf = rg1.get_sumsquares();
  rg1.print_stats(stderr);
  rg1.go_real();
  double sumsq_sr = rg1.get_sumsquares();

  // copysharpk
  rg1.go_fourier();
  st Nmodes = 0;
  auto out = rg1.copysharpk(NG, kcut, ftype, &Nmodes);
  double sumsq_df = out->get_sumsquares();

  // print cross-correlation coefficient for some values off/on Nyquist planes
  print_r(rg1, *out, { NG/2-1, 2, 3});
  print_r(rg1, *out, { NG/2, 2, 3});
  print_r(rg1, *out, { NG/2, NG/2, 3});
  print_r(rg1, *out, { NG/2, NG/2, NG/2});

  // rescale factor
  // - note that resizeFourier rescales Fourier amplitudes in correspondence
  //   to grid size, so that things like the RMS are preserved; this means
  //   that we need this factor to compare sumsquares()
  double F = pow(double(rg1.getNG()), 3.) / double(NG*NG*NG);

  // print stats for output
  logger.printf(ESSENTIAL, "Output grid:");
  out->go_real();
  out->print_stats(stderr);
  double sumsq_dr = out->get_sumsquares();

  // print results
  logger.printf(ESSENTIAL, "Alternatively, compare sum of squares in destination and source:");
  // - real and Fourier space versions are just test of Parseval's theorem...
  logger.printf(ESSENTIAL, "Fourier:  %.6e, %.6e, ratio: 1 + %.4e",
    F * sumsq_df, sumsq_sf, F * sumsq_df / sumsq_sf - 1.);
  logger.printf(ESSENTIAL, "Real:  %.6e, %.6e, ratio: 1 + %.4e",
    F * sumsq_dr, sumsq_sr, F * sumsq_dr / sumsq_sr - 1.);

  return 0;
}

