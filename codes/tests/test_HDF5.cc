/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <scalar_grid.h>
#include <tensor_grid.h>
#include <HDF5_IO.h>


// *********************************************************************
// test_HDF5.cc
//
// test simple HDF5 I/O functionality
// - generate grid and write to file
// - read back into empty grid
//
// (C) Fabian Schmidt, 2020
// *********************************************************************

using namespace LEFTfield;

int main()
{
  const st NG = 16;
  const st ind[] = { 3, 9, 15 };
  
  scalar_grid g(NG);

  // set some values
  g(ind[0],ind[1],ind[2]) = 42.0;

  // make sure the output file doesn't exist
  removeIfExists("test_HDF5.h5");
  // try to read from a non-existing file
  try {
    scalar_grid g2;
    g2.read_HDF5("test_HDF5.h5", "phases");
  }
  catch (const FileNotFound &err) {
    std::cout << "FileNotFound thrown (as expected)" << std::endl;
  }

  // write
  g.write_HDF5("test_HDF5.h5", "phases");

  tensor_grid tg(NG);
  tg[3](ind[0],ind[1],ind[2]) = 43.0;
  tg.go_fourier(); // test writing in Fourier space
  tg.write_HDF5("test_HDF5.h5", "tensor");
  tg.go_real();

  // try to read from a non-existing data set
  try {
    scalar_grid g2;
    g2.read_HDF5("test_HDF5.h5", "abcde");
  }
  catch (const DatasetNotFound &err) {
    std::cout << "DatasetNotFound thrown (as expected)" << std::endl;
  }

  // read to another grid
  scalar_grid g2;
  g2.read_HDF5("test_HDF5.h5", "phases");
  tensor_grid tg2;
  tg2.read_HDF5("test_HDF5.h5", "tensor");
  tg2.go_real();

  fprintf(stderr, "Check: g2[i,j,k] = %.3f != %.3f\n",
	  g2(ind[0],ind[1],ind[2]),
	  g (ind[0],ind[1],ind[2]) );

  fprintf(stderr, "Check: tg2[i,j,k] = %.3f != %.3f\n",
	  tg2[3](ind[0],ind[1],ind[2]),
	  tg [3](ind[0],ind[1],ind[2]) );
  return 0;
}

