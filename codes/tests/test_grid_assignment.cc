/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string.h>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "grid.h"
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"
#include "assigners.h"

using namespace LEFTfield;

// *********************************************************************
// test_grid_assignment
//
// - read in particles in gadget format
// - assign to coarse grid via various schemes
// - interpolate to fine grid
// - compute power spectrum
//
// (C) Fabian Schmidt, 2019
// *********************************************************************

// compute \partial_i \partial_j ( \Delta^ij \delta)
// and compare with expected result: 2/3 \nabla^2 \delta
// - test passes
void test_tensor_divergence(scalar_grid &delta)
{
  tensor_grid tg(delta);
  scalar_grid dcheck(tg.divergence());
  delta.Laplace();
  delta *= 2./3.;

  // stats and info
  const st I = 8, J = 9, K = 10;
  fprintf(stderr, "(2/3) lapl delta:\n");
  delta.print_stats(stderr);
  fprintf(stderr, " [%zu][%zu][%zu] = %.5e\n", I, J, K, delta(I,J,K));
  fprintf(stderr, "divergence tidal(delta):\n");
  dcheck.print_stats(stderr);
  fprintf(stderr, " [%zu][%zu][%zu] = %.5e\n", I, J, K, dcheck(I,J,K));

  fprintf(stderr, "difference:\n");
  dcheck.add(-1., delta);
  dcheck.print_stats(stderr);  
}


sptr<ScalarGridAssigner> make_assigner(const std::string& type,
						  const st NG)
{
  if (type == "NGP")  {
    auto as = std::make_shared<NGPAssigner>(NG);
    return as;
  } else if (type == "FT")  {
    auto as = std::make_shared<FTAssigner>(NG);
    return as;
  } else
  if (type == "FTNLO")  {
    auto as = std::make_shared<FTNLOAssigner>(NG);
    return as;
  } 

  // default: CIC
  auto as = std::make_shared<CICAssigner>(NG);
  return as;
}

int main(int argc, char **argv)
{
  if (argc < 4) {
    std::cerr << "\n Usage: " << argv[0] << " < base file > < N_files > < assignment: NGP, CIC, FT, FTNLO > \n\n";
    return 0;
  }

  const int Nfiles = atoi(argv[2]);
  const st NGcoarse = 256;
  const st NGfine = 1024;

  // make assigner of desired type and assign to coarse grid
  auto assigner = make_assigner(argv[3], NGcoarse);
  double Lbox = assign_density_Gadget(*assigner,
				      argv[1], Nfiles, 0., 0., 0., 0.);
  auto deltac = assigner->getGrid();
  grid::set_Lbox(Lbox);
  
  // some helpful info
  fprintf(stderr, "Box size: %.3e h^-1 Mpc. Stats for grid with NG=%zu:\n", Lbox, NGcoarse);
  deltac->print_stats(stderr);

  // // --- testing ---
  // test_tensor_divergence(*deltac);
  // return 0;
  
  // now interpolate to fine grid
  scalar_grid deltaf(NGfine);
  deltaf.interpolateNGP(*deltac);

  // finally, CIC-assign directly to fine grid for comparison
  CICAssigner fassigner(NGfine);
  assign_density_Gadget(fassigner, argv[1], Nfiles,
			0., 0., 0., 0.);
  auto deltaref = fassigner.getGrid();
    
  // compute power spectrum
  // log k bins to match HPMpowerspectrum.cc:
  const double lkMin = log10(0.003), lkMax = 2.4597;
  const int Nbins = 264;
  powerSpectrumSet pks(lkMin, lkMax, Nbins);

  char str[1024];
  sprintf(str, "%s-coarse", argv[3]);
  deltaf.auto_powerspectrum(* pks.push_back(str));
  deltaref->auto_powerspectrum(* pks.push_back("CIC-fine"));
  deltaf.cross_powerspectrum(*deltaref, * pks.push_back("cross"));
  pks.print(stdout);

  return 0;
}
