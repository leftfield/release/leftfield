#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "cosmology/cosmological.h"

// *********************************************************************
// test_scaling.cc
//
// - print scaling indices for LCDM
//
// (C) Fabian Schmidt, 2020
// *********************************************************************

using namespace LEFTfield;

// cosmology, for growth scaling:
const double Om=0.3, OL=1.-Om;
const double h=0.7, ns=0.967;


int main()
{
  // ====================================================================
  // print scaling indices

  const double Lambda=0.2;
  const double knl=0.25, Rstar=5., Dnorm=1.;
  // // - based on scaling
  // exppar_LCDM_scaling ep(Om, h, ns, Lambda);
  // - based on parameters
  exppar_LCDM_Rstar ep(Om, h, ns, Lambda, knl, Rstar, Dnorm);

  // ep.set_tolerance(1.);

  const st order = 4;
  op_order ref(order, 0, 0);

  for (st ord=1; ord <= order; ord++)  {
    // - derivs
    for (st deriv=1; deriv <= 2; deriv++)  {
      op_order comp(ord, deriv, 0);
      if (ep.is_lowerequal_order(comp, ref)) {
	fprintf(stderr, "ref order %zu: derivative order %zu is relevant for operators of order %zu\n",
		order, deriv, ord);
      }
    }
    // - stoch
    for (st stoch=1; stoch <= 2; stoch++)  {
      op_order comp(ord, 0, stoch);
      if (ep.is_lowerequal_order(comp, ref)) {
	fprintf(stderr, "ref order %zu: stochastic field order %zu is relevant for operators of order %zu\n",
		order, stoch, ord);
      }
    }
  }  

  return 0;
}
