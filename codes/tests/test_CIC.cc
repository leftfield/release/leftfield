/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "scalar_grid.h"
#include "powerSpectrum.h"

using namespace LEFTfield;

// *********************************************************************
// test_CIC
//
// - read in two fields of different resolution in nbtl format
// - restrict using copysharpk
// - print out auto- and cross power spectra
//
// (C) Fabian Schmidt, 2019
// *********************************************************************

int main(int argc, char **argv)
{
  if (argc < 6) {
    std::cerr << "\n Usage: " << argv[0] << " < nbtl file 1 > < NG_1> < nbtl file 2 >  < NG_2 >  <NG_out>  [< Lbox = 2000 Mpc/h, for scaling power spectra >] \n\n";
    return 0;
  }

  const st NG1 = atoi(argv[2]);
  const st NG2 = atoi(argv[4]);
  const st NGout = atoi(argv[5]);
  double Lbox = 2000.;
  if (argc > 6)  Lbox = atof(argv[6]);
  grid::set_Lbox(Lbox);

  // read in grids
  scalar_grid g1(NG1);
  scalar_grid g2(NG2);
  g1.read(argv[1]);
  g2.read(argv[3]);

  // restrict using pretty much maximal k_cut
  scalar_grid g1o(NGout);
  g1.copysharpk(g1o, g1o.get_kNy());
  scalar_grid g2o(NGout);
  g2.copysharpk(g2o, g1o.get_kNy());
  
  // if survived, compute power spectra
  const double lkMin = log10(0.003), lkMax = 2.4597;
  const int Nbins = 264;
  powerSpectrumSet pks(lkMin, lkMax, Nbins);

  g1o.auto_powerspectrum(* pks.push_back("CIC-auto"));
  g2o.auto_powerspectrum(* pks.push_back("CIC-auto"));
  g1o.cross_powerspectrum(g2o, * pks.push_back("cross"));

  pks.print(stdout);

  return 0;
}
