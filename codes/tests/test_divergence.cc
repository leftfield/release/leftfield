/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<iostream>
#include <stdlib.h>
#include <math.h>

#include "scalar_grid.h"
#include "vector_grid.h"
#include "utilities/rng.h"

using namespace LEFTfield;

// *********************************************************************
// test_divergence
// - test some identities involving Fourier-space derivative operators
//
// (C) Fabian Schmidt, 2021
// *********************************************************************

const st seed = 4321;

// *********************************************************************
// investigating validity of delta = div( inverseLaplace(grad(delta)) )

void test_vector_divergence(const st NG, const double kcutNy = 0.9,
			    const bool subtract_mean = true)
{
  scalar_grid delta(NG);
  RNG rng(seed);
  rng.fill_Gauss(delta, 0., 1.);
  delta.go_fourier();
  // mean subtraction: does not make a significant difference
  if (subtract_mean) delta.subtract_mean();
  // sharp-k filter
  // - issue: without sharp-k filtering Nyquist modes, the central grid values
  //          on the div(grad(delta)) grid have the opposite sign
  delta.cutsharpk( sharpkFilter(CUBE, kcutNy * delta.get_kNy()) );

  // vs = - inverseLaplace(grad(delta))
  vector_grid vs;  vs.generate_displacement(delta);
  scalar_grid div;
  vs.divergence(div); // = div( vs ) != - delta
  logger.printf(ESSENTIAL, "delta stats:");
  delta.print_stats(stderr);
  logger.printf(ESSENTIAL, "divergence stats:");
  div.print_stats(stderr);

  // some diagnostic print out
  logger.printf(ESSENTIAL, "cells with mismatch:");
  for (st i=0; i < NG; i++) {
    for (st j=0; j < NG; j++) {
      for (st k=0; k < NG; k++) {
	if (fabs(delta(i,j,k) + div(i,j,k)) > 1.e-4)
	  printf("( %zu, %zu, %zu ) %.6e  %.6e\n", i,j,k,
		 delta(i,j,k), div(i,j,k));
      }
    }
  }

  // with sharp-k filter, the sum of div and delta is vanishing (RMS ~ 10^-16)
  div += delta;
  logger.printf(ESSENTIAL, "difference stats:");
  div.print_stats(stderr);
}

// *********************************************************************
// investigating validity of delta = div( inverseLaplace(grad(delta)) )

void test_tensor_divergence(const st NG, const double kcutNy = 0.9,
			    const bool subtract_mean = true)
{
  scalar_grid delta(NG);
  RNG rng(seed);
  rng.fill_Gauss(delta, 0., 1.);
  delta.go_fourier();
  // mean subtraction: does not make a significant difference
  if (subtract_mean) delta.subtract_mean();
  // sharp-k filter
  // - issue: without sharp-k filtering Nyquist modes, the central grid values
  //          on the div(grad(delta)) grid have the opposite sign
  delta.cutsharpk( sharpkFilter(CUBE, kcutNy * delta.get_kNy()) );

  tensor_grid M(delta, false);
  scalar_grid div(M.trace());
  logger.printf(ESSENTIAL, "delta stats:");
  delta.print_stats(stderr);
  logger.printf(ESSENTIAL, "tr M stats:");
  div.print_stats(stderr);

  // some diagnostic print out
  logger.printf(ESSENTIAL, "cells with mismatch:");
  for (st i=0; i < NG; i++) {
    for (st j=0; j < NG; j++) {
      for (st k=0; k < NG; k++) {
	if (delta(i,j,k) - div(i,j,k) > 1.e-4)
	  printf("( %zu, %zu, %zu ) %.6e  %.6e\n", i,j,k,
		 delta(i,j,k), div(i,j,k));
      }
    }
  }

  // with sharp-k filter, the sum of div and delta is vanishing (RMS ~ 10^-16)
  div.add(-1., delta);
  logger.printf(ESSENTIAL, "difference stats:");
  div.print_stats(stderr);
}


// *********************************************************************

int main(int argc, char **argv)
{
  LogContext lc("test_divergence.cc");

  st NG = 16;
  if (argc > 1)
    NG = atoi(argv[1]);

  // - vector test
  logger.printf(ESSENTIAL, "=== vector_grid divergence ===");
  logger.printf(ESSENTIAL, "=== no filter ===");
  test_vector_divergence(NG, 0.); // fails
  logger.printf(ESSENTIAL, "=== sharp-k filter ===");
  test_vector_divergence(NG, 0.9); // passes

  // -tensor test
  logger.printf(ESSENTIAL, "=== tensor_grid divergence ===");
  logger.printf(ESSENTIAL, "=== no filter ===");
  test_tensor_divergence(NG, 0.); // passes both with and without filter

  return 0;
}

