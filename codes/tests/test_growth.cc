/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "cosmology/cosmological.h"
#include "utilities/integrate.h"

// *********************************************************************
// test_growth.cc
//
// - various small tests of growthLCDM and expansion parameter objects
//
// (C) Fabian Schmidt, 2020
// *********************************************************************

using namespace LEFTfield;

// cosmology:
const double Om=0.3, OL=1.-Om;
const double h=0.7, ns=0.967;


// *********************************************************************
// print growth factor and growth rate at given redshift

void print_test_growth(growthLCDM& g, const double z)
{
  const double a = 1./(1.+z), Oma = g.OmegamOfa(a);

  double f=0., D = g.growthFactorOfa(a, &f);

  fprintf(stderr, "z = %.3f, D(a)/a = %.7f, Om(a) = %.3f, f / Om^0.55 = %.7f\n",
	  z, D/a, Oma, f/pow(Oma, 0.55));  
}

// *********************************************************************
// compare growth factor to LCDM integral solution

double D1Int(double a)
{
  double Hnorm = sqrt(Om / (a*a*a) + OL);
  return 1. / pow(a*Hnorm, 3.);
}

void print_test_growthintegral(growthLCDM& g, const double z)
{
  const double EPS = 1.e-5;
  const double a = 1./(1.+z), Om = g.OmegamOfa(1.);

  double D = g.growthFactorOfa(a);
  double Hnorm = sqrt(Om / (a*a*a) + OL);
  double Dint = 2.5*Om * Hnorm * romberg(D1Int, 1.e-4, a, EPS);
  
  fprintf(stderr, "z = %.3f, D(a)/a = %.7f (odeint) = %.7f (LCDM integral solution)\n",
	  z, D/a, Dint/a);
}


int main()
{
  
  // ====================================================================
  // test linear growth
  
  growthLCDM g(Om, OL);
  for (double z=128.; z >= 0.0625; z /= 2.)
    print_test_growth(g, z);

  fprintf(stderr, "\n");

  for (double z=2.; z > -0.01; z -= 0.5)
    print_test_growthintegral(g, z);

  fprintf(stderr, "\n");

  return 0;
}
