#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "cosmology/cosmological.h"
#include "utilities/integrate.h"

// *********************************************************************
// test_odeint.cc
//
// - testing odeint using "parachute equation"
// - source: https://math.stackexchange.com/questions/397461/examples-of-nonlinear-ordinary-differential-equations-with-elementary-solutions
//
// (C) Fabian Schmidt, 2020
// *********************************************************************

using namespace LEFTfield;


// *********************************************************************
// r.h.s of parachute equation

inline void derivs(const double x, const std::array<double,2> y,
		   std::array<double,2>& yprime,
		   const double k, const double g)
{
  yprime[0] = y[1];
  yprime[1] = -k*y[1]*y[1] + g;
}


// analytical solution
double parachute_solution(const double x, const double k, const double g)
{
  double z = sqrt(g*k)*x;
  return (log(0.5*(exp(2.*z)+1.)) - z)/k;
}

// *********************************************************************

void print_test_odeint(const double k, const double g, const double x)
{
  const double EPS = 1.e-5;
  using namespace std::placeholders;
  std::array<double,2> y({0., 0.});
  odeint(y, 0., x, EPS, 1.e-3, 0., 0.1,
	 std::bind(derivs, _1, _2, _3, k, g));

  fprintf(stderr, "x = %.5f, odeint solution: y(x) = %.7e <=> %.7e analytical solution\n",
	  x, y[0], parachute_solution(x, k, g));
}

// *********************************************************************

int main()
{
  const double k = 1., g = 1.; 
  for (double x=0.; x <= 5.; x += 0.01)
    print_test_odeint(k, g, x);

  fprintf(stderr, "\n");

  return 0;
}
