/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <stdlib.h>
#include <math.h>

#include "grid.h"
#include "scalar_grid.h"
#include "tensor_grid.h"
#include "vector_grid.h"
#include "powerSpectrum.h"

using namespace LEFTfield;

// *********************************************************************
// test_contractions
//
// - consistency tests of contraction functions
//
// (C) Fabian Schmidt, 2020
// *********************************************************************

// *********************************************************************
// main - driver routine for any of the above

int main()
{ 
  // =================================================================

  // =================================================================
  // determinant
  // - could be significantly extended to check multilinearity etc.

  tensor_grid kg(1, true);
  kg[0](0,0,0) =  2.;
  kg[1](0,0,0) =  2.;
  kg[2](0,0,0) =  0.;
  kg[3](0,0,0) =  1.;
  kg[4](0,0,0) =  0.;
  kg[5](0,0,0) =  0.;
  fprintf(stderr, "trace:\n");
  kg.trace().print_stats(stderr);
  fprintf(stderr, "determinant:\n");
  kg.determinant().print_stats(stderr);

  // =================================================================
  // double contraction
  // - compare with tensor_manipulations.nb
  
  tensor_grid A(1, true);
  A[0](0,0,0) =  1.;
  A[1](0,0,0) =  0.2;
  A[2](0,0,0) =  3.4;
  A[3](0,0,0) =  2.9;
  A[4](0,0,0) =  9.8;
  A[5](0,0,0) =  -5.;
  tensor_grid B(1, true);
  B[0](0,0,0) =  3.5;
  B[1](0,0,0) =  2.6;
  B[2](0,0,0) = -7.8;
  B[3](0,0,0) = -4.5;
  B[4](0,0,0) =  6.5;
  B[5](0,0,0) =  3.4;
  tensor_grid F(1, true);
  F[0](0,0,0) = -5.3;
  F[1](0,0,0) =  6.2;
  F[2](0,0,0) =  8.7;
  F[3](0,0,0) = -5.4;
  F[4](0,0,0) =  5.6;
  F[5](0,0,0) = -4.3;

  fprintf(stderr, "double contraction %.5f, %.5f, %.5f, %.5f, %.5f, %.5f != -315.774.\n",
	  A.doubleContraction(B,F)(0,0,0), A.doubleContraction(F,B)(0,0,0),
	  B.doubleContraction(F,A)(0,0,0), B.doubleContraction(A,F)(0,0,0),
	  F.doubleContraction(B,A)(0,0,0), F.doubleContraction(A,B)(0,0,0) );

  // =================================================================
  // vector product
  // - compare with tensor_manipulations.nb

  vector_grid V(A % B);
  fprintf(stderr, "tensor vector product: { %.5f, %.5f, %.5f } != { 104.07, 138.98, 59.61 }\n",
	  V[0](0,0,0), V[1](0,0,0), V[2](0,0,0));
  
  // =================================================================
  // double Levi-Civita contraction
  // - compare with tensor_manipulations.nb

  fprintf(stderr, "double eps contraction %.5f, %.5f, %.5f, %.5f, %.5f, %.5f != -987.47.\n",
	  A.doubleEpsContraction(B,F)(0,0,0), A.doubleEpsContraction(F,B)(0,0,0),
	  B.doubleEpsContraction(F,A)(0,0,0), B.doubleEpsContraction(A,F)(0,0,0),
	  F.doubleEpsContraction(B,A)(0,0,0), F.doubleEpsContraction(A,B)(0,0,0) );

  // =================================================================
  // determinant relation

  double tr = A.trace()(0,0,0);
  double trsq = (A*A)(0,0,0);
  double trcu = A.doubleContraction(A,A)(0,0,0);

  fprintf(stderr, "determinant = %.5f != %.5f != -356.322\n",
	  A.determinant()(0,0,0),
	  1./6.*(tr*tr*tr -3. * tr*trsq + 2.*trcu));
  
  // =================================================================
  return 0;
}
