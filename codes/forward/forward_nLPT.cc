/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "grid.h"
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"
#include "forward/nLPT.h"
#include "forward/nLPT_general.h"
#include "cosmology/cosmological.h"
#include "param/parameter_base.h"

using namespace LEFTfield;

// *********************************************************************
// test_nLPT
//
// - given linear matter density and settings (read from ini file),
//   compute nLPT density field
// - print auto and cross power with given reference density field (e.g.,
//   from N-body)
//
// (C) Fabian Schmidt, 2020
// *********************************************************************

// running for high ("infinite") Lambda
// (...)
// #define LAMBDA_INF

#ifdef LAMBDA_INF
const st NG = 512;
#endif

// *********************************************************************
// main - driver routine for any of the above

int main(int argc, char **argv)
{
  // ---------------------------------------------------------------------
  // 0. read ini file

  if (argc < 3)  {
    fprintf(stderr, "\n Usage: %s <ini file, see ini_reference.ini for example> <reference matter file>\n\n", argv[0]);
    return 0;
  }
  LogContext lc("test_nLPT");
  
  metaParameters param(argv[1]);
  print_param(param, stderr);

  cosmo_info ci = get_cosmo_info(param);
  grid::set_Lbox(param.get("Lbox", "metaparameters", 2000.));
  
  // ---------------------------------------------------------------------
  // I. allocate and read in density fields

  // determine k_cut = Lambda divided by the fundamental of the box
  const double Lambda = param.get<double>("Lambda", "metaparameters");
  auto ftype = param.getEnum<sharpkFilterType>("filtertype", "bias");
  double kcut = Lambda;
#ifndef LAMBDA_INF
  // determine NG to be allocated (ensure that it is even)
  const st NG = grid::get_NGofkNy_ceil(param.get("kNyLambda", "metaparameters", 2) * kcut);
  const double kcutLPT = 0.;
#else
  const double kcutLPT = kcut;
  kcut = 0.;
#endif
  logger.printf(INFO, "kcut = %.3f, NG = %zu", kcut, NG);
  // ???
  logger.printf(INFO, "attempting to read from %s",
		param.get<std::string>("matterfile", "data").c_str());
  // read in
  auto deltaCIC = make_sptr<scalar_grid>();
  deltaCIC->read(param.get<std::string>("matterfile", "data"),
		 param.get("matterfield", "data", std::string("delta")));
  // allocate
  auto deltaLin = make_sptr<scalar_grid>(NG);

  // copy grid in Fourier space
  deltaCIC->go_fourier();
  st N_modes=0;
  deltaCIC->copysharpk(*deltaLin, kcut, ftype, &N_modes);
  deltaCIC->resize(0); // free up memory
  logger.printf(INFO, "working with %zu modes after Lambda cut.", N_modes);

  // scale matter to z=0, if not already there
  double zmatter = param.get("matter_redshift", "data", 0.);
  if (zmatter != 0.)  {
    double Dnorm = 1. / ci.g->growthFactorNormOfz(zmatter);
    logger.printf(INFO, "scaling linear density by %.5f (z=%.2f -> 0).",
		  Dnorm, zmatter);
    *deltaLin *= Dnorm;
  }

  // diagnostics
  double kNy = deltaLin->get_kNy();
  logger.printf(INFO, "kNy = %.3f h/Mpc = %.3f Lambda", kNy, kNy/Lambda);

  // ---------------------------------------------------------------------
  // II. generate nLPT density field

  st order = param.get("lptorder", "gravity", 2);
  st NGeul = param.get("NGeul", "gravity", NG);
  sptr<nLPT> lpt;
  if (param.get("lptEdS", "gravity", true))  { 
    // -- using EdS approximation
    lpt = std::static_pointer_cast<nLPT>(make_sptr<nLPT_EdS>(*deltaLin, order, kcutLPT, ftype));
  } else {
    // -- general expansion history
    lpt = std::static_pointer_cast<nLPT>(make_sptr<nLPT_general>(*deltaLin, order, *ci.g, kcutLPT, ftype));
  }

  double Dnorm = ci.g->growthFactorNormOfz(param.get("tracer_redshift", "data", 0.));
  scalar_grid deltaLPT(lpt->Eulerian_density(Dnorm,
					     param.get("supersampling", "gravity", 1.),
					     NGeul));

  // ***********************************************************
  // measure cross power with reference field

  const char *file_compare = argv[2];
  scalar_grid deltaCIC2;
  deltaCIC2.read(file_compare);
  deltaCIC2.go_fourier();

  scalar_grid deltaref(NGeul);
  deltaCIC2.copysharpk(deltaref, 0, ftype);

  // compute power spectra
  const double lkMin = log10(0.003), lkMax = 2.4597;
  const int Nbins = 264;
  powerSpectrumSet pks(lkMin, lkMax, Nbins);
  
  deltaLPT.auto_powerspectrum(* pks.push_back("nLPT"));
  deltaref.auto_powerspectrum(* pks.push_back("ref"));
  deltaLPT.cross_powerspectrum(deltaref, * pks.push_back("cross"));
  
  pks.print(stdout);
  return 0;
}
