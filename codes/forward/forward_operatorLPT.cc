/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "scalar_grid.h"
#include "forward/nLPT.h"
#include "forward/nLPT_general.h"
#include "forward/generate_operatorSet.h"
#include "forward/generate_operatorSet_LPT.h"
#include "cosmology/cosmological.h"

using namespace LEFTfield;

// *********************************************************************
// test_operatorLPT.cc
//
// - read in linear density field
// - compute n-LPT density field
// - construct bias operators
// - compute power spectra
//
// (C) Fabian Schmidt, 2020
// *********************************************************************

// whether to use EdS approximation
// #define USE_EDS

// cosmology, for growth scaling:
const double Om=0.3, OL=1.-Om;
const double h=0.7, ns=0.967;

// default settings:
std::string ds_name("deltaL");
double Lbox = 2000.;
// - reduce the following grid size as necessary for testing:
st NGeul = 512;
double supersampling = 1.; // super sampling in units of NGeul

// sharp-k filter type
const sharpkFilterType ftype = SPHERE;

// cut to do at each LPT order (referring to Lbox); default: none
const double kcutLPT = 0.;

// *********************************************************************

int main(int argc, char **argv)
{
  LogContext lc("test_operatorLPT.cc");

  if (argc < 5)  {
    fprintf(stderr, "\n Usage: %s <linear density input file> <Lambda> <redshift> <order> [<NGeul=%zu>] [<supersampling=%.1f>] [<Lbox=%.0f>] [<output file>] [<output dataset prefix>]\n",
	    argv[0], NGeul, supersampling, Lbox);
    fprintf(stderr, " Density field is assumed to be at z=0 and scaled to redshift using linear growth.\n");
    fprintf(stderr, " Operators are written to output file in HDF5 format if given.\n\n");
    return 0;
  }

  std::string file_deltalinear = argv[1];
  const double Lin = atof(argv[2]);
  const double z = atof(argv[3]);
  const st order = atoi(argv[4]);
  if (argc > 5) NGeul = atoi(argv[5]);
  if (argc > 6) supersampling = atof(argv[6]);
  if (argc > 7) Lbox = atof(argv[7]);

  grid::set_Lbox(Lbox);
  std::string outfile, ds_prefix;
  if (argc > 8) {
    outfile = argv[8];
    logger.printf(INFO, "Output file: %s", outfile.c_str());
  }
  if (argc > 9) {
    ds_prefix = argv[9];
    logger.printf(INFO, "Output data set prefix: %s", ds_prefix.c_str());
  }
  
  // read in linear grid
  scalar_grid deltaCIC;
  deltaCIC.read(file_deltalinear, ds_name);
  deltaCIC.go_fourier();

  // reduce to minimum necessary size (hence 'ceil')
  const double kNyLambda = std::max<st>(2, order);
  const st NG = grid::get_NGofkNy_ceil(kNyLambda * Lin);  
  logger.printf(ESSENTIAL,
		"Lbox = %.2f h^-1 Mpc, Lambda_in = %.3f h Mpc^-1; NG = %zu",
		Lbox, Lin, NG);

  scalar_grid delta1(NG);
  // supply with physical cut value, since we set Lbox above
  deltaCIC.copysharpk(delta1, Lin, ftype); 
  
  // ***********************************************************
  // construct LPT
  
  growthLCDM g(Om, OL);
  const double Dnorm = g.growthFactorNormOfz(z);

#ifdef USE_EDS
  // -- EdS --
  nLPT_EdS lpt (delta1, order, kcutLPT, ftype);
#else
  // -- general expansion history --
  nLPT_general lpt (delta1, order, g, kcutLPT, ftype);  
#endif

  // ***********************************************************
  // LPT bias operators

  // invariants:
  // // - general
  // LPTinvariants_general Linv(order+1, lpt, Dnorm, 0., ftype);
  // - total M at each order only
  LPTinvariants_EdS Linv(order+1, lpt, Dnorm, 0., ftype);

  operatorSet osetlpt;
  // // - Lagrangian space
  // generate_operatorSet_LPT(osetlpt, order+1, Linv,
  // 			      0., //double(NG)/6.,
  // 			   ftype);
  // - Eulerian space
  generate_operatorSet_LPTEulerian(osetlpt, order+1, Linv, lpt, Dnorm,
  				   supersampling, NGeul, false,
  				   // sharp-k cuts:
  				   0., // double(NG)/6.,
				   0., //Lin, // lambda cut before displacing
  				   ftype);

  // add relevant higher-derivative terms
  // // - based on scaling indices
  // exppar_LCDM_scaling ep(Om, h, ns, 0.1);
  // - based on Rstar
  exppar_LCDM_Rstar ep(Om, h, ns, 0.14, 0.25, 5., 0.);
  st N = add_derivatives_full(osetlpt, op_order(order+1), ep);
  logger.printf(ESSENTIAL, "Added %zu higher-derivative operators.", N);

  osetlpt.print_stats(stderr);

  if (outfile != "")  {
    logger.printf(ESSENTIAL, "Writing operators to %s.", outfile.c_str());
    osetlpt.write(outfile, ds_prefix);
    return 0;
  }

  // // for timing/testing
  // return 0;

  // == compute some power spectra ===
  const double lkMin = log10(0.003), lkMax = 2.4597;
  const int Nbins = 264;
  powerSpectrumSet pks(lkMin, lkMax, Nbins);

// #ifndef USE_EDS
//   // test different shapes that appear beyond EdS; only makes sense if we use LPTinvariants_general!
//   auto O1 = osetlpt["tr[M^(1) M^(3)]"].grd;
//   auto O2 = osetlpt["tr[M^(1,0) M^(3,1)]"].grd;
//   O1->auto_powerspectrum(* pks.push_back("M1 M3,0"), true);
//   O2->auto_powerspectrum(* pks.push_back("M1 M3,1"), true);
//   O1->cross_powerspectrum(*O2, * pks.push_back("cross"), true);
  
//   pks.print(stdout);
//   return 0;
// #endif
  
  // == compute all power spectra ===

  osetlpt.fill_powerspectra(pks);
  pks.print(stdout);
  return 0;
}
