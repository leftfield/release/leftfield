/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include <iostream>
#include <string>
#include <strings.h>
#include <scalar_grid.h>
#include <tensor_grid.h>

using namespace LEFTfield;

// *********************************************************************
// nbtl_to_hdf5.cc
//
// convert given nbtl file to HDF5
//
// (C) Fabian Schmidt, 2021
// *********************************************************************


int main(int argc, char **argv)
{
  LogContext lc("nbtl_to_hdf5.cc");

  std::string fieldname("delta");
  if (argc < 2) {
    std::cerr << "\n Usage: " << argv[0] << " < input nbtl file > [<fieldname = " << fieldname
	      << ">] [< ensure real or fourier (default real) >] \n\n";
    return 0;
  }
  if (argc > 2)  fieldname = argv[2];
      
  scalar_grid g;
  g.read_nbtl(argv[1]);

  if (argc > 3)  {
    if (! strncasecmp(argv[3], "r", 1))  {
      logger.printf(ESSENTIAL, "Going to real space.");
      g.go_real();
    } else if (! strncasecmp(argv[3], "f", 1))  {
      logger.printf(ESSENTIAL, "Going to Fourier space.");
      g.go_fourier();
    } else {
      logger.printf(ESSENTIAL, "Wrong flag '%s'. Aborting.", argv[3]);
      return -1;
    }
  }
  
  // create output file name
  std::string str(argv[1]);
  st index = str.rfind(".nbtl");
  if (index == 0) {
    logger.printf(ESSENTIAL, "Couldn't find .nbtl ending; aborting for safety.\n");
    return -1;
  }
  // make the replacement
  str.replace(index, 5, ".h5");
  logger.printf(ESSENTIAL, "Writing to '%s'.", str.c_str());

  g.write_HDF5(str, fieldname);
  return 0;
}

