/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "grid.h"
#include "scalar_grid.h"
#include "powerSpectrum.h"
#include "assigners.h"

using namespace LEFTfield;

// *********************************************************************
// halos_to_nbtl_massweighted
//
// - read in halos in AHF format
// --- can be generalized in the future
// - assign to grid of given size
// - version that adds ALL halos above given minimum mass, WEIGHTED BY MASS
// - write grid to nbtl file (optionally in Fourier space)
//
// (C) Fabian Schmidt, 2019
// *********************************************************************

// choose assignment scheme here:
#define ASSIGN_CIC
//#define ASSIGN_FT


// mass weighting
double fweight(const double M)
{
  return M;
}

int main(int argc, char **argv)
{
  if (argc < 6) {
    std::cerr << "\n Usage: " << argv[0] << " < AHF file >  < lg M_min > < lg M_max > < NG >  <output file>  [< real or fourier (default real) >]  [< Lbox = 2000 Mpc/h>]\n\n";
    return 0;
  }

  const double lMmin = atof(argv[2]), lMmax = atof(argv[3]);
  st NG = atoi(argv[4]);
  const char *output = argv[5];
  double Lbox = 2000.;
  if (argc > 7)  Lbox = atof(argv[7]);
  
  int real = 1;
  if (argc > 6)  {
    if (! strncasecmp(argv[6], "r", 1))  {
      real = 1;
      fprintf(stderr, "Writing real-space grid to '%s'.\n", output);
    } else if (! strncasecmp(argv[6], "f", 1))  {
      real = 0;
      fprintf(stderr, "Writing Fourier-space grid to '%s'.\n", output);
    } else {
      fprintf(stderr, "Wrong flag '%s'.\n", argv[6]);
      return 0;
    }
  }

#ifdef ASSIGN_CIC
  CICAssigner assigner(NG);
#elif defined(ASSIGN_FT)
  FTAssigner assigner(NG);
#endif
  
  assign_density_AHF_halos(assigner, argv[1], Lbox, pow(10., lMmin),
			   pow(10., lMmax), fweight);

  // get grid
  auto deltaCIC = assigner.getGrid();

  // some helpful info
  deltaCIC->print_stats(stderr);
  
  // go to Fourier space if desired
  if (!real)
    deltaCIC->go_fourier();

  // write output
  deltaCIC->write(output);

  return 0;
}
