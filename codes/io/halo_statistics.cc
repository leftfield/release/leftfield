/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "grid.h"
#include "scalar_grid.h"
#include "powerSpectrum.h"
#include "assigners.h"

using namespace LEFTfield;

// *********************************************************************
// halo_statistics
//
// - read in halos in AHF format
// - get number of halos and determine stochasticity scaling parameters
//
// (C) Fabian Schmidt, 2019
// *********************************************************************

// - for computing Lagrangian radius
const double Om = 0.3;
const double rho_m = Om * 2.7754e11;
const double kNL = 0.25;

int main(int argc, char **argv)
{
  if (argc < 4) {
    std::cerr << "\n Usage: " << argv[0] << " < AHF file >  < NG(like) > < Lbox > \n\n";
    return 0;
  }

  const st NG = atoi(argv[2]);
  const double Lbox = atof(argv[3]);

  // allocate (dummy) grid
  scalar_grid deltaCIC(64); 

  // output
  printf("# lg M_min / lg M_max / n_h [(Mpc/h)^-3] / <M> [M_sun/h] / <R_L> [Mpc/h] / sigmaEps(Poisson) / R_L^2 [(Mpc/h)^2] / k_NL^-2 [(Mpc/h)^2] / number of halos in bin, for halo file '%s'\n",
	 argv[1]);
  
  // loop over mass bins
  const double dlM = 0.5;
  double lMmin = 12.5;
  while (lMmin < 14.1)  {
    double lMmax = lMmin + dlM;

    // use assign_density for counting halos
    fprintf(stderr, "--- lg M = %.2f - %.2f ---\n", lMmin, lMmax);
    CICAssigner assigner(NG);
    const st Nh = assign_density_AHF_halos(assigner, argv[1], Lbox,
					   pow(10., lMmin),
					   pow(10., lMmax));

    // compute relevant quantities
    // - comoving number density
    const double nh = double(Nh) / pow(Lbox, 3.);
    // - Lagrangian radius (very rough)
    const double M = pow(10., 0.5*(lMmin + lMmax));
    const double RL = pow(M / (4.*M_PI/3.*rho_m), 0.333333);
    fprintf(stderr, "\n Estimated mean mass: %.3e M_sun/h ; R_L = %.3e Mpc/h ; n_h = %.3e (Mpc/h)^-3.\n", M, RL, nh);
  
    // - Poisson power spectrum level in code units
    //   (note depends sensitively on grid scale used in likelihood analysis)
    const double Pepscode = pow(NG, 6.) / double(Nh);
    fprintf(stderr, " Poisson expectation for sigmaEps: %.4e\n\n", sqrt(Pepscode));

    // output
    printf("%.2f %.2f  %.5e  %.5e  %.5e  %.5e  %.5e  %.5e  %zu\n",
	   lMmin, lMmax, nh, M, RL, sqrt(Pepscode), RL*RL, 1./(kNL*kNL), Nh);

    lMmin += dlM;
  }
  
  
  return 0;
}
