/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "grid.h"
#include "scalar_grid.h"
#include "powerSpectrum.h"
#include "assigners.h"

using namespace LEFTfield;

// *********************************************************************
// gadget_to_nbtl
//
// - read in particles in gadget format
// - assign to grid of given size
// - write grid to nbtl file (optionally in Fourier space)
//
// (C) Fabian Schmidt, 2019
// *********************************************************************

// choose assignment scheme here:
#define ASSIGN_CIC
//#define ASSIGN_FT

int main(int argc, char **argv)
{
  if (argc < 6) {
    std::cerr << "\n Usage: " << argv[0] << " < base file > < N_files >  < NG >  < nbar to subsample to > <output file>  [< real or fourier (default real) >] \n\n";
    return 0;
  }

  const int Nfiles = atoi(argv[2]);
  st NG = atoi(argv[3]);
  const double nbar = atof(argv[4]);
  
  const char *output = argv[5];
  int real = 0;
  if (argc > 6)  {
    if (! strncasecmp(argv[6], "r", 1))  {
      real = 1;
      fprintf(stderr, "Writing real-space grid to '%s'.\n", output);
    } else if (! strncasecmp(argv[6], "f", 1))  {
      real = 0;
      fprintf(stderr, "Writing Fourier-space grid to '%s'.\n", output);
    } else {
      fprintf(stderr, "Wrong flag '%s'.\n", argv[6]);
      return 0;
    }
  }

#ifdef ASSIGN_CIC
  CICAssigner assigner(NG);
#elif defined(ASSIGN_FT)
  FTAssigner assigner(NG);
#endif

  double Lbox = assign_density_Gadget(assigner, argv[1], Nfiles,
    0., 0., 0., nbar);

  // get grid
  auto deltaCIC = assigner.getGrid();

  // some helpful info
  fprintf(stderr, "Box size: %.3e h^-1 Mpc.\n", Lbox);
  deltaCIC->print_stats(stderr);
  
  // go to Fourier space if desired
  if (!real)
    deltaCIC->go_fourier();

  // write output
  deltaCIC->write(output);

  return 0;
}
