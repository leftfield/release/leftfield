/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "grid.h"
#include "scalar_grid.h"
#include "assigners.h"
#include "powerSpectrum.h"

using namespace LEFTfield;

// *********************************************************************
// HDF5catalog_to_nbtl
//
// - read in particles from HDF5 file
// - assign to grid of given size
// - write grid to nbtl or HDF5 file (optionally in Fourier space)
// - option to account for mass fractions of different chemical elements written specifically for use with Arepo/TNG HDF5 snapshot data (but easily adaptable to other formats).
//
// (C) Fabian Schmidt, 2021
// *********************************************************************

#define ASSIGN_CIC
//#define ASSIGN_FT

const std::string ending("hdf5"); // file ending for input files
const double Mmin = 0., Mmax = 1.e30; // mass limits (default none)
const bool mass_weight = true; // whether to weight by mass. In this case, note additional arguments to accout for mass fractions, if desired

int main(int argc, char **argv)
{
  if (argc < 5) {
    std::cerr << "\n Usage: " << argv[0] << " < base filename > < N_files > < Lbox > < position field name >  < NG >  <output file>  [< real or fourier (default Fourier) >] [< mass field>] [<element field>] [<element ind 0..9>] [H mass fraction field] [<output field name = delta>]\n\n";
    return 0;
  }

  LogContext lc("HDF5catalog_to_nbtl");
  std::string filebase(argv[1]);
  st Nfiles = atoi(argv[2]);
  double Lbox = atof(argv[3]);
  std::string posfield(argv[4]);
  st NG = atoi(argv[5]);
  std::string output(argv[6]);
  int real = 0;
  if (argc > 7)  {
    if (! strncasecmp(argv[7], "r", 1))  {
      real = 1;
      logger.printf(ESSENTIAL, "Writing real-space grid to '%s'.\n", output.c_str());
    } else if (! strncasecmp(argv[7], "f", 1))  {
      real = 0;
      logger.printf(ESSENTIAL, "Writing Fourier-space grid to '%s'.\n", output.c_str());
    } else {
      logger.printf(ESSENTIAL, "Wrong flag '%s'.\n", argv[7]);
      return 0;
    }
  }

#ifdef ASSIGN_CIC
  CICAssigner assigner(NG);
#elif defined(ASSIGN_FT)
  FTAssigner assigner(NG);
#endif

  // assign, running over files
  // - default: all objects same weight = 1
  double sow = 0.;
  for (st n=0; n < Nfiles; n++)  {
    std::string file = filebase + "." + std::to_string(n) + "." + ending;
    std::cerr << "Attempting to read from " << file << ".\n";
    if (mass_weight)
      sow += assign_density_HDF5catalog(assigner, file, Lbox, posfield,
					argc > 8 ? argv[8] : "",
					argc > 9 ? argv[9] : "",
					argc > 10 ? atoi(argv[10]) : 0,
					argc > 11 ? argv[11] : "",
					Mmin, Mmax,
					[](const double M) { return M; });
    else
      sow += assign_density_HDF5catalog(assigner, file, Lbox, posfield,
					argc > 8 ? argv[8] : "",
					argc > 9 ? argv[9] : "",
					argc > 10 ? atoi(argv[10]) : 0,
					argc > 11 ? argv[11] : "",
					Mmin, Mmax);
  }

  // determine and apply weight rescale factor
  const double fn = double(NG*NG*NG) / sow;
  logger.printf(ESSENTIAL, "Sum of weights = %.5e, rescaling grid by factor %.5e.\n",
	  sow, fn);

  // get grid
  auto deltaCIC = assigner.getGrid();
  *deltaCIC *= fn;

  // some helpful info
  deltaCIC->print_stats(stderr);

  // go to Fourier space if desired
  if (!real)
    deltaCIC->go_fourier();

  // write output
  deltaCIC->write(output, argc > 12 ? argv[12] : "delta");

  return 0;
}
