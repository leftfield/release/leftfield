/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "grid.h"
#include "scalar_grid.h"
#include "powerSpectrum.h"
#include "assigners.h"

using namespace LEFTfield;

// *********************************************************************
// halos_to_nbtl
//
// - read in halos in AHF format
// --- can be generalized in the future
// - assign to grid of given size
// --- given mass cut, can also be generalized
// - write grid to nbtl file (optionally in Fourier space)
//
// (C) Fabian Schmidt, 2019
// *********************************************************************

// choose assignment scheme here:
#define ASSIGN_CIC
//#define ASSIGN_FT

// enable Rockstar format (default AHF)
// #define ROCKSTAR

double LNmean = 0., LNsigma = 1.;

// log-normal mass weighting
// - note that we don't need normalizing factor, since assign_density_halos
//   takes care of normalization
double fweight(const double M)
{
  double x = log10(M);
  return exp(-0.5 * (x-LNmean)*(x-LNmean) / (LNsigma*LNsigma));
}

int main(int argc, char **argv)
{
  if (argc < 6) {
    std::cerr << "\n Usage: " << argv[0] << " < AHF file >  < lg M_min > < lg M_max> < NG >  <output file>  [< real or fourier (default real) >]  [< Lbox = 2000 Mpc/h>]\n\n";
    std::cerr << "       - if lg M_max < 0, then a log-normal weighting is employed, centered on lg M_min and with RMS given by - lg M_max.\n\n";
    return 0;
  }

  bool LNW = false;
  const double lMmin = atof(argv[2]), lMmax = atof(argv[3]);
  if (lMmax < 0.)  {
    LNW = true;
    LNmean = lMmin;  LNsigma = fabs(lMmax);
    std::cerr << "Employing log-normal weighting with mean " << LNmean
	      << " and RMS " << LNsigma << ".\n";
  }
  
  st NG = atoi(argv[4]);
  const char *output = argv[5];
  double Lbox = 2000.;
  if (argc > 7)  Lbox = atof(argv[7]);
  
  int real = 1;
  if (argc > 6)  {
    if (! strncasecmp(argv[6], "r", 1))  {
      real = 1;
      fprintf(stderr, "Writing real-space grid to '%s'.\n", output);
    } else if (! strncasecmp(argv[6], "f", 1))  {
      real = 0;
      fprintf(stderr, "Writing Fourier-space grid to '%s'.\n", output);
    } else {
      fprintf(stderr, "Wrong flag '%s'.\n", argv[6]);
      return 0;
    }
  }

#ifdef ASSIGN_CIC
  CICAssigner assigner(NG);
#elif defined(ASSIGN_FT)
  FTAssigner assigner(NG);
#endif
  
  if (LNW)  {
    // hard coded minimum mass! (no maximum when using Gaussian weighting)
    const double lMcut = 12.5;    
    assign_density_AHF_halos(assigner, argv[1], Lbox, pow(10., lMcut), 1.e18,
			     fweight);
  } else {
#ifndef ROCKSTAR
    // - AHF
    assign_density_AHF_halos(assigner, argv[1], Lbox, pow(10., lMmin),
			     pow(10., lMmax));
    
    // // - Alex' catalogs
    // st Mcol = argc > 8 ? atoi(argv[8]) : 4;
    // assign_density_halos(assigner, argv[1], Lbox, 1, 2, 3, Mcol,
    // 			 pow(10., lMmin), pow(10., lMmax), 0);
#else
    // Rockstar
    const st Mcol = 3; // M_200b with proper settings in config file
    // const st Mcol = 3; // M_vir
    // const st Mcol = 27;  // M_200b
    assign_density_halos(assigner, argv[1], Lbox, 9, 10, 11, Mcol,
			 pow(10., lMmin), pow(10., lMmax));
#endif
}

  // get grid
  auto deltaCIC = assigner.getGrid();

  // some helpful info
  deltaCIC->print_stats(stderr);
  
  // go to Fourier space if desired
  if (!real)
    deltaCIC->go_fourier();

  // write output
  deltaCIC->write(output);

  return 0;
}
