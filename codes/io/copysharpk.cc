/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>
#include <algorithm>

#include "grid.h"
#include "scalar_grid.h"
#include "vector_grid.h"
#include "tensor_grid.h"
#include "powerSpectrum.h"
#include "param/parameter_base.h"
#include "cosmology/cosmological.h"

using namespace LEFTfield;

// *********************************************************************
// copysharpk
//
// - read in field in nbtl format
// - do copysharpk to desired grid size
// - output
//
// (C) Fabian Schmidt, 2020
// *********************************************************************


int main(int argc, char **argv)
{
  if (argc < 4) {
    std::cerr << "\n Usage: " << argv[0] << " < input nbtl file > < output file >  < NG_target > <[ini file]>\n\n";
    std::cerr << " If ini file is provided, field is scaled by D(z_tracer)/D(z_matter) and by alpha; see ini_reference.ini for example>\n\n";
    return 0;
  }

  double factor = 1.; // scaling factor
  // if ini file provided, read source ("matter") and targer ("tracer") redshift, as well as alpha
  if (argc > 4)  {
    metaParameters param(argv[4]);    
    print_param(param, stderr);
    cosmo_info ci = get_cosmo_info(param);

    // scale matter grid to redshift
    const double zmatter = param.get("matter_redshift", "data", 0.),
      z = param.get("tracer_redshift", "data", 0.);
    factor = ci.g->growthFactorOfz(z)
      / ci.g->growthFactorOfz(zmatter);
    fprintf(stderr, "z = %.2f, rescaling density by %.4f\n",
	    z, factor);

    // scale matter grid by alpha
    const double alpha = param.get("alpha", "metaparameters", 1.);
    fprintf(stderr, "rescaling linear field by %.3f.\n", alpha);
    factor *= alpha;
  }
  
  // read in grid and rescale
  scalar_grid rg1;
  rg1.read(argv[1]);
  rg1 *= factor;
  
  const st NG = (st) atoi(argv[3]);
  if (! (NG > 0))  return -1;
  rg1.go_fourier();
  auto out = make_sptr<scalar_grid>(NG);
  rg1.copysharpk(*out, out->get_kNy(), CUBE);
  out->write(argv[2]);

  return 0;
}
