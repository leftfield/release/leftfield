/* LEFTfield - Lagrangian, effective-field-theory-based forward model
 * of cosmological density fields.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Copyright (C) 2017-2021 Max-Planck-Society
 * Author: Fabian Schmidt
 */

#include<complex>
#include<cstdlib>
#include<iostream>
#include<map>
#include<string>
#include<strings.h>
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>

#include "scalar_grid.h"

using namespace LEFTfield;

// *********************************************************************
// read_density_slabs_raw
//
// - read in density slabs (binary) produced by my modification of
//   IC2LPTPNGNONLOCAL
// - reduce to grid of given size
// - write grid in Fourier space to nbtl file
//
// (C) Fabian Schmidt, 2020
// *********************************************************************


int main(int argc, char **argv)
{
  if (argc < 6) {
    std::cerr << "\n Usage: " << argv[0] << " < base file > < N_files >  < NG_in >  <output file>  < NG_out > \n\n";
    return 0;
  }

  const st Nfiles = atoi(argv[2]);
  const st NGin = atoi(argv[3]);
  const char *output = argv[4];
  const st NGout = atoi(argv[5]);

  // allocate grid
  scalar_grid deltain(NGin);

  // read in
  st Nslab = NGin / Nfiles;
  fprintf(stderr, "Reading in %zu files for %zu^3 grid, slab size = %zu.\n",
	  Nfiles, NGin, Nslab);

  char file[1024];
  st istart=0;
  for (st I=0; I < Nfiles; I++)  {
    sprintf(file, "%s_task%zu_istart%zu.dat",
	    argv[1], I, istart);
    fprintf(stderr, "Attempting to open '%s' for reading.\n", file);
  
    deltain.read_nbtl_slab_raw(file, Nslab, istart);
    istart += Nslab;
  }

  deltain.go_fourier();

  scalar_grid deltaout(NGout);

  // copy to small grid, keep all modes up to Nyquist of small grid
  deltain.copysharpk(deltaout, 0.);
  deltaout.write(output);

  return 0;
}
